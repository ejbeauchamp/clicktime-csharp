﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Tester
{
	class Program
	{
		static void Main(string[] args)
		{
			// Configure API key authorization: apiKey
			Configuration.Default.AddApiKeyPrefix("Authorization", "Token");
			Configuration.Default.BasePath = "https://api.clicktime.com/v2";

			var cfg = new Configuration();
			cfg.AddApiKey("Authorization", "uReBKLTYTiMiLziFsFzSVlpK5wy5dehnRpsaTeVFS4xyFONa6G9cOw2");
			var clients = new ClientsApi(cfg);

			var clientID = string.Empty;

			try
			{
				var newClient = clients.ClientsPost(new PostClientModelRoot(
					name: "Api Test",
					shortName: "TESTER"
				));

				clientID = newClient.data.ID;

			}
			finally
			{
				clients.ClientsDelete(clientID);
			}



		}
	}
}
