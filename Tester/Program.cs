﻿using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Tester
{
    class Program
    {
        static void Main(string[] args)
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "uReBKLTYTiMiLziFsFzSVlpK5wy5dehnRpsaTeVFS4xyFONa6G9cOw2");
            Configuration.Default.AddApiKeyPrefix("Authorization", "Token");
            Configuration.Default.BasePath = "https://api.clicktime.com/v2";

            var apiInstance = new CompanyApi();

            try
            {
                // Get your Company.
                ResponseBodyGetCompanyModelRoot result = apiInstance.CompanyGet();
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling CompanyApi.CompanyGet: " + e.Message);
            }
        }
    }
}
