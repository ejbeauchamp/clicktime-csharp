/* 
 * REST API v2
 *
 * <p>This REST API offers programmatic access to common ClickTime functions. You can review the available endpoints by clicking on the category names below. Click on an endpoint itself to further drill down and review the available filters, usage requirements, response examples and the respective model.</p><p>Review the <a href=\"https://support.clicktime.com/hc/en-us/articles/360002884071\">REST API v2 General Information</a> to get an overview of how to use the API including usable examples of authentication and making requests.</p>
 *
 * OpenAPI spec version: v2
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.ComponentModel.DataAnnotations;
using SwaggerDateConverter = IO.Swagger.Client.SwaggerDateConverter;

namespace IO.Swagger.Model
{
    /// <summary>
    /// Holiday model for POST requests.
    /// </summary>
    [DataContract]
    public partial class PostHolidayModelRoot :  IEquatable<PostHolidayModelRoot>, IValidatableObject
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PostHolidayModelRoot" /> class.
        /// </summary>
        [JsonConstructorAttribute]
        protected PostHolidayModelRoot() { }
        /// <summary>
        /// Initializes a new instance of the <see cref="PostHolidayModelRoot" /> class.
        /// </summary>
        /// <param name="date">date (required).</param>
        /// <param name="holidayTypeID">holidayTypeID (required).</param>
        /// <param name="hours">hours (required).</param>
        /// <param name="notes">notes.</param>
        public PostHolidayModelRoot(string date = default(string), string holidayTypeID = default(string), double? hours = default(double?), string notes = default(string))
        {
            // to ensure "date" is required (not null)
            if (date == null)
            {
                throw new InvalidDataException("date is a required property for PostHolidayModelRoot and cannot be null");
            }
            else
            {
                this.Date = date;
            }
            // to ensure "holidayTypeID" is required (not null)
            if (holidayTypeID == null)
            {
                throw new InvalidDataException("holidayTypeID is a required property for PostHolidayModelRoot and cannot be null");
            }
            else
            {
                this.HolidayTypeID = holidayTypeID;
            }
            // to ensure "hours" is required (not null)
            if (hours == null)
            {
                throw new InvalidDataException("hours is a required property for PostHolidayModelRoot and cannot be null");
            }
            else
            {
                this.Hours = hours;
            }
            this.Notes = notes;
        }
        
        /// <summary>
        /// Gets or Sets Date
        /// </summary>
        [DataMember(Name="Date", EmitDefaultValue=false)]
        public string Date { get; set; }

        /// <summary>
        /// Gets or Sets HolidayTypeID
        /// </summary>
        [DataMember(Name="HolidayTypeID", EmitDefaultValue=false)]
        public string HolidayTypeID { get; set; }

        /// <summary>
        /// Gets or Sets Hours
        /// </summary>
        [DataMember(Name="Hours", EmitDefaultValue=false)]
        public double? Hours { get; set; }

        /// <summary>
        /// Gets or Sets Notes
        /// </summary>
        [DataMember(Name="Notes", EmitDefaultValue=false)]
        public string Notes { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class PostHolidayModelRoot {\n");
            sb.Append("  Date: ").Append(Date).Append("\n");
            sb.Append("  HolidayTypeID: ").Append(HolidayTypeID).Append("\n");
            sb.Append("  Hours: ").Append(Hours).Append("\n");
            sb.Append("  Notes: ").Append(Notes).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }
  
        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public virtual string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="input">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object input)
        {
            return this.Equals(input as PostHolidayModelRoot);
        }

        /// <summary>
        /// Returns true if PostHolidayModelRoot instances are equal
        /// </summary>
        /// <param name="input">Instance of PostHolidayModelRoot to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(PostHolidayModelRoot input)
        {
            if (input == null)
                return false;

            return 
                (
                    this.Date == input.Date ||
                    (this.Date != null &&
                    this.Date.Equals(input.Date))
                ) && 
                (
                    this.HolidayTypeID == input.HolidayTypeID ||
                    (this.HolidayTypeID != null &&
                    this.HolidayTypeID.Equals(input.HolidayTypeID))
                ) && 
                (
                    this.Hours == input.Hours ||
                    (this.Hours != null &&
                    this.Hours.Equals(input.Hours))
                ) && 
                (
                    this.Notes == input.Notes ||
                    (this.Notes != null &&
                    this.Notes.Equals(input.Notes))
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hashCode = 41;
                if (this.Date != null)
                    hashCode = hashCode * 59 + this.Date.GetHashCode();
                if (this.HolidayTypeID != null)
                    hashCode = hashCode * 59 + this.HolidayTypeID.GetHashCode();
                if (this.Hours != null)
                    hashCode = hashCode * 59 + this.Hours.GetHashCode();
                if (this.Notes != null)
                    hashCode = hashCode * 59 + this.Notes.GetHashCode();
                return hashCode;
            }
        }

        /// <summary>
        /// To validate all properties of the instance
        /// </summary>
        /// <param name="validationContext">Validation context</param>
        /// <returns>Validation Result</returns>
        IEnumerable<System.ComponentModel.DataAnnotations.ValidationResult> IValidatableObject.Validate(ValidationContext validationContext)
        {
            // Hours (double?) maximum
            if(this.Hours > (double?)24)
            {
                yield return new System.ComponentModel.DataAnnotations.ValidationResult("Invalid value for Hours, must be a value less than or equal to 24.", new [] { "Hours" });
            }

            // Hours (double?) minimum
            if(this.Hours < (double?)0)
            {
                yield return new System.ComponentModel.DataAnnotations.ValidationResult("Invalid value for Hours, must be a value greater than or equal to 0.", new [] { "Hours" });
            }

            // Notes (string) maxLength
            if(this.Notes != null && this.Notes.Length > 500)
            {
                yield return new System.ComponentModel.DataAnnotations.ValidationResult("Invalid value for Notes, length must be less than 500.", new [] { "Notes" });
            }

            // Notes (string) minLength
            if(this.Notes != null && this.Notes.Length < 0)
            {
                yield return new System.ComponentModel.DataAnnotations.ValidationResult("Invalid value for Notes, length must be greater than 0.", new [] { "Notes" });
            }

            yield break;
        }
    }

}
