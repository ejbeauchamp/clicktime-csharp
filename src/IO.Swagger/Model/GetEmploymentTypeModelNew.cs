/* 
 * REST API v2
 *
 * <p>This REST API offers programmatic access to common ClickTime functions. You can review the available endpoints by clicking on the category names below. Click on an endpoint itself to further drill down and review the available filters, usage requirements, response examples and the respective model.</p><p>Review the <a href=\"https://support.clicktime.com/hc/en-us/articles/360002884071\">REST API v2 General Information</a> to get an overview of how to use the API including usable examples of authentication and making requests.</p>
 *
 * OpenAPI spec version: v2
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.ComponentModel.DataAnnotations;
using SwaggerDateConverter = IO.Swagger.Client.SwaggerDateConverter;

namespace IO.Swagger.Model
{
    /// <summary>
    /// GetEmploymentTypeModelNew
    /// </summary>
    [DataContract]
    public partial class GetEmploymentTypeModelNew :  IEquatable<GetEmploymentTypeModelNew>, IValidatableObject
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GetEmploymentTypeModelNew" /> class.
        /// </summary>
        /// <param name="accountingPackageID">       User requirements:    Security Level(s): Admin, Manager (View Employment Types).</param>
        /// <param name="holidayTypeIDs">       Request requirements:    Verbose&#x3D;true       Company requirements:    Leave Type Restrictions: By Employment Type.</param>
        /// <param name="iD">iD.</param>
        /// <param name="isActive">isActive.</param>
        /// <param name="name">name.</param>
        /// <param name="notes">       User requirements:    Security Level(s): Admin, Manager (View Employment Types).</param>
        /// <param name="timeOffTypeIDs">       Request requirements:    Verbose&#x3D;true       Company requirements:    Leave Type Restrictions: By Employment Type.</param>
        /// <param name="users">       Request requirements:    Verbose&#x3D;true.</param>
        public GetEmploymentTypeModelNew(string accountingPackageID = default(string), List<string> holidayTypeIDs = default(List<string>), string iD = default(string), bool? isActive = default(bool?), string name = default(string), string notes = default(string), List<string> timeOffTypeIDs = default(List<string>), List<GetBasicUserModelUsersGetEmploymentTypeModelNew> users = default(List<GetBasicUserModelUsersGetEmploymentTypeModelNew>))
        {
            this.AccountingPackageID = accountingPackageID;
            this.HolidayTypeIDs = holidayTypeIDs;
            this.ID = iD;
            this.IsActive = isActive;
            this.Name = name;
            this.Notes = notes;
            this.TimeOffTypeIDs = timeOffTypeIDs;
            this.Users = users;
        }
        
        /// <summary>
        ///        User requirements:    Security Level(s): Admin, Manager (View Employment Types)
        /// </summary>
        /// <value>       User requirements:    Security Level(s): Admin, Manager (View Employment Types)</value>
        [DataMember(Name="AccountingPackageID", EmitDefaultValue=false)]
        public string AccountingPackageID { get; set; }

        /// <summary>
        ///        Request requirements:    Verbose&#x3D;true       Company requirements:    Leave Type Restrictions: By Employment Type
        /// </summary>
        /// <value>       Request requirements:    Verbose&#x3D;true       Company requirements:    Leave Type Restrictions: By Employment Type</value>
        [DataMember(Name="HolidayTypeIDs", EmitDefaultValue=false)]
        public List<string> HolidayTypeIDs { get; set; }

        /// <summary>
        /// Gets or Sets ID
        /// </summary>
        [DataMember(Name="ID", EmitDefaultValue=false)]
        public string ID { get; set; }

        /// <summary>
        /// Gets or Sets IsActive
        /// </summary>
        [DataMember(Name="IsActive", EmitDefaultValue=false)]
        public bool? IsActive { get; set; }

        /// <summary>
        /// Gets or Sets Name
        /// </summary>
        [DataMember(Name="Name", EmitDefaultValue=false)]
        public string Name { get; set; }

        /// <summary>
        ///        User requirements:    Security Level(s): Admin, Manager (View Employment Types)
        /// </summary>
        /// <value>       User requirements:    Security Level(s): Admin, Manager (View Employment Types)</value>
        [DataMember(Name="Notes", EmitDefaultValue=false)]
        public string Notes { get; set; }

        /// <summary>
        ///        Request requirements:    Verbose&#x3D;true       Company requirements:    Leave Type Restrictions: By Employment Type
        /// </summary>
        /// <value>       Request requirements:    Verbose&#x3D;true       Company requirements:    Leave Type Restrictions: By Employment Type</value>
        [DataMember(Name="TimeOffTypeIDs", EmitDefaultValue=false)]
        public List<string> TimeOffTypeIDs { get; set; }

        /// <summary>
        ///        Request requirements:    Verbose&#x3D;true
        /// </summary>
        /// <value>       Request requirements:    Verbose&#x3D;true</value>
        [DataMember(Name="Users", EmitDefaultValue=false)]
        public List<GetBasicUserModelUsersGetEmploymentTypeModelNew> Users { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class GetEmploymentTypeModelNew {\n");
            sb.Append("  AccountingPackageID: ").Append(AccountingPackageID).Append("\n");
            sb.Append("  HolidayTypeIDs: ").Append(HolidayTypeIDs).Append("\n");
            sb.Append("  ID: ").Append(ID).Append("\n");
            sb.Append("  IsActive: ").Append(IsActive).Append("\n");
            sb.Append("  Name: ").Append(Name).Append("\n");
            sb.Append("  Notes: ").Append(Notes).Append("\n");
            sb.Append("  TimeOffTypeIDs: ").Append(TimeOffTypeIDs).Append("\n");
            sb.Append("  Users: ").Append(Users).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }
  
        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public virtual string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="input">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object input)
        {
            return this.Equals(input as GetEmploymentTypeModelNew);
        }

        /// <summary>
        /// Returns true if GetEmploymentTypeModelNew instances are equal
        /// </summary>
        /// <param name="input">Instance of GetEmploymentTypeModelNew to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(GetEmploymentTypeModelNew input)
        {
            if (input == null)
                return false;

            return 
                (
                    this.AccountingPackageID == input.AccountingPackageID ||
                    (this.AccountingPackageID != null &&
                    this.AccountingPackageID.Equals(input.AccountingPackageID))
                ) && 
                (
                    this.HolidayTypeIDs == input.HolidayTypeIDs ||
                    this.HolidayTypeIDs != null &&
                    this.HolidayTypeIDs.SequenceEqual(input.HolidayTypeIDs)
                ) && 
                (
                    this.ID == input.ID ||
                    (this.ID != null &&
                    this.ID.Equals(input.ID))
                ) && 
                (
                    this.IsActive == input.IsActive ||
                    (this.IsActive != null &&
                    this.IsActive.Equals(input.IsActive))
                ) && 
                (
                    this.Name == input.Name ||
                    (this.Name != null &&
                    this.Name.Equals(input.Name))
                ) && 
                (
                    this.Notes == input.Notes ||
                    (this.Notes != null &&
                    this.Notes.Equals(input.Notes))
                ) && 
                (
                    this.TimeOffTypeIDs == input.TimeOffTypeIDs ||
                    this.TimeOffTypeIDs != null &&
                    this.TimeOffTypeIDs.SequenceEqual(input.TimeOffTypeIDs)
                ) && 
                (
                    this.Users == input.Users ||
                    this.Users != null &&
                    this.Users.SequenceEqual(input.Users)
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hashCode = 41;
                if (this.AccountingPackageID != null)
                    hashCode = hashCode * 59 + this.AccountingPackageID.GetHashCode();
                if (this.HolidayTypeIDs != null)
                    hashCode = hashCode * 59 + this.HolidayTypeIDs.GetHashCode();
                if (this.ID != null)
                    hashCode = hashCode * 59 + this.ID.GetHashCode();
                if (this.IsActive != null)
                    hashCode = hashCode * 59 + this.IsActive.GetHashCode();
                if (this.Name != null)
                    hashCode = hashCode * 59 + this.Name.GetHashCode();
                if (this.Notes != null)
                    hashCode = hashCode * 59 + this.Notes.GetHashCode();
                if (this.TimeOffTypeIDs != null)
                    hashCode = hashCode * 59 + this.TimeOffTypeIDs.GetHashCode();
                if (this.Users != null)
                    hashCode = hashCode * 59 + this.Users.GetHashCode();
                return hashCode;
            }
        }

        /// <summary>
        /// To validate all properties of the instance
        /// </summary>
        /// <param name="validationContext">Validation context</param>
        /// <returns>Validation Result</returns>
        IEnumerable<System.ComponentModel.DataAnnotations.ValidationResult> IValidatableObject.Validate(ValidationContext validationContext)
        {
            yield break;
        }
    }

}
