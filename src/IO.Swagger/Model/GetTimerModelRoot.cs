/* 
 * REST API v2
 *
 * <p>This REST API offers programmatic access to common ClickTime functions. You can review the available endpoints by clicking on the category names below. Click on an endpoint itself to further drill down and review the available filters, usage requirements, response examples and the respective model.</p><p>Review the <a href=\"https://support.clicktime.com/hc/en-us/articles/360002884071\">REST API v2 General Information</a> to get an overview of how to use the API including usable examples of authentication and making requests.</p>
 *
 * OpenAPI spec version: v2
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.ComponentModel.DataAnnotations;
using SwaggerDateConverter = IO.Swagger.Client.SwaggerDateConverter;

namespace IO.Swagger.Model
{
    /// <summary>
    /// GetTimerModelRoot
    /// </summary>
    [DataContract]
    public partial class GetTimerModelRoot :  IEquatable<GetTimerModelRoot>, IValidatableObject
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GetTimerModelRoot" /> class.
        /// </summary>
        /// <param name="elapsedSeconds">elapsedSeconds.</param>
        /// <param name="iD">iD.</param>
        /// <param name="initialTimeInSeconds">initialTimeInSeconds.</param>
        /// <param name="intervals">intervals.</param>
        /// <param name="legacyID">       Request requirements:    CTLegacyScramble&#x3D;true.</param>
        /// <param name="timeEntryDate">timeEntryDate.</param>
        /// <param name="timeEntryID">timeEntryID.</param>
        /// <param name="userID">userID.</param>
        public GetTimerModelRoot(int? elapsedSeconds = default(int?), string iD = default(string), int? initialTimeInSeconds = default(int?), List<GetTimerIntervalModelIntervalsGetTimerModelRoot> intervals = default(List<GetTimerIntervalModelIntervalsGetTimerModelRoot>), string legacyID = default(string), string timeEntryDate = default(string), string timeEntryID = default(string), string userID = default(string))
        {
            this.ElapsedSeconds = elapsedSeconds;
            this.ID = iD;
            this.InitialTimeInSeconds = initialTimeInSeconds;
            this.Intervals = intervals;
            this.LegacyID = legacyID;
            this.TimeEntryDate = timeEntryDate;
            this.TimeEntryID = timeEntryID;
            this.UserID = userID;
        }
        
        /// <summary>
        /// Gets or Sets ElapsedSeconds
        /// </summary>
        [DataMember(Name="ElapsedSeconds", EmitDefaultValue=false)]
        public int? ElapsedSeconds { get; set; }

        /// <summary>
        /// Gets or Sets ID
        /// </summary>
        [DataMember(Name="ID", EmitDefaultValue=false)]
        public string ID { get; set; }

        /// <summary>
        /// Gets or Sets InitialTimeInSeconds
        /// </summary>
        [DataMember(Name="InitialTimeInSeconds", EmitDefaultValue=false)]
        public int? InitialTimeInSeconds { get; set; }

        /// <summary>
        /// Gets or Sets Intervals
        /// </summary>
        [DataMember(Name="Intervals", EmitDefaultValue=false)]
        public List<GetTimerIntervalModelIntervalsGetTimerModelRoot> Intervals { get; set; }

        /// <summary>
        ///        Request requirements:    CTLegacyScramble&#x3D;true
        /// </summary>
        /// <value>       Request requirements:    CTLegacyScramble&#x3D;true</value>
        [DataMember(Name="LegacyID", EmitDefaultValue=false)]
        public string LegacyID { get; set; }

        /// <summary>
        /// Gets or Sets TimeEntryDate
        /// </summary>
        [DataMember(Name="TimeEntryDate", EmitDefaultValue=false)]
        public string TimeEntryDate { get; set; }

        /// <summary>
        /// Gets or Sets TimeEntryID
        /// </summary>
        [DataMember(Name="TimeEntryID", EmitDefaultValue=false)]
        public string TimeEntryID { get; set; }

        /// <summary>
        /// Gets or Sets UserID
        /// </summary>
        [DataMember(Name="UserID", EmitDefaultValue=false)]
        public string UserID { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class GetTimerModelRoot {\n");
            sb.Append("  ElapsedSeconds: ").Append(ElapsedSeconds).Append("\n");
            sb.Append("  ID: ").Append(ID).Append("\n");
            sb.Append("  InitialTimeInSeconds: ").Append(InitialTimeInSeconds).Append("\n");
            sb.Append("  Intervals: ").Append(Intervals).Append("\n");
            sb.Append("  LegacyID: ").Append(LegacyID).Append("\n");
            sb.Append("  TimeEntryDate: ").Append(TimeEntryDate).Append("\n");
            sb.Append("  TimeEntryID: ").Append(TimeEntryID).Append("\n");
            sb.Append("  UserID: ").Append(UserID).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }
  
        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public virtual string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="input">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object input)
        {
            return this.Equals(input as GetTimerModelRoot);
        }

        /// <summary>
        /// Returns true if GetTimerModelRoot instances are equal
        /// </summary>
        /// <param name="input">Instance of GetTimerModelRoot to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(GetTimerModelRoot input)
        {
            if (input == null)
                return false;

            return 
                (
                    this.ElapsedSeconds == input.ElapsedSeconds ||
                    (this.ElapsedSeconds != null &&
                    this.ElapsedSeconds.Equals(input.ElapsedSeconds))
                ) && 
                (
                    this.ID == input.ID ||
                    (this.ID != null &&
                    this.ID.Equals(input.ID))
                ) && 
                (
                    this.InitialTimeInSeconds == input.InitialTimeInSeconds ||
                    (this.InitialTimeInSeconds != null &&
                    this.InitialTimeInSeconds.Equals(input.InitialTimeInSeconds))
                ) && 
                (
                    this.Intervals == input.Intervals ||
                    this.Intervals != null &&
                    this.Intervals.SequenceEqual(input.Intervals)
                ) && 
                (
                    this.LegacyID == input.LegacyID ||
                    (this.LegacyID != null &&
                    this.LegacyID.Equals(input.LegacyID))
                ) && 
                (
                    this.TimeEntryDate == input.TimeEntryDate ||
                    (this.TimeEntryDate != null &&
                    this.TimeEntryDate.Equals(input.TimeEntryDate))
                ) && 
                (
                    this.TimeEntryID == input.TimeEntryID ||
                    (this.TimeEntryID != null &&
                    this.TimeEntryID.Equals(input.TimeEntryID))
                ) && 
                (
                    this.UserID == input.UserID ||
                    (this.UserID != null &&
                    this.UserID.Equals(input.UserID))
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hashCode = 41;
                if (this.ElapsedSeconds != null)
                    hashCode = hashCode * 59 + this.ElapsedSeconds.GetHashCode();
                if (this.ID != null)
                    hashCode = hashCode * 59 + this.ID.GetHashCode();
                if (this.InitialTimeInSeconds != null)
                    hashCode = hashCode * 59 + this.InitialTimeInSeconds.GetHashCode();
                if (this.Intervals != null)
                    hashCode = hashCode * 59 + this.Intervals.GetHashCode();
                if (this.LegacyID != null)
                    hashCode = hashCode * 59 + this.LegacyID.GetHashCode();
                if (this.TimeEntryDate != null)
                    hashCode = hashCode * 59 + this.TimeEntryDate.GetHashCode();
                if (this.TimeEntryID != null)
                    hashCode = hashCode * 59 + this.TimeEntryID.GetHashCode();
                if (this.UserID != null)
                    hashCode = hashCode * 59 + this.UserID.GetHashCode();
                return hashCode;
            }
        }

        /// <summary>
        /// To validate all properties of the instance
        /// </summary>
        /// <param name="validationContext">Validation context</param>
        /// <returns>Validation Result</returns>
        IEnumerable<System.ComponentModel.DataAnnotations.ValidationResult> IValidatableObject.Validate(ValidationContext validationContext)
        {
            yield break;
        }
    }

}
