/* 
 * REST API v2
 *
 * <p>This REST API offers programmatic access to common ClickTime functions. You can review the available endpoints by clicking on the category names below. Click on an endpoint itself to further drill down and review the available filters, usage requirements, response examples and the respective model.</p><p>Review the <a href=\"https://support.clicktime.com/hc/en-us/articles/360002884071\">REST API v2 General Information</a> to get an overview of how to use the API including usable examples of authentication and making requests.</p>
 *
 * OpenAPI spec version: v2
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.ComponentModel.DataAnnotations;
using SwaggerDateConverter = IO.Swagger.Client.SwaggerDateConverter;

namespace IO.Swagger.Model
{
    /// <summary>
    /// PostTimeOffTypeAssociationModelRoot
    /// </summary>
    [DataContract]
    public partial class PostTimeOffTypeAssociationModelRoot :  IEquatable<PostTimeOffTypeAssociationModelRoot>, IValidatableObject
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PostTimeOffTypeAssociationModelRoot" /> class.
        /// </summary>
        [JsonConstructorAttribute]
        protected PostTimeOffTypeAssociationModelRoot() { }
        /// <summary>
        /// Initializes a new instance of the <see cref="PostTimeOffTypeAssociationModelRoot" /> class.
        /// </summary>
        /// <param name="timeOffTypeID">timeOffTypeID (required).</param>
        public PostTimeOffTypeAssociationModelRoot(string timeOffTypeID = default(string))
        {
            // to ensure "timeOffTypeID" is required (not null)
            if (timeOffTypeID == null)
            {
                throw new InvalidDataException("timeOffTypeID is a required property for PostTimeOffTypeAssociationModelRoot and cannot be null");
            }
            else
            {
                this.TimeOffTypeID = timeOffTypeID;
            }
        }
        
        /// <summary>
        /// Gets or Sets TimeOffTypeID
        /// </summary>
        [DataMember(Name="TimeOffTypeID", EmitDefaultValue=false)]
        public string TimeOffTypeID { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class PostTimeOffTypeAssociationModelRoot {\n");
            sb.Append("  TimeOffTypeID: ").Append(TimeOffTypeID).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }
  
        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public virtual string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="input">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object input)
        {
            return this.Equals(input as PostTimeOffTypeAssociationModelRoot);
        }

        /// <summary>
        /// Returns true if PostTimeOffTypeAssociationModelRoot instances are equal
        /// </summary>
        /// <param name="input">Instance of PostTimeOffTypeAssociationModelRoot to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(PostTimeOffTypeAssociationModelRoot input)
        {
            if (input == null)
                return false;

            return 
                (
                    this.TimeOffTypeID == input.TimeOffTypeID ||
                    (this.TimeOffTypeID != null &&
                    this.TimeOffTypeID.Equals(input.TimeOffTypeID))
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hashCode = 41;
                if (this.TimeOffTypeID != null)
                    hashCode = hashCode * 59 + this.TimeOffTypeID.GetHashCode();
                return hashCode;
            }
        }

        /// <summary>
        /// To validate all properties of the instance
        /// </summary>
        /// <param name="validationContext">Validation context</param>
        /// <returns>Validation Result</returns>
        IEnumerable<System.ComponentModel.DataAnnotations.ValidationResult> IValidatableObject.Validate(ValidationContext validationContext)
        {
            yield break;
        }
    }

}
