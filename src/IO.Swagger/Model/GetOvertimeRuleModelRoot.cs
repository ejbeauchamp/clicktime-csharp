/* 
 * REST API v2
 *
 * <p>This REST API offers programmatic access to common ClickTime functions. You can review the available endpoints by clicking on the category names below. Click on an endpoint itself to further drill down and review the available filters, usage requirements, response examples and the respective model.</p><p>Review the <a href=\"https://support.clicktime.com/hc/en-us/articles/360002884071\">REST API v2 General Information</a> to get an overview of how to use the API including usable examples of authentication and making requests.</p>
 *
 * OpenAPI spec version: v2
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.ComponentModel.DataAnnotations;
using SwaggerDateConverter = IO.Swagger.Client.SwaggerDateConverter;

namespace IO.Swagger.Model
{
    /// <summary>
    /// GetOvertimeRuleModelRoot
    /// </summary>
    [DataContract]
    public partial class GetOvertimeRuleModelRoot :  IEquatable<GetOvertimeRuleModelRoot>, IValidatableObject
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GetOvertimeRuleModelRoot" /> class.
        /// </summary>
        /// <param name="createdDate">createdDate.</param>
        /// <param name="iD">iD.</param>
        /// <param name="isActive">isActive.</param>
        /// <param name="legacyID">       Request requirements:    CTLegacyScramble&#x3D;true.</param>
        /// <param name="modifiedDate">modifiedDate.</param>
        /// <param name="overtimeDefinitionID">overtimeDefinitionID.</param>
        /// <param name="ruleDefinition">ruleDefinition.</param>
        public GetOvertimeRuleModelRoot(string createdDate = default(string), string iD = default(string), bool? isActive = default(bool?), string legacyID = default(string), string modifiedDate = default(string), string overtimeDefinitionID = default(string), GetBasicRuleDefinitionModelRuleDefinitionGetOvertimeRuleModelRoot ruleDefinition = default(GetBasicRuleDefinitionModelRuleDefinitionGetOvertimeRuleModelRoot))
        {
            this.CreatedDate = createdDate;
            this.ID = iD;
            this.IsActive = isActive;
            this.LegacyID = legacyID;
            this.ModifiedDate = modifiedDate;
            this.OvertimeDefinitionID = overtimeDefinitionID;
            this.RuleDefinition = ruleDefinition;
        }
        
        /// <summary>
        /// Gets or Sets CreatedDate
        /// </summary>
        [DataMember(Name="CreatedDate", EmitDefaultValue=false)]
        public string CreatedDate { get; set; }

        /// <summary>
        /// Gets or Sets ID
        /// </summary>
        [DataMember(Name="ID", EmitDefaultValue=false)]
        public string ID { get; set; }

        /// <summary>
        /// Gets or Sets IsActive
        /// </summary>
        [DataMember(Name="IsActive", EmitDefaultValue=false)]
        public bool? IsActive { get; set; }

        /// <summary>
        ///        Request requirements:    CTLegacyScramble&#x3D;true
        /// </summary>
        /// <value>       Request requirements:    CTLegacyScramble&#x3D;true</value>
        [DataMember(Name="LegacyID", EmitDefaultValue=false)]
        public string LegacyID { get; set; }

        /// <summary>
        /// Gets or Sets ModifiedDate
        /// </summary>
        [DataMember(Name="ModifiedDate", EmitDefaultValue=false)]
        public string ModifiedDate { get; set; }

        /// <summary>
        /// Gets or Sets OvertimeDefinitionID
        /// </summary>
        [DataMember(Name="OvertimeDefinitionID", EmitDefaultValue=false)]
        public string OvertimeDefinitionID { get; set; }

        /// <summary>
        /// Gets or Sets RuleDefinition
        /// </summary>
        [DataMember(Name="RuleDefinition", EmitDefaultValue=false)]
        public GetBasicRuleDefinitionModelRuleDefinitionGetOvertimeRuleModelRoot RuleDefinition { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class GetOvertimeRuleModelRoot {\n");
            sb.Append("  CreatedDate: ").Append(CreatedDate).Append("\n");
            sb.Append("  ID: ").Append(ID).Append("\n");
            sb.Append("  IsActive: ").Append(IsActive).Append("\n");
            sb.Append("  LegacyID: ").Append(LegacyID).Append("\n");
            sb.Append("  ModifiedDate: ").Append(ModifiedDate).Append("\n");
            sb.Append("  OvertimeDefinitionID: ").Append(OvertimeDefinitionID).Append("\n");
            sb.Append("  RuleDefinition: ").Append(RuleDefinition).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }
  
        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public virtual string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="input">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object input)
        {
            return this.Equals(input as GetOvertimeRuleModelRoot);
        }

        /// <summary>
        /// Returns true if GetOvertimeRuleModelRoot instances are equal
        /// </summary>
        /// <param name="input">Instance of GetOvertimeRuleModelRoot to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(GetOvertimeRuleModelRoot input)
        {
            if (input == null)
                return false;

            return 
                (
                    this.CreatedDate == input.CreatedDate ||
                    (this.CreatedDate != null &&
                    this.CreatedDate.Equals(input.CreatedDate))
                ) && 
                (
                    this.ID == input.ID ||
                    (this.ID != null &&
                    this.ID.Equals(input.ID))
                ) && 
                (
                    this.IsActive == input.IsActive ||
                    (this.IsActive != null &&
                    this.IsActive.Equals(input.IsActive))
                ) && 
                (
                    this.LegacyID == input.LegacyID ||
                    (this.LegacyID != null &&
                    this.LegacyID.Equals(input.LegacyID))
                ) && 
                (
                    this.ModifiedDate == input.ModifiedDate ||
                    (this.ModifiedDate != null &&
                    this.ModifiedDate.Equals(input.ModifiedDate))
                ) && 
                (
                    this.OvertimeDefinitionID == input.OvertimeDefinitionID ||
                    (this.OvertimeDefinitionID != null &&
                    this.OvertimeDefinitionID.Equals(input.OvertimeDefinitionID))
                ) && 
                (
                    this.RuleDefinition == input.RuleDefinition ||
                    (this.RuleDefinition != null &&
                    this.RuleDefinition.Equals(input.RuleDefinition))
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hashCode = 41;
                if (this.CreatedDate != null)
                    hashCode = hashCode * 59 + this.CreatedDate.GetHashCode();
                if (this.ID != null)
                    hashCode = hashCode * 59 + this.ID.GetHashCode();
                if (this.IsActive != null)
                    hashCode = hashCode * 59 + this.IsActive.GetHashCode();
                if (this.LegacyID != null)
                    hashCode = hashCode * 59 + this.LegacyID.GetHashCode();
                if (this.ModifiedDate != null)
                    hashCode = hashCode * 59 + this.ModifiedDate.GetHashCode();
                if (this.OvertimeDefinitionID != null)
                    hashCode = hashCode * 59 + this.OvertimeDefinitionID.GetHashCode();
                if (this.RuleDefinition != null)
                    hashCode = hashCode * 59 + this.RuleDefinition.GetHashCode();
                return hashCode;
            }
        }

        /// <summary>
        /// To validate all properties of the instance
        /// </summary>
        /// <param name="validationContext">Validation context</param>
        /// <returns>Validation Result</returns>
        IEnumerable<System.ComponentModel.DataAnnotations.ValidationResult> IValidatableObject.Validate(ValidationContext validationContext)
        {
            yield break;
        }
    }

}
