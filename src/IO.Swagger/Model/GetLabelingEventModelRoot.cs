/* 
 * REST API v2
 *
 * <p>This REST API offers programmatic access to common ClickTime functions. You can review the available endpoints by clicking on the category names below. Click on an endpoint itself to further drill down and review the available filters, usage requirements, response examples and the respective model.</p><p>Review the <a href=\"https://support.clicktime.com/hc/en-us/articles/360002884071\">REST API v2 General Information</a> to get an overview of how to use the API including usable examples of authentication and making requests.</p>
 *
 * OpenAPI spec version: v2
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.ComponentModel.DataAnnotations;
using SwaggerDateConverter = IO.Swagger.Client.SwaggerDateConverter;

namespace IO.Swagger.Model
{
    /// <summary>
    /// Labeling Event model for GET requests.
    /// </summary>
    [DataContract]
    public partial class GetLabelingEventModelRoot :  IEquatable<GetLabelingEventModelRoot>, IValidatableObject
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GetLabelingEventModelRoot" /> class.
        /// </summary>
        /// <param name="entityIDs">       User requirements:    Security Level(s): Admin, Manager (Run Company Reports).</param>
        /// <param name="entityType">       User requirements:    Security Level(s): Admin, Manager (Run Company Reports).</param>
        /// <param name="iD">       User requirements:    Security Level(s): Admin, Manager (Run Company Reports).</param>
        /// <param name="labelID">       User requirements:    Security Level(s): Admin, Manager (Run Company Reports).</param>
        /// <param name="legacyID">       Request requirements:    CTLegacyScramble&#x3D;true       User requirements:    Security Level(s): Admin, Manager (Run Company Reports).</param>
        public GetLabelingEventModelRoot(string entityIDs = default(string), string entityType = default(string), string iD = default(string), string labelID = default(string), string legacyID = default(string))
        {
            this.EntityIDs = entityIDs;
            this.EntityType = entityType;
            this.ID = iD;
            this.LabelID = labelID;
            this.LegacyID = legacyID;
        }
        
        /// <summary>
        ///        User requirements:    Security Level(s): Admin, Manager (Run Company Reports)
        /// </summary>
        /// <value>       User requirements:    Security Level(s): Admin, Manager (Run Company Reports)</value>
        [DataMember(Name="EntityIDs", EmitDefaultValue=false)]
        public string EntityIDs { get; set; }

        /// <summary>
        ///        User requirements:    Security Level(s): Admin, Manager (Run Company Reports)
        /// </summary>
        /// <value>       User requirements:    Security Level(s): Admin, Manager (Run Company Reports)</value>
        [DataMember(Name="EntityType", EmitDefaultValue=false)]
        public string EntityType { get; set; }

        /// <summary>
        ///        User requirements:    Security Level(s): Admin, Manager (Run Company Reports)
        /// </summary>
        /// <value>       User requirements:    Security Level(s): Admin, Manager (Run Company Reports)</value>
        [DataMember(Name="ID", EmitDefaultValue=false)]
        public string ID { get; set; }

        /// <summary>
        ///        User requirements:    Security Level(s): Admin, Manager (Run Company Reports)
        /// </summary>
        /// <value>       User requirements:    Security Level(s): Admin, Manager (Run Company Reports)</value>
        [DataMember(Name="LabelID", EmitDefaultValue=false)]
        public string LabelID { get; set; }

        /// <summary>
        ///        Request requirements:    CTLegacyScramble&#x3D;true       User requirements:    Security Level(s): Admin, Manager (Run Company Reports)
        /// </summary>
        /// <value>       Request requirements:    CTLegacyScramble&#x3D;true       User requirements:    Security Level(s): Admin, Manager (Run Company Reports)</value>
        [DataMember(Name="LegacyID", EmitDefaultValue=false)]
        public string LegacyID { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class GetLabelingEventModelRoot {\n");
            sb.Append("  EntityIDs: ").Append(EntityIDs).Append("\n");
            sb.Append("  EntityType: ").Append(EntityType).Append("\n");
            sb.Append("  ID: ").Append(ID).Append("\n");
            sb.Append("  LabelID: ").Append(LabelID).Append("\n");
            sb.Append("  LegacyID: ").Append(LegacyID).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }
  
        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public virtual string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="input">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object input)
        {
            return this.Equals(input as GetLabelingEventModelRoot);
        }

        /// <summary>
        /// Returns true if GetLabelingEventModelRoot instances are equal
        /// </summary>
        /// <param name="input">Instance of GetLabelingEventModelRoot to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(GetLabelingEventModelRoot input)
        {
            if (input == null)
                return false;

            return 
                (
                    this.EntityIDs == input.EntityIDs ||
                    (this.EntityIDs != null &&
                    this.EntityIDs.Equals(input.EntityIDs))
                ) && 
                (
                    this.EntityType == input.EntityType ||
                    (this.EntityType != null &&
                    this.EntityType.Equals(input.EntityType))
                ) && 
                (
                    this.ID == input.ID ||
                    (this.ID != null &&
                    this.ID.Equals(input.ID))
                ) && 
                (
                    this.LabelID == input.LabelID ||
                    (this.LabelID != null &&
                    this.LabelID.Equals(input.LabelID))
                ) && 
                (
                    this.LegacyID == input.LegacyID ||
                    (this.LegacyID != null &&
                    this.LegacyID.Equals(input.LegacyID))
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hashCode = 41;
                if (this.EntityIDs != null)
                    hashCode = hashCode * 59 + this.EntityIDs.GetHashCode();
                if (this.EntityType != null)
                    hashCode = hashCode * 59 + this.EntityType.GetHashCode();
                if (this.ID != null)
                    hashCode = hashCode * 59 + this.ID.GetHashCode();
                if (this.LabelID != null)
                    hashCode = hashCode * 59 + this.LabelID.GetHashCode();
                if (this.LegacyID != null)
                    hashCode = hashCode * 59 + this.LegacyID.GetHashCode();
                return hashCode;
            }
        }

        /// <summary>
        /// To validate all properties of the instance
        /// </summary>
        /// <param name="validationContext">Validation context</param>
        /// <returns>Validation Result</returns>
        IEnumerable<System.ComponentModel.DataAnnotations.ValidationResult> IValidatableObject.Validate(ValidationContext validationContext)
        {
            yield break;
        }
    }

}
