/* 
 * REST API v2
 *
 * <p>This REST API offers programmatic access to common ClickTime functions. You can review the available endpoints by clicking on the category names below. Click on an endpoint itself to further drill down and review the available filters, usage requirements, response examples and the respective model.</p><p>Review the <a href=\"https://support.clicktime.com/hc/en-us/articles/360002884071\">REST API v2 General Information</a> to get an overview of how to use the API including usable examples of authentication and making requests.</p>
 *
 * OpenAPI spec version: v2
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.ComponentModel.DataAnnotations;
using SwaggerDateConverter = IO.Swagger.Client.SwaggerDateConverter;

namespace IO.Swagger.Model
{
    /// <summary>
    /// GetTaskModelNew
    /// </summary>
    [DataContract]
    public partial class GetTaskModelNew :  IEquatable<GetTaskModelNew>, IValidatableObject
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GetTaskModelNew" /> class.
        /// </summary>
        /// <param name="accountingPackageID">       User requirements:    Security Level(s): Admin, Manager (View Tasks).</param>
        /// <param name="billingRate">       User requirements:    Security Level(s): Admin, Manager (View Tasks)    Security Level(s): Admin, Manager (View Billingrates)    Security Level(s): Admin, Manager (Add/Edit Tasks)       Company requirements:    Billing rate model(s): Task, Task x User, Task x Job.</param>
        /// <param name="customFields">       Company requirements:    Custom Fields must be configured for Tasks.       User requirements:    Security Level(s): Admin, Manager (View Tasks)    Security Level(s): Admin, Manager (Add/Edit Tasks).</param>
        /// <param name="iD">iD.</param>
        /// <param name="isActive">isActive.</param>
        /// <param name="isBillable">       User requirements:    Security Level(s): Admin, Manager (View Tasks)    Security Level(s): Admin, Manager (Add/Edit Tasks)       Company requirements:    Billability by: Task.</param>
        /// <param name="listDisplayText">listDisplayText.</param>
        /// <param name="name">name.</param>
        /// <param name="notes">       User requirements:    Security Level(s): Admin, Manager (View Tasks).</param>
        /// <param name="taskCode">taskCode.</param>
        /// <param name="useCompanyBillingRate">       User requirements:    Security Level(s): Admin, Manager (View Tasks)    Security Level(s): Admin, Manager (View Billingrates)    Security Level(s): Admin, Manager (Add/Edit Tasks)       Company requirements:    Billing rate model(s): Task, Task x User, Task x Job.</param>
        public GetTaskModelNew(string accountingPackageID = default(string), double? billingRate = default(double?), Dictionary<string, Object> customFields = default(Dictionary<string, Object>), string iD = default(string), bool? isActive = default(bool?), bool? isBillable = default(bool?), string listDisplayText = default(string), string name = default(string), string notes = default(string), string taskCode = default(string), bool? useCompanyBillingRate = default(bool?))
        {
            this.AccountingPackageID = accountingPackageID;
            this.BillingRate = billingRate;
            this.CustomFields = customFields;
            this.ID = iD;
            this.IsActive = isActive;
            this.IsBillable = isBillable;
            this.ListDisplayText = listDisplayText;
            this.Name = name;
            this.Notes = notes;
            this.TaskCode = taskCode;
            this.UseCompanyBillingRate = useCompanyBillingRate;
        }
        
        /// <summary>
        ///        User requirements:    Security Level(s): Admin, Manager (View Tasks)
        /// </summary>
        /// <value>       User requirements:    Security Level(s): Admin, Manager (View Tasks)</value>
        [DataMember(Name="AccountingPackageID", EmitDefaultValue=false)]
        public string AccountingPackageID { get; set; }

        /// <summary>
        ///        User requirements:    Security Level(s): Admin, Manager (View Tasks)    Security Level(s): Admin, Manager (View Billingrates)    Security Level(s): Admin, Manager (Add/Edit Tasks)       Company requirements:    Billing rate model(s): Task, Task x User, Task x Job
        /// </summary>
        /// <value>       User requirements:    Security Level(s): Admin, Manager (View Tasks)    Security Level(s): Admin, Manager (View Billingrates)    Security Level(s): Admin, Manager (Add/Edit Tasks)       Company requirements:    Billing rate model(s): Task, Task x User, Task x Job</value>
        [DataMember(Name="BillingRate", EmitDefaultValue=false)]
        public double? BillingRate { get; set; }

        /// <summary>
        ///        Company requirements:    Custom Fields must be configured for Tasks.       User requirements:    Security Level(s): Admin, Manager (View Tasks)    Security Level(s): Admin, Manager (Add/Edit Tasks)
        /// </summary>
        /// <value>       Company requirements:    Custom Fields must be configured for Tasks.       User requirements:    Security Level(s): Admin, Manager (View Tasks)    Security Level(s): Admin, Manager (Add/Edit Tasks)</value>
        [DataMember(Name="CustomFields", EmitDefaultValue=false)]
        public Dictionary<string, Object> CustomFields { get; set; }

        /// <summary>
        /// Gets or Sets ID
        /// </summary>
        [DataMember(Name="ID", EmitDefaultValue=false)]
        public string ID { get; set; }

        /// <summary>
        /// Gets or Sets IsActive
        /// </summary>
        [DataMember(Name="IsActive", EmitDefaultValue=false)]
        public bool? IsActive { get; set; }

        /// <summary>
        ///        User requirements:    Security Level(s): Admin, Manager (View Tasks)    Security Level(s): Admin, Manager (Add/Edit Tasks)       Company requirements:    Billability by: Task
        /// </summary>
        /// <value>       User requirements:    Security Level(s): Admin, Manager (View Tasks)    Security Level(s): Admin, Manager (Add/Edit Tasks)       Company requirements:    Billability by: Task</value>
        [DataMember(Name="IsBillable", EmitDefaultValue=false)]
        public bool? IsBillable { get; set; }

        /// <summary>
        /// Gets or Sets ListDisplayText
        /// </summary>
        [DataMember(Name="ListDisplayText", EmitDefaultValue=false)]
        public string ListDisplayText { get; set; }

        /// <summary>
        /// Gets or Sets Name
        /// </summary>
        [DataMember(Name="Name", EmitDefaultValue=false)]
        public string Name { get; set; }

        /// <summary>
        ///        User requirements:    Security Level(s): Admin, Manager (View Tasks)
        /// </summary>
        /// <value>       User requirements:    Security Level(s): Admin, Manager (View Tasks)</value>
        [DataMember(Name="Notes", EmitDefaultValue=false)]
        public string Notes { get; set; }

        /// <summary>
        /// Gets or Sets TaskCode
        /// </summary>
        [DataMember(Name="TaskCode", EmitDefaultValue=false)]
        public string TaskCode { get; set; }

        /// <summary>
        ///        User requirements:    Security Level(s): Admin, Manager (View Tasks)    Security Level(s): Admin, Manager (View Billingrates)    Security Level(s): Admin, Manager (Add/Edit Tasks)       Company requirements:    Billing rate model(s): Task, Task x User, Task x Job
        /// </summary>
        /// <value>       User requirements:    Security Level(s): Admin, Manager (View Tasks)    Security Level(s): Admin, Manager (View Billingrates)    Security Level(s): Admin, Manager (Add/Edit Tasks)       Company requirements:    Billing rate model(s): Task, Task x User, Task x Job</value>
        [DataMember(Name="UseCompanyBillingRate", EmitDefaultValue=false)]
        public bool? UseCompanyBillingRate { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class GetTaskModelNew {\n");
            sb.Append("  AccountingPackageID: ").Append(AccountingPackageID).Append("\n");
            sb.Append("  BillingRate: ").Append(BillingRate).Append("\n");
            sb.Append("  CustomFields: ").Append(CustomFields).Append("\n");
            sb.Append("  ID: ").Append(ID).Append("\n");
            sb.Append("  IsActive: ").Append(IsActive).Append("\n");
            sb.Append("  IsBillable: ").Append(IsBillable).Append("\n");
            sb.Append("  ListDisplayText: ").Append(ListDisplayText).Append("\n");
            sb.Append("  Name: ").Append(Name).Append("\n");
            sb.Append("  Notes: ").Append(Notes).Append("\n");
            sb.Append("  TaskCode: ").Append(TaskCode).Append("\n");
            sb.Append("  UseCompanyBillingRate: ").Append(UseCompanyBillingRate).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }
  
        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public virtual string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="input">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object input)
        {
            return this.Equals(input as GetTaskModelNew);
        }

        /// <summary>
        /// Returns true if GetTaskModelNew instances are equal
        /// </summary>
        /// <param name="input">Instance of GetTaskModelNew to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(GetTaskModelNew input)
        {
            if (input == null)
                return false;

            return 
                (
                    this.AccountingPackageID == input.AccountingPackageID ||
                    (this.AccountingPackageID != null &&
                    this.AccountingPackageID.Equals(input.AccountingPackageID))
                ) && 
                (
                    this.BillingRate == input.BillingRate ||
                    (this.BillingRate != null &&
                    this.BillingRate.Equals(input.BillingRate))
                ) && 
                (
                    this.CustomFields == input.CustomFields ||
                    this.CustomFields != null &&
                    this.CustomFields.SequenceEqual(input.CustomFields)
                ) && 
                (
                    this.ID == input.ID ||
                    (this.ID != null &&
                    this.ID.Equals(input.ID))
                ) && 
                (
                    this.IsActive == input.IsActive ||
                    (this.IsActive != null &&
                    this.IsActive.Equals(input.IsActive))
                ) && 
                (
                    this.IsBillable == input.IsBillable ||
                    (this.IsBillable != null &&
                    this.IsBillable.Equals(input.IsBillable))
                ) && 
                (
                    this.ListDisplayText == input.ListDisplayText ||
                    (this.ListDisplayText != null &&
                    this.ListDisplayText.Equals(input.ListDisplayText))
                ) && 
                (
                    this.Name == input.Name ||
                    (this.Name != null &&
                    this.Name.Equals(input.Name))
                ) && 
                (
                    this.Notes == input.Notes ||
                    (this.Notes != null &&
                    this.Notes.Equals(input.Notes))
                ) && 
                (
                    this.TaskCode == input.TaskCode ||
                    (this.TaskCode != null &&
                    this.TaskCode.Equals(input.TaskCode))
                ) && 
                (
                    this.UseCompanyBillingRate == input.UseCompanyBillingRate ||
                    (this.UseCompanyBillingRate != null &&
                    this.UseCompanyBillingRate.Equals(input.UseCompanyBillingRate))
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hashCode = 41;
                if (this.AccountingPackageID != null)
                    hashCode = hashCode * 59 + this.AccountingPackageID.GetHashCode();
                if (this.BillingRate != null)
                    hashCode = hashCode * 59 + this.BillingRate.GetHashCode();
                if (this.CustomFields != null)
                    hashCode = hashCode * 59 + this.CustomFields.GetHashCode();
                if (this.ID != null)
                    hashCode = hashCode * 59 + this.ID.GetHashCode();
                if (this.IsActive != null)
                    hashCode = hashCode * 59 + this.IsActive.GetHashCode();
                if (this.IsBillable != null)
                    hashCode = hashCode * 59 + this.IsBillable.GetHashCode();
                if (this.ListDisplayText != null)
                    hashCode = hashCode * 59 + this.ListDisplayText.GetHashCode();
                if (this.Name != null)
                    hashCode = hashCode * 59 + this.Name.GetHashCode();
                if (this.Notes != null)
                    hashCode = hashCode * 59 + this.Notes.GetHashCode();
                if (this.TaskCode != null)
                    hashCode = hashCode * 59 + this.TaskCode.GetHashCode();
                if (this.UseCompanyBillingRate != null)
                    hashCode = hashCode * 59 + this.UseCompanyBillingRate.GetHashCode();
                return hashCode;
            }
        }

        /// <summary>
        /// To validate all properties of the instance
        /// </summary>
        /// <param name="validationContext">Validation context</param>
        /// <returns>Validation Result</returns>
        IEnumerable<System.ComponentModel.DataAnnotations.ValidationResult> IValidatableObject.Validate(ValidationContext validationContext)
        {
            yield break;
        }
    }

}
