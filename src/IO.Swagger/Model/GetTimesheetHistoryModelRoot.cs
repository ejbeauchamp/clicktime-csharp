/* 
 * REST API v2
 *
 * <p>This REST API offers programmatic access to common ClickTime functions. You can review the available endpoints by clicking on the category names below. Click on an endpoint itself to further drill down and review the available filters, usage requirements, response examples and the respective model.</p><p>Review the <a href=\"https://support.clicktime.com/hc/en-us/articles/360002884071\">REST API v2 General Information</a> to get an overview of how to use the API including usable examples of authentication and making requests.</p>
 *
 * OpenAPI spec version: v2
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.ComponentModel.DataAnnotations;
using SwaggerDateConverter = IO.Swagger.Client.SwaggerDateConverter;

namespace IO.Swagger.Model
{
    /// <summary>
    /// GetTimesheetHistoryModelRoot
    /// </summary>
    [DataContract]
    public partial class GetTimesheetHistoryModelRoot :  IEquatable<GetTimesheetHistoryModelRoot>, IValidatableObject
    {
        /// <summary>
        /// Defines JobStatus
        /// </summary>
        [JsonConverter(typeof(StringEnumConverter))]
        public enum JobStatusEnum
        {
            
            /// <summary>
            /// Enum Approved for value: Approved
            /// </summary>
            [EnumMember(Value = "Approved")]
            Approved = 1,
            
            /// <summary>
            /// Enum Rejected for value: Rejected
            /// </summary>
            [EnumMember(Value = "Rejected")]
            Rejected = 2
        }

        /// <summary>
        /// Gets or Sets JobStatus
        /// </summary>
        [DataMember(Name="JobStatus", EmitDefaultValue=false)]
        public JobStatusEnum? JobStatus { get; set; }
        /// <summary>
        /// Initializes a new instance of the <see cref="GetTimesheetHistoryModelRoot" /> class.
        /// </summary>
        /// <param name="action">action.</param>
        /// <param name="actionByUser">       Request requirements:    Verbose&#x3D;true.</param>
        /// <param name="actionByUserID">actionByUserID.</param>
        /// <param name="actionByUserName">actionByUserName.</param>
        /// <param name="comment">comment.</param>
        /// <param name="date">date.</param>
        /// <param name="job">       Request requirements:    Verbose&#x3D;true.</param>
        /// <param name="jobID">jobID.</param>
        /// <param name="jobStatus">jobStatus.</param>
        public GetTimesheetHistoryModelRoot(string action = default(string), GetBasicUserModelActionByUserGetTimesheetHistoryModelRoot actionByUser = default(GetBasicUserModelActionByUserGetTimesheetHistoryModelRoot), string actionByUserID = default(string), string actionByUserName = default(string), string comment = default(string), string date = default(string), GetBasicJobModelJobGetTimesheetHistoryModelRoot job = default(GetBasicJobModelJobGetTimesheetHistoryModelRoot), string jobID = default(string), JobStatusEnum? jobStatus = default(JobStatusEnum?))
        {
            this.Action = action;
            this.ActionByUser = actionByUser;
            this.ActionByUserID = actionByUserID;
            this.ActionByUserName = actionByUserName;
            this.Comment = comment;
            this.Date = date;
            this.Job = job;
            this.JobID = jobID;
            this.JobStatus = jobStatus;
        }
        
        /// <summary>
        /// Gets or Sets Action
        /// </summary>
        [DataMember(Name="Action", EmitDefaultValue=false)]
        public string Action { get; set; }

        /// <summary>
        ///        Request requirements:    Verbose&#x3D;true
        /// </summary>
        /// <value>       Request requirements:    Verbose&#x3D;true</value>
        [DataMember(Name="ActionByUser", EmitDefaultValue=false)]
        public GetBasicUserModelActionByUserGetTimesheetHistoryModelRoot ActionByUser { get; set; }

        /// <summary>
        /// Gets or Sets ActionByUserID
        /// </summary>
        [DataMember(Name="ActionByUserID", EmitDefaultValue=false)]
        public string ActionByUserID { get; set; }

        /// <summary>
        /// Gets or Sets ActionByUserName
        /// </summary>
        [DataMember(Name="ActionByUserName", EmitDefaultValue=false)]
        public string ActionByUserName { get; set; }

        /// <summary>
        /// Gets or Sets Comment
        /// </summary>
        [DataMember(Name="Comment", EmitDefaultValue=false)]
        public string Comment { get; set; }

        /// <summary>
        /// Gets or Sets Date
        /// </summary>
        [DataMember(Name="Date", EmitDefaultValue=false)]
        public string Date { get; set; }

        /// <summary>
        ///        Request requirements:    Verbose&#x3D;true
        /// </summary>
        /// <value>       Request requirements:    Verbose&#x3D;true</value>
        [DataMember(Name="Job", EmitDefaultValue=false)]
        public GetBasicJobModelJobGetTimesheetHistoryModelRoot Job { get; set; }

        /// <summary>
        /// Gets or Sets JobID
        /// </summary>
        [DataMember(Name="JobID", EmitDefaultValue=false)]
        public string JobID { get; set; }


        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class GetTimesheetHistoryModelRoot {\n");
            sb.Append("  Action: ").Append(Action).Append("\n");
            sb.Append("  ActionByUser: ").Append(ActionByUser).Append("\n");
            sb.Append("  ActionByUserID: ").Append(ActionByUserID).Append("\n");
            sb.Append("  ActionByUserName: ").Append(ActionByUserName).Append("\n");
            sb.Append("  Comment: ").Append(Comment).Append("\n");
            sb.Append("  Date: ").Append(Date).Append("\n");
            sb.Append("  Job: ").Append(Job).Append("\n");
            sb.Append("  JobID: ").Append(JobID).Append("\n");
            sb.Append("  JobStatus: ").Append(JobStatus).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }
  
        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public virtual string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="input">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object input)
        {
            return this.Equals(input as GetTimesheetHistoryModelRoot);
        }

        /// <summary>
        /// Returns true if GetTimesheetHistoryModelRoot instances are equal
        /// </summary>
        /// <param name="input">Instance of GetTimesheetHistoryModelRoot to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(GetTimesheetHistoryModelRoot input)
        {
            if (input == null)
                return false;

            return 
                (
                    this.Action == input.Action ||
                    (this.Action != null &&
                    this.Action.Equals(input.Action))
                ) && 
                (
                    this.ActionByUser == input.ActionByUser ||
                    (this.ActionByUser != null &&
                    this.ActionByUser.Equals(input.ActionByUser))
                ) && 
                (
                    this.ActionByUserID == input.ActionByUserID ||
                    (this.ActionByUserID != null &&
                    this.ActionByUserID.Equals(input.ActionByUserID))
                ) && 
                (
                    this.ActionByUserName == input.ActionByUserName ||
                    (this.ActionByUserName != null &&
                    this.ActionByUserName.Equals(input.ActionByUserName))
                ) && 
                (
                    this.Comment == input.Comment ||
                    (this.Comment != null &&
                    this.Comment.Equals(input.Comment))
                ) && 
                (
                    this.Date == input.Date ||
                    (this.Date != null &&
                    this.Date.Equals(input.Date))
                ) && 
                (
                    this.Job == input.Job ||
                    (this.Job != null &&
                    this.Job.Equals(input.Job))
                ) && 
                (
                    this.JobID == input.JobID ||
                    (this.JobID != null &&
                    this.JobID.Equals(input.JobID))
                ) && 
                (
                    this.JobStatus == input.JobStatus ||
                    (this.JobStatus != null &&
                    this.JobStatus.Equals(input.JobStatus))
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hashCode = 41;
                if (this.Action != null)
                    hashCode = hashCode * 59 + this.Action.GetHashCode();
                if (this.ActionByUser != null)
                    hashCode = hashCode * 59 + this.ActionByUser.GetHashCode();
                if (this.ActionByUserID != null)
                    hashCode = hashCode * 59 + this.ActionByUserID.GetHashCode();
                if (this.ActionByUserName != null)
                    hashCode = hashCode * 59 + this.ActionByUserName.GetHashCode();
                if (this.Comment != null)
                    hashCode = hashCode * 59 + this.Comment.GetHashCode();
                if (this.Date != null)
                    hashCode = hashCode * 59 + this.Date.GetHashCode();
                if (this.Job != null)
                    hashCode = hashCode * 59 + this.Job.GetHashCode();
                if (this.JobID != null)
                    hashCode = hashCode * 59 + this.JobID.GetHashCode();
                if (this.JobStatus != null)
                    hashCode = hashCode * 59 + this.JobStatus.GetHashCode();
                return hashCode;
            }
        }

        /// <summary>
        /// To validate all properties of the instance
        /// </summary>
        /// <param name="validationContext">Validation context</param>
        /// <returns>Validation Result</returns>
        IEnumerable<System.ComponentModel.DataAnnotations.ValidationResult> IValidatableObject.Validate(ValidationContext validationContext)
        {
            yield break;
        }
    }

}
