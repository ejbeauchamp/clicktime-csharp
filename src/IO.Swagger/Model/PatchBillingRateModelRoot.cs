/* 
 * REST API v2
 *
 * <p>This REST API offers programmatic access to common ClickTime functions. You can review the available endpoints by clicking on the category names below. Click on an endpoint itself to further drill down and review the available filters, usage requirements, response examples and the respective model.</p><p>Review the <a href=\"https://support.clicktime.com/hc/en-us/articles/360002884071\">REST API v2 General Information</a> to get an overview of how to use the API including usable examples of authentication and making requests.</p>
 *
 * OpenAPI spec version: v2
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.ComponentModel.DataAnnotations;
using SwaggerDateConverter = IO.Swagger.Client.SwaggerDateConverter;

namespace IO.Swagger.Model
{
    /// <summary>
    /// PatchBillingRateModelRoot
    /// </summary>
    [DataContract]
    public partial class PatchBillingRateModelRoot :  IEquatable<PatchBillingRateModelRoot>, IValidatableObject
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PatchBillingRateModelRoot" /> class.
        /// </summary>
        [JsonConstructorAttribute]
        protected PatchBillingRateModelRoot() { }
        /// <summary>
        /// Initializes a new instance of the <see cref="PatchBillingRateModelRoot" /> class.
        /// </summary>
        /// <param name="billingRate">billingRate (required).</param>
        public PatchBillingRateModelRoot(double? billingRate = default(double?))
        {
            // to ensure "billingRate" is required (not null)
            if (billingRate == null)
            {
                throw new InvalidDataException("billingRate is a required property for PatchBillingRateModelRoot and cannot be null");
            }
            else
            {
                this.BillingRate = billingRate;
            }
        }
        
        /// <summary>
        /// Gets or Sets BillingRate
        /// </summary>
        [DataMember(Name="BillingRate", EmitDefaultValue=false)]
        public double? BillingRate { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class PatchBillingRateModelRoot {\n");
            sb.Append("  BillingRate: ").Append(BillingRate).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }
  
        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public virtual string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="input">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object input)
        {
            return this.Equals(input as PatchBillingRateModelRoot);
        }

        /// <summary>
        /// Returns true if PatchBillingRateModelRoot instances are equal
        /// </summary>
        /// <param name="input">Instance of PatchBillingRateModelRoot to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(PatchBillingRateModelRoot input)
        {
            if (input == null)
                return false;

            return 
                (
                    this.BillingRate == input.BillingRate ||
                    (this.BillingRate != null &&
                    this.BillingRate.Equals(input.BillingRate))
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hashCode = 41;
                if (this.BillingRate != null)
                    hashCode = hashCode * 59 + this.BillingRate.GetHashCode();
                return hashCode;
            }
        }

        /// <summary>
        /// To validate all properties of the instance
        /// </summary>
        /// <param name="validationContext">Validation context</param>
        /// <returns>Validation Result</returns>
        IEnumerable<System.ComponentModel.DataAnnotations.ValidationResult> IValidatableObject.Validate(ValidationContext validationContext)
        {
            yield break;
        }
    }

}
