/* 
 * REST API v2
 *
 * <p>This REST API offers programmatic access to common ClickTime functions. You can review the available endpoints by clicking on the category names below. Click on an endpoint itself to further drill down and review the available filters, usage requirements, response examples and the respective model.</p><p>Review the <a href=\"https://support.clicktime.com/hc/en-us/articles/360002884071\">REST API v2 General Information</a> to get an overview of how to use the API including usable examples of authentication and making requests.</p>
 *
 * OpenAPI spec version: v2
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.ComponentModel.DataAnnotations;
using SwaggerDateConverter = IO.Swagger.Client.SwaggerDateConverter;

namespace IO.Swagger.Model
{
    /// <summary>
    /// Paging data for an API response.
    /// </summary>
    [DataContract]
    public partial class Page :  IEquatable<Page>, IValidatableObject
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Page" /> class.
        /// </summary>
        /// <param name="count">count.</param>
        /// <param name="limit">limit.</param>
        /// <param name="links">links.</param>
        /// <param name="offset">offset.</param>
        public Page(int? count = default(int?), int? limit = default(int?), Links links = default(Links), int? offset = default(int?))
        {
            this.count = count;
            this.limit = limit;
            this.links = links;
            this.offset = offset;
        }
        
        /// <summary>
        /// Gets or Sets count
        /// </summary>
        [DataMember(Name="count", EmitDefaultValue=false)]
        public int? count { get; set; }

        /// <summary>
        /// Gets or Sets limit
        /// </summary>
        [DataMember(Name="limit", EmitDefaultValue=false)]
        public int? limit { get; set; }

        /// <summary>
        /// Gets or Sets links
        /// </summary>
        [DataMember(Name="links", EmitDefaultValue=false)]
        public Links links { get; set; }

        /// <summary>
        /// Gets or Sets offset
        /// </summary>
        [DataMember(Name="offset", EmitDefaultValue=false)]
        public int? offset { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class Page {\n");
            sb.Append("  count: ").Append(count).Append("\n");
            sb.Append("  limit: ").Append(limit).Append("\n");
            sb.Append("  links: ").Append(links).Append("\n");
            sb.Append("  offset: ").Append(offset).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }
  
        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public virtual string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="input">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object input)
        {
            return this.Equals(input as Page);
        }

        /// <summary>
        /// Returns true if Page instances are equal
        /// </summary>
        /// <param name="input">Instance of Page to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(Page input)
        {
            if (input == null)
                return false;

            return 
                (
                    this.count == input.count ||
                    (this.count != null &&
                    this.count.Equals(input.count))
                ) && 
                (
                    this.limit == input.limit ||
                    (this.limit != null &&
                    this.limit.Equals(input.limit))
                ) && 
                (
                    this.links == input.links ||
                    (this.links != null &&
                    this.links.Equals(input.links))
                ) && 
                (
                    this.offset == input.offset ||
                    (this.offset != null &&
                    this.offset.Equals(input.offset))
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hashCode = 41;
                if (this.count != null)
                    hashCode = hashCode * 59 + this.count.GetHashCode();
                if (this.limit != null)
                    hashCode = hashCode * 59 + this.limit.GetHashCode();
                if (this.links != null)
                    hashCode = hashCode * 59 + this.links.GetHashCode();
                if (this.offset != null)
                    hashCode = hashCode * 59 + this.offset.GetHashCode();
                return hashCode;
            }
        }

        /// <summary>
        /// To validate all properties of the instance
        /// </summary>
        /// <param name="validationContext">Validation context</param>
        /// <returns>Validation Result</returns>
        IEnumerable<System.ComponentModel.DataAnnotations.ValidationResult> IValidatableObject.Validate(ValidationContext validationContext)
        {
            yield break;
        }
    }

}
