/* 
 * REST API v2
 *
 * <p>This REST API offers programmatic access to common ClickTime functions. You can review the available endpoints by clicking on the category names below. Click on an endpoint itself to further drill down and review the available filters, usage requirements, response examples and the respective model.</p><p>Review the <a href=\"https://support.clicktime.com/hc/en-us/articles/360002884071\">REST API v2 General Information</a> to get an overview of how to use the API including usable examples of authentication and making requests.</p>
 *
 * OpenAPI spec version: v2
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.ComponentModel.DataAnnotations;
using SwaggerDateConverter = IO.Swagger.Client.SwaggerDateConverter;

namespace IO.Swagger.Model
{
    /// <summary>
    /// PostTimerIntervalModelIntervalsPostTimerModelRoot
    /// </summary>
    [DataContract]
    public partial class PostTimerIntervalModelIntervalsPostTimerModelRoot :  IEquatable<PostTimerIntervalModelIntervalsPostTimerModelRoot>, IValidatableObject
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PostTimerIntervalModelIntervalsPostTimerModelRoot" /> class.
        /// </summary>
        [JsonConstructorAttribute]
        protected PostTimerIntervalModelIntervalsPostTimerModelRoot() { }
        /// <summary>
        /// Initializes a new instance of the <see cref="PostTimerIntervalModelIntervalsPostTimerModelRoot" /> class.
        /// </summary>
        /// <param name="end">hh:mm:ss (required).</param>
        /// <param name="start">hh:mm:ss (required).</param>
        /// <param name="timeZoneOffset">+hh:mm or -hh:mm from UTC.</param>
        public PostTimerIntervalModelIntervalsPostTimerModelRoot(string end = default(string), string start = default(string), string timeZoneOffset = default(string))
        {
            // to ensure "end" is required (not null)
            if (end == null)
            {
                throw new InvalidDataException("end is a required property for PostTimerIntervalModelIntervalsPostTimerModelRoot and cannot be null");
            }
            else
            {
                this.End = end;
            }
            // to ensure "start" is required (not null)
            if (start == null)
            {
                throw new InvalidDataException("start is a required property for PostTimerIntervalModelIntervalsPostTimerModelRoot and cannot be null");
            }
            else
            {
                this.Start = start;
            }
            this.TimeZoneOffset = timeZoneOffset;
        }
        
        /// <summary>
        /// hh:mm:ss
        /// </summary>
        /// <value>hh:mm:ss</value>
        [DataMember(Name="End", EmitDefaultValue=false)]
        public string End { get; set; }

        /// <summary>
        /// hh:mm:ss
        /// </summary>
        /// <value>hh:mm:ss</value>
        [DataMember(Name="Start", EmitDefaultValue=false)]
        public string Start { get; set; }

        /// <summary>
        /// +hh:mm or -hh:mm from UTC
        /// </summary>
        /// <value>+hh:mm or -hh:mm from UTC</value>
        [DataMember(Name="TimeZoneOffset", EmitDefaultValue=false)]
        public string TimeZoneOffset { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class PostTimerIntervalModelIntervalsPostTimerModelRoot {\n");
            sb.Append("  End: ").Append(End).Append("\n");
            sb.Append("  Start: ").Append(Start).Append("\n");
            sb.Append("  TimeZoneOffset: ").Append(TimeZoneOffset).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }
  
        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public virtual string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="input">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object input)
        {
            return this.Equals(input as PostTimerIntervalModelIntervalsPostTimerModelRoot);
        }

        /// <summary>
        /// Returns true if PostTimerIntervalModelIntervalsPostTimerModelRoot instances are equal
        /// </summary>
        /// <param name="input">Instance of PostTimerIntervalModelIntervalsPostTimerModelRoot to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(PostTimerIntervalModelIntervalsPostTimerModelRoot input)
        {
            if (input == null)
                return false;

            return 
                (
                    this.End == input.End ||
                    (this.End != null &&
                    this.End.Equals(input.End))
                ) && 
                (
                    this.Start == input.Start ||
                    (this.Start != null &&
                    this.Start.Equals(input.Start))
                ) && 
                (
                    this.TimeZoneOffset == input.TimeZoneOffset ||
                    (this.TimeZoneOffset != null &&
                    this.TimeZoneOffset.Equals(input.TimeZoneOffset))
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hashCode = 41;
                if (this.End != null)
                    hashCode = hashCode * 59 + this.End.GetHashCode();
                if (this.Start != null)
                    hashCode = hashCode * 59 + this.Start.GetHashCode();
                if (this.TimeZoneOffset != null)
                    hashCode = hashCode * 59 + this.TimeZoneOffset.GetHashCode();
                return hashCode;
            }
        }

        /// <summary>
        /// To validate all properties of the instance
        /// </summary>
        /// <param name="validationContext">Validation context</param>
        /// <returns>Validation Result</returns>
        IEnumerable<System.ComponentModel.DataAnnotations.ValidationResult> IValidatableObject.Validate(ValidationContext validationContext)
        {
            // TimeZoneOffset (string) pattern
            Regex regexTimeZoneOffset = new Regex(@"^[\\+-]\\d{2}:\\d{2}$", RegexOptions.CultureInvariant);
            if (false == regexTimeZoneOffset.Match(this.TimeZoneOffset).Success)
            {
                yield return new System.ComponentModel.DataAnnotations.ValidationResult("Invalid value for TimeZoneOffset, must match a pattern of " + regexTimeZoneOffset, new [] { "TimeZoneOffset" });
            }

            yield break;
        }
    }

}
