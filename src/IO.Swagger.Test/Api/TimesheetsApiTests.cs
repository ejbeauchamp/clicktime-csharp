/* 
 * REST API v2
 *
 * <p>This REST API offers programmatic access to common ClickTime functions. You can review the available endpoints by clicking on the category names below. Click on an endpoint itself to further drill down and review the available filters, usage requirements, response examples and the respective model.</p><p>Review the <a href=\"https://support.clicktime.com/hc/en-us/articles/360002884071\">REST API v2 General Information</a> to get an overview of how to use the API including usable examples of authentication and making requests.</p>
 *
 * OpenAPI spec version: v2
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

using System;
using System.IO;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using RestSharp;
using NUnit.Framework;

using IO.Swagger.Client;
using IO.Swagger.Api;
using IO.Swagger.Model;

namespace IO.Swagger.Test
{
    /// <summary>
    ///  Class for testing TimesheetsApi
    /// </summary>
    /// <remarks>
    /// This file is automatically generated by Swagger Codegen.
    /// Please update the test case below to test the API endpoint.
    /// </remarks>
    [TestFixture]
    public class TimesheetsApiTests
    {
        private TimesheetsApi instance;

        /// <summary>
        /// Setup before each unit test
        /// </summary>
        [SetUp]
        public void Init()
        {
            instance = new TimesheetsApi();
        }

        /// <summary>
        /// Clean up after each unit test
        /// </summary>
        [TearDown]
        public void Cleanup()
        {

        }

        /// <summary>
        /// Test an instance of TimesheetsApi
        /// </summary>
        [Test]
        public void InstanceTest()
        {
            // TODO uncomment below to test 'IsInstanceOfType' TimesheetsApi
            //Assert.IsInstanceOfType(typeof(TimesheetsApi), instance, "instance is a TimesheetsApi");
        }

        
        /// <summary>
        /// Test ManageTimesheetsTimeEntriesDelete
        /// </summary>
        [Test]
        public void ManageTimesheetsTimeEntriesDeleteTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //string timeEntryID = null;
            //instance.ManageTimesheetsTimeEntriesDelete(timeEntryID);
            
        }
        
        /// <summary>
        /// Test ManageTimesheetsTimeEntriesGet
        /// </summary>
        [Test]
        public void ManageTimesheetsTimeEntriesGetTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //List<string> ID = null;
            //List<string> jobID = null;
            //List<string> taskID = null;
            //List<string> userID = null;
            //string startDate = null;
            //string endDate = null;
            //List<string> entryDate = null;
            //int? limit = null;
            //int? offset = null;
            //var response = instance.ManageTimesheetsTimeEntriesGet(ID, jobID, taskID, userID, startDate, endDate, entryDate, limit, offset);
            //Assert.IsInstanceOf<ResponseBodyListGetTimeEntryModelManage> (response, "response is ResponseBodyListGetTimeEntryModelManage");
        }
        
        /// <summary>
        /// Test ManageTimesheetsTimeEntriesGetByID
        /// </summary>
        [Test]
        public void ManageTimesheetsTimeEntriesGetByIDTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //string timeEntryID = null;
            //var response = instance.ManageTimesheetsTimeEntriesGetByID(timeEntryID);
            //Assert.IsInstanceOf<ResponseBodyGetTimeEntryModelManage> (response, "response is ResponseBodyGetTimeEntryModelManage");
        }
        
        /// <summary>
        /// Test ManageTimesheetsTimeEntriesPatch
        /// </summary>
        [Test]
        public void ManageTimesheetsTimeEntriesPatchTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //string timeEntryID = null;
            //PatchTimeEntryModelManage model = null;
            //var response = instance.ManageTimesheetsTimeEntriesPatch(timeEntryID, model);
            //Assert.IsInstanceOf<ResponseBodyGetTimeEntryModelManage> (response, "response is ResponseBodyGetTimeEntryModelManage");
        }
        
        /// <summary>
        /// Test ManageTimesheetsTimeEntriesPost
        /// </summary>
        [Test]
        public void ManageTimesheetsTimeEntriesPostTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //PostTimeEntryModelManage model = null;
            //var response = instance.ManageTimesheetsTimeEntriesPost(model);
            //Assert.IsInstanceOf<ResponseBodyGetTimeEntryModelManage> (response, "response is ResponseBodyGetTimeEntryModelManage");
        }
        
        /// <summary>
        /// Test TimesheetsGet
        /// </summary>
        [Test]
        public void TimesheetsGetTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //List<string> status = null;
            //List<string> userID = null;
            //List<string> divisionID = null;
            //List<string> employmentTypeID = null;
            //string fromDate = null;
            //string toDate = null;
            //int? limit = null;
            //int? offset = null;
            //var response = instance.TimesheetsGet(status, userID, divisionID, employmentTypeID, fromDate, toDate, limit, offset);
            //Assert.IsInstanceOf<ResponseBodyListGetTimesheetModelRoot> (response, "response is ResponseBodyListGetTimesheetModelRoot");
        }
        
        /// <summary>
        /// Test TimesheetsGetByID
        /// </summary>
        [Test]
        public void TimesheetsGetByIDTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //string timesheetID = null;
            //var response = instance.TimesheetsGetByID(timesheetID);
            //Assert.IsInstanceOf<ResponseBodyGetTimesheetModelRoot> (response, "response is ResponseBodyGetTimesheetModelRoot");
        }
        
        /// <summary>
        /// Test TimesheetsGetTimesheetsTimeEntries
        /// </summary>
        [Test]
        public void TimesheetsGetTimesheetsTimeEntriesTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //string timesheetID = null;
            //List<string> jobID = null;
            //var response = instance.TimesheetsGetTimesheetsTimeEntries(timesheetID, jobID);
            //Assert.IsInstanceOf<ResponseBodyListGetTimeEntryModelRoot> (response, "response is ResponseBodyListGetTimeEntryModelRoot");
        }
        
        /// <summary>
        /// Test TimesheetsHistoryGetManagedTimesheetHistory
        /// </summary>
        [Test]
        public void TimesheetsHistoryGetManagedTimesheetHistoryTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //string timesheetID = null;
            //var response = instance.TimesheetsHistoryGetManagedTimesheetHistory(timesheetID);
            //Assert.IsInstanceOf<ResponseBodyListGetTimesheetHistoryModelManage> (response, "response is ResponseBodyListGetTimesheetHistoryModelManage");
        }
        
        /// <summary>
        /// Test TimesheetsHistoryGetMyTimesheetHistory
        /// </summary>
        [Test]
        public void TimesheetsHistoryGetMyTimesheetHistoryTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //string timesheetID = null;
            //var response = instance.TimesheetsHistoryGetMyTimesheetHistory(timesheetID);
            //Assert.IsInstanceOf<ResponseBodyListGetTimesheetHistoryModelMe> (response, "response is ResponseBodyListGetTimesheetHistoryModelMe");
        }
        
        /// <summary>
        /// Test TimesheetsHistoryGetTimesheetHistory
        /// </summary>
        [Test]
        public void TimesheetsHistoryGetTimesheetHistoryTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //string timesheetID = null;
            //var response = instance.TimesheetsHistoryGetTimesheetHistory(timesheetID);
            //Assert.IsInstanceOf<ResponseBodyListGetTimesheetHistoryModelRoot> (response, "response is ResponseBodyListGetTimesheetHistoryModelRoot");
        }
        
        /// <summary>
        /// Test TimesheetsManageTimesheetsActionsGET
        /// </summary>
        [Test]
        public void TimesheetsManageTimesheetsActionsGETTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //string timesheetID = null;
            //var response = instance.TimesheetsManageTimesheetsActionsGET(timesheetID);
            //Assert.IsInstanceOf<ResponseBodyListGetActionModelTimesheetActionManage> (response, "response is ResponseBodyListGetActionModelTimesheetActionManage");
        }
        
        /// <summary>
        /// Test TimesheetsManageTimesheetsActionsPOST
        /// </summary>
        [Test]
        public void TimesheetsManageTimesheetsActionsPOSTTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //string timesheetID = null;
            //PostTimesheetActionModelManage model = null;
            //var response = instance.TimesheetsManageTimesheetsActionsPOST(timesheetID, model);
            //Assert.IsInstanceOf<ResponseBodyGetTimesheetModelManage> (response, "response is ResponseBodyGetTimesheetModelManage");
        }
        
        /// <summary>
        /// Test TimesheetsManageTimesheetsDateGETENTITY
        /// </summary>
        [Test]
        public void TimesheetsManageTimesheetsDateGETENTITYTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //DateTime? date = null;
            //string userID = null;
            //var response = instance.TimesheetsManageTimesheetsDateGETENTITY(date, userID);
            //Assert.IsInstanceOf<ResponseBodyGetTimesheetModelRoot> (response, "response is ResponseBodyGetTimesheetModelRoot");
        }
        
        /// <summary>
        /// Test TimesheetsManageTimesheetsGET
        /// </summary>
        [Test]
        public void TimesheetsManageTimesheetsGETTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //List<string> status = null;
            //List<string> userID = null;
            //List<string> divisionID = null;
            //List<string> employmentTypeID = null;
            //string fromDate = null;
            //string toDate = null;
            //int? limit = null;
            //int? offset = null;
            //var response = instance.TimesheetsManageTimesheetsGET(status, userID, divisionID, employmentTypeID, fromDate, toDate, limit, offset);
            //Assert.IsInstanceOf<ResponseBodyListGetTimesheetModelManage> (response, "response is ResponseBodyListGetTimesheetModelManage");
        }
        
        /// <summary>
        /// Test TimesheetsManageTimesheetsGETENTITY
        /// </summary>
        [Test]
        public void TimesheetsManageTimesheetsGETENTITYTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //string timesheetID = null;
            //var response = instance.TimesheetsManageTimesheetsGETENTITY(timesheetID);
            //Assert.IsInstanceOf<ResponseBodyGetTimesheetModelManage> (response, "response is ResponseBodyGetTimesheetModelManage");
        }
        
        /// <summary>
        /// Test TimesheetsMeTimesheetsActionsGET
        /// </summary>
        [Test]
        public void TimesheetsMeTimesheetsActionsGETTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //string timesheetID = null;
            //var response = instance.TimesheetsMeTimesheetsActionsGET(timesheetID);
            //Assert.IsInstanceOf<ResponseBodyListGetActionModelTimesheetActionMe> (response, "response is ResponseBodyListGetActionModelTimesheetActionMe");
        }
        
        /// <summary>
        /// Test TimesheetsMeTimesheetsActionsPOST
        /// </summary>
        [Test]
        public void TimesheetsMeTimesheetsActionsPOSTTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //string timesheetID = null;
            //PostTimesheetActionModelMe model = null;
            //var response = instance.TimesheetsMeTimesheetsActionsPOST(timesheetID, model);
            //Assert.IsInstanceOf<ResponseBodyGetTimesheetModelMe> (response, "response is ResponseBodyGetTimesheetModelMe");
        }
        
        /// <summary>
        /// Test TimesheetsMeTimesheetsDateActionsGET
        /// </summary>
        [Test]
        public void TimesheetsMeTimesheetsDateActionsGETTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //DateTime? date = null;
            //var response = instance.TimesheetsMeTimesheetsDateActionsGET(date);
            //Assert.IsInstanceOf<ResponseBodyListGetActionModelTimesheetActionMe> (response, "response is ResponseBodyListGetActionModelTimesheetActionMe");
        }
        
        /// <summary>
        /// Test TimesheetsMeTimesheetsDateActionsPOST
        /// </summary>
        [Test]
        public void TimesheetsMeTimesheetsDateActionsPOSTTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //DateTime? date = null;
            //PostTimesheetActionModelMe model = null;
            //var response = instance.TimesheetsMeTimesheetsDateActionsPOST(date, model);
            //Assert.IsInstanceOf<ResponseBodyGetTimesheetModelMe> (response, "response is ResponseBodyGetTimesheetModelMe");
        }
        
        /// <summary>
        /// Test TimesheetsMeTimesheetsDateGETENTITY
        /// </summary>
        [Test]
        public void TimesheetsMeTimesheetsDateGETENTITYTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //DateTime? date = null;
            //var response = instance.TimesheetsMeTimesheetsDateGETENTITY(date);
            //Assert.IsInstanceOf<ResponseBodyGetTimesheetModelMe> (response, "response is ResponseBodyGetTimesheetModelMe");
        }
        
        /// <summary>
        /// Test TimesheetsMeTimesheetsGET
        /// </summary>
        [Test]
        public void TimesheetsMeTimesheetsGETTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //List<string> status = null;
            //string fromDate = null;
            //string toDate = null;
            //int? limit = null;
            //int? offset = null;
            //var response = instance.TimesheetsMeTimesheetsGET(status, fromDate, toDate, limit, offset);
            //Assert.IsInstanceOf<ResponseBodyListGetTimesheetModelMe> (response, "response is ResponseBodyListGetTimesheetModelMe");
        }
        
        /// <summary>
        /// Test TimesheetsMeTimesheetsGETENTITY
        /// </summary>
        [Test]
        public void TimesheetsMeTimesheetsGETENTITYTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //string timesheetID = null;
            //var response = instance.TimesheetsMeTimesheetsGETENTITY(timesheetID);
            //Assert.IsInstanceOf<ResponseBodyGetTimesheetModelMe> (response, "response is ResponseBodyGetTimesheetModelMe");
        }
        
        /// <summary>
        /// Test TimesheetsTimeOffDeleteManagedTimesheetsTimeOffEntry
        /// </summary>
        [Test]
        public void TimesheetsTimeOffDeleteManagedTimesheetsTimeOffEntryTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //string timeOffID = null;
            //instance.TimesheetsTimeOffDeleteManagedTimesheetsTimeOffEntry(timeOffID);
            
        }
        
        /// <summary>
        /// Test TimesheetsTimeOffGetManagedTimesheetTimeOffEntries
        /// </summary>
        [Test]
        public void TimesheetsTimeOffGetManagedTimesheetTimeOffEntriesTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //string timesheetID = null;
            //List<string> ID = null;
            //List<string> timeOffTypeID = null;
            //int? limit = null;
            //int? offset = null;
            //var response = instance.TimesheetsTimeOffGetManagedTimesheetTimeOffEntries(timesheetID, ID, timeOffTypeID, limit, offset);
            //Assert.IsInstanceOf<ResponseBodyListGetTimeOffModelRoot> (response, "response is ResponseBodyListGetTimeOffModelRoot");
        }
        
        /// <summary>
        /// Test TimesheetsTimeOffGetManagedTimesheetTimeOffEntry
        /// </summary>
        [Test]
        public void TimesheetsTimeOffGetManagedTimesheetTimeOffEntryTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //string timesheetID = null;
            //string timeOffID = null;
            //var response = instance.TimesheetsTimeOffGetManagedTimesheetTimeOffEntry(timesheetID, timeOffID);
            //Assert.IsInstanceOf<ResponseBodyGetTimeOffModelRoot> (response, "response is ResponseBodyGetTimeOffModelRoot");
        }
        
        /// <summary>
        /// Test TimesheetsTimeOffGetManagedTimesheetsTimeOffEntries
        /// </summary>
        [Test]
        public void TimesheetsTimeOffGetManagedTimesheetsTimeOffEntriesTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //List<string> ID = null;
            //List<string> timeOffTypeID = null;
            //List<string> userID = null;
            //string fromDate = null;
            //string toDate = null;
            //int? limit = null;
            //int? offset = null;
            //var response = instance.TimesheetsTimeOffGetManagedTimesheetsTimeOffEntries(ID, timeOffTypeID, userID, fromDate, toDate, limit, offset);
            //Assert.IsInstanceOf<ResponseBodyListGetTimeOffModelRoot> (response, "response is ResponseBodyListGetTimeOffModelRoot");
        }
        
        /// <summary>
        /// Test TimesheetsTimeOffGetManagedTimesheetsTimeOffEntry
        /// </summary>
        [Test]
        public void TimesheetsTimeOffGetManagedTimesheetsTimeOffEntryTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //string timeOffID = null;
            //var response = instance.TimesheetsTimeOffGetManagedTimesheetsTimeOffEntry(timeOffID);
            //Assert.IsInstanceOf<ResponseBodyGetTimeOffModelRoot> (response, "response is ResponseBodyGetTimeOffModelRoot");
        }
        
        /// <summary>
        /// Test TimesheetsTimeOffGetTimesheetsTimeOff
        /// </summary>
        [Test]
        public void TimesheetsTimeOffGetTimesheetsTimeOffTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //string timesheetID = null;
            //List<string> ID = null;
            //List<string> timeOffTypeID = null;
            //int? limit = null;
            //int? offset = null;
            //var response = instance.TimesheetsTimeOffGetTimesheetsTimeOff(timesheetID, ID, timeOffTypeID, limit, offset);
            //Assert.IsInstanceOf<ResponseBodyListGetTimeOffModelRoot> (response, "response is ResponseBodyListGetTimeOffModelRoot");
        }
        
        /// <summary>
        /// Test TimesheetsTimeOffPatchManagedTimesheetsTimeOffEntry
        /// </summary>
        [Test]
        public void TimesheetsTimeOffPatchManagedTimesheetsTimeOffEntryTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //string timeOffID = null;
            //PatchTimeOffModelRoot model = null;
            //var response = instance.TimesheetsTimeOffPatchManagedTimesheetsTimeOffEntry(timeOffID, model);
            //Assert.IsInstanceOf<ResponseBodyGetTimeOffModelRoot> (response, "response is ResponseBodyGetTimeOffModelRoot");
        }
        
        /// <summary>
        /// Test TimesheetsTimeOffPostManagedTimesheetsTimeOffEntry
        /// </summary>
        [Test]
        public void TimesheetsTimeOffPostManagedTimesheetsTimeOffEntryTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //PostTimeOffModelRoot model = null;
            //var response = instance.TimesheetsTimeOffPostManagedTimesheetsTimeOffEntry(model);
            //Assert.IsInstanceOf<ResponseBodyGetTimeOffModelRoot> (response, "response is ResponseBodyGetTimeOffModelRoot");
        }
        
        /// <summary>
        /// Test TimesheetsTimesheetsActionsGET
        /// </summary>
        [Test]
        public void TimesheetsTimesheetsActionsGETTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //string timesheetID = null;
            //var response = instance.TimesheetsTimesheetsActionsGET(timesheetID);
            //Assert.IsInstanceOf<ResponseBodyListGetActionModelTimesheetActionRoot> (response, "response is ResponseBodyListGetActionModelTimesheetActionRoot");
        }
        
        /// <summary>
        /// Test TimesheetsTimesheetsActionsPOST
        /// </summary>
        [Test]
        public void TimesheetsTimesheetsActionsPOSTTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //string timesheetID = null;
            //PostTimesheetActionModelRoot model = null;
            //var response = instance.TimesheetsTimesheetsActionsPOST(timesheetID, model);
            //Assert.IsInstanceOf<ResponseBodyGetTimesheetModelRoot> (response, "response is ResponseBodyGetTimesheetModelRoot");
        }
        
        /// <summary>
        /// Test TimesheetsTimesheetsDateGETENTITY
        /// </summary>
        [Test]
        public void TimesheetsTimesheetsDateGETENTITYTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //DateTime? date = null;
            //string userID = null;
            //var response = instance.TimesheetsTimesheetsDateGETENTITY(date, userID);
            //Assert.IsInstanceOf<ResponseBodyGetTimesheetModelRoot> (response, "response is ResponseBodyGetTimesheetModelRoot");
        }
        
    }

}
