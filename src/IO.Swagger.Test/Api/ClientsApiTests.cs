/* 
 * REST API v2
 *
 * <p>This REST API offers programmatic access to common ClickTime functions. You can review the available endpoints by clicking on the category names below. Click on an endpoint itself to further drill down and review the available filters, usage requirements, response examples and the respective model.</p><p>Review the <a href=\"https://support.clicktime.com/hc/en-us/articles/360002884071\">REST API v2 General Information</a> to get an overview of how to use the API including usable examples of authentication and making requests.</p>
 *
 * OpenAPI spec version: v2
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

using System;
using System.IO;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using RestSharp;
using NUnit.Framework;

using IO.Swagger.Client;
using IO.Swagger.Api;
using IO.Swagger.Model;

namespace IO.Swagger.Test
{
    /// <summary>
    ///  Class for testing ClientsApi
    /// </summary>
    /// <remarks>
    /// This file is automatically generated by Swagger Codegen.
    /// Please update the test case below to test the API endpoint.
    /// </remarks>
    [TestFixture]
    public class ClientsApiTests
    {
        private ClientsApi instance;

        /// <summary>
        /// Setup before each unit test
        /// </summary>
        [SetUp]
        public void Init()
        {
            instance = new ClientsApi();
        }

        /// <summary>
        /// Clean up after each unit test
        /// </summary>
        [TearDown]
        public void Cleanup()
        {

        }

        /// <summary>
        /// Test an instance of ClientsApi
        /// </summary>
        [Test]
        public void InstanceTest()
        {
            // TODO uncomment below to test 'IsInstanceOfType' ClientsApi
            //Assert.IsInstanceOfType(typeof(ClientsApi), instance, "instance is a ClientsApi");
        }

        
        /// <summary>
        /// Test ClientsDelete
        /// </summary>
        [Test]
        public void ClientsDeleteTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //string clientID = null;
            //instance.ClientsDelete(clientID);
            
        }
        
        /// <summary>
        /// Test ClientsGETClientsBillingRates
        /// </summary>
        [Test]
        public void ClientsGETClientsBillingRatesTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //string clientID = null;
            //List<string> ID = null;
            //var response = instance.ClientsGETClientsBillingRates(clientID, ID);
            //Assert.IsInstanceOf<ResponseBodyListGetBillingRateModelRoot> (response, "response is ResponseBodyListGetBillingRateModelRoot");
        }
        
        /// <summary>
        /// Test ClientsGETClientsBillingRatesEntity
        /// </summary>
        [Test]
        public void ClientsGETClientsBillingRatesEntityTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //string clientID = null;
            //string billingRateID = null;
            //var response = instance.ClientsGETClientsBillingRatesEntity(clientID, billingRateID);
            //Assert.IsInstanceOf<ResponseBodyGetBillingRateModelRoot> (response, "response is ResponseBodyGetBillingRateModelRoot");
        }
        
        /// <summary>
        /// Test ClientsGETCustomFieldDefinitionByID
        /// </summary>
        [Test]
        public void ClientsGETCustomFieldDefinitionByIDTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //string customFieldDefinitionID = null;
            //var response = instance.ClientsGETCustomFieldDefinitionByID(customFieldDefinitionID);
            //Assert.IsInstanceOf<ResponseBodyGetCustomFieldDefinitionModelRoot> (response, "response is ResponseBodyGetCustomFieldDefinitionModelRoot");
        }
        
        /// <summary>
        /// Test ClientsGETCustomFieldDefinitions
        /// </summary>
        [Test]
        public void ClientsGETCustomFieldDefinitionsTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //int? limit = null;
            //int? offset = null;
            //var response = instance.ClientsGETCustomFieldDefinitions(limit, offset);
            //Assert.IsInstanceOf<ResponseBodyListGetCustomFieldDefinitionModelRoot> (response, "response is ResponseBodyListGetCustomFieldDefinitionModelRoot");
        }
        
        /// <summary>
        /// Test ClientsGet
        /// </summary>
        [Test]
        public void ClientsGetTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //List<string> ID = null;
            //List<bool?> isActive = null;
            //List<string> name = null;
            //List<string> shortName = null;
            //List<string> clientNumber = null;
            //int? limit = null;
            //int? offset = null;
            //var response = instance.ClientsGet(ID, isActive, name, shortName, clientNumber, limit, offset);
            //Assert.IsInstanceOf<ResponseBodyListGetClientModelRoot> (response, "response is ResponseBodyListGetClientModelRoot");
        }
        
        /// <summary>
        /// Test ClientsGetByID
        /// </summary>
        [Test]
        public void ClientsGetByIDTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //string clientID = null;
            //var response = instance.ClientsGetByID(clientID);
            //Assert.IsInstanceOf<ResponseBodyGetClientModelRoot> (response, "response is ResponseBodyGetClientModelRoot");
        }
        
        /// <summary>
        /// Test ClientsMeClientsGET
        /// </summary>
        [Test]
        public void ClientsMeClientsGETTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //List<string> ID = null;
            //List<bool?> isActive = null;
            //List<string> name = null;
            //List<string> shortName = null;
            //List<string> clientNumber = null;
            //int? limit = null;
            //int? offset = null;
            //var response = instance.ClientsMeClientsGET(ID, isActive, name, shortName, clientNumber, limit, offset);
            //Assert.IsInstanceOf<ResponseBodyListGetClientModelMe> (response, "response is ResponseBodyListGetClientModelMe");
        }
        
        /// <summary>
        /// Test ClientsMeClientsGetEntity
        /// </summary>
        [Test]
        public void ClientsMeClientsGetEntityTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //string clientID = null;
            //var response = instance.ClientsMeClientsGetEntity(clientID);
            //Assert.IsInstanceOf<ResponseBodyGetClientModelMe> (response, "response is ResponseBodyGetClientModelMe");
        }
        
        /// <summary>
        /// Test ClientsNew
        /// </summary>
        [Test]
        public void ClientsNewTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //var response = instance.ClientsNew();
            //Assert.IsInstanceOf<ResponseBodyGetClientModelNew> (response, "response is ResponseBodyGetClientModelNew");
        }
        
        /// <summary>
        /// Test ClientsPATCHClientsBillingRatesEntity
        /// </summary>
        [Test]
        public void ClientsPATCHClientsBillingRatesEntityTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //string clientID = null;
            //string billingRateID = null;
            //PatchBillingRateModelRoot model = null;
            //var response = instance.ClientsPATCHClientsBillingRatesEntity(clientID, billingRateID, model);
            //Assert.IsInstanceOf<ResponseBodyGetBillingRateModelRoot> (response, "response is ResponseBodyGetBillingRateModelRoot");
        }
        
        /// <summary>
        /// Test ClientsPatch
        /// </summary>
        [Test]
        public void ClientsPatchTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //string clientID = null;
            //PatchClientModelRoot model = null;
            //var response = instance.ClientsPatch(clientID, model);
            //Assert.IsInstanceOf<ResponseBodyGetClientModelRoot> (response, "response is ResponseBodyGetClientModelRoot");
        }
        
        /// <summary>
        /// Test ClientsPost
        /// </summary>
        [Test]
        public void ClientsPostTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //PostClientModelRoot model = null;
            //var response = instance.ClientsPost(model);
            //Assert.IsInstanceOf<ResponseBodyGetClientModelRoot> (response, "response is ResponseBodyGetClientModelRoot");
        }
        
    }

}
