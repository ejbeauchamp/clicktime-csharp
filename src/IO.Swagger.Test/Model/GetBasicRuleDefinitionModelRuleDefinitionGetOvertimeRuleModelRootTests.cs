/* 
 * REST API v2
 *
 * <p>This REST API offers programmatic access to common ClickTime functions. You can review the available endpoints by clicking on the category names below. Click on an endpoint itself to further drill down and review the available filters, usage requirements, response examples and the respective model.</p><p>Review the <a href=\"https://support.clicktime.com/hc/en-us/articles/360002884071\">REST API v2 General Information</a> to get an overview of how to use the API including usable examples of authentication and making requests.</p>
 *
 * OpenAPI spec version: v2
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */


using NUnit.Framework;

using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using IO.Swagger.Api;
using IO.Swagger.Model;
using IO.Swagger.Client;
using System.Reflection;
using Newtonsoft.Json;

namespace IO.Swagger.Test
{
    /// <summary>
    ///  Class for testing GetBasicRuleDefinitionModelRuleDefinitionGetOvertimeRuleModelRoot
    /// </summary>
    /// <remarks>
    /// This file is automatically generated by Swagger Codegen.
    /// Please update the test case below to test the model.
    /// </remarks>
    [TestFixture]
    public class GetBasicRuleDefinitionModelRuleDefinitionGetOvertimeRuleModelRootTests
    {
        // TODO uncomment below to declare an instance variable for GetBasicRuleDefinitionModelRuleDefinitionGetOvertimeRuleModelRoot
        //private GetBasicRuleDefinitionModelRuleDefinitionGetOvertimeRuleModelRoot instance;

        /// <summary>
        /// Setup before each test
        /// </summary>
        [SetUp]
        public void Init()
        {
            // TODO uncomment below to create an instance of GetBasicRuleDefinitionModelRuleDefinitionGetOvertimeRuleModelRoot
            //instance = new GetBasicRuleDefinitionModelRuleDefinitionGetOvertimeRuleModelRoot();
        }

        /// <summary>
        /// Clean up after each test
        /// </summary>
        [TearDown]
        public void Cleanup()
        {

        }

        /// <summary>
        /// Test an instance of GetBasicRuleDefinitionModelRuleDefinitionGetOvertimeRuleModelRoot
        /// </summary>
        [Test]
        public void GetBasicRuleDefinitionModelRuleDefinitionGetOvertimeRuleModelRootInstanceTest()
        {
            // TODO uncomment below to test "IsInstanceOfType" GetBasicRuleDefinitionModelRuleDefinitionGetOvertimeRuleModelRoot
            //Assert.IsInstanceOfType<GetBasicRuleDefinitionModelRuleDefinitionGetOvertimeRuleModelRoot> (instance, "variable 'instance' is a GetBasicRuleDefinitionModelRuleDefinitionGetOvertimeRuleModelRoot");
        }


        /// <summary>
        /// Test the property 'Multiplier'
        /// </summary>
        [Test]
        public void MultiplierTest()
        {
            // TODO unit test for the property 'Multiplier'
        }
        /// <summary>
        /// Test the property 'PayCode'
        /// </summary>
        [Test]
        public void PayCodeTest()
        {
            // TODO unit test for the property 'PayCode'
        }
        /// <summary>
        /// Test the property 'RuleType'
        /// </summary>
        [Test]
        public void RuleTypeTest()
        {
            // TODO unit test for the property 'RuleType'
        }
        /// <summary>
        /// Test the property 'Threshold'
        /// </summary>
        [Test]
        public void ThresholdTest()
        {
            // TODO unit test for the property 'Threshold'
        }
        /// <summary>
        /// Test the property 'TimeSpan'
        /// </summary>
        [Test]
        public void TimeSpanTest()
        {
            // TODO unit test for the property 'TimeSpan'
        }

    }

}
