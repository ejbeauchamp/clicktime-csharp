/* 
 * REST API v2
 *
 * <p>This REST API offers programmatic access to common ClickTime functions. You can review the available endpoints by clicking on the category names below. Click on an endpoint itself to further drill down and review the available filters, usage requirements, response examples and the respective model.</p><p>Review the <a href=\"https://support.clicktime.com/hc/en-us/articles/360002884071\">REST API v2 General Information</a> to get an overview of how to use the API including usable examples of authentication and making requests.</p>
 *
 * OpenAPI spec version: v2
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */


using NUnit.Framework;

using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using IO.Swagger.Api;
using IO.Swagger.Model;
using IO.Swagger.Client;
using System.Reflection;
using Newtonsoft.Json;

namespace IO.Swagger.Test
{
    /// <summary>
    ///  Class for testing GetTimeOffRequestHistoryModel
    /// </summary>
    /// <remarks>
    /// This file is automatically generated by Swagger Codegen.
    /// Please update the test case below to test the model.
    /// </remarks>
    [TestFixture]
    public class GetTimeOffRequestHistoryModelTests
    {
        // TODO uncomment below to declare an instance variable for GetTimeOffRequestHistoryModel
        //private GetTimeOffRequestHistoryModel instance;

        /// <summary>
        /// Setup before each test
        /// </summary>
        [SetUp]
        public void Init()
        {
            // TODO uncomment below to create an instance of GetTimeOffRequestHistoryModel
            //instance = new GetTimeOffRequestHistoryModel();
        }

        /// <summary>
        /// Clean up after each test
        /// </summary>
        [TearDown]
        public void Cleanup()
        {

        }

        /// <summary>
        /// Test an instance of GetTimeOffRequestHistoryModel
        /// </summary>
        [Test]
        public void GetTimeOffRequestHistoryModelInstanceTest()
        {
            // TODO uncomment below to test "IsInstanceOfType" GetTimeOffRequestHistoryModel
            //Assert.IsInstanceOfType<GetTimeOffRequestHistoryModel> (instance, "variable 'instance' is a GetTimeOffRequestHistoryModel");
        }


        /// <summary>
        /// Test the property 'ActionByUserID'
        /// </summary>
        [Test]
        public void ActionByUserIDTest()
        {
            // TODO unit test for the property 'ActionByUserID'
        }
        /// <summary>
        /// Test the property 'ActionByUserName'
        /// </summary>
        [Test]
        public void ActionByUserNameTest()
        {
            // TODO unit test for the property 'ActionByUserName'
        }
        /// <summary>
        /// Test the property 'Comment'
        /// </summary>
        [Test]
        public void CommentTest()
        {
            // TODO unit test for the property 'Comment'
        }
        /// <summary>
        /// Test the property 'Date'
        /// </summary>
        [Test]
        public void DateTest()
        {
            // TODO unit test for the property 'Date'
        }
        /// <summary>
        /// Test the property 'LegacyID'
        /// </summary>
        [Test]
        public void LegacyIDTest()
        {
            // TODO unit test for the property 'LegacyID'
        }
        /// <summary>
        /// Test the property 'Status'
        /// </summary>
        [Test]
        public void StatusTest()
        {
            // TODO unit test for the property 'Status'
        }

    }

}
