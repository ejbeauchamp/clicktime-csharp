/* 
 * REST API v2
 *
 * <p>This REST API offers programmatic access to common ClickTime functions. You can review the available endpoints by clicking on the category names below. Click on an endpoint itself to further drill down and review the available filters, usage requirements, response examples and the respective model.</p><p>Review the <a href=\"https://support.clicktime.com/hc/en-us/articles/360002884071\">REST API v2 General Information</a> to get an overview of how to use the API including usable examples of authentication and making requests.</p>
 *
 * OpenAPI spec version: v2
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */


using NUnit.Framework;

using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using IO.Swagger.Api;
using IO.Swagger.Model;
using IO.Swagger.Client;
using System.Reflection;
using Newtonsoft.Json;

namespace IO.Swagger.Test
{
    /// <summary>
    ///  Class for testing PostTimerModel
    /// </summary>
    /// <remarks>
    /// This file is automatically generated by Swagger Codegen.
    /// Please update the test case below to test the model.
    /// </remarks>
    [TestFixture]
    public class PostTimerModelTests
    {
        // TODO uncomment below to declare an instance variable for PostTimerModel
        //private PostTimerModel instance;

        /// <summary>
        /// Setup before each test
        /// </summary>
        [SetUp]
        public void Init()
        {
            // TODO uncomment below to create an instance of PostTimerModel
            //instance = new PostTimerModel();
        }

        /// <summary>
        /// Clean up after each test
        /// </summary>
        [TearDown]
        public void Cleanup()
        {

        }

        /// <summary>
        /// Test an instance of PostTimerModel
        /// </summary>
        [Test]
        public void PostTimerModelInstanceTest()
        {
            // TODO uncomment below to test "IsInstanceOfType" PostTimerModel
            //Assert.IsInstanceOfType<PostTimerModel> (instance, "variable 'instance' is a PostTimerModel");
        }


        /// <summary>
        /// Test the property 'Intervals'
        /// </summary>
        [Test]
        public void IntervalsTest()
        {
            // TODO unit test for the property 'Intervals'
        }
        /// <summary>
        /// Test the property 'TimeEntryID'
        /// </summary>
        [Test]
        public void TimeEntryIDTest()
        {
            // TODO unit test for the property 'TimeEntryID'
        }

    }

}
