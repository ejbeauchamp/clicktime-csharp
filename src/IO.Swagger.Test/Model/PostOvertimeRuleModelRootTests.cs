/* 
 * REST API v2
 *
 * <p>This REST API offers programmatic access to common ClickTime functions. You can review the available endpoints by clicking on the category names below. Click on an endpoint itself to further drill down and review the available filters, usage requirements, response examples and the respective model.</p><p>Review the <a href=\"https://support.clicktime.com/hc/en-us/articles/360002884071\">REST API v2 General Information</a> to get an overview of how to use the API including usable examples of authentication and making requests.</p>
 *
 * OpenAPI spec version: v2
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */


using NUnit.Framework;

using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using IO.Swagger.Api;
using IO.Swagger.Model;
using IO.Swagger.Client;
using System.Reflection;
using Newtonsoft.Json;

namespace IO.Swagger.Test
{
    /// <summary>
    ///  Class for testing PostOvertimeRuleModelRoot
    /// </summary>
    /// <remarks>
    /// This file is automatically generated by Swagger Codegen.
    /// Please update the test case below to test the model.
    /// </remarks>
    [TestFixture]
    public class PostOvertimeRuleModelRootTests
    {
        // TODO uncomment below to declare an instance variable for PostOvertimeRuleModelRoot
        //private PostOvertimeRuleModelRoot instance;

        /// <summary>
        /// Setup before each test
        /// </summary>
        [SetUp]
        public void Init()
        {
            // TODO uncomment below to create an instance of PostOvertimeRuleModelRoot
            //instance = new PostOvertimeRuleModelRoot();
        }

        /// <summary>
        /// Clean up after each test
        /// </summary>
        [TearDown]
        public void Cleanup()
        {

        }

        /// <summary>
        /// Test an instance of PostOvertimeRuleModelRoot
        /// </summary>
        [Test]
        public void PostOvertimeRuleModelRootInstanceTest()
        {
            // TODO uncomment below to test "IsInstanceOfType" PostOvertimeRuleModelRoot
            //Assert.IsInstanceOfType<PostOvertimeRuleModelRoot> (instance, "variable 'instance' is a PostOvertimeRuleModelRoot");
        }


        /// <summary>
        /// Test the property 'RuleDefinition'
        /// </summary>
        [Test]
        public void RuleDefinitionTest()
        {
            // TODO unit test for the property 'RuleDefinition'
        }

    }

}
