/* 
 * REST API v2
 *
 * <p>This REST API offers programmatic access to common ClickTime functions. You can review the available endpoints by clicking on the category names below. Click on an endpoint itself to further drill down and review the available filters, usage requirements, response examples and the respective model.</p><p>Review the <a href=\"https://support.clicktime.com/hc/en-us/articles/360002884071\">REST API v2 General Information</a> to get an overview of how to use the API including usable examples of authentication and making requests.</p>
 *
 * OpenAPI spec version: v2
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */


using NUnit.Framework;

using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using IO.Swagger.Api;
using IO.Swagger.Model;
using IO.Swagger.Client;
using System.Reflection;
using Newtonsoft.Json;

namespace IO.Swagger.Test
{
    /// <summary>
    ///  Class for testing PostTimeOffModelMe
    /// </summary>
    /// <remarks>
    /// This file is automatically generated by Swagger Codegen.
    /// Please update the test case below to test the model.
    /// </remarks>
    [TestFixture]
    public class PostTimeOffModelMeTests
    {
        // TODO uncomment below to declare an instance variable for PostTimeOffModelMe
        //private PostTimeOffModelMe instance;

        /// <summary>
        /// Setup before each test
        /// </summary>
        [SetUp]
        public void Init()
        {
            // TODO uncomment below to create an instance of PostTimeOffModelMe
            //instance = new PostTimeOffModelMe();
        }

        /// <summary>
        /// Clean up after each test
        /// </summary>
        [TearDown]
        public void Cleanup()
        {

        }

        /// <summary>
        /// Test an instance of PostTimeOffModelMe
        /// </summary>
        [Test]
        public void PostTimeOffModelMeInstanceTest()
        {
            // TODO uncomment below to test "IsInstanceOfType" PostTimeOffModelMe
            //Assert.IsInstanceOfType<PostTimeOffModelMe> (instance, "variable 'instance' is a PostTimeOffModelMe");
        }


        /// <summary>
        /// Test the property 'Date'
        /// </summary>
        [Test]
        public void DateTest()
        {
            // TODO unit test for the property 'Date'
        }
        /// <summary>
        /// Test the property 'Hours'
        /// </summary>
        [Test]
        public void HoursTest()
        {
            // TODO unit test for the property 'Hours'
        }
        /// <summary>
        /// Test the property 'Notes'
        /// </summary>
        [Test]
        public void NotesTest()
        {
            // TODO unit test for the property 'Notes'
        }
        /// <summary>
        /// Test the property 'TimeOffTypeID'
        /// </summary>
        [Test]
        public void TimeOffTypeIDTest()
        {
            // TODO unit test for the property 'TimeOffTypeID'
        }

    }

}
