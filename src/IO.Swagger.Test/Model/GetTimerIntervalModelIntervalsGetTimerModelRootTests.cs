/* 
 * REST API v2
 *
 * <p>This REST API offers programmatic access to common ClickTime functions. You can review the available endpoints by clicking on the category names below. Click on an endpoint itself to further drill down and review the available filters, usage requirements, response examples and the respective model.</p><p>Review the <a href=\"https://support.clicktime.com/hc/en-us/articles/360002884071\">REST API v2 General Information</a> to get an overview of how to use the API including usable examples of authentication and making requests.</p>
 *
 * OpenAPI spec version: v2
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */


using NUnit.Framework;

using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using IO.Swagger.Api;
using IO.Swagger.Model;
using IO.Swagger.Client;
using System.Reflection;
using Newtonsoft.Json;

namespace IO.Swagger.Test
{
    /// <summary>
    ///  Class for testing GetTimerIntervalModelIntervalsGetTimerModelRoot
    /// </summary>
    /// <remarks>
    /// This file is automatically generated by Swagger Codegen.
    /// Please update the test case below to test the model.
    /// </remarks>
    [TestFixture]
    public class GetTimerIntervalModelIntervalsGetTimerModelRootTests
    {
        // TODO uncomment below to declare an instance variable for GetTimerIntervalModelIntervalsGetTimerModelRoot
        //private GetTimerIntervalModelIntervalsGetTimerModelRoot instance;

        /// <summary>
        /// Setup before each test
        /// </summary>
        [SetUp]
        public void Init()
        {
            // TODO uncomment below to create an instance of GetTimerIntervalModelIntervalsGetTimerModelRoot
            //instance = new GetTimerIntervalModelIntervalsGetTimerModelRoot();
        }

        /// <summary>
        /// Clean up after each test
        /// </summary>
        [TearDown]
        public void Cleanup()
        {

        }

        /// <summary>
        /// Test an instance of GetTimerIntervalModelIntervalsGetTimerModelRoot
        /// </summary>
        [Test]
        public void GetTimerIntervalModelIntervalsGetTimerModelRootInstanceTest()
        {
            // TODO uncomment below to test "IsInstanceOfType" GetTimerIntervalModelIntervalsGetTimerModelRoot
            //Assert.IsInstanceOfType<GetTimerIntervalModelIntervalsGetTimerModelRoot> (instance, "variable 'instance' is a GetTimerIntervalModelIntervalsGetTimerModelRoot");
        }


        /// <summary>
        /// Test the property 'End'
        /// </summary>
        [Test]
        public void EndTest()
        {
            // TODO unit test for the property 'End'
        }
        /// <summary>
        /// Test the property 'ID'
        /// </summary>
        [Test]
        public void IDTest()
        {
            // TODO unit test for the property 'ID'
        }
        /// <summary>
        /// Test the property 'LegacyID'
        /// </summary>
        [Test]
        public void LegacyIDTest()
        {
            // TODO unit test for the property 'LegacyID'
        }
        /// <summary>
        /// Test the property 'Start'
        /// </summary>
        [Test]
        public void StartTest()
        {
            // TODO unit test for the property 'Start'
        }
        /// <summary>
        /// Test the property 'TimeZoneOffset'
        /// </summary>
        [Test]
        public void TimeZoneOffsetTest()
        {
            // TODO unit test for the property 'TimeZoneOffset'
        }

    }

}
