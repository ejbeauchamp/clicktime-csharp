# IO.Swagger.Model.PostDivisionAssociationModel
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DivisionID** | **string** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

