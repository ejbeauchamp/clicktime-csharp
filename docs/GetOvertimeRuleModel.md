# IO.Swagger.Model.GetOvertimeRuleModel
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**CreatedDate** | **string** |  | [optional] 
**ID** | **string** |  | [optional] 
**IsActive** | **bool?** |  | [optional] 
**LegacyID** | **string** |  | [optional] 
**ModifiedDate** | **string** |  | [optional] 
**OvertimeDefinitionID** | **string** |  | [optional] 
**RuleDefinition** | [**GetBasicRuleDefinitionModel**](GetBasicRuleDefinitionModel.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

