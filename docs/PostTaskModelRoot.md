# IO.Swagger.Model.PostTaskModelRoot
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**AccountingPackageID** | **string** |  | [optional] 
**BillingRate** | **double?** |        User requirements:    Security Level(s): Admin, Manager (Add/Edit Billingrates)       Company requirements:    Billing rate model(s): Task, Task x User, Task x Job | [optional] 
**CustomFields** | **Dictionary&lt;string, Object&gt;** |        Company requirements:    Custom Fields must be configured for Tasks. | [optional] 
**IsActive** | **bool?** |  | [optional] 
**IsBillable** | **bool?** |        Company requirements:    Billability by: Task | [optional] 
**Name** | **string** |  | 
**Notes** | **string** |  | [optional] 
**TaskCode** | **string** |  | 
**UseCompanyBillingRate** | **bool?** |        User requirements:    Security Level(s): Admin, Manager (Add/Edit Billingrates)       Company requirements:    Billing rate model(s): Task, Task x User, Task x Job | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

