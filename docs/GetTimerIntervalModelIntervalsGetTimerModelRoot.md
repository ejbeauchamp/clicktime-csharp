# IO.Swagger.Model.GetTimerIntervalModelIntervalsGetTimerModelRoot
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**End** | **string** | hh:mm:ss | [optional] 
**ID** | **string** |  | [optional] 
**LegacyID** | **string** |        Request requirements:    CTLegacyScramble&#x3D;true | [optional] 
**Start** | **string** | hh:mm:ss | [optional] 
**TimeZoneOffset** | **string** | +hh:mm or -hh:mm from UTC | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

