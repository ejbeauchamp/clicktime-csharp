# IO.Swagger.Api.TimeEntriesApi

All URIs are relative to *https://api.clicktime.com/V2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**CreateMyTimeEntry**](TimeEntriesApi.md#createmytimeentry) | **POST** /Me/TimeEntries | Create a Time Entry for myself.
[**CreateTimeEntry**](TimeEntriesApi.md#createtimeentry) | **POST** /TimeEntries | Create a Time Entry.
[**DeleteMyTimeEntry**](TimeEntriesApi.md#deletemytimeentry) | **DELETE** /Me/TimeEntries/{timeEntryID} | Delete my Time Entry.
[**DeleteTimeEntry**](TimeEntriesApi.md#deletetimeentry) | **DELETE** /TimeEntries/{timeEntryID} | Delete a Time Entry.
[**GetDefaultTimeEntry**](TimeEntriesApi.md#getdefaulttimeentry) | **GET** /TimeEntries/new | Get a new Time Entry with default values.
[**GetMyTimeEntry**](TimeEntriesApi.md#getmytimeentry) | **GET** /Me/TimeEntries/{timeEntryID} | Get my Time Entry.
[**GetTimeEntry**](TimeEntriesApi.md#gettimeentry) | **GET** /TimeEntries/{timeEntryID} | Get a Time Entry.
[**GetTimeEntryCustomFieldDefinition**](TimeEntriesApi.md#gettimeentrycustomfielddefinition) | **GET** /TimeEntries/CustomFieldDefinitions/{customFieldDefinitionID} | Get a Custom Field Definition for Time Entries.
[**ListMyTimeEntries**](TimeEntriesApi.md#listmytimeentries) | **GET** /Me/TimeEntries | Get my Time Entries.
[**ListTimeEntries**](TimeEntriesApi.md#listtimeentries) | **GET** /TimeEntries | Get Time Entries. This endpoint is intended for smaller data sets, for example when when you&#39;re retrieving time entries for a single timesheet period. For bulk operations, please use the Reports/Time endpoint for larger data sets - - you&#39;ll find this more performant and you&#39;ll be able to retrieve more time entries in a single request.
[**ListTimeEntryCustomFieldDefinitions**](TimeEntriesApi.md#listtimeentrycustomfielddefinitions) | **GET** /TimeEntries/CustomFieldDefinitions | Get Custom Field Definitions for TimeEntries.
[**UpdateMyTimeEntry**](TimeEntriesApi.md#updatemytimeentry) | **PATCH** /Me/TimeEntries/{timeEntryID} | Update my Time Entry.
[**UpdateTimeEntry**](TimeEntriesApi.md#updatetimeentry) | **PATCH** /TimeEntries/{timeEntryID} | Update a Time Entry.


<a name="createmytimeentry"></a>
# **CreateMyTimeEntry**
> ResponseBodyGetTimeEntryModelMe CreateMyTimeEntry (PostTimeEntryModelMe model)

Create a Time Entry for myself.

       Company requirements:    DCAA must be disabled.    Company must not have SubJobs enabled.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class CreateMyTimeEntryExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TimeEntriesApi();
            var model = new PostTimeEntryModelMe(); // PostTimeEntryModelMe |       Required

            try
            {
                // Create a Time Entry for myself.
                ResponseBodyGetTimeEntryModelMe result = apiInstance.CreateMyTimeEntry(model);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TimeEntriesApi.CreateMyTimeEntry: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **model** | [**PostTimeEntryModelMe**](PostTimeEntryModelMe.md)|       Required | 

### Return type

[**ResponseBodyGetTimeEntryModelMe**](ResponseBodyGetTimeEntryModelMe.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded, application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="createtimeentry"></a>
# **CreateTimeEntry**
> ResponseBodyGetTimeEntryModelRoot CreateTimeEntry (PostTimeEntryModelRoot model)

Create a Time Entry.

       Company requirements:    DCAA must be disabled.    Company must not have SubJobs enabled.       User requirements:    Admin, or Manager with permissions to Override timesheets.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class CreateTimeEntryExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TimeEntriesApi();
            var model = new PostTimeEntryModelRoot(); // PostTimeEntryModelRoot |       Required

            try
            {
                // Create a Time Entry.
                ResponseBodyGetTimeEntryModelRoot result = apiInstance.CreateTimeEntry(model);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TimeEntriesApi.CreateTimeEntry: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **model** | [**PostTimeEntryModelRoot**](PostTimeEntryModelRoot.md)|       Required | 

### Return type

[**ResponseBodyGetTimeEntryModelRoot**](ResponseBodyGetTimeEntryModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded, application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="deletemytimeentry"></a>
# **DeleteMyTimeEntry**
> void DeleteMyTimeEntry (string timeEntryID)

Delete my Time Entry.

       Company requirements:    DCAA must be disabled.    Company must not have SubJobs enabled.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class DeleteMyTimeEntryExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TimeEntriesApi();
            var timeEntryID = timeEntryID_example;  // string |       Required

            try
            {
                // Delete my Time Entry.
                apiInstance.DeleteMyTimeEntry(timeEntryID);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TimeEntriesApi.DeleteMyTimeEntry: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **timeEntryID** | **string**|       Required | 

### Return type

void (empty response body)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="deletetimeentry"></a>
# **DeleteTimeEntry**
> void DeleteTimeEntry (string timeEntryID)

Delete a Time Entry.

       Company requirements:    DCAA must be disabled.    Company must not have SubJobs enabled.       User requirements:    Admin, or Manager with permissions to Override timesheets.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class DeleteTimeEntryExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TimeEntriesApi();
            var timeEntryID = timeEntryID_example;  // string |       Required

            try
            {
                // Delete a Time Entry.
                apiInstance.DeleteTimeEntry(timeEntryID);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TimeEntriesApi.DeleteTimeEntry: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **timeEntryID** | **string**|       Required | 

### Return type

void (empty response body)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getdefaulttimeentry"></a>
# **GetDefaultTimeEntry**
> ResponseBodyGetTimeEntryModelNew GetDefaultTimeEntry ()

Get a new Time Entry with default values.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class GetDefaultTimeEntryExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TimeEntriesApi();

            try
            {
                // Get a new Time Entry with default values.
                ResponseBodyGetTimeEntryModelNew result = apiInstance.GetDefaultTimeEntry();
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TimeEntriesApi.GetDefaultTimeEntry: " + e.Message );
            }
        }
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ResponseBodyGetTimeEntryModelNew**](ResponseBodyGetTimeEntryModelNew.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getmytimeentry"></a>
# **GetMyTimeEntry**
> ResponseBodyGetTimeEntryModelMe GetMyTimeEntry (string timeEntryID)

Get my Time Entry.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class GetMyTimeEntryExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TimeEntriesApi();
            var timeEntryID = timeEntryID_example;  // string |       Required

            try
            {
                // Get my Time Entry.
                ResponseBodyGetTimeEntryModelMe result = apiInstance.GetMyTimeEntry(timeEntryID);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TimeEntriesApi.GetMyTimeEntry: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **timeEntryID** | **string**|       Required | 

### Return type

[**ResponseBodyGetTimeEntryModelMe**](ResponseBodyGetTimeEntryModelMe.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="gettimeentry"></a>
# **GetTimeEntry**
> ResponseBodyGetTimeEntryModelRoot GetTimeEntry (string timeEntryID)

Get a Time Entry.

       User requirements:    Admin, or Manager with permissions to Review timesheets.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class GetTimeEntryExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TimeEntriesApi();
            var timeEntryID = timeEntryID_example;  // string |       Required

            try
            {
                // Get a Time Entry.
                ResponseBodyGetTimeEntryModelRoot result = apiInstance.GetTimeEntry(timeEntryID);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TimeEntriesApi.GetTimeEntry: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **timeEntryID** | **string**|       Required | 

### Return type

[**ResponseBodyGetTimeEntryModelRoot**](ResponseBodyGetTimeEntryModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="gettimeentrycustomfielddefinition"></a>
# **GetTimeEntryCustomFieldDefinition**
> ResponseBodyGetCustomFieldDefinitionModelRoot GetTimeEntryCustomFieldDefinition (string customFieldDefinitionID)

Get a Custom Field Definition for Time Entries.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class GetTimeEntryCustomFieldDefinitionExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TimeEntriesApi();
            var customFieldDefinitionID = customFieldDefinitionID_example;  // string |       Required

            try
            {
                // Get a Custom Field Definition for Time Entries.
                ResponseBodyGetCustomFieldDefinitionModelRoot result = apiInstance.GetTimeEntryCustomFieldDefinition(customFieldDefinitionID);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TimeEntriesApi.GetTimeEntryCustomFieldDefinition: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customFieldDefinitionID** | **string**|       Required | 

### Return type

[**ResponseBodyGetCustomFieldDefinitionModelRoot**](ResponseBodyGetCustomFieldDefinitionModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="listmytimeentries"></a>
# **ListMyTimeEntries**
> ResponseBodyListGetTimeEntryModelMe ListMyTimeEntries (List<string> ID = null, List<string> jobID = null, List<string> taskID = null, string startDate = null, string endDate = null, List<string> entryDate = null, int? limit = null, int? offset = null)

Get my Time Entries.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ListMyTimeEntriesExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TimeEntriesApi();
            var ID = new List<string>(); // List<string> |  (optional) 
            var jobID = new List<string>(); // List<string> |  (optional) 
            var taskID = new List<string>(); // List<string> |  (optional) 
            var startDate = startDate_example;  // string |  (optional) 
            var endDate = endDate_example;  // string |  (optional) 
            var entryDate = new List<string>(); // List<string> |  (optional) 
            var limit = 56;  // int? | Up to 500 (optional) 
            var offset = 56;  // int? |  (optional) 

            try
            {
                // Get my Time Entries.
                ResponseBodyListGetTimeEntryModelMe result = apiInstance.ListMyTimeEntries(ID, jobID, taskID, startDate, endDate, entryDate, limit, offset);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TimeEntriesApi.ListMyTimeEntries: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ID** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **jobID** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **taskID** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **startDate** | **string**|  | [optional] 
 **endDate** | **string**|  | [optional] 
 **entryDate** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **limit** | **int?**| Up to 500 | [optional] 
 **offset** | **int?**|  | [optional] 

### Return type

[**ResponseBodyListGetTimeEntryModelMe**](ResponseBodyListGetTimeEntryModelMe.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="listtimeentries"></a>
# **ListTimeEntries**
> ResponseBodyListGetTimeEntryModelRoot ListTimeEntries (List<string> ID = null, List<string> jobID = null, List<string> taskID = null, List<string> userID = null, string startDate = null, string endDate = null, List<string> entryDate = null, int? limit = null, int? offset = null)

Get Time Entries. This endpoint is intended for smaller data sets, for example when when you're retrieving time entries for a single timesheet period. For bulk operations, please use the Reports/Time endpoint for larger data sets - - you'll find this more performant and you'll be able to retrieve more time entries in a single request.

       User requirements:    Admin, or Manager with permissions to Review timesheets.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ListTimeEntriesExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TimeEntriesApi();
            var ID = new List<string>(); // List<string> |  (optional) 
            var jobID = new List<string>(); // List<string> |  (optional) 
            var taskID = new List<string>(); // List<string> |  (optional) 
            var userID = new List<string>(); // List<string> |  (optional) 
            var startDate = startDate_example;  // string |  (optional) 
            var endDate = endDate_example;  // string |  (optional) 
            var entryDate = new List<string>(); // List<string> |  (optional) 
            var limit = 56;  // int? | Up to 500 (optional) 
            var offset = 56;  // int? |  (optional) 

            try
            {
                // Get Time Entries. This endpoint is intended for smaller data sets, for example when when you're retrieving time entries for a single timesheet period. For bulk operations, please use the Reports/Time endpoint for larger data sets - - you'll find this more performant and you'll be able to retrieve more time entries in a single request.
                ResponseBodyListGetTimeEntryModelRoot result = apiInstance.ListTimeEntries(ID, jobID, taskID, userID, startDate, endDate, entryDate, limit, offset);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TimeEntriesApi.ListTimeEntries: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ID** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **jobID** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **taskID** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **userID** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **startDate** | **string**|  | [optional] 
 **endDate** | **string**|  | [optional] 
 **entryDate** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **limit** | **int?**| Up to 500 | [optional] 
 **offset** | **int?**|  | [optional] 

### Return type

[**ResponseBodyListGetTimeEntryModelRoot**](ResponseBodyListGetTimeEntryModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="listtimeentrycustomfielddefinitions"></a>
# **ListTimeEntryCustomFieldDefinitions**
> ResponseBodyListGetCustomFieldDefinitionModelRoot ListTimeEntryCustomFieldDefinitions (int? limit = null, int? offset = null)

Get Custom Field Definitions for TimeEntries.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ListTimeEntryCustomFieldDefinitionsExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TimeEntriesApi();
            var limit = 56;  // int? |  (optional) 
            var offset = 56;  // int? |  (optional) 

            try
            {
                // Get Custom Field Definitions for TimeEntries.
                ResponseBodyListGetCustomFieldDefinitionModelRoot result = apiInstance.ListTimeEntryCustomFieldDefinitions(limit, offset);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TimeEntriesApi.ListTimeEntryCustomFieldDefinitions: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **limit** | **int?**|  | [optional] 
 **offset** | **int?**|  | [optional] 

### Return type

[**ResponseBodyListGetCustomFieldDefinitionModelRoot**](ResponseBodyListGetCustomFieldDefinitionModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="updatemytimeentry"></a>
# **UpdateMyTimeEntry**
> ResponseBodyGetTimeEntryModelMe UpdateMyTimeEntry (string timeEntryID, PatchTimeEntryModelMe model)

Update my Time Entry.

       Company requirements:    DCAA must be disabled.    Company must not have SubJobs enabled.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class UpdateMyTimeEntryExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TimeEntriesApi();
            var timeEntryID = timeEntryID_example;  // string |       Required
            var model = new PatchTimeEntryModelMe(); // PatchTimeEntryModelMe |       Required

            try
            {
                // Update my Time Entry.
                ResponseBodyGetTimeEntryModelMe result = apiInstance.UpdateMyTimeEntry(timeEntryID, model);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TimeEntriesApi.UpdateMyTimeEntry: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **timeEntryID** | **string**|       Required | 
 **model** | [**PatchTimeEntryModelMe**](PatchTimeEntryModelMe.md)|       Required | 

### Return type

[**ResponseBodyGetTimeEntryModelMe**](ResponseBodyGetTimeEntryModelMe.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded, application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="updatetimeentry"></a>
# **UpdateTimeEntry**
> ResponseBodyGetTimeEntryModelRoot UpdateTimeEntry (string timeEntryID, PatchTimeEntryModelRoot model)

Update a Time Entry.

       Company requirements:    DCAA must be disabled.    Company must not have SubJobs enabled.       User requirements:    Admin, or Manager with permissions to Override timesheets.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class UpdateTimeEntryExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TimeEntriesApi();
            var timeEntryID = timeEntryID_example;  // string |       Required
            var model = new PatchTimeEntryModelRoot(); // PatchTimeEntryModelRoot |       Required

            try
            {
                // Update a Time Entry.
                ResponseBodyGetTimeEntryModelRoot result = apiInstance.UpdateTimeEntry(timeEntryID, model);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TimeEntriesApi.UpdateTimeEntry: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **timeEntryID** | **string**|       Required | 
 **model** | [**PatchTimeEntryModelRoot**](PatchTimeEntryModelRoot.md)|       Required | 

### Return type

[**ResponseBodyGetTimeEntryModelRoot**](ResponseBodyGetTimeEntryModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded, application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

