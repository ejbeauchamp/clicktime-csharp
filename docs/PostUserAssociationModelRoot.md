# IO.Swagger.Model.PostUserAssociationModelRoot
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**UserID** | **string** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

