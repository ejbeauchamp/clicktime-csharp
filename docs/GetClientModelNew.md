# IO.Swagger.Model.GetClientModelNew
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**AccountingPackageID** | **string** |        User requirements:    Security Level(s): Admin, Manager (View Clients) | [optional] 
**BillingRate** | **double?** |        User requirements:    Security Level(s): Admin, Manager (View Clients)    Security Level(s): Admin, Manager (View Billingrates)       Company requirements:    Billing rate model(s): User x Client | [optional] 
**ClientNumber** | **string** |  | [optional] 
**CreatedDate** | **DateTime?** |        User requirements:    Security Level(s): Admin, Manager (View Clients) | [optional] 
**CustomFields** | **Dictionary&lt;string, Object&gt;** |        Company requirements:    Custom Fields must be configured for Clients.       User requirements:    Security Level(s): Admin, Manager (View Clients) | [optional] 
**ID** | **string** |  | [optional] 
**IsActive** | **bool?** |  | [optional] 
**ListDisplayText** | **string** |  | [optional] 
**Name** | **string** |  | [optional] 
**Notes** | **string** |        User requirements:    Security Level(s): Admin, Manager (View Clients) | [optional] 
**SecondaryBillingRateMode** | **string** |        User requirements:    Security Level(s): Admin, Manager (View Clients)    Security Level(s): Admin, Manager (View Billingrates)       Company requirements:    Billing rate model(s): User x Client | [optional] 
**ShortName** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

