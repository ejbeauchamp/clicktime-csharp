# IO.Swagger.Model.PatchTaskModel
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**AccountingPackageID** | **string** |  | [optional] 
**BillingRate** | **double?** |  | [optional] 
**CustomFields** | **Dictionary&lt;string, Object&gt;** |  | [optional] 
**IsActive** | **bool?** |  | [optional] 
**IsBillable** | **bool?** |  | [optional] 
**Name** | **string** |  | [optional] 
**Notes** | **string** |  | [optional] 
**TaskCode** | **string** |  | [optional] 
**UseCompanyBillingRate** | **bool?** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

