# IO.Swagger.Model.GetBillingRateModel
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**BillingRate** | **double?** |  | [optional] 
**ID** | **string** |  | [optional] 
**IsBillable** | **bool?** |  | [optional] 
**LegacyID** | **string** |  | [optional] 
**Name** | **string** |  | [optional] 
**Type** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

