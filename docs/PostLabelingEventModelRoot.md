# IO.Swagger.Model.PostLabelingEventModelRoot
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**EntityIDs** | **string** |  | 
**EntityType** | **string** |  | 
**LabelID** | **string** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

