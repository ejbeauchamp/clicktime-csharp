# IO.Swagger.Api.OvertimeRulesApi

All URIs are relative to *https://api.clicktime.com/V2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**CreateOvertimeRule**](OvertimeRulesApi.md#createovertimerule) | **POST** /Overtime/{overtimeDefinitionID}/Rules | Create an Overtime Rule.
[**DeleteOvertimeRule**](OvertimeRulesApi.md#deleteovertimerule) | **DELETE** /Overtime/{overtimeDefinitionID}/Rules/{overtimeRuleID} | Delete an Overtime Rule.
[**GetDefaultOvertimeRule**](OvertimeRulesApi.md#getdefaultovertimerule) | **GET** /Overtime/{overtimeDefinitionID}/Rules/new | Get a new Overtime Rule with default values.
[**ListOvertimeRules**](OvertimeRulesApi.md#listovertimerules) | **GET** /Overtime/{overtimeDefinitionID}/Rules | Get Overtime Rules.
[**UpdateOvertimeRule**](OvertimeRulesApi.md#updateovertimerule) | **PATCH** /Overtime/{overtimeDefinitionID}/Rules/{overtimeRuleID} | Update an Overtime Rule.


<a name="createovertimerule"></a>
# **CreateOvertimeRule**
> ResponseBodyGetOvertimeRuleModelRoot CreateOvertimeRule (string overtimeDefinitionID, PostOvertimeRuleModelRoot model)

Create an Overtime Rule.

       User requirements:    User must be an Admin.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class CreateOvertimeRuleExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new OvertimeRulesApi();
            var overtimeDefinitionID = overtimeDefinitionID_example;  // string |       Required
            var model = new PostOvertimeRuleModelRoot(); // PostOvertimeRuleModelRoot |       Required

            try
            {
                // Create an Overtime Rule.
                ResponseBodyGetOvertimeRuleModelRoot result = apiInstance.CreateOvertimeRule(overtimeDefinitionID, model);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling OvertimeRulesApi.CreateOvertimeRule: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **overtimeDefinitionID** | **string**|       Required | 
 **model** | [**PostOvertimeRuleModelRoot**](PostOvertimeRuleModelRoot.md)|       Required | 

### Return type

[**ResponseBodyGetOvertimeRuleModelRoot**](ResponseBodyGetOvertimeRuleModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded, application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="deleteovertimerule"></a>
# **DeleteOvertimeRule**
> void DeleteOvertimeRule (string overtimeDefinitionID, string overtimeRuleID)

Delete an Overtime Rule.

       User requirements:    User must be an Admin.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class DeleteOvertimeRuleExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new OvertimeRulesApi();
            var overtimeDefinitionID = overtimeDefinitionID_example;  // string |       Required
            var overtimeRuleID = overtimeRuleID_example;  // string |       Required

            try
            {
                // Delete an Overtime Rule.
                apiInstance.DeleteOvertimeRule(overtimeDefinitionID, overtimeRuleID);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling OvertimeRulesApi.DeleteOvertimeRule: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **overtimeDefinitionID** | **string**|       Required | 
 **overtimeRuleID** | **string**|       Required | 

### Return type

void (empty response body)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getdefaultovertimerule"></a>
# **GetDefaultOvertimeRule**
> ResponseBodyGetOvertimeRuleModelNew GetDefaultOvertimeRule (string overtimeDefinitionID)

Get a new Overtime Rule with default values.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class GetDefaultOvertimeRuleExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new OvertimeRulesApi();
            var overtimeDefinitionID = overtimeDefinitionID_example;  // string |       Required

            try
            {
                // Get a new Overtime Rule with default values.
                ResponseBodyGetOvertimeRuleModelNew result = apiInstance.GetDefaultOvertimeRule(overtimeDefinitionID);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling OvertimeRulesApi.GetDefaultOvertimeRule: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **overtimeDefinitionID** | **string**|       Required | 

### Return type

[**ResponseBodyGetOvertimeRuleModelNew**](ResponseBodyGetOvertimeRuleModelNew.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="listovertimerules"></a>
# **ListOvertimeRules**
> ResponseBodyListGetOvertimeRuleModelRoot ListOvertimeRules (string overtimeDefinitionID, int? limit = null, int? offset = null)

Get Overtime Rules.

       User requirements:    User must be an Admin.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ListOvertimeRulesExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new OvertimeRulesApi();
            var overtimeDefinitionID = overtimeDefinitionID_example;  // string |       Required
            var limit = 56;  // int? |  (optional) 
            var offset = 56;  // int? |  (optional) 

            try
            {
                // Get Overtime Rules.
                ResponseBodyListGetOvertimeRuleModelRoot result = apiInstance.ListOvertimeRules(overtimeDefinitionID, limit, offset);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling OvertimeRulesApi.ListOvertimeRules: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **overtimeDefinitionID** | **string**|       Required | 
 **limit** | **int?**|  | [optional] 
 **offset** | **int?**|  | [optional] 

### Return type

[**ResponseBodyListGetOvertimeRuleModelRoot**](ResponseBodyListGetOvertimeRuleModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="updateovertimerule"></a>
# **UpdateOvertimeRule**
> ResponseBodyGetOvertimeRuleModelRoot UpdateOvertimeRule (string overtimeDefinitionID, string overtimeRuleID, PostOvertimeRuleModelRoot model)

Update an Overtime Rule.

       User requirements:    User must be an Admin.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class UpdateOvertimeRuleExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new OvertimeRulesApi();
            var overtimeDefinitionID = overtimeDefinitionID_example;  // string |       Required
            var overtimeRuleID = overtimeRuleID_example;  // string |       Required
            var model = new PostOvertimeRuleModelRoot(); // PostOvertimeRuleModelRoot |       Required

            try
            {
                // Update an Overtime Rule.
                ResponseBodyGetOvertimeRuleModelRoot result = apiInstance.UpdateOvertimeRule(overtimeDefinitionID, overtimeRuleID, model);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling OvertimeRulesApi.UpdateOvertimeRule: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **overtimeDefinitionID** | **string**|       Required | 
 **overtimeRuleID** | **string**|       Required | 
 **model** | [**PostOvertimeRuleModelRoot**](PostOvertimeRuleModelRoot.md)|       Required | 

### Return type

[**ResponseBodyGetOvertimeRuleModelRoot**](ResponseBodyGetOvertimeRuleModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded, application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

