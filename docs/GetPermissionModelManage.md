# IO.Swagger.Model.GetPermissionModelManage
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**AccessLevel** | **string** | The availability of this permission (e.g. All, or restricted by Division). | [optional] 
**Name** | **string** | The name of the permission. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

