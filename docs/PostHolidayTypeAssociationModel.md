# IO.Swagger.Model.PostHolidayTypeAssociationModel
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**HolidayTypeID** | **string** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

