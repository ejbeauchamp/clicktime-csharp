# IO.Swagger.Model.PostTimerModelRoot
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Intervals** | [**List&lt;PostTimerIntervalModelIntervalsPostTimerModelRoot&gt;**](PostTimerIntervalModelIntervalsPostTimerModelRoot.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

