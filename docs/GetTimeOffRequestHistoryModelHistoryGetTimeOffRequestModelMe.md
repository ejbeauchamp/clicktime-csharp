# IO.Swagger.Model.GetTimeOffRequestHistoryModelHistoryGetTimeOffRequestModelMe
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ActionByUserID** | **string** |  | [optional] 
**ActionByUserName** | **string** |  | [optional] 
**Comment** | **string** |  | [optional] 
**Date** | **string** |  | [optional] 
**LegacyID** | **string** |        Request requirements:    CTLegacyScramble&#x3D;true | [optional] 
**Status** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

