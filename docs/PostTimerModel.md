# IO.Swagger.Model.PostTimerModel
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Intervals** | [**List&lt;PostTimerIntervalModel&gt;**](PostTimerIntervalModel.md) |  | 
**TimeEntryID** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

