# IO.Swagger.Model.PatchJobModel
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**AccountingPackageID** | **string** |  | [optional] 
**BillingRate** | **double?** |  | [optional] 
**ClientID** | **string** |  | [optional] 
**CustomFields** | **Dictionary&lt;string, Object&gt;** |  | [optional] 
**IsActive** | **bool?** |  | [optional] 
**IsBillable** | **bool?** |  | [optional] 
**JobNumber** | **string** |  | [optional] 
**Name** | **string** |  | [optional] 
**Notes** | **string** |  | [optional] 
**ProjectManagerID** | **string** | Required if TimeRequiresApproval is true. | [optional] 
**SecondaryBillingRateMode** | **string** |  | [optional] 
**TimeRequiresApproval** | **bool?** |  | [optional] 
**UseCompanyBillingRate** | **bool?** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

