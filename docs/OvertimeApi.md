# IO.Swagger.Api.OvertimeApi

All URIs are relative to *https://api.clicktime.com/V2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**CreateEmploymentTypeOvertime**](OvertimeApi.md#createemploymenttypeovertime) | **POST** /EmploymentTypes/{employmentTypeID}/Overtime | Create an Overtime definition for an Employment Type.
[**CreateOvertime**](OvertimeApi.md#createovertime) | **POST** /Overtime | Create an Overtime definition.
[**GetDefaultOvertime**](OvertimeApi.md#getdefaultovertime) | **GET** /Overtime/new | Get a new Overtime definition with default values.
[**GetEmploymentTypeOvertime**](OvertimeApi.md#getemploymenttypeovertime) | **GET** /EmploymentTypes/{employmentTypeID}/Overtime | Get the Overtime definition for an Employment Type.
[**GetOvertime**](OvertimeApi.md#getovertime) | **GET** /Overtime/{overtimeDefinitionID} | Get an Overtime definition.
[**ListOvertime**](OvertimeApi.md#listovertime) | **GET** /Overtime | Get Overtime definitions.
[**ListOvertime_0**](OvertimeApi.md#listovertime_0) | **GET** /Overtime/RulePresets | Get Overtime Rule Presets.
[**UpdateEmploymentTypeOvertime**](OvertimeApi.md#updateemploymenttypeovertime) | **PATCH** /EmploymentTypes/{employmentTypeID}/Overtime | Update the Overtime definition for an Employment Type.
[**UpdateOvertime**](OvertimeApi.md#updateovertime) | **PATCH** /Overtime/{overtimeDefinitionID} | Update an Overtime definition.


<a name="createemploymenttypeovertime"></a>
# **CreateEmploymentTypeOvertime**
> ResponseBodyGetOvertimeModelRoot CreateEmploymentTypeOvertime (string employmentTypeID, PostEmploymentTypeOvertimeModelRoot model)

Create an Overtime definition for an Employment Type.

       User requirements:    User must be an Admin.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class CreateEmploymentTypeOvertimeExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new OvertimeApi();
            var employmentTypeID = employmentTypeID_example;  // string |       Required
            var model = new PostEmploymentTypeOvertimeModelRoot(); // PostEmploymentTypeOvertimeModelRoot |       Required

            try
            {
                // Create an Overtime definition for an Employment Type.
                ResponseBodyGetOvertimeModelRoot result = apiInstance.CreateEmploymentTypeOvertime(employmentTypeID, model);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling OvertimeApi.CreateEmploymentTypeOvertime: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **employmentTypeID** | **string**|       Required | 
 **model** | [**PostEmploymentTypeOvertimeModelRoot**](PostEmploymentTypeOvertimeModelRoot.md)|       Required | 

### Return type

[**ResponseBodyGetOvertimeModelRoot**](ResponseBodyGetOvertimeModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded, application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="createovertime"></a>
# **CreateOvertime**
> ResponseBodyGetOvertimeModelRoot CreateOvertime (PostOvertimeModelRoot model)

Create an Overtime definition.

       User requirements:    User must be an Admin.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class CreateOvertimeExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new OvertimeApi();
            var model = new PostOvertimeModelRoot(); // PostOvertimeModelRoot |       Required

            try
            {
                // Create an Overtime definition.
                ResponseBodyGetOvertimeModelRoot result = apiInstance.CreateOvertime(model);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling OvertimeApi.CreateOvertime: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **model** | [**PostOvertimeModelRoot**](PostOvertimeModelRoot.md)|       Required | 

### Return type

[**ResponseBodyGetOvertimeModelRoot**](ResponseBodyGetOvertimeModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded, application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getdefaultovertime"></a>
# **GetDefaultOvertime**
> ResponseBodyGetOvertimeModelNew GetDefaultOvertime ()

Get a new Overtime definition with default values.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class GetDefaultOvertimeExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new OvertimeApi();

            try
            {
                // Get a new Overtime definition with default values.
                ResponseBodyGetOvertimeModelNew result = apiInstance.GetDefaultOvertime();
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling OvertimeApi.GetDefaultOvertime: " + e.Message );
            }
        }
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ResponseBodyGetOvertimeModelNew**](ResponseBodyGetOvertimeModelNew.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getemploymenttypeovertime"></a>
# **GetEmploymentTypeOvertime**
> ResponseBodyGetOvertimeModelRoot GetEmploymentTypeOvertime (string employmentTypeID)

Get the Overtime definition for an Employment Type.

       User requirements:    Admin, or Manager with permissions to Add/Edit People.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class GetEmploymentTypeOvertimeExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new OvertimeApi();
            var employmentTypeID = employmentTypeID_example;  // string |       Required

            try
            {
                // Get the Overtime definition for an Employment Type.
                ResponseBodyGetOvertimeModelRoot result = apiInstance.GetEmploymentTypeOvertime(employmentTypeID);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling OvertimeApi.GetEmploymentTypeOvertime: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **employmentTypeID** | **string**|       Required | 

### Return type

[**ResponseBodyGetOvertimeModelRoot**](ResponseBodyGetOvertimeModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getovertime"></a>
# **GetOvertime**
> ResponseBodyGetOvertimeModelRoot GetOvertime (string overtimeDefinitionID)

Get an Overtime definition.

       User requirements:    User must be an Admin.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class GetOvertimeExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new OvertimeApi();
            var overtimeDefinitionID = overtimeDefinitionID_example;  // string |       Required

            try
            {
                // Get an Overtime definition.
                ResponseBodyGetOvertimeModelRoot result = apiInstance.GetOvertime(overtimeDefinitionID);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling OvertimeApi.GetOvertime: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **overtimeDefinitionID** | **string**|       Required | 

### Return type

[**ResponseBodyGetOvertimeModelRoot**](ResponseBodyGetOvertimeModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="listovertime"></a>
# **ListOvertime**
> ResponseBodyListGetOvertimeModelRoot ListOvertime (List<string> ID = null, List<string> employmentTypeID = null, List<bool?> isActive = null, int? limit = null, int? offset = null)

Get Overtime definitions.

       User requirements:    Admin, or Manager with permissions to Add/Edit People.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ListOvertimeExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new OvertimeApi();
            var ID = new List<string>(); // List<string> |  (optional) 
            var employmentTypeID = new List<string>(); // List<string> |  (optional) 
            var isActive = new List<bool?>(); // List<bool?> |  (optional) 
            var limit = 56;  // int? |  (optional) 
            var offset = 56;  // int? |  (optional) 

            try
            {
                // Get Overtime definitions.
                ResponseBodyListGetOvertimeModelRoot result = apiInstance.ListOvertime(ID, employmentTypeID, isActive, limit, offset);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling OvertimeApi.ListOvertime: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ID** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **employmentTypeID** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **isActive** | [**List&lt;bool?&gt;**](bool?.md)|  | [optional] 
 **limit** | **int?**|  | [optional] 
 **offset** | **int?**|  | [optional] 

### Return type

[**ResponseBodyListGetOvertimeModelRoot**](ResponseBodyListGetOvertimeModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="listovertime_0"></a>
# **ListOvertime_0**
> ResponseBodyListGetOvertimeRulePresetModel ListOvertime_0 ()

Get Overtime Rule Presets.

       User requirements:    User must be an Admin.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ListOvertime_0Example
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new OvertimeApi();

            try
            {
                // Get Overtime Rule Presets.
                ResponseBodyListGetOvertimeRulePresetModel result = apiInstance.ListOvertime_0();
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling OvertimeApi.ListOvertime_0: " + e.Message );
            }
        }
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ResponseBodyListGetOvertimeRulePresetModel**](ResponseBodyListGetOvertimeRulePresetModel.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="updateemploymenttypeovertime"></a>
# **UpdateEmploymentTypeOvertime**
> ResponseBodyGetOvertimeModelRoot UpdateEmploymentTypeOvertime (string employmentTypeID, PostEmploymentTypeOvertimeModelRoot model)

Update the Overtime definition for an Employment Type.

       User requirements:    User must be an Admin.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class UpdateEmploymentTypeOvertimeExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new OvertimeApi();
            var employmentTypeID = employmentTypeID_example;  // string |       Required
            var model = new PostEmploymentTypeOvertimeModelRoot(); // PostEmploymentTypeOvertimeModelRoot |       Required

            try
            {
                // Update the Overtime definition for an Employment Type.
                ResponseBodyGetOvertimeModelRoot result = apiInstance.UpdateEmploymentTypeOvertime(employmentTypeID, model);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling OvertimeApi.UpdateEmploymentTypeOvertime: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **employmentTypeID** | **string**|       Required | 
 **model** | [**PostEmploymentTypeOvertimeModelRoot**](PostEmploymentTypeOvertimeModelRoot.md)|       Required | 

### Return type

[**ResponseBodyGetOvertimeModelRoot**](ResponseBodyGetOvertimeModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded, application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="updateovertime"></a>
# **UpdateOvertime**
> ResponseBodyGetOvertimeModelRoot UpdateOvertime (string overtimeDefinitionID, PatchOvertimeModelRoot model)

Update an Overtime definition.

       User requirements:    User must be an Admin.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class UpdateOvertimeExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new OvertimeApi();
            var overtimeDefinitionID = overtimeDefinitionID_example;  // string |       Required
            var model = new PatchOvertimeModelRoot(); // PatchOvertimeModelRoot |       Required

            try
            {
                // Update an Overtime definition.
                ResponseBodyGetOvertimeModelRoot result = apiInstance.UpdateOvertime(overtimeDefinitionID, model);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling OvertimeApi.UpdateOvertime: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **overtimeDefinitionID** | **string**|       Required | 
 **model** | [**PatchOvertimeModelRoot**](PatchOvertimeModelRoot.md)|       Required | 

### Return type

[**ResponseBodyGetOvertimeModelRoot**](ResponseBodyGetOvertimeModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded, application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

