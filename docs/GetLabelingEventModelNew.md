# IO.Swagger.Model.GetLabelingEventModelNew
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**EntityIDs** | **string** |        User requirements:    Security Level(s): Admin, Manager (Run Company Reports) | [optional] 
**EntityType** | **string** |        User requirements:    Security Level(s): Admin, Manager (Run Company Reports) | [optional] 
**ID** | **string** |        User requirements:    Security Level(s): Admin, Manager (Run Company Reports) | [optional] 
**LabelID** | **string** |        User requirements:    Security Level(s): Admin, Manager (Run Company Reports) | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

