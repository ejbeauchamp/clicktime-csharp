# IO.Swagger.Model.GetBasicExpenseSheetModel
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ExpenseSheetDate** | **string** |  | [optional] 
**ID** | **string** |  | [optional] 
**LegacyID** | **string** |  | [optional] 
**Status** | **string** |  | [optional] 
**Title** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

