# IO.Swagger.Model.PostTimeEntryModel
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**BillingRate** | **double?** |  | [optional] 
**BreakTime** | **double?** |  | [optional] 
**Comment** | **string** | Company may require a user to enter a comment with Time Entries. | [optional] 
**CostRate** | **double?** |  | [optional] 
**CustomFields** | **Dictionary&lt;string, Object&gt;** |  | [optional] 
**Date** | **string** |  | [optional] 
**EndTime** | **string** | Company may require a user to enter an end time with Time Entries.  Format: HH:MM (24-hour) | [optional] 
**Hours** | **double?** |  | 
**JobID** | **string** |  | 
**StartTime** | **string** | Company may require a user to enter a start time with Time Entries.  Format: HH:MM (24-hour) | [optional] 
**TaskID** | **string** |  | 
**UserID** | **string** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

