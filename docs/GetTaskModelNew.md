# IO.Swagger.Model.GetTaskModelNew
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**AccountingPackageID** | **string** |        User requirements:    Security Level(s): Admin, Manager (View Tasks) | [optional] 
**BillingRate** | **double?** |        User requirements:    Security Level(s): Admin, Manager (View Tasks)    Security Level(s): Admin, Manager (View Billingrates)    Security Level(s): Admin, Manager (Add/Edit Tasks)       Company requirements:    Billing rate model(s): Task, Task x User, Task x Job | [optional] 
**CustomFields** | **Dictionary&lt;string, Object&gt;** |        Company requirements:    Custom Fields must be configured for Tasks.       User requirements:    Security Level(s): Admin, Manager (View Tasks)    Security Level(s): Admin, Manager (Add/Edit Tasks) | [optional] 
**ID** | **string** |  | [optional] 
**IsActive** | **bool?** |  | [optional] 
**IsBillable** | **bool?** |        User requirements:    Security Level(s): Admin, Manager (View Tasks)    Security Level(s): Admin, Manager (Add/Edit Tasks)       Company requirements:    Billability by: Task | [optional] 
**ListDisplayText** | **string** |  | [optional] 
**Name** | **string** |  | [optional] 
**Notes** | **string** |        User requirements:    Security Level(s): Admin, Manager (View Tasks) | [optional] 
**TaskCode** | **string** |  | [optional] 
**UseCompanyBillingRate** | **bool?** |        User requirements:    Security Level(s): Admin, Manager (View Tasks)    Security Level(s): Admin, Manager (View Billingrates)    Security Level(s): Admin, Manager (Add/Edit Tasks)       Company requirements:    Billing rate model(s): Task, Task x User, Task x Job | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

