# IO.Swagger.Model.GetLabelingEventModel
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**EntityIDs** | **string** |  | [optional] 
**EntityType** | **string** |  | [optional] 
**ID** | **string** |  | [optional] 
**LabelID** | **string** |  | [optional] 
**LegacyID** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

