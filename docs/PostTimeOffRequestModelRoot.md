# IO.Swagger.Model.PostTimeOffRequestModelRoot
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Dates** | [**List&lt;PostTimeOffRequestDateModelDatesPostTimeOffRequestModelRoot&gt;**](PostTimeOffRequestDateModelDatesPostTimeOffRequestModelRoot.md) |  | 
**Notes** | **string** |  | [optional] 
**RequestedByUserID** | **string** |  | [optional] 
**TimeOffTypeID** | **string** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

