# IO.Swagger.Model.GetEmploymentTypeModel
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**AccountingPackageID** | **string** |  | [optional] 
**HolidayTypeIDs** | **List&lt;string&gt;** |  | [optional] 
**ID** | **string** |  | [optional] 
**IsActive** | **bool?** |  | [optional] 
**LegacyID** | **string** |  | [optional] 
**Name** | **string** |  | [optional] 
**Notes** | **string** |  | [optional] 
**TimeOffTypeIDs** | **List&lt;string&gt;** |  | [optional] 
**Users** | [**List&lt;GetBasicUserModel&gt;**](GetBasicUserModel.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

