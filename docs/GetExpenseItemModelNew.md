# IO.Swagger.Model.GetExpenseItemModelNew
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Amount** | **double?** |  | [optional] 
**AmountForeign** | **double?** |  | [optional] 
**AmountForeignCurrency** | **string** |  | [optional] 
**AmountForeignExchangeRate** | **double?** |  | [optional] 
**Billable** | **bool?** |  | [optional] 
**Comment** | **string** |  | [optional] 
**Currency** | **string** |  | [optional] 
**Description** | **string** |  | [optional] 
**ExpenseDate** | **string** |  | [optional] 
**ExpenseSheet** | [**GetBasicExpenseSheetModelExpenseSheetGetExpenseItemModelNew**](GetBasicExpenseSheetModelExpenseSheetGetExpenseItemModelNew.md) |        Request requirements:    Verbose&#x3D;true | [optional] 
**ExpenseSheetID** | **string** |  | [optional] 
**ExpenseType** | [**GetBasicExpenseTypeModelExpenseTypeGetExpenseItemModelNew**](GetBasicExpenseTypeModelExpenseTypeGetExpenseItemModelNew.md) |        Request requirements:    Verbose&#x3D;true | [optional] 
**ExpenseTypeID** | **string** |  | [optional] 
**HasForeignCurrency** | **bool?** |  | [optional] 
**ID** | **string** |  | [optional] 
**Job** | [**GetBasicJobModelJobGetExpenseItemModelNew**](GetBasicJobModelJobGetExpenseItemModelNew.md) |        Request requirements:    Verbose&#x3D;true | [optional] 
**JobID** | **string** |  | [optional] 
**PaymentType** | [**GetBasicPaymentTypeModelPaymentTypeGetExpenseItemModelNew**](GetBasicPaymentTypeModelPaymentTypeGetExpenseItemModelNew.md) |        Request requirements:    Verbose&#x3D;true | [optional] 
**PaymentTypeID** | **string** |  | [optional] 
**Quantity** | **double?** |  | [optional] 
**Rate** | **double?** |  | [optional] 
**ReceiptURL** | **string** |  | [optional] 
**Reimbursable** | **bool?** |  | [optional] 
**UserID** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

