# IO.Swagger.Model.GetOvertimeRulePresetModel
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** |  | [optional] 
**RulePreset** | **string** |  | [optional] 
**Rules** | [**List&lt;IOvertimeRule&gt;**](IOvertimeRule.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

