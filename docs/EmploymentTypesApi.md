# IO.Swagger.Api.EmploymentTypesApi

All URIs are relative to *https://api.clicktime.com/V2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**AddEmploymentTypeHolidayType**](EmploymentTypesApi.md#addemploymenttypeholidaytype) | **POST** /EmploymentTypes/{employmentTypeID}/HolidayTypes | Add a Holiday Type to an Employment Type.
[**AddEmploymentTypeTimeOffType**](EmploymentTypesApi.md#addemploymenttypetimeofftype) | **POST** /EmploymentTypes/{employmentTypeID}/TimeOffTypes | Add time off type to employment type
[**AddEmploymentTypeUsers**](EmploymentTypesApi.md#addemploymenttypeusers) | **POST** /EmploymentTypes/{employmentTypeID}/Users | Add Users to an Employment Type.
[**CreateEmploymentType**](EmploymentTypesApi.md#createemploymenttype) | **POST** /EmploymentTypes | Create an Employment Type.
[**GetDefaultEmploymentType**](EmploymentTypesApi.md#getdefaultemploymenttype) | **GET** /EmploymentTypes/new | Get a new Employment Type with default values.
[**GetEmploymentType**](EmploymentTypesApi.md#getemploymenttype) | **GET** /EmploymentTypes/{employmentTypeID} | Get an Employment Type.
[**ListEmploymentTypeHolidayTypes**](EmploymentTypesApi.md#listemploymenttypeholidaytypes) | **GET** /EmploymentTypes/{employmentTypeID}/HolidayTypes | Get available Holiday Types.
[**ListEmploymentTypeTimeOffTypes**](EmploymentTypesApi.md#listemploymenttypetimeofftypes) | **GET** /EmploymentTypes/{employmentTypeID}/TimeOffTypes | Get available Time Off Types.
[**ListEmploymentTypeUsers**](EmploymentTypesApi.md#listemploymenttypeusers) | **GET** /EmploymentTypes/{employmentTypeID}/Users | Get active Users for an Employment Type.
[**ListEmploymentTypes**](EmploymentTypesApi.md#listemploymenttypes) | **GET** /EmploymentTypes | Get all Employment Types.
[**RemoveEmploymentTypeHolidayType**](EmploymentTypesApi.md#removeemploymenttypeholidaytype) | **DELETE** /EmploymentTypes/{employmentTypeID}/HolidayTypes/{holidayTypeID} | Remove a Holiday Type from an Employment Type.
[**RemoveEmploymentTypeTimeOffType**](EmploymentTypesApi.md#removeemploymenttypetimeofftype) | **DELETE** /EmploymentTypes/{employmentTypeID}/TimeOffTypes/{timeOffTypeID} | Remove a Time Off Type from an Employment Type.
[**UpdateEmploymentType**](EmploymentTypesApi.md#updateemploymenttype) | **PATCH** /EmploymentTypes/{employmentTypeID} | Update an Employment Type.
[**UpdateEmploymentTypeHolidayTypes**](EmploymentTypesApi.md#updateemploymenttypeholidaytypes) | **PUT** /EmploymentTypes/{employmentTypeID}/HolidayTypes | Set available Holiday Types.
[**UpdateEmploymentTypeTimeOffTypes**](EmploymentTypesApi.md#updateemploymenttypetimeofftypes) | **PUT** /EmploymentTypes/{employmentTypeID}/TimeOffTypes | Set available Time Off Types.


<a name="addemploymenttypeholidaytype"></a>
# **AddEmploymentTypeHolidayType**
> ResponseBodyListSystemString AddEmploymentTypeHolidayType (string employmentTypeID, PostHolidayTypeAssociationModelRoot model)

Add a Holiday Type to an Employment Type.

       User requirements:    Admin, or Manager with permissions to Add/Edit Employment Types.       Company requirements:    Leave Type List Control must be restricted by Employment Type.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class AddEmploymentTypeHolidayTypeExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new EmploymentTypesApi();
            var employmentTypeID = employmentTypeID_example;  // string |       Required
            var model = new PostHolidayTypeAssociationModelRoot(); // PostHolidayTypeAssociationModelRoot |       Required

            try
            {
                // Add a Holiday Type to an Employment Type.
                ResponseBodyListSystemString result = apiInstance.AddEmploymentTypeHolidayType(employmentTypeID, model);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling EmploymentTypesApi.AddEmploymentTypeHolidayType: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **employmentTypeID** | **string**|       Required | 
 **model** | [**PostHolidayTypeAssociationModelRoot**](PostHolidayTypeAssociationModelRoot.md)|       Required | 

### Return type

[**ResponseBodyListSystemString**](ResponseBodyListSystemString.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded, application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="addemploymenttypetimeofftype"></a>
# **AddEmploymentTypeTimeOffType**
> ResponseBodyListSystemString AddEmploymentTypeTimeOffType (string employmentTypeID, PostTimeOffTypeAssociationModelRoot model)

Add time off type to employment type

       User requirements:    Admin, or Manager with permissions to Add/Edit Employment Types.       Company requirements:    Leave Type List Control must be restricted by Employment Type.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class AddEmploymentTypeTimeOffTypeExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new EmploymentTypesApi();
            var employmentTypeID = employmentTypeID_example;  // string |       Required
            var model = new PostTimeOffTypeAssociationModelRoot(); // PostTimeOffTypeAssociationModelRoot |       Required

            try
            {
                // Add time off type to employment type
                ResponseBodyListSystemString result = apiInstance.AddEmploymentTypeTimeOffType(employmentTypeID, model);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling EmploymentTypesApi.AddEmploymentTypeTimeOffType: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **employmentTypeID** | **string**|       Required | 
 **model** | [**PostTimeOffTypeAssociationModelRoot**](PostTimeOffTypeAssociationModelRoot.md)|       Required | 

### Return type

[**ResponseBodyListSystemString**](ResponseBodyListSystemString.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded, application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="addemploymenttypeusers"></a>
# **AddEmploymentTypeUsers**
> void AddEmploymentTypeUsers (string employmentTypeID, List<string> userIDs)

Add Users to an Employment Type.

       User requirements:    Admin, or Manager with permissions to Add/Edit People.    Admin, or Manager with permissions to View Employment Types.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class AddEmploymentTypeUsersExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new EmploymentTypesApi();
            var employmentTypeID = employmentTypeID_example;  // string |       Required
            var userIDs = ;  // List<string> |       Required

            try
            {
                // Add Users to an Employment Type.
                apiInstance.AddEmploymentTypeUsers(employmentTypeID, userIDs);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling EmploymentTypesApi.AddEmploymentTypeUsers: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **employmentTypeID** | **string**|       Required | 
 **userIDs** | **List&lt;string&gt;**|       Required | 

### Return type

void (empty response body)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded, application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="createemploymenttype"></a>
# **CreateEmploymentType**
> ResponseBodyGetEmploymentTypeModelRoot CreateEmploymentType (PostEmploymentTypeModelRoot model)

Create an Employment Type.

       User requirements:    Admin, or Manager with permissions to Add/Edit Employment Types.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class CreateEmploymentTypeExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new EmploymentTypesApi();
            var model = new PostEmploymentTypeModelRoot(); // PostEmploymentTypeModelRoot |       Required

            try
            {
                // Create an Employment Type.
                ResponseBodyGetEmploymentTypeModelRoot result = apiInstance.CreateEmploymentType(model);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling EmploymentTypesApi.CreateEmploymentType: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **model** | [**PostEmploymentTypeModelRoot**](PostEmploymentTypeModelRoot.md)|       Required | 

### Return type

[**ResponseBodyGetEmploymentTypeModelRoot**](ResponseBodyGetEmploymentTypeModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded, application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getdefaultemploymenttype"></a>
# **GetDefaultEmploymentType**
> ResponseBodyGetEmploymentTypeModelNew GetDefaultEmploymentType ()

Get a new Employment Type with default values.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class GetDefaultEmploymentTypeExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new EmploymentTypesApi();

            try
            {
                // Get a new Employment Type with default values.
                ResponseBodyGetEmploymentTypeModelNew result = apiInstance.GetDefaultEmploymentType();
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling EmploymentTypesApi.GetDefaultEmploymentType: " + e.Message );
            }
        }
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ResponseBodyGetEmploymentTypeModelNew**](ResponseBodyGetEmploymentTypeModelNew.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getemploymenttype"></a>
# **GetEmploymentType**
> ResponseBodyGetEmploymentTypeModelRoot GetEmploymentType (string employmentTypeID)

Get an Employment Type.

       User requirements:    User must be a Manager or an Admin.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class GetEmploymentTypeExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new EmploymentTypesApi();
            var employmentTypeID = employmentTypeID_example;  // string |       Required

            try
            {
                // Get an Employment Type.
                ResponseBodyGetEmploymentTypeModelRoot result = apiInstance.GetEmploymentType(employmentTypeID);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling EmploymentTypesApi.GetEmploymentType: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **employmentTypeID** | **string**|       Required | 

### Return type

[**ResponseBodyGetEmploymentTypeModelRoot**](ResponseBodyGetEmploymentTypeModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="listemploymenttypeholidaytypes"></a>
# **ListEmploymentTypeHolidayTypes**
> ResponseBodyListSystemString ListEmploymentTypeHolidayTypes (string employmentTypeID)

Get available Holiday Types.

       User requirements:    Admin, or Manager with permissions to View Employment Types.       Company requirements:    Leave Type List Control must be restricted by Employment Type.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ListEmploymentTypeHolidayTypesExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new EmploymentTypesApi();
            var employmentTypeID = employmentTypeID_example;  // string |       Required

            try
            {
                // Get available Holiday Types.
                ResponseBodyListSystemString result = apiInstance.ListEmploymentTypeHolidayTypes(employmentTypeID);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling EmploymentTypesApi.ListEmploymentTypeHolidayTypes: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **employmentTypeID** | **string**|       Required | 

### Return type

[**ResponseBodyListSystemString**](ResponseBodyListSystemString.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="listemploymenttypetimeofftypes"></a>
# **ListEmploymentTypeTimeOffTypes**
> ResponseBodyListSystemString ListEmploymentTypeTimeOffTypes (string employmentTypeID)

Get available Time Off Types.

       User requirements:    Admin, or Manager with permissions to View Employment Types.       Company requirements:    Leave Type List Control must be restricted by Employment Type.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ListEmploymentTypeTimeOffTypesExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new EmploymentTypesApi();
            var employmentTypeID = employmentTypeID_example;  // string |       Required

            try
            {
                // Get available Time Off Types.
                ResponseBodyListSystemString result = apiInstance.ListEmploymentTypeTimeOffTypes(employmentTypeID);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling EmploymentTypesApi.ListEmploymentTypeTimeOffTypes: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **employmentTypeID** | **string**|       Required | 

### Return type

[**ResponseBodyListSystemString**](ResponseBodyListSystemString.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="listemploymenttypeusers"></a>
# **ListEmploymentTypeUsers**
> ResponseBodyListSystemString ListEmploymentTypeUsers (string employmentTypeID)

Get active Users for an Employment Type.

       User requirements:    Admin, or Manager with permissions to View Employment Types.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ListEmploymentTypeUsersExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new EmploymentTypesApi();
            var employmentTypeID = employmentTypeID_example;  // string |       Required

            try
            {
                // Get active Users for an Employment Type.
                ResponseBodyListSystemString result = apiInstance.ListEmploymentTypeUsers(employmentTypeID);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling EmploymentTypesApi.ListEmploymentTypeUsers: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **employmentTypeID** | **string**|       Required | 

### Return type

[**ResponseBodyListSystemString**](ResponseBodyListSystemString.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="listemploymenttypes"></a>
# **ListEmploymentTypes**
> ResponseBodyListGetEmploymentTypeModelRoot ListEmploymentTypes (List<string> ID = null, List<bool?> isActive = null, int? limit = null, int? offset = null)

Get all Employment Types.

       User requirements:    User must be a Manager or an Admin.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ListEmploymentTypesExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new EmploymentTypesApi();
            var ID = new List<string>(); // List<string> |  (optional) 
            var isActive = new List<bool?>(); // List<bool?> |  (optional) 
            var limit = 56;  // int? |  (optional) 
            var offset = 56;  // int? |  (optional) 

            try
            {
                // Get all Employment Types.
                ResponseBodyListGetEmploymentTypeModelRoot result = apiInstance.ListEmploymentTypes(ID, isActive, limit, offset);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling EmploymentTypesApi.ListEmploymentTypes: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ID** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **isActive** | [**List&lt;bool?&gt;**](bool?.md)|  | [optional] 
 **limit** | **int?**|  | [optional] 
 **offset** | **int?**|  | [optional] 

### Return type

[**ResponseBodyListGetEmploymentTypeModelRoot**](ResponseBodyListGetEmploymentTypeModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="removeemploymenttypeholidaytype"></a>
# **RemoveEmploymentTypeHolidayType**
> ResponseBodyListSystemString RemoveEmploymentTypeHolidayType (string employmentTypeID, string holidayTypeID)

Remove a Holiday Type from an Employment Type.

       User requirements:    Admin, or Manager with permissions to Add/Edit Employment Types.       Company requirements:    Leave Type List Control must be restricted by Employment Type.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class RemoveEmploymentTypeHolidayTypeExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new EmploymentTypesApi();
            var employmentTypeID = employmentTypeID_example;  // string |       Required
            var holidayTypeID = holidayTypeID_example;  // string |       Required

            try
            {
                // Remove a Holiday Type from an Employment Type.
                ResponseBodyListSystemString result = apiInstance.RemoveEmploymentTypeHolidayType(employmentTypeID, holidayTypeID);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling EmploymentTypesApi.RemoveEmploymentTypeHolidayType: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **employmentTypeID** | **string**|       Required | 
 **holidayTypeID** | **string**|       Required | 

### Return type

[**ResponseBodyListSystemString**](ResponseBodyListSystemString.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="removeemploymenttypetimeofftype"></a>
# **RemoveEmploymentTypeTimeOffType**
> ResponseBodyListSystemString RemoveEmploymentTypeTimeOffType (string employmentTypeID, string timeOffTypeID)

Remove a Time Off Type from an Employment Type.

       User requirements:    Admin, or Manager with permissions to Add/Edit Employment Types.       Company requirements:    Leave Type List Control must be restricted by Employment Type.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class RemoveEmploymentTypeTimeOffTypeExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new EmploymentTypesApi();
            var employmentTypeID = employmentTypeID_example;  // string |       Required
            var timeOffTypeID = timeOffTypeID_example;  // string |       Required

            try
            {
                // Remove a Time Off Type from an Employment Type.
                ResponseBodyListSystemString result = apiInstance.RemoveEmploymentTypeTimeOffType(employmentTypeID, timeOffTypeID);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling EmploymentTypesApi.RemoveEmploymentTypeTimeOffType: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **employmentTypeID** | **string**|       Required | 
 **timeOffTypeID** | **string**|       Required | 

### Return type

[**ResponseBodyListSystemString**](ResponseBodyListSystemString.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="updateemploymenttype"></a>
# **UpdateEmploymentType**
> ResponseBodyGetEmploymentTypeModelRoot UpdateEmploymentType (string employmentTypeID, PatchEmploymentTypeModelRoot model)

Update an Employment Type.

       User requirements:    Admin, or Manager with permissions to Add/Edit Employment Types.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class UpdateEmploymentTypeExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new EmploymentTypesApi();
            var employmentTypeID = employmentTypeID_example;  // string |       Required
            var model = new PatchEmploymentTypeModelRoot(); // PatchEmploymentTypeModelRoot |       Required

            try
            {
                // Update an Employment Type.
                ResponseBodyGetEmploymentTypeModelRoot result = apiInstance.UpdateEmploymentType(employmentTypeID, model);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling EmploymentTypesApi.UpdateEmploymentType: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **employmentTypeID** | **string**|       Required | 
 **model** | [**PatchEmploymentTypeModelRoot**](PatchEmploymentTypeModelRoot.md)|       Required | 

### Return type

[**ResponseBodyGetEmploymentTypeModelRoot**](ResponseBodyGetEmploymentTypeModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded, application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="updateemploymenttypeholidaytypes"></a>
# **UpdateEmploymentTypeHolidayTypes**
> ResponseBodyListSystemString UpdateEmploymentTypeHolidayTypes (string employmentTypeID, List<string> holidayTypeIDs)

Set available Holiday Types.

       User requirements:    Admin, or Manager with permissions to Add/Edit Employment Types.       Company requirements:    Leave Type List Control must be restricted by Employment Type.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class UpdateEmploymentTypeHolidayTypesExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new EmploymentTypesApi();
            var employmentTypeID = employmentTypeID_example;  // string |       Required
            var holidayTypeIDs = ;  // List<string> |       Required

            try
            {
                // Set available Holiday Types.
                ResponseBodyListSystemString result = apiInstance.UpdateEmploymentTypeHolidayTypes(employmentTypeID, holidayTypeIDs);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling EmploymentTypesApi.UpdateEmploymentTypeHolidayTypes: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **employmentTypeID** | **string**|       Required | 
 **holidayTypeIDs** | **List&lt;string&gt;**|       Required | 

### Return type

[**ResponseBodyListSystemString**](ResponseBodyListSystemString.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded, application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="updateemploymenttypetimeofftypes"></a>
# **UpdateEmploymentTypeTimeOffTypes**
> ResponseBodyListSystemString UpdateEmploymentTypeTimeOffTypes (string employmentTypeID, List<string> timeOffTypeIDs)

Set available Time Off Types.

       User requirements:    Admin, or Manager with permissions to Add/Edit Employment Types.       Company requirements:    Leave Type List Control must be restricted by Employment Type.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class UpdateEmploymentTypeTimeOffTypesExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new EmploymentTypesApi();
            var employmentTypeID = employmentTypeID_example;  // string |       Required
            var timeOffTypeIDs = ;  // List<string> |       Required

            try
            {
                // Set available Time Off Types.
                ResponseBodyListSystemString result = apiInstance.UpdateEmploymentTypeTimeOffTypes(employmentTypeID, timeOffTypeIDs);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling EmploymentTypesApi.UpdateEmploymentTypeTimeOffTypes: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **employmentTypeID** | **string**|       Required | 
 **timeOffTypeIDs** | **List&lt;string&gt;**|       Required | 

### Return type

[**ResponseBodyListSystemString**](ResponseBodyListSystemString.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded, application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

