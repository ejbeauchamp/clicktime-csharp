# IO.Swagger.Model.PatchAllocationModelRoot
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**EstHours** | **double?** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

