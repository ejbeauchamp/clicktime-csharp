# IO.Swagger.Model.PostClientModelRoot
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**AccountingPackageID** | **string** |  | [optional] 
**BillingRate** | **double?** |        User requirements:    Security Level(s): Admin, Manager (Add/Edit Billingrates)       Company requirements:    Billing rate model(s): User x Client | [optional] 
**ClientNumber** | **string** |  | [optional] 
**CustomFields** | **Dictionary&lt;string, Object&gt;** |        Company requirements:    Custom Fields must be configured for Clients. | [optional] 
**IsActive** | **bool?** |  | [optional] 
**Name** | **string** |  | 
**Notes** | **string** |  | [optional] 
**SecondaryBillingRateMode** | **string** |        User requirements:    Security Level(s): Admin, Manager (Add/Edit Billingrates)       Company requirements:    Billing rate model(s): User x Client | [optional] 
**ShortName** | **string** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

