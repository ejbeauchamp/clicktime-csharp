# IO.Swagger.Model.GetHolidayTypeModel
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**AccountingPackageID** | **string** |  | [optional] 
**Dates** | [**List&lt;GetHolidayModel&gt;**](GetHolidayModel.md) |  | [optional] 
**EmploymentTypeIDs** | **List&lt;string&gt;** |  | [optional] 
**ID** | **string** |  | [optional] 
**IsActive** | **bool?** |  | [optional] 
**LegacyID** | **string** |  | [optional] 
**Name** | **string** |  | [optional] 
**Notes** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

