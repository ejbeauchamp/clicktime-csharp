# IO.Swagger.Model.GetBillingRateModelRoot
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**BillingRate** | **double?** |  | [optional] 
**ID** | **string** |  | [optional] 
**IsBillable** | **bool?** |        Company requirements:    Billability by: Task    Billing rate model(s): Task x User, Task x Job | [optional] 
**LegacyID** | **string** |        Request requirements:    CTLegacyScramble&#x3D;true | [optional] 
**Name** | **string** |  | [optional] 
**Type** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

