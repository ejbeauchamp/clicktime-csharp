# IO.Swagger.Api.TimersApi

All URIs are relative to *https://api.clicktime.com/V2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**CreateMyTimeEntryTimer**](TimersApi.md#createmytimeentrytimer) | **POST** /Me/TimeEntries/{timeEntryID}/Timer | 
[**DeleteMyTimeEntryTimer**](TimersApi.md#deletemytimeentrytimer) | **DELETE** /Me/TimeEntries/{timeEntryID}/Timer | 
[**GetMyTimeEntryTimer**](TimersApi.md#getmytimeentrytimer) | **GET** /Me/TimeEntries/{timeEntryID}/Timer | 
[**GetMyTimer**](TimersApi.md#getmytimer) | **GET** /Me/Timers/{timerId} | 
[**ListMyTimers**](TimersApi.md#listmytimers) | **GET** /Me/Timers | 
[**UpdateMyTimeEntryTimer**](TimersApi.md#updatemytimeentrytimer) | **PUT** /Me/TimeEntries/{timeEntryID}/Timer | 


<a name="createmytimeentrytimer"></a>
# **CreateMyTimeEntryTimer**
> ResponseBodyGetTimerModel CreateMyTimeEntryTimer (string timeEntryID, PostTimerModelRoot model)



### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class CreateMyTimeEntryTimerExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TimersApi();
            var timeEntryID = timeEntryID_example;  // string |       Required
            var model = new PostTimerModelRoot(); // PostTimerModelRoot |       Required

            try
            {
                ResponseBodyGetTimerModel result = apiInstance.CreateMyTimeEntryTimer(timeEntryID, model);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TimersApi.CreateMyTimeEntryTimer: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **timeEntryID** | **string**|       Required | 
 **model** | [**PostTimerModelRoot**](PostTimerModelRoot.md)|       Required | 

### Return type

[**ResponseBodyGetTimerModel**](ResponseBodyGetTimerModel.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded, application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="deletemytimeentrytimer"></a>
# **DeleteMyTimeEntryTimer**
> void DeleteMyTimeEntryTimer (string timeEntryID)



### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class DeleteMyTimeEntryTimerExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TimersApi();
            var timeEntryID = timeEntryID_example;  // string |       Required

            try
            {
                apiInstance.DeleteMyTimeEntryTimer(timeEntryID);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TimersApi.DeleteMyTimeEntryTimer: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **timeEntryID** | **string**|       Required | 

### Return type

void (empty response body)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getmytimeentrytimer"></a>
# **GetMyTimeEntryTimer**
> ResponseBodyGetTimerModelRoot GetMyTimeEntryTimer (string timeEntryID)



### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class GetMyTimeEntryTimerExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TimersApi();
            var timeEntryID = timeEntryID_example;  // string |       Required

            try
            {
                ResponseBodyGetTimerModelRoot result = apiInstance.GetMyTimeEntryTimer(timeEntryID);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TimersApi.GetMyTimeEntryTimer: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **timeEntryID** | **string**|       Required | 

### Return type

[**ResponseBodyGetTimerModelRoot**](ResponseBodyGetTimerModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getmytimer"></a>
# **GetMyTimer**
> ResponseBodyGetTimerModelRoot GetMyTimer (string timerId)



### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class GetMyTimerExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TimersApi();
            var timerId = timerId_example;  // string |       Required

            try
            {
                ResponseBodyGetTimerModelRoot result = apiInstance.GetMyTimer(timerId);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TimersApi.GetMyTimer: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **timerId** | **string**|       Required | 

### Return type

[**ResponseBodyGetTimerModelRoot**](ResponseBodyGetTimerModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="listmytimers"></a>
# **ListMyTimers**
> ResponseBodyListGetTimerModelRoot ListMyTimers (List<string> ID = null, List<string> timeEntryID = null, string fromDate = null, string toDate = null, List<bool?> isRunning = null, int? limit = null, int? offset = null)



### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ListMyTimersExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TimersApi();
            var ID = new List<string>(); // List<string> |  (optional) 
            var timeEntryID = new List<string>(); // List<string> |  (optional) 
            var fromDate = fromDate_example;  // string |  (optional) 
            var toDate = toDate_example;  // string |  (optional) 
            var isRunning = new List<bool?>(); // List<bool?> |  (optional) 
            var limit = 56;  // int? |  (optional) 
            var offset = 56;  // int? |  (optional) 

            try
            {
                ResponseBodyListGetTimerModelRoot result = apiInstance.ListMyTimers(ID, timeEntryID, fromDate, toDate, isRunning, limit, offset);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TimersApi.ListMyTimers: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ID** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **timeEntryID** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **fromDate** | **string**|  | [optional] 
 **toDate** | **string**|  | [optional] 
 **isRunning** | [**List&lt;bool?&gt;**](bool?.md)|  | [optional] 
 **limit** | **int?**|  | [optional] 
 **offset** | **int?**|  | [optional] 

### Return type

[**ResponseBodyListGetTimerModelRoot**](ResponseBodyListGetTimerModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="updatemytimeentrytimer"></a>
# **UpdateMyTimeEntryTimer**
> ResponseBodyGetTimerModel UpdateMyTimeEntryTimer (string timeEntryID, PostTimerModelRoot model)



### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class UpdateMyTimeEntryTimerExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TimersApi();
            var timeEntryID = timeEntryID_example;  // string |       Required
            var model = new PostTimerModelRoot(); // PostTimerModelRoot |       Required

            try
            {
                ResponseBodyGetTimerModel result = apiInstance.UpdateMyTimeEntryTimer(timeEntryID, model);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TimersApi.UpdateMyTimeEntryTimer: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **timeEntryID** | **string**|       Required | 
 **model** | [**PostTimerModelRoot**](PostTimerModelRoot.md)|       Required | 

### Return type

[**ResponseBodyGetTimerModel**](ResponseBodyGetTimerModel.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded, application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

