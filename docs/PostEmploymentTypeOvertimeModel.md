# IO.Swagger.Model.PostEmploymentTypeOvertimeModel
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**IncludeTimeOff** | **bool?** |  | [optional] 
**IsActive** | **bool?** |  | [optional] 
**Notes** | **string** |  | [optional] 
**RulePreset** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

