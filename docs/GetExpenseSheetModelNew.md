# IO.Swagger.Model.GetExpenseSheetModelNew
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ApprovedByUserID** | **string** |  | [optional] 
**Check** | **string** |  | [optional] 
**Description** | **string** |  | [optional] 
**EnableForeignCurrency** | **bool?** |  | [optional] 
**ExpenseSheetDate** | **string** |  | [optional] 
**ID** | **string** |  | [optional] 
**Paid** | **bool?** |  | [optional] 
**ReimbursableAmount** | **double?** |  | [optional] 
**RejectedByUserID** | **string** |  | [optional] 
**Status** | **string** |  | [optional] 
**SubmittedByUserID** | **string** |  | [optional] 
**Title** | **string** |  | [optional] 
**TotalAmount** | **double?** |  | [optional] 
**TrackingID** | **string** |  | [optional] 
**UserID** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

