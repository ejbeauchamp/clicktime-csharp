# IO.Swagger.Model.GetOvertimeModel
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**CompanyID** | **string** |  | [optional] 
**CreatedDate** | **string** |  | [optional] 
**EmploymentTypeID** | **string** |  | [optional] 
**ID** | **string** |  | [optional] 
**IncludeTimeOff** | **bool?** |  | [optional] 
**IsActive** | **bool?** |  | [optional] 
**LegacyEmploymentTypeID** | **string** |  | [optional] 
**LegacyID** | **string** |  | [optional] 
**ModifiedDate** | **string** |  | [optional] 
**Notes** | **string** |  | [optional] 
**OvertimeRules** | [**List&lt;GetOvertimeRuleModel&gt;**](GetOvertimeRuleModel.md) |  | [optional] 
**RulePreset** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

