# IO.Swagger.Model.GetClientModelMe
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ClientNumber** | **string** |  | [optional] 
**ID** | **string** |  | [optional] 
**IsActive** | **bool?** |  | [optional] 
**LegacyID** | **string** |        Request requirements:    CTLegacyScramble&#x3D;true | [optional] 
**ListDisplayText** | **string** |  | [optional] 
**Name** | **string** |  | [optional] 
**ShortName** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

