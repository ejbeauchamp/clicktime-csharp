# IO.Swagger.Model.PostEmploymentTypeAssociationModelRoot
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**EmploymentTypeID** | **string** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

