# IO.Swagger.Model.PostBasicRuleDefinitionModel
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Multiplier** | **double?** |  | 
**PayCode** | **string** |  | [optional] 
**RuleType** | **string** |  | 
**Threshold** | **double?** |  | 
**TimeSpan** | **string** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

