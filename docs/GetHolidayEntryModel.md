# IO.Swagger.Model.GetHolidayEntryModel
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Date** | **string** |  | [optional] 
**HolidayType** | [**GetBasicTimeOffTypeModel**](GetBasicTimeOffTypeModel.md) |  | [optional] 
**HolidayTypeID** | **string** |  | [optional] 
**Hours** | **double?** |  | [optional] 
**ID** | **string** |  | [optional] 
**LegacyID** | **string** |  | [optional] 
**Notes** | **string** |  | [optional] 
**User** | [**GetBasicUserModel**](GetBasicUserModel.md) |  | [optional] 
**UserID** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

