# IO.Swagger.Model.PostOvertimeModel
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**EmploymentTypeID** | **string** |  | 
**IncludeTimeOff** | **bool?** |  | [optional] 
**IsActive** | **bool?** |  | [optional] 
**Notes** | **string** |  | [optional] 
**RulePreset** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

