# IO.Swagger.Model.GetOvertimeRuleModelOvertimeRulesGetOvertimeModelNew
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**CreatedDate** | **string** |  | [optional] 
**ID** | **string** |  | [optional] 
**IsActive** | **bool?** |  | [optional] 
**ModifiedDate** | **string** |  | [optional] 
**OvertimeDefinitionID** | **string** |  | [optional] 
**RuleDefinition** | [**GetBasicRuleDefinitionModelRuleDefinitionGetOvertimeRuleModelOvertimeRulesGetOvertimeModelNew**](GetBasicRuleDefinitionModelRuleDefinitionGetOvertimeRuleModelOvertimeRulesGetOvertimeModelNew.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

