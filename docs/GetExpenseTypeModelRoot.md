# IO.Swagger.Model.GetExpenseTypeModelRoot
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**AccountingPackageID** | **string** |        User requirements:    Security Level(s): Admin, Manager (Add/Edit Expense Types) | [optional] 
**Description** | **string** |        User requirements:    Security Level(s): Admin, Manager (Add/Edit Expense Types) | [optional] 
**ID** | **string** |  | [optional] 
**IsActive** | **bool?** |  | [optional] 
**LegacyID** | **string** |        Request requirements:    CTLegacyScramble&#x3D;true | [optional] 
**Name** | **string** |  | [optional] 
**Rate** | **double?** |  | [optional] 
**Type** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

