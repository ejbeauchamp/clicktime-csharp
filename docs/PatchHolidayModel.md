# IO.Swagger.Model.PatchHolidayModel
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Hours** | **double?** |  | [optional] 
**Notes** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

