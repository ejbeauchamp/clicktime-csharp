# IO.Swagger.Model.GetExpenseSheetModelMe
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Actions** | [**List&lt;GetActionModelExpenseSheetAction&gt;**](GetActionModelExpenseSheetAction.md) |        Request requirements:    Verbose&#x3D;true | [optional] 
**ApprovedBy** | **string** |        Request requirements:    Verbose&#x3D;true | [optional] 
**ApprovedByUserID** | **string** |  | [optional] 
**ApprovedDate** | **string** |        Request requirements:    Verbose&#x3D;true | [optional] 
**Check** | **string** |  | [optional] 
**Description** | **string** |  | [optional] 
**EnableForeignCurrency** | **bool?** |  | [optional] 
**ExpenseSheetDate** | **string** |  | [optional] 
**ID** | **string** |  | [optional] 
**LegacyID** | **string** |        Request requirements:    CTLegacyScramble&#x3D;true | [optional] 
**Paid** | **bool?** |  | [optional] 
**ReimbursableAmount** | **double?** |  | [optional] 
**RejectedBy** | **string** |        Request requirements:    Verbose&#x3D;true | [optional] 
**RejectedByUserID** | **string** |  | [optional] 
**RejectedDate** | **string** |        Request requirements:    Verbose&#x3D;true | [optional] 
**Status** | **string** |  | [optional] 
**SubmittedBy** | **string** |        Request requirements:    Verbose&#x3D;true | [optional] 
**SubmittedByUserID** | **string** |  | [optional] 
**SubmittedDate** | **string** |        Request requirements:    Verbose&#x3D;true | [optional] 
**Title** | **string** |  | [optional] 
**TotalAmount** | **double?** |  | [optional] 
**TrackingID** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

