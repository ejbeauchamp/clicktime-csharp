# IO.Swagger.Model.GetEmploymentTypeModelNew
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**AccountingPackageID** | **string** |        User requirements:    Security Level(s): Admin, Manager (View Employment Types) | [optional] 
**HolidayTypeIDs** | **List&lt;string&gt;** |        Request requirements:    Verbose&#x3D;true       Company requirements:    Leave Type Restrictions: By Employment Type | [optional] 
**ID** | **string** |  | [optional] 
**IsActive** | **bool?** |  | [optional] 
**Name** | **string** |  | [optional] 
**Notes** | **string** |        User requirements:    Security Level(s): Admin, Manager (View Employment Types) | [optional] 
**TimeOffTypeIDs** | **List&lt;string&gt;** |        Request requirements:    Verbose&#x3D;true       Company requirements:    Leave Type Restrictions: By Employment Type | [optional] 
**Users** | [**List&lt;GetBasicUserModelUsersGetEmploymentTypeModelNew&gt;**](GetBasicUserModelUsersGetEmploymentTypeModelNew.md) |        Request requirements:    Verbose&#x3D;true | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

