# IO.Swagger.Model.GetTimeOffModelNew
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Date** | **string** |  | [optional] 
**Hours** | **double?** |  | [optional] 
**ID** | **string** |  | [optional] 
**Notes** | **string** |  | [optional] 
**TimeOffRequestID** | **string** |  | [optional] 
**TimeOffTypeID** | **string** |  | [optional] 
**UserID** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

