# IO.Swagger.Model.PatchDivisionModelManage
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**AccountingPackageID** | **string** |  | [optional] 
**CustomFields** | **Dictionary&lt;string, Object&gt;** |        Company requirements:    Custom Fields must be configured for Divisions. | [optional] 
**IsActive** | **bool?** |  | [optional] 
**Name** | **string** |  | [optional] 
**Notes** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

