# IO.Swagger.Model.PatchCustomMessageModel
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ExpirationDate** | **string** |  | [optional] 
**IsActive** | **bool?** |  | [optional] 
**MessageText** | **string** |  | [optional] 
**Name** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

