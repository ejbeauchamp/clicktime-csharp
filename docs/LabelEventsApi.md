# IO.Swagger.Api.LabelEventsApi

All URIs are relative to *https://api.clicktime.com/V2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**CreateLabelEvent**](LabelEventsApi.md#createlabelevent) | **POST** /LabelEvents | Create a Label Event.
[**DeleteLabelEvent**](LabelEventsApi.md#deletelabelevent) | **DELETE** /LabelEvents/{labelEventID} | Delete a Label Event.
[**GetDefaultLabelEvent**](LabelEventsApi.md#getdefaultlabelevent) | **GET** /LabelEvents/new | Get a new Label Event with default values.
[**GetLabelEvent**](LabelEventsApi.md#getlabelevent) | **GET** /LabelEvents/{labelEventID} | Get a Label Event.
[**ListLabelEvents**](LabelEventsApi.md#listlabelevents) | **GET** /LabelEvents | Get Label Events.


<a name="createlabelevent"></a>
# **CreateLabelEvent**
> ResponseBodyGetLabelingEventModelRoot CreateLabelEvent (PostLabelingEventModelRoot model)

Create a Label Event.

       User requirements:    Admin, or Manager with permissions to Run Company Reports.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class CreateLabelEventExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new LabelEventsApi();
            var model = new PostLabelingEventModelRoot(); // PostLabelingEventModelRoot |       Required

            try
            {
                // Create a Label Event.
                ResponseBodyGetLabelingEventModelRoot result = apiInstance.CreateLabelEvent(model);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling LabelEventsApi.CreateLabelEvent: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **model** | [**PostLabelingEventModelRoot**](PostLabelingEventModelRoot.md)|       Required | 

### Return type

[**ResponseBodyGetLabelingEventModelRoot**](ResponseBodyGetLabelingEventModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded, application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="deletelabelevent"></a>
# **DeleteLabelEvent**
> void DeleteLabelEvent (string labelEventID)

Delete a Label Event.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class DeleteLabelEventExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new LabelEventsApi();
            var labelEventID = labelEventID_example;  // string |       Required

            try
            {
                // Delete a Label Event.
                apiInstance.DeleteLabelEvent(labelEventID);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling LabelEventsApi.DeleteLabelEvent: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **labelEventID** | **string**|       Required | 

### Return type

void (empty response body)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getdefaultlabelevent"></a>
# **GetDefaultLabelEvent**
> ResponseBodyGetLabelingEventModelNew GetDefaultLabelEvent ()

Get a new Label Event with default values.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class GetDefaultLabelEventExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new LabelEventsApi();

            try
            {
                // Get a new Label Event with default values.
                ResponseBodyGetLabelingEventModelNew result = apiInstance.GetDefaultLabelEvent();
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling LabelEventsApi.GetDefaultLabelEvent: " + e.Message );
            }
        }
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ResponseBodyGetLabelingEventModelNew**](ResponseBodyGetLabelingEventModelNew.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getlabelevent"></a>
# **GetLabelEvent**
> ResponseBodyGetLabelingEventModelRoot GetLabelEvent (string labelEventID)

Get a Label Event.

       User requirements:    Admin, or Manager with permissions to Run Company Reports.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class GetLabelEventExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new LabelEventsApi();
            var labelEventID = labelEventID_example;  // string |       Required

            try
            {
                // Get a Label Event.
                ResponseBodyGetLabelingEventModelRoot result = apiInstance.GetLabelEvent(labelEventID);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling LabelEventsApi.GetLabelEvent: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **labelEventID** | **string**|       Required | 

### Return type

[**ResponseBodyGetLabelingEventModelRoot**](ResponseBodyGetLabelingEventModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="listlabelevents"></a>
# **ListLabelEvents**
> ResponseBodyListGetLabelingEventModelRoot ListLabelEvents (List<string> labelID = null, int? limit = null, int? offset = null)

Get Label Events.

       User requirements:    Admin, or Manager with permissions to Run Company Reports.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ListLabelEventsExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new LabelEventsApi();
            var labelID = new List<string>(); // List<string> |  (optional) 
            var limit = 56;  // int? |  (optional) 
            var offset = 56;  // int? |  (optional) 

            try
            {
                // Get Label Events.
                ResponseBodyListGetLabelingEventModelRoot result = apiInstance.ListLabelEvents(labelID, limit, offset);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling LabelEventsApi.ListLabelEvents: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **labelID** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **limit** | **int?**|  | [optional] 
 **offset** | **int?**|  | [optional] 

### Return type

[**ResponseBodyListGetLabelingEventModelRoot**](ResponseBodyListGetLabelingEventModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

