# IO.Swagger.Model.GetAuditLogAction
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Action** | **string** |  | [optional] 
**ActionComment** | **string** |  | [optional] 
**FieldName** | **string** |  | [optional] 
**Item** | **string** |  | [optional] 
**ItemID** | **string** |  | [optional] 
**LegacyItemID** | **string** |  | [optional] 
**LegacyNewID** | **string** |  | [optional] 
**LegacyOriginalID** | **string** |  | [optional] 
**LogDate** | **string** |  | [optional] 
**LogID** | **int?** |  | [optional] 
**NewID** | **string** |  | [optional] 
**NewValue** | **string** |  | [optional] 
**OriginalID** | **string** |  | [optional] 
**OriginalValue** | **string** |  | [optional] 
**Timesheet** | [**GetLogTimesheetModel**](GetLogTimesheetModel.md) |  | [optional] 
**TransactionID** | **int?** |  | [optional] 
**UserID** | **string** |  | [optional] 
**UserName** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

