# IO.Swagger.Model.PatchCompanyModel
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ClientTermPlural** | **string** |  | [optional] 
**ClientTermSingular** | **string** |  | [optional] 
**DivisionTermPlural** | **string** |  | [optional] 
**DivisionTermSingular** | **string** |  | [optional] 
**JobTermPlural** | **string** |  | [optional] 
**JobTermSingular** | **string** |  | [optional] 
**PhaseTermPlural** | **string** |  | [optional] 
**PhaseTermSingular** | **string** |  | [optional] 
**SetupWizardComplete** | **bool?** | This property can only be set by Companies with a demo pricing plan. It can only be set to true and, once set, cannot be changed back. | [optional] 
**SubPhaseTermPlural** | **string** |  | [optional] 
**SubPhaseTermSingular** | **string** |  | [optional] 
**TaskTermPlural** | **string** |  | [optional] 
**TaskTermSingular** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

