# IO.Swagger.Api.CustomMessagesApi

All URIs are relative to *https://api.clicktime.com/V2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**GetCustomMessage**](CustomMessagesApi.md#getcustommessage) | **GET** /CustomMessages/{customMessageID} | Get a Custom Message.
[**ListCustomMessages**](CustomMessagesApi.md#listcustommessages) | **GET** /CustomMessages | Get Custom Messages.
[**UpdateCustomMessage**](CustomMessagesApi.md#updatecustommessage) | **PATCH** /CustomMessages/{customMessageID} | Update a Custom Message.


<a name="getcustommessage"></a>
# **GetCustomMessage**
> ResponseBodyGetCustomMessageModelRoot GetCustomMessage (string customMessageID)

Get a Custom Message.

       Company requirements:    Messaging Permission

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class GetCustomMessageExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new CustomMessagesApi();
            var customMessageID = customMessageID_example;  // string |       Required

            try
            {
                // Get a Custom Message.
                ResponseBodyGetCustomMessageModelRoot result = apiInstance.GetCustomMessage(customMessageID);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling CustomMessagesApi.GetCustomMessage: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customMessageID** | **string**|       Required | 

### Return type

[**ResponseBodyGetCustomMessageModelRoot**](ResponseBodyGetCustomMessageModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="listcustommessages"></a>
# **ListCustomMessages**
> ResponseBodyListGetCustomMessageModelRoot ListCustomMessages (List<string> ID = null, List<bool?> isActive = null, int? limit = null, int? offset = null)

Get Custom Messages.

       Company requirements:    Messaging Permission

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ListCustomMessagesExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new CustomMessagesApi();
            var ID = new List<string>(); // List<string> |  (optional) 
            var isActive = new List<bool?>(); // List<bool?> |  (optional) 
            var limit = 56;  // int? |  (optional) 
            var offset = 56;  // int? |  (optional) 

            try
            {
                // Get Custom Messages.
                ResponseBodyListGetCustomMessageModelRoot result = apiInstance.ListCustomMessages(ID, isActive, limit, offset);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling CustomMessagesApi.ListCustomMessages: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ID** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **isActive** | [**List&lt;bool?&gt;**](bool?.md)|  | [optional] 
 **limit** | **int?**|  | [optional] 
 **offset** | **int?**|  | [optional] 

### Return type

[**ResponseBodyListGetCustomMessageModelRoot**](ResponseBodyListGetCustomMessageModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="updatecustommessage"></a>
# **UpdateCustomMessage**
> ResponseBodyGetCustomMessageModelRoot UpdateCustomMessage (string customMessageID, PatchCustomMessageModelRoot model)

Update a Custom Message.

       User requirements:    User must be an Admin.       Company requirements:    Messaging Permission

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class UpdateCustomMessageExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new CustomMessagesApi();
            var customMessageID = customMessageID_example;  // string |       Required
            var model = new PatchCustomMessageModelRoot(); // PatchCustomMessageModelRoot |       Required

            try
            {
                // Update a Custom Message.
                ResponseBodyGetCustomMessageModelRoot result = apiInstance.UpdateCustomMessage(customMessageID, model);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling CustomMessagesApi.UpdateCustomMessage: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customMessageID** | **string**|       Required | 
 **model** | [**PatchCustomMessageModelRoot**](PatchCustomMessageModelRoot.md)|       Required | 

### Return type

[**ResponseBodyGetCustomMessageModelRoot**](ResponseBodyGetCustomMessageModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded, application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

