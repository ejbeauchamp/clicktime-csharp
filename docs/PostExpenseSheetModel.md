# IO.Swagger.Model.PostExpenseSheetModel
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Description** | **string** |  | [optional] 
**EnableForeignCurrency** | **bool?** |  | [optional] 
**ExpenseSheetDate** | **string** |  | 
**Title** | **string** |  | 
**TrackingID** | **string** |  | [optional] 
**UserID** | **string** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

