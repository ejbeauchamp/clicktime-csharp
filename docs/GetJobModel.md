# IO.Swagger.Model.GetJobModel
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**AccountingPackageID** | **string** |  | [optional] 
**BillingRate** | **double?** |  | [optional] 
**_Client** | [**GetBasicClientModel**](GetBasicClientModel.md) |  | [optional] 
**ClientID** | **string** |  | [optional] 
**CustomFields** | **Dictionary&lt;string, Object&gt;** |  | [optional] 
**ID** | **string** |  | [optional] 
**IsActive** | **bool?** |  | [optional] 
**IsBillable** | **bool?** |  | [optional] 
**JobNumber** | **string** |  | [optional] 
**LegacyID** | **string** |  | [optional] 
**LifetimeBudget** | **double?** |  | [optional] 
**ListDisplayName** | **string** |  | [optional] 
**ListDisplayText** | **string** |  | [optional] 
**MyJobs** | **bool?** |  | [optional] 
**Name** | **string** |  | [optional] 
**Notes** | **string** |  | [optional] 
**ProjectManager** | [**GetMinimalUserModel**](GetMinimalUserModel.md) |  | [optional] 
**ProjectManagerID** | **string** |  | [optional] 
**SecondaryBillingRateMode** | **string** |  | [optional] 
**TimeRequiresApproval** | **bool?** |  | [optional] 
**UseCompanyBillingRate** | **bool?** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

