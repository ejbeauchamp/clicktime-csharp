# IO.Swagger.Model.PostJobModelRoot
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**AccountingPackageID** | **string** |  | [optional] 
**BillingRate** | **double?** |        User requirements:    Security Level(s): Admin, Manager (Add/Edit Billingrates)       Company requirements:    Billing rate model(s): Job | [optional] 
**ClientID** | **string** |  | 
**CustomFields** | **Dictionary&lt;string, Object&gt;** |        Company requirements:    Custom Fields must be configured for Jobs. | [optional] 
**IsActive** | **bool?** |  | [optional] 
**IsBillable** | **bool?** |        Company requirements:    Billability by: Job | [optional] 
**JobNumber** | **string** |  | 
**Name** | **string** |  | 
**Notes** | **string** |  | [optional] 
**ProjectManagerID** | **string** | Required if TimeRequiresApproval is true. | [optional] 
**SecondaryBillingRateMode** | **string** |        User requirements:    Security Level(s): Admin, Manager (Add/Edit Billingrates)       Company requirements:    Billing rate model(s): Task x Job, User x Job | [optional] 
**TimeRequiresApproval** | **bool?** |        Company requirements:    Job Approvals: enabled | [optional] 
**UseCompanyBillingRate** | **bool?** |        User requirements:    Security Level(s): Admin, Manager (Add/Edit Billingrates)       Company requirements:    Billability by: Job | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

