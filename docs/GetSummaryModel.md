# IO.Swagger.Model.GetSummaryModel
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**BillableDollars** | **double?** |  | [optional] 
**BillableHoursWorked** | **double?** |  | [optional] 
**Cost** | **double?** |  | [optional] 
**HoursWorked** | **double?** |  | [optional] 
**ID** | **string** |  | [optional] 
**LegacyID** | **string** |  | [optional] 
**Name** | **string** |  | [optional] 
**SecondaryID** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

