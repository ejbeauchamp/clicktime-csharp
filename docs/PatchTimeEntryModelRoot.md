# IO.Swagger.Model.PatchTimeEntryModelRoot
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**BillingRate** | **double?** |        User requirements:    Security Level(s): Admin | [optional] 
**BreakTime** | **double?** |  | [optional] 
**Comment** | **string** |  | [optional] 
**CostRate** | **double?** |        User requirements:    Security Level(s): Admin | [optional] 
**CustomFields** | **Dictionary&lt;string, Object&gt;** |        Company requirements:    Custom Fields must be configured for Time Entries. | [optional] 
**EndTime** | **string** | Format: HH:MM (24-hour) | [optional] 
**Hours** | **double?** |  | [optional] 
**JobID** | **string** |  | [optional] 
**StartTime** | **string** | Format: HH:MM (24-hour) | [optional] 
**TaskID** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

