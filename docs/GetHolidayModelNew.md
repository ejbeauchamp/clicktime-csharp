# IO.Swagger.Model.GetHolidayModelNew
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Date** | **string** |  | [optional] 
**HolidayTypeID** | **string** |  | [optional] 
**Hours** | **double?** |  | [optional] 
**ID** | **string** |  | [optional] 
**Notes** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

