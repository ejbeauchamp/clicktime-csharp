# IO.Swagger.Model.GetReportTimeModel
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**BillingRate** | **double?** |  | [optional] 
**Comment** | **string** |  | [optional] 
**CostRate** | **double?** |  | [optional] 
**CustomFields** | **Dictionary&lt;string, Object&gt;** |  | [optional] 
**Date** | **string** |  | [optional] 
**Hours** | **double?** |  | [optional] 
**ID** | **string** |  | [optional] 
**IsBillable** | **bool?** |  | [optional] 
**JobID** | **string** |  | [optional] 
**LegacyID** | **string** |  | [optional] 
**TaskID** | **string** |  | [optional] 
**UserID** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

