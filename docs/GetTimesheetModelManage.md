# IO.Swagger.Model.GetTimesheetModelManage
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Actions** | [**List&lt;GetActionModelTimesheetAction&gt;**](GetActionModelTimesheetAction.md) | The actions listed in this endpoint’s response are generalized. To obtain  a more explicit set of available actions for a particular Timesheet ID, it  is recommended to use the appropriate /Timesheets/ID/Actions endpoint.       Request requirements:    Verbose&#x3D;true | [optional] 
**ApprovedBy** | **string** |        Request requirements:    Verbose&#x3D;true | [optional] 
**ApprovedByUserID** | **string** |  | [optional] 
**ApprovedDate** | **string** |        Request requirements:    Verbose&#x3D;true | [optional] 
**Comment** | **string** |        Request requirements:    Verbose&#x3D;true | [optional] 
**DayTotals** | [**List&lt;GetDayTotalModel&gt;**](GetDayTotalModel.md) |        Request requirements:    Verbose&#x3D;true | [optional] 
**EndDate** | **string** |  | [optional] 
**ID** | **string** |  | [optional] 
**LegacyID** | **string** |        Request requirements:    CTLegacyScramble&#x3D;true | [optional] 
**RejectedBy** | **string** |        Request requirements:    Verbose&#x3D;true | [optional] 
**RejectedByUserID** | **string** |  | [optional] 
**RejectedDate** | **string** |        Request requirements:    Verbose&#x3D;true | [optional] 
**StartDate** | **string** |  | [optional] 
**Status** | **string** |  | [optional] 
**SubmittedBy** | **string** |        Request requirements:    Verbose&#x3D;true | [optional] 
**SubmittedByUserID** | **string** |  | [optional] 
**SubmittedDate** | **string** |        Request requirements:    Verbose&#x3D;true | [optional] 
**User** | [**GetBasicUserModelUserGetTimesheetModelManage**](GetBasicUserModelUserGetTimesheetModelManage.md) |        Request requirements:    Verbose&#x3D;true | [optional] 
**UserID** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

