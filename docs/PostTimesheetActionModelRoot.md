# IO.Swagger.Model.PostTimesheetActionModelRoot
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Action** | **string** |  | 
**CCNotifications** | **List&lt;string&gt;** |  | [optional] 
**Comment** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

