# IO.Swagger.Api.DivisionsApi

All URIs are relative to *https://api.clicktime.com/V2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**CreateDivision**](DivisionsApi.md#createdivision) | **POST** /Divisions | Create a Division
[**CreateManagedDivision**](DivisionsApi.md#createmanageddivision) | **POST** /Manage/Divisions | Create a managed Division.
[**GetDefaultDivision**](DivisionsApi.md#getdefaultdivision) | **GET** /Divisions/new | Get a new Division with default values.
[**GetDivision**](DivisionsApi.md#getdivision) | **GET** /Divisions/{divisionID} | Get a Division.
[**GetDivisionCustomFieldDefinition**](DivisionsApi.md#getdivisioncustomfielddefinition) | **GET** /Divisions/CustomFieldDefinitions/{customFieldDefinitionID} | Get a Custom Field Definition for Divisions.
[**GetManagedDivision**](DivisionsApi.md#getmanageddivision) | **GET** /Manage/Divisions/{divisionID} | Get a managed Division.
[**ListDivisionCustomFieldDefinitions**](DivisionsApi.md#listdivisioncustomfielddefinitions) | **GET** /Divisions/CustomFieldDefinitions | Get Custom Field Definitions for Divisions.
[**ListDivisions**](DivisionsApi.md#listdivisions) | **GET** /Divisions | Get Divisions
[**ListManagedDivisions**](DivisionsApi.md#listmanageddivisions) | **GET** /Manage/Divisions | Get managed Divisions.
[**UpdateDivision**](DivisionsApi.md#updatedivision) | **PATCH** /Divisions/{divisionID} | Update a Division
[**UpdateManagedDivision**](DivisionsApi.md#updatemanageddivision) | **PATCH** /Manage/Divisions/{divisionID} | Update a managed Division.


<a name="createdivision"></a>
# **CreateDivision**
> ResponseBodyGetDivisionModelRoot CreateDivision (PostDivisionModelRoot model)

Create a Division

       User requirements:    Admin, or Manager with permissions to Add/Edit Divisions (all Divisions).       Company requirements:    Company must have one of the following optional modules: Corporate, Enterprise.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class CreateDivisionExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new DivisionsApi();
            var model = new PostDivisionModelRoot(); // PostDivisionModelRoot |       Required

            try
            {
                // Create a Division
                ResponseBodyGetDivisionModelRoot result = apiInstance.CreateDivision(model);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling DivisionsApi.CreateDivision: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **model** | [**PostDivisionModelRoot**](PostDivisionModelRoot.md)|       Required | 

### Return type

[**ResponseBodyGetDivisionModelRoot**](ResponseBodyGetDivisionModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded, application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="createmanageddivision"></a>
# **CreateManagedDivision**
> ResponseBodyGetDivisionModelManage CreateManagedDivision (PostDivisionModelManage model)

Create a managed Division.

       User requirements:    Admin, or Manager with permissions to Add/Edit Divisions.       Company requirements:    Company must have one of the following optional modules: Corporate, Enterprise.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class CreateManagedDivisionExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new DivisionsApi();
            var model = new PostDivisionModelManage(); // PostDivisionModelManage |       Required

            try
            {
                // Create a managed Division.
                ResponseBodyGetDivisionModelManage result = apiInstance.CreateManagedDivision(model);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling DivisionsApi.CreateManagedDivision: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **model** | [**PostDivisionModelManage**](PostDivisionModelManage.md)|       Required | 

### Return type

[**ResponseBodyGetDivisionModelManage**](ResponseBodyGetDivisionModelManage.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded, application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getdefaultdivision"></a>
# **GetDefaultDivision**
> ResponseBodyGetDivisionModelNew GetDefaultDivision ()

Get a new Division with default values.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class GetDefaultDivisionExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new DivisionsApi();

            try
            {
                // Get a new Division with default values.
                ResponseBodyGetDivisionModelNew result = apiInstance.GetDefaultDivision();
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling DivisionsApi.GetDefaultDivision: " + e.Message );
            }
        }
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ResponseBodyGetDivisionModelNew**](ResponseBodyGetDivisionModelNew.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getdivision"></a>
# **GetDivision**
> ResponseBodyGetDivisionModelRoot GetDivision (string divisionID)

Get a Division.

       User requirements:    User must be a Manager or an Admin.       Company requirements:    Company must have one of the following optional modules: Corporate, Enterprise.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class GetDivisionExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new DivisionsApi();
            var divisionID = divisionID_example;  // string |       Required

            try
            {
                // Get a Division.
                ResponseBodyGetDivisionModelRoot result = apiInstance.GetDivision(divisionID);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling DivisionsApi.GetDivision: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **divisionID** | **string**|       Required | 

### Return type

[**ResponseBodyGetDivisionModelRoot**](ResponseBodyGetDivisionModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getdivisioncustomfielddefinition"></a>
# **GetDivisionCustomFieldDefinition**
> ResponseBodyGetCustomFieldDefinitionModelRoot GetDivisionCustomFieldDefinition (string customFieldDefinitionID)

Get a Custom Field Definition for Divisions.

       User requirements:    Admin, or Manager with permissions to View Divisions.       Company requirements:    Company must have one of the following optional modules: Corporate, Enterprise.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class GetDivisionCustomFieldDefinitionExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new DivisionsApi();
            var customFieldDefinitionID = customFieldDefinitionID_example;  // string |       Required

            try
            {
                // Get a Custom Field Definition for Divisions.
                ResponseBodyGetCustomFieldDefinitionModelRoot result = apiInstance.GetDivisionCustomFieldDefinition(customFieldDefinitionID);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling DivisionsApi.GetDivisionCustomFieldDefinition: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customFieldDefinitionID** | **string**|       Required | 

### Return type

[**ResponseBodyGetCustomFieldDefinitionModelRoot**](ResponseBodyGetCustomFieldDefinitionModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getmanageddivision"></a>
# **GetManagedDivision**
> ResponseBodyGetDivisionModelManage GetManagedDivision (string divisionID)

Get a managed Division.

       User requirements:    Admin, or Manager with permissions to View Divisions.       Company requirements:    Company must have one of the following optional modules: Corporate, Enterprise.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class GetManagedDivisionExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new DivisionsApi();
            var divisionID = divisionID_example;  // string |       Required

            try
            {
                // Get a managed Division.
                ResponseBodyGetDivisionModelManage result = apiInstance.GetManagedDivision(divisionID);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling DivisionsApi.GetManagedDivision: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **divisionID** | **string**|       Required | 

### Return type

[**ResponseBodyGetDivisionModelManage**](ResponseBodyGetDivisionModelManage.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="listdivisioncustomfielddefinitions"></a>
# **ListDivisionCustomFieldDefinitions**
> ResponseBodyListGetCustomFieldDefinitionModelRoot ListDivisionCustomFieldDefinitions (int? limit = null, int? offset = null)

Get Custom Field Definitions for Divisions.

       User requirements:    Admin, or Manager with permissions to View Divisions.       Company requirements:    Company must have one of the following optional modules: Corporate, Enterprise.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ListDivisionCustomFieldDefinitionsExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new DivisionsApi();
            var limit = 56;  // int? |  (optional) 
            var offset = 56;  // int? |  (optional) 

            try
            {
                // Get Custom Field Definitions for Divisions.
                ResponseBodyListGetCustomFieldDefinitionModelRoot result = apiInstance.ListDivisionCustomFieldDefinitions(limit, offset);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling DivisionsApi.ListDivisionCustomFieldDefinitions: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **limit** | **int?**|  | [optional] 
 **offset** | **int?**|  | [optional] 

### Return type

[**ResponseBodyListGetCustomFieldDefinitionModelRoot**](ResponseBodyListGetCustomFieldDefinitionModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="listdivisions"></a>
# **ListDivisions**
> ResponseBodyListGetDivisionModelRoot ListDivisions (List<string> ID = null, List<string> name = null, List<bool?> isActive = null, int? limit = null, int? offset = null)

Get Divisions

       User requirements:    User must be a Manager or an Admin.       Company requirements:    Company must have one of the following optional modules: Corporate, Enterprise.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ListDivisionsExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new DivisionsApi();
            var ID = new List<string>(); // List<string> |  (optional) 
            var name = new List<string>(); // List<string> |  (optional) 
            var isActive = new List<bool?>(); // List<bool?> |  (optional) 
            var limit = 56;  // int? |  (optional) 
            var offset = 56;  // int? |  (optional) 

            try
            {
                // Get Divisions
                ResponseBodyListGetDivisionModelRoot result = apiInstance.ListDivisions(ID, name, isActive, limit, offset);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling DivisionsApi.ListDivisions: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ID** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **name** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **isActive** | [**List&lt;bool?&gt;**](bool?.md)|  | [optional] 
 **limit** | **int?**|  | [optional] 
 **offset** | **int?**|  | [optional] 

### Return type

[**ResponseBodyListGetDivisionModelRoot**](ResponseBodyListGetDivisionModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="listmanageddivisions"></a>
# **ListManagedDivisions**
> ResponseBodyListGetDivisionModelManage ListManagedDivisions (List<string> ID = null, List<string> name = null, List<bool?> isActive = null, int? limit = null, int? offset = null)

Get managed Divisions.

       User requirements:    Admin, or Manager with permissions to View Divisions.       Company requirements:    Company must have one of the following optional modules: Corporate, Enterprise.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ListManagedDivisionsExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new DivisionsApi();
            var ID = new List<string>(); // List<string> |  (optional) 
            var name = new List<string>(); // List<string> |  (optional) 
            var isActive = new List<bool?>(); // List<bool?> |  (optional) 
            var limit = 56;  // int? |  (optional) 
            var offset = 56;  // int? |  (optional) 

            try
            {
                // Get managed Divisions.
                ResponseBodyListGetDivisionModelManage result = apiInstance.ListManagedDivisions(ID, name, isActive, limit, offset);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling DivisionsApi.ListManagedDivisions: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ID** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **name** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **isActive** | [**List&lt;bool?&gt;**](bool?.md)|  | [optional] 
 **limit** | **int?**|  | [optional] 
 **offset** | **int?**|  | [optional] 

### Return type

[**ResponseBodyListGetDivisionModelManage**](ResponseBodyListGetDivisionModelManage.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="updatedivision"></a>
# **UpdateDivision**
> ResponseBodyGetDivisionModelRoot UpdateDivision (string divisionID, PatchDivisionModelRoot model)

Update a Division

       User requirements:    Admin, or Manager with permissions to Add/Edit Divisions (all Divisions).       Company requirements:    Company must have one of the following optional modules: Corporate, Enterprise.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class UpdateDivisionExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new DivisionsApi();
            var divisionID = divisionID_example;  // string |       Required
            var model = new PatchDivisionModelRoot(); // PatchDivisionModelRoot |       Required

            try
            {
                // Update a Division
                ResponseBodyGetDivisionModelRoot result = apiInstance.UpdateDivision(divisionID, model);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling DivisionsApi.UpdateDivision: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **divisionID** | **string**|       Required | 
 **model** | [**PatchDivisionModelRoot**](PatchDivisionModelRoot.md)|       Required | 

### Return type

[**ResponseBodyGetDivisionModelRoot**](ResponseBodyGetDivisionModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded, application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="updatemanageddivision"></a>
# **UpdateManagedDivision**
> ResponseBodyGetDivisionModelManage UpdateManagedDivision (string divisionID, PatchDivisionModelManage model)

Update a managed Division.

       User requirements:    Admin, or Manager with permissions to Add/Edit Divisions.       Company requirements:    Company must have one of the following optional modules: Corporate, Enterprise.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class UpdateManagedDivisionExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new DivisionsApi();
            var divisionID = divisionID_example;  // string |       Required
            var model = new PatchDivisionModelManage(); // PatchDivisionModelManage |       Required

            try
            {
                // Update a managed Division.
                ResponseBodyGetDivisionModelManage result = apiInstance.UpdateManagedDivision(divisionID, model);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling DivisionsApi.UpdateManagedDivision: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **divisionID** | **string**|       Required | 
 **model** | [**PatchDivisionModelManage**](PatchDivisionModelManage.md)|       Required | 

### Return type

[**ResponseBodyGetDivisionModelManage**](ResponseBodyGetDivisionModelManage.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded, application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

