# IO.Swagger.Model.GetTimeEntryModelMe
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**BillingRate** | **double?** |        User requirements:    Security Level(s): Admin, Manager (View Billingrates) | [optional] 
**BreakTime** | **double?** |  | [optional] 
**_Client** | [**GetBasicClientModelClientGetTimeEntryModelMe**](GetBasicClientModelClientGetTimeEntryModelMe.md) |        Request requirements:    Verbose&#x3D;true | [optional] 
**Comment** | **string** |  | [optional] 
**CostRate** | **double?** |        User requirements:    Security Level(s): Admin, Manager (View Costs) | [optional] 
**CustomFields** | **Dictionary&lt;string, Object&gt;** |        Company requirements:    Custom Fields must be configured for Time Entries. | [optional] 
**Date** | **string** |  | [optional] 
**EndTime** | **string** |  | [optional] 
**Hours** | **double?** |  | [optional] 
**ID** | **string** |  | [optional] 
**Job** | [**GetBasicJobModelJobGetTimeEntryModelMe**](GetBasicJobModelJobGetTimeEntryModelMe.md) |        Request requirements:    Verbose&#x3D;true | [optional] 
**JobID** | **string** |  | [optional] 
**LegacyID** | **string** |        Request requirements:    CTLegacyScramble&#x3D;true | [optional] 
**StartTime** | **string** |  | [optional] 
**Task** | [**GetBasicTaskModelTaskGetTimeEntryModelMe**](GetBasicTaskModelTaskGetTimeEntryModelMe.md) |        Request requirements:    Verbose&#x3D;true | [optional] 
**TaskID** | **string** |  | [optional] 
**User** | [**GetBasicUserModelUserGetTimeEntryModelMe**](GetBasicUserModelUserGetTimeEntryModelMe.md) |        Request requirements:    Verbose&#x3D;true | [optional] 
**UserID** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

