# IO.Swagger.Model.PatchTimeEntryModel
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**BillingRate** | **double?** |  | [optional] 
**BreakTime** | **double?** |  | [optional] 
**Comment** | **string** |  | [optional] 
**CostRate** | **double?** |  | [optional] 
**CustomFields** | **Dictionary&lt;string, Object&gt;** |  | [optional] 
**EndTime** | **string** | Format: HH:MM (24-hour) | [optional] 
**Hours** | **double?** |  | [optional] 
**JobID** | **string** |  | [optional] 
**StartTime** | **string** | Format: HH:MM (24-hour) | [optional] 
**TaskID** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

