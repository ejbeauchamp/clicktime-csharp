# IO.Swagger.Model.GetJobModelMe
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_Client** | [**GetBasicClientModelClientGetJobModelMe**](GetBasicClientModelClientGetJobModelMe.md) |        Request requirements:    Verbose&#x3D;true | [optional] 
**ClientID** | **string** |  | [optional] 
**ID** | **string** |  | [optional] 
**IsActive** | **bool?** |  | [optional] 
**JobNumber** | **string** |  | [optional] 
**LegacyID** | **string** |        Request requirements:    CTLegacyScramble&#x3D;true | [optional] 
**ListDisplayText** | **string** |  | [optional] 
**MyJobs** | **bool?** |        Request requirements:    Verbose&#x3D;true       Company requirements:    Job restriction by My Jobs | [optional] 
**Name** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

