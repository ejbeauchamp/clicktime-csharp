# IO.Swagger.Api.JobsApi

All URIs are relative to *https://api.clicktime.com/V2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**AddJobDivision**](JobsApi.md#addjobdivision) | **POST** /Jobs/{jobID}/Divisions | Make a Job available to a Division.    This end-point is only available to companies that have their Job List Controls set to Moderate.
[**AddJobTask**](JobsApi.md#addjobtask) | **POST** /Jobs/{jobID}/Tasks | Make a Task available to a Job.
[**AddJobUser**](JobsApi.md#addjobuser) | **POST** /Jobs/{jobID}/Users | Make a Job available to a User.    This end-point is only available to companies that have their Job List Controls set to Maximum.
[**CreateJob**](JobsApi.md#createjob) | **POST** /Jobs | Create a Job.
[**DeleteJob**](JobsApi.md#deletejob) | **DELETE** /Jobs/{jobID} | Delete a Job.
[**GetDefaultJob**](JobsApi.md#getdefaultjob) | **GET** /Jobs/new | Get a new Job with default values.
[**GetJob**](JobsApi.md#getjob) | **GET** /Jobs/{jobID} | Get a Job.
[**GetJobBillingRate**](JobsApi.md#getjobbillingrate) | **GET** /Jobs/{jobID}/BillingRates/{billingRateID} | Get a secondary billing rate for a Job.
[**GetJobCustomFieldDefinition**](JobsApi.md#getjobcustomfielddefinition) | **GET** /Jobs/CustomFieldDefinitions/{customFieldDefinitionID} | Get a Custom Field Definition for Jobs.
[**GetMyJob**](JobsApi.md#getmyjob) | **GET** /Me/Jobs/{jobID} | Get my Job.
[**ListJobBillingRates**](JobsApi.md#listjobbillingrates) | **GET** /Jobs/{jobID}/BillingRates | Get secondary billing rates for a Job.
[**ListJobCustomFieldDefinitions**](JobsApi.md#listjobcustomfielddefinitions) | **GET** /Jobs/CustomFieldDefinitions | Get Custom Field Definitions for Jobs.
[**ListJobDivisions**](JobsApi.md#listjobdivisions) | **GET** /Jobs/{jobID}/Divisions | Get the IDs of Divisions to which a Job is available.    This end-point is only available to companies that have their Job List Controls set to Moderate.
[**ListJobTasks**](JobsApi.md#listjobtasks) | **GET** /Jobs/{jobID}/Tasks | Get a Job&#39;s available Tasks.
[**ListJobTimeSummary**](JobsApi.md#listjobtimesummary) | **GET** /Jobs/{jobID}/TimeSummary | Get Job time summary.
[**ListJobUsers**](JobsApi.md#listjobusers) | **GET** /Jobs/{jobID}/Users | Get the IDs of Users to whom a Job is available.    This end-point is only available to companies that have their Job List Controls set to Maximum.
[**ListJobs**](JobsApi.md#listjobs) | **GET** /Jobs | Get Jobs.
[**ListMyJobTaskControls**](JobsApi.md#listmyjobtaskcontrols) | **GET** /Me/Jobs/TaskControls | Get a list of Jobs and available Tasks I have access to.
[**ListMyJobs**](JobsApi.md#listmyjobs) | **GET** /Me/Jobs | Get my Jobs.
[**RemoveJobDivision**](JobsApi.md#removejobdivision) | **DELETE** /Jobs/{jobID}/Divisions/{divisionID} | Make a Job unavailable to a Division.    This end-point is only available to companies that have their Job List Controls set to Moderate.
[**RemoveJobTask**](JobsApi.md#removejobtask) | **DELETE** /Jobs/{jobID}/Tasks/{taskID} | Make a Task unavailable to a Job.
[**RemoveJobUser**](JobsApi.md#removejobuser) | **DELETE** /Jobs/{jobID}/Users/{userID} | Make a job unavailable to a user.    This end-point is only available to companies that have their Job List Controls set to Maximum.
[**UpdateJob**](JobsApi.md#updatejob) | **PATCH** /Jobs/{jobID} | Update a Job.
[**UpdateJobBillingRate**](JobsApi.md#updatejobbillingrate) | **PATCH** /Jobs/{jobID}/BillingRates/{billingRateID} | Update a secondary billing rate for a Job
[**UpdateJobDivisions**](JobsApi.md#updatejobdivisions) | **PUT** /Jobs/{jobID}/Divisions | Define a set of Divisions to which a Job is available.    This end-point is only available to companies that have their Job List Controls set to Moderate.
[**UpdateJobTasks**](JobsApi.md#updatejobtasks) | **PUT** /Jobs/{jobID}/Tasks | Set a Job&#39;s available Tasks.
[**UpdateJobUsers**](JobsApi.md#updatejobusers) | **PUT** /Jobs/{jobID}/Users | Define a set of Users to which a Job is available.    This end-point is only available to companies that have their Job List Controls set to Maximum.


<a name="addjobdivision"></a>
# **AddJobDivision**
> ResponseBodyListSystemString AddJobDivision (string jobID, PostDivisionAssociationModelRoot model)

Make a Job available to a Division.    This end-point is only available to companies that have their Job List Controls set to Moderate.

       User requirements:    Admin, or Manager with permissions to Add/Edit Jobs.    Admin, or Manager with permissions to View People (all Divisions).       Company requirements:    Job List Control must be restricted by division.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class AddJobDivisionExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new JobsApi();
            var jobID = jobID_example;  // string |       Required
            var model = new PostDivisionAssociationModelRoot(); // PostDivisionAssociationModelRoot |       Required

            try
            {
                // Make a Job available to a Division.    This end-point is only available to companies that have their Job List Controls set to Moderate.
                ResponseBodyListSystemString result = apiInstance.AddJobDivision(jobID, model);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling JobsApi.AddJobDivision: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **jobID** | **string**|       Required | 
 **model** | [**PostDivisionAssociationModelRoot**](PostDivisionAssociationModelRoot.md)|       Required | 

### Return type

[**ResponseBodyListSystemString**](ResponseBodyListSystemString.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded, application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="addjobtask"></a>
# **AddJobTask**
> ResponseBodyListSystemString AddJobTask (string jobID, PostTaskAssociationModelRoot model)

Make a Task available to a Job.

       Company requirements:    Task List Control must be restricted by Job.       User requirements:    Admin, or Manager with permissions to Add/Edit Jobs.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class AddJobTaskExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new JobsApi();
            var jobID = jobID_example;  // string |       Required
            var model = new PostTaskAssociationModelRoot(); // PostTaskAssociationModelRoot |       Required

            try
            {
                // Make a Task available to a Job.
                ResponseBodyListSystemString result = apiInstance.AddJobTask(jobID, model);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling JobsApi.AddJobTask: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **jobID** | **string**|       Required | 
 **model** | [**PostTaskAssociationModelRoot**](PostTaskAssociationModelRoot.md)|       Required | 

### Return type

[**ResponseBodyListSystemString**](ResponseBodyListSystemString.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded, application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="addjobuser"></a>
# **AddJobUser**
> ResponseBodyListSystemString AddJobUser (string jobID, PostUserAssociationModelRoot model)

Make a Job available to a User.    This end-point is only available to companies that have their Job List Controls set to Maximum.

       User requirements:    Admin, or Manager with permissions to Add/Edit Jobs.    Admin, or Manager with permissions to View People (all Divisions).       Company requirements:    Job List Control must be restricted by person.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class AddJobUserExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new JobsApi();
            var jobID = jobID_example;  // string |       Required
            var model = new PostUserAssociationModelRoot(); // PostUserAssociationModelRoot |       Required

            try
            {
                // Make a Job available to a User.    This end-point is only available to companies that have their Job List Controls set to Maximum.
                ResponseBodyListSystemString result = apiInstance.AddJobUser(jobID, model);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling JobsApi.AddJobUser: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **jobID** | **string**|       Required | 
 **model** | [**PostUserAssociationModelRoot**](PostUserAssociationModelRoot.md)|       Required | 

### Return type

[**ResponseBodyListSystemString**](ResponseBodyListSystemString.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded, application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="createjob"></a>
# **CreateJob**
> ResponseBodyGetJobModelRoot CreateJob (PostJobModelRoot model)

Create a Job.

       User requirements:    Admin, or Manager with permissions to Add/Edit Jobs.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class CreateJobExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new JobsApi();
            var model = new PostJobModelRoot(); // PostJobModelRoot |       Required

            try
            {
                // Create a Job.
                ResponseBodyGetJobModelRoot result = apiInstance.CreateJob(model);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling JobsApi.CreateJob: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **model** | [**PostJobModelRoot**](PostJobModelRoot.md)|       Required | 

### Return type

[**ResponseBodyGetJobModelRoot**](ResponseBodyGetJobModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded, application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="deletejob"></a>
# **DeleteJob**
> void DeleteJob (string jobID)

Delete a Job.

       User requirements:    User must be an Admin.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class DeleteJobExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new JobsApi();
            var jobID = jobID_example;  // string |       Required

            try
            {
                // Delete a Job.
                apiInstance.DeleteJob(jobID);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling JobsApi.DeleteJob: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **jobID** | **string**|       Required | 

### Return type

void (empty response body)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getdefaultjob"></a>
# **GetDefaultJob**
> ResponseBodyGetJobModelNew GetDefaultJob ()

Get a new Job with default values.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class GetDefaultJobExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new JobsApi();

            try
            {
                // Get a new Job with default values.
                ResponseBodyGetJobModelNew result = apiInstance.GetDefaultJob();
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling JobsApi.GetDefaultJob: " + e.Message );
            }
        }
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ResponseBodyGetJobModelNew**](ResponseBodyGetJobModelNew.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getjob"></a>
# **GetJob**
> ResponseBodyGetJobModelRoot GetJob (string jobID)

Get a Job.

       User requirements:    User must be a Manager or an Admin.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class GetJobExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new JobsApi();
            var jobID = jobID_example;  // string |       Required

            try
            {
                // Get a Job.
                ResponseBodyGetJobModelRoot result = apiInstance.GetJob(jobID);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling JobsApi.GetJob: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **jobID** | **string**|       Required | 

### Return type

[**ResponseBodyGetJobModelRoot**](ResponseBodyGetJobModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getjobbillingrate"></a>
# **GetJobBillingRate**
> ResponseBodyGetBillingRateModelRoot GetJobBillingRate (string jobID, string billingRateID)

Get a secondary billing rate for a Job.

       User requirements:    Admin, or Manager with permissions to View Billingrates.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class GetJobBillingRateExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new JobsApi();
            var jobID = jobID_example;  // string |       Required
            var billingRateID = billingRateID_example;  // string |       Required

            try
            {
                // Get a secondary billing rate for a Job.
                ResponseBodyGetBillingRateModelRoot result = apiInstance.GetJobBillingRate(jobID, billingRateID);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling JobsApi.GetJobBillingRate: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **jobID** | **string**|       Required | 
 **billingRateID** | **string**|       Required | 

### Return type

[**ResponseBodyGetBillingRateModelRoot**](ResponseBodyGetBillingRateModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getjobcustomfielddefinition"></a>
# **GetJobCustomFieldDefinition**
> ResponseBodyGetCustomFieldDefinitionModelRoot GetJobCustomFieldDefinition (string customFieldDefinitionID)

Get a Custom Field Definition for Jobs.

       User requirements:    Admin, or Manager with permissions to View Jobs.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class GetJobCustomFieldDefinitionExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new JobsApi();
            var customFieldDefinitionID = customFieldDefinitionID_example;  // string |       Required

            try
            {
                // Get a Custom Field Definition for Jobs.
                ResponseBodyGetCustomFieldDefinitionModelRoot result = apiInstance.GetJobCustomFieldDefinition(customFieldDefinitionID);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling JobsApi.GetJobCustomFieldDefinition: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customFieldDefinitionID** | **string**|       Required | 

### Return type

[**ResponseBodyGetCustomFieldDefinitionModelRoot**](ResponseBodyGetCustomFieldDefinitionModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getmyjob"></a>
# **GetMyJob**
> ResponseBodyGetJobModelMe GetMyJob (string jobID)

Get my Job.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class GetMyJobExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new JobsApi();
            var jobID = jobID_example;  // string |       Required

            try
            {
                // Get my Job.
                ResponseBodyGetJobModelMe result = apiInstance.GetMyJob(jobID);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling JobsApi.GetMyJob: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **jobID** | **string**|       Required | 

### Return type

[**ResponseBodyGetJobModelMe**](ResponseBodyGetJobModelMe.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="listjobbillingrates"></a>
# **ListJobBillingRates**
> ResponseBodyListGetBillingRateModelRoot ListJobBillingRates (string jobID, List<string> ID = null)

Get secondary billing rates for a Job.

       User requirements:    Admin, or Manager with permissions to View Billingrates.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ListJobBillingRatesExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new JobsApi();
            var jobID = jobID_example;  // string |       Required
            var ID = new List<string>(); // List<string> | User or Task IDs (optional) 

            try
            {
                // Get secondary billing rates for a Job.
                ResponseBodyListGetBillingRateModelRoot result = apiInstance.ListJobBillingRates(jobID, ID);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling JobsApi.ListJobBillingRates: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **jobID** | **string**|       Required | 
 **ID** | [**List&lt;string&gt;**](string.md)| User or Task IDs | [optional] 

### Return type

[**ResponseBodyListGetBillingRateModelRoot**](ResponseBodyListGetBillingRateModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="listjobcustomfielddefinitions"></a>
# **ListJobCustomFieldDefinitions**
> ResponseBodyListGetCustomFieldDefinitionModelRoot ListJobCustomFieldDefinitions (int? limit = null, int? offset = null)

Get Custom Field Definitions for Jobs.

       User requirements:    Admin, or Manager with permissions to View Jobs.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ListJobCustomFieldDefinitionsExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new JobsApi();
            var limit = 56;  // int? |  (optional) 
            var offset = 56;  // int? |  (optional) 

            try
            {
                // Get Custom Field Definitions for Jobs.
                ResponseBodyListGetCustomFieldDefinitionModelRoot result = apiInstance.ListJobCustomFieldDefinitions(limit, offset);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling JobsApi.ListJobCustomFieldDefinitions: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **limit** | **int?**|  | [optional] 
 **offset** | **int?**|  | [optional] 

### Return type

[**ResponseBodyListGetCustomFieldDefinitionModelRoot**](ResponseBodyListGetCustomFieldDefinitionModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="listjobdivisions"></a>
# **ListJobDivisions**
> ResponseBodyListSystemString ListJobDivisions (string jobID)

Get the IDs of Divisions to which a Job is available.    This end-point is only available to companies that have their Job List Controls set to Moderate.

       User requirements:    Admin, or Manager with permissions to View Jobs.       Company requirements:    Job List Control must be restricted by division.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ListJobDivisionsExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new JobsApi();
            var jobID = jobID_example;  // string |       Required

            try
            {
                // Get the IDs of Divisions to which a Job is available.    This end-point is only available to companies that have their Job List Controls set to Moderate.
                ResponseBodyListSystemString result = apiInstance.ListJobDivisions(jobID);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling JobsApi.ListJobDivisions: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **jobID** | **string**|       Required | 

### Return type

[**ResponseBodyListSystemString**](ResponseBodyListSystemString.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="listjobtasks"></a>
# **ListJobTasks**
> ResponseBodyListSystemString ListJobTasks (string jobID)

Get a Job's available Tasks.

       Company requirements:    Task List Control must be restricted by Job.       User requirements:    Admin, or Manager with permissions to View Jobs.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ListJobTasksExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new JobsApi();
            var jobID = jobID_example;  // string |       Required

            try
            {
                // Get a Job's available Tasks.
                ResponseBodyListSystemString result = apiInstance.ListJobTasks(jobID);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling JobsApi.ListJobTasks: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **jobID** | **string**|       Required | 

### Return type

[**ResponseBodyListSystemString**](ResponseBodyListSystemString.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="listjobtimesummary"></a>
# **ListJobTimeSummary**
> ResponseBodyGetSummaryModelRoot ListJobTimeSummary (string jobID, string groupBy = null)

Get Job time summary.

       User requirements:    Admin, or Manager with permissions to View Jobs.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ListJobTimeSummaryExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new JobsApi();
            var jobID = jobID_example;  // string |       Required
            var groupBy = groupBy_example;  // string |  (optional) 

            try
            {
                // Get Job time summary.
                ResponseBodyGetSummaryModelRoot result = apiInstance.ListJobTimeSummary(jobID, groupBy);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling JobsApi.ListJobTimeSummary: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **jobID** | **string**|       Required | 
 **groupBy** | **string**|  | [optional] 

### Return type

[**ResponseBodyGetSummaryModelRoot**](ResponseBodyGetSummaryModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="listjobusers"></a>
# **ListJobUsers**
> ResponseBodyListSystemString ListJobUsers (string jobID)

Get the IDs of Users to whom a Job is available.    This end-point is only available to companies that have their Job List Controls set to Maximum.

       User requirements:    Admin, or Manager with permissions to View Jobs.       Company requirements:    Job List Control must be restricted by person.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ListJobUsersExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new JobsApi();
            var jobID = jobID_example;  // string |       Required

            try
            {
                // Get the IDs of Users to whom a Job is available.    This end-point is only available to companies that have their Job List Controls set to Maximum.
                ResponseBodyListSystemString result = apiInstance.ListJobUsers(jobID);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling JobsApi.ListJobUsers: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **jobID** | **string**|       Required | 

### Return type

[**ResponseBodyListSystemString**](ResponseBodyListSystemString.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="listjobs"></a>
# **ListJobs**
> ResponseBodyListGetJobModelRoot ListJobs (List<string> ID = null, List<string> clientID = null, List<string> name = null, List<string> jobNumber = null, List<bool?> isActive = null, List<string> projectManagerID = null, int? limit = null, int? offset = null)

Get Jobs.

       User requirements:    User must be a Manager or an Admin.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ListJobsExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new JobsApi();
            var ID = new List<string>(); // List<string> |  (optional) 
            var clientID = new List<string>(); // List<string> |  (optional) 
            var name = new List<string>(); // List<string> |  (optional) 
            var jobNumber = new List<string>(); // List<string> |  (optional) 
            var isActive = new List<bool?>(); // List<bool?> |  (optional) 
            var projectManagerID = new List<string>(); // List<string> |  (optional) 
            var limit = 56;  // int? |  (optional) 
            var offset = 56;  // int? |  (optional) 

            try
            {
                // Get Jobs.
                ResponseBodyListGetJobModelRoot result = apiInstance.ListJobs(ID, clientID, name, jobNumber, isActive, projectManagerID, limit, offset);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling JobsApi.ListJobs: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ID** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **clientID** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **name** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **jobNumber** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **isActive** | [**List&lt;bool?&gt;**](bool?.md)|  | [optional] 
 **projectManagerID** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **limit** | **int?**|  | [optional] 
 **offset** | **int?**|  | [optional] 

### Return type

[**ResponseBodyListGetJobModelRoot**](ResponseBodyListGetJobModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="listmyjobtaskcontrols"></a>
# **ListMyJobTaskControls**
> ResponseBodyListGetJobTaskControlModelRoot ListMyJobTaskControls (List<string> jobID = null, List<string> taskID = null, List<bool?> jobIsActive = null, List<bool?> taskIsActive = null, int? limit = null, int? offset = null)

Get a list of Jobs and available Tasks I have access to.

       Company requirements:    Task List Control must be restricted by Job.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ListMyJobTaskControlsExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new JobsApi();
            var jobID = new List<string>(); // List<string> |  (optional) 
            var taskID = new List<string>(); // List<string> |  (optional) 
            var jobIsActive = new List<bool?>(); // List<bool?> |  (optional) 
            var taskIsActive = new List<bool?>(); // List<bool?> |  (optional) 
            var limit = 56;  // int? |  (optional) 
            var offset = 56;  // int? |  (optional) 

            try
            {
                // Get a list of Jobs and available Tasks I have access to.
                ResponseBodyListGetJobTaskControlModelRoot result = apiInstance.ListMyJobTaskControls(jobID, taskID, jobIsActive, taskIsActive, limit, offset);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling JobsApi.ListMyJobTaskControls: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **jobID** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **taskID** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **jobIsActive** | [**List&lt;bool?&gt;**](bool?.md)|  | [optional] 
 **taskIsActive** | [**List&lt;bool?&gt;**](bool?.md)|  | [optional] 
 **limit** | **int?**|  | [optional] 
 **offset** | **int?**|  | [optional] 

### Return type

[**ResponseBodyListGetJobTaskControlModelRoot**](ResponseBodyListGetJobTaskControlModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="listmyjobs"></a>
# **ListMyJobs**
> ResponseBodyListGetJobModelMe ListMyJobs (List<string> name = null, List<string> jobNumber = null, List<bool?> isActive = null, List<string> clientID = null, List<bool?> myJobs = null, int? limit = null, int? offset = null)

Get my Jobs.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ListMyJobsExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new JobsApi();
            var name = new List<string>(); // List<string> |  (optional) 
            var jobNumber = new List<string>(); // List<string> |  (optional) 
            var isActive = new List<bool?>(); // List<bool?> |  (optional) 
            var clientID = new List<string>(); // List<string> |  (optional) 
            var myJobs = new List<bool?>(); // List<bool?> |  (optional) 
            var limit = 56;  // int? |  (optional) 
            var offset = 56;  // int? |  (optional) 

            try
            {
                // Get my Jobs.
                ResponseBodyListGetJobModelMe result = apiInstance.ListMyJobs(name, jobNumber, isActive, clientID, myJobs, limit, offset);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling JobsApi.ListMyJobs: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **jobNumber** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **isActive** | [**List&lt;bool?&gt;**](bool?.md)|  | [optional] 
 **clientID** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **myJobs** | [**List&lt;bool?&gt;**](bool?.md)|  | [optional] 
 **limit** | **int?**|  | [optional] 
 **offset** | **int?**|  | [optional] 

### Return type

[**ResponseBodyListGetJobModelMe**](ResponseBodyListGetJobModelMe.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="removejobdivision"></a>
# **RemoveJobDivision**
> ResponseBodyListSystemString RemoveJobDivision (string jobID, string divisionID)

Make a Job unavailable to a Division.    This end-point is only available to companies that have their Job List Controls set to Moderate.

       User requirements:    Admin, or Manager with permissions to Add/Edit Jobs.    Admin, or Manager with permissions to View People (all Divisions).       Company requirements:    Job List Control must be restricted by division.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class RemoveJobDivisionExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new JobsApi();
            var jobID = jobID_example;  // string |       Required
            var divisionID = divisionID_example;  // string |       Required

            try
            {
                // Make a Job unavailable to a Division.    This end-point is only available to companies that have their Job List Controls set to Moderate.
                ResponseBodyListSystemString result = apiInstance.RemoveJobDivision(jobID, divisionID);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling JobsApi.RemoveJobDivision: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **jobID** | **string**|       Required | 
 **divisionID** | **string**|       Required | 

### Return type

[**ResponseBodyListSystemString**](ResponseBodyListSystemString.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="removejobtask"></a>
# **RemoveJobTask**
> ResponseBodyListSystemString RemoveJobTask (string jobID, string taskID)

Make a Task unavailable to a Job.

       Company requirements:    Task List Control must be restricted by Job.       User requirements:    Admin, or Manager with permissions to Add/Edit Jobs.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class RemoveJobTaskExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new JobsApi();
            var jobID = jobID_example;  // string |       Required
            var taskID = taskID_example;  // string |       Required

            try
            {
                // Make a Task unavailable to a Job.
                ResponseBodyListSystemString result = apiInstance.RemoveJobTask(jobID, taskID);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling JobsApi.RemoveJobTask: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **jobID** | **string**|       Required | 
 **taskID** | **string**|       Required | 

### Return type

[**ResponseBodyListSystemString**](ResponseBodyListSystemString.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="removejobuser"></a>
# **RemoveJobUser**
> ResponseBodyListSystemString RemoveJobUser (string jobID, string userID)

Make a job unavailable to a user.    This end-point is only available to companies that have their Job List Controls set to Maximum.

       User requirements:    Admin, or Manager with permissions to Add/Edit Jobs.    Admin, or Manager with permissions to View People (all Divisions).       Company requirements:    Job List Control must be restricted by person.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class RemoveJobUserExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new JobsApi();
            var jobID = jobID_example;  // string |       Required
            var userID = userID_example;  // string |       Required

            try
            {
                // Make a job unavailable to a user.    This end-point is only available to companies that have their Job List Controls set to Maximum.
                ResponseBodyListSystemString result = apiInstance.RemoveJobUser(jobID, userID);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling JobsApi.RemoveJobUser: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **jobID** | **string**|       Required | 
 **userID** | **string**|       Required | 

### Return type

[**ResponseBodyListSystemString**](ResponseBodyListSystemString.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="updatejob"></a>
# **UpdateJob**
> ResponseBodyGetJobModelRoot UpdateJob (string jobID, PatchJobModelRoot model)

Update a Job.

       User requirements:    Admin, or Manager with permissions to Add/Edit Jobs.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class UpdateJobExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new JobsApi();
            var jobID = jobID_example;  // string |       Required
            var model = new PatchJobModelRoot(); // PatchJobModelRoot |       Required

            try
            {
                // Update a Job.
                ResponseBodyGetJobModelRoot result = apiInstance.UpdateJob(jobID, model);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling JobsApi.UpdateJob: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **jobID** | **string**|       Required | 
 **model** | [**PatchJobModelRoot**](PatchJobModelRoot.md)|       Required | 

### Return type

[**ResponseBodyGetJobModelRoot**](ResponseBodyGetJobModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded, application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="updatejobbillingrate"></a>
# **UpdateJobBillingRate**
> ResponseBodyGetBillingRateModelRoot UpdateJobBillingRate (string jobID, string billingRateID, PatchBillingRateModelRoot model)

Update a secondary billing rate for a Job

       User requirements:    Admin, or Manager with permissions to View Billingrates.    Admin, or Manager with permissions to Add/Edit Jobs.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class UpdateJobBillingRateExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new JobsApi();
            var jobID = jobID_example;  // string |       Required
            var billingRateID = billingRateID_example;  // string | User or Task ID      Required
            var model = new PatchBillingRateModelRoot(); // PatchBillingRateModelRoot |       Required

            try
            {
                // Update a secondary billing rate for a Job
                ResponseBodyGetBillingRateModelRoot result = apiInstance.UpdateJobBillingRate(jobID, billingRateID, model);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling JobsApi.UpdateJobBillingRate: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **jobID** | **string**|       Required | 
 **billingRateID** | **string**| User or Task ID      Required | 
 **model** | [**PatchBillingRateModelRoot**](PatchBillingRateModelRoot.md)|       Required | 

### Return type

[**ResponseBodyGetBillingRateModelRoot**](ResponseBodyGetBillingRateModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded, application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="updatejobdivisions"></a>
# **UpdateJobDivisions**
> ResponseBodyListSystemString UpdateJobDivisions (string jobID, List<string> divisionIDs)

Define a set of Divisions to which a Job is available.    This end-point is only available to companies that have their Job List Controls set to Moderate.

       User requirements:    Admin, or Manager with permissions to Add/Edit Jobs.    Admin, or Manager with permissions to View People (all Divisions).       Company requirements:    Job List Control must be restricted by division.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class UpdateJobDivisionsExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new JobsApi();
            var jobID = jobID_example;  // string |       Required
            var divisionIDs = ;  // List<string> |       Required

            try
            {
                // Define a set of Divisions to which a Job is available.    This end-point is only available to companies that have their Job List Controls set to Moderate.
                ResponseBodyListSystemString result = apiInstance.UpdateJobDivisions(jobID, divisionIDs);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling JobsApi.UpdateJobDivisions: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **jobID** | **string**|       Required | 
 **divisionIDs** | **List&lt;string&gt;**|       Required | 

### Return type

[**ResponseBodyListSystemString**](ResponseBodyListSystemString.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded, application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="updatejobtasks"></a>
# **UpdateJobTasks**
> ResponseBodyListSystemString UpdateJobTasks (string jobID, List<string> ids)

Set a Job's available Tasks.

       Company requirements:    Task List Control must be restricted by Job.       User requirements:    Admin, or Manager with permissions to Add/Edit Jobs.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class UpdateJobTasksExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new JobsApi();
            var jobID = jobID_example;  // string |       Required
            var ids = ;  // List<string> |       Required

            try
            {
                // Set a Job's available Tasks.
                ResponseBodyListSystemString result = apiInstance.UpdateJobTasks(jobID, ids);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling JobsApi.UpdateJobTasks: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **jobID** | **string**|       Required | 
 **ids** | **List&lt;string&gt;**|       Required | 

### Return type

[**ResponseBodyListSystemString**](ResponseBodyListSystemString.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded, application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="updatejobusers"></a>
# **UpdateJobUsers**
> ResponseBodyListSystemString UpdateJobUsers (string jobID, List<string> userIDs)

Define a set of Users to which a Job is available.    This end-point is only available to companies that have their Job List Controls set to Maximum.

       User requirements:    Admin, or Manager with permissions to Add/Edit Jobs.    Admin, or Manager with permissions to View People (all Divisions).       Company requirements:    Job List Control must be restricted by person.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class UpdateJobUsersExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new JobsApi();
            var jobID = jobID_example;  // string |       Required
            var userIDs = ;  // List<string> |       Required

            try
            {
                // Define a set of Users to which a Job is available.    This end-point is only available to companies that have their Job List Controls set to Maximum.
                ResponseBodyListSystemString result = apiInstance.UpdateJobUsers(jobID, userIDs);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling JobsApi.UpdateJobUsers: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **jobID** | **string**|       Required | 
 **userIDs** | **List&lt;string&gt;**|       Required | 

### Return type

[**ResponseBodyListSystemString**](ResponseBodyListSystemString.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded, application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

