# IO.Swagger.Api.TimeOffRequestsApi

All URIs are relative to *https://api.clicktime.com/V2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**CreateMyTimeOffRequest**](TimeOffRequestsApi.md#createmytimeoffrequest) | **POST** /Me/TimeOffRequests | Create my Time Off Request.
[**CreateTimeOffRequest**](TimeOffRequestsApi.md#createtimeoffrequest) | **POST** /TimeOffRequests | Create a Time Off Request.
[**GetDefaultTimeOffRequest**](TimeOffRequestsApi.md#getdefaulttimeoffrequest) | **GET** /TimeOffRequests/new | Get a new Time Off Request with default values.
[**GetManagedTimeOffRequest**](TimeOffRequestsApi.md#getmanagedtimeoffrequest) | **GET** /Manage/TimeOffRequests/{timeOffRequestID} | Get a managed Time Off Request.
[**GetMyTimeOffRequest**](TimeOffRequestsApi.md#getmytimeoffrequest) | **GET** /Me/TimeOffRequests/{timeOffRequestID} | Get my Time Off Request.
[**GetTimeOffRequest**](TimeOffRequestsApi.md#gettimeoffrequest) | **GET** /TimeOffRequests/{timeOffRequestID} | Get a Time Off Request.
[**ListManagedTimeOffRequestActions**](TimeOffRequestsApi.md#listmanagedtimeoffrequestactions) | **GET** /Manage/TimeOffRequests/{timeOffRequestID}/Actions | Get actions for a managed Time Off Request.
[**ListManagedTimeOffRequests**](TimeOffRequestsApi.md#listmanagedtimeoffrequests) | **GET** /Manage/TimeOffRequests | Get managed Time Off Requests.
[**ListMyTimeOffRequestActions**](TimeOffRequestsApi.md#listmytimeoffrequestactions) | **GET** /Me/TimeOffRequests/{timeOffRequestID}/Actions | Get available actions for my Time Off Request.
[**ListMyTimeOffRequests**](TimeOffRequestsApi.md#listmytimeoffrequests) | **GET** /Me/TimeOffRequests | Get my Time Off Requests.
[**ListTimeOffRequestActions**](TimeOffRequestsApi.md#listtimeoffrequestactions) | **GET** /TimeOffRequests/{timeOffRequestID}/Actions | Get actions for a Time Off Request.
[**ListTimeOffRequests**](TimeOffRequestsApi.md#listtimeoffrequests) | **GET** /TimeOffRequests | Get Time Off Requests.
[**UpdateManagedTimeOffRequestStatus**](TimeOffRequestsApi.md#updatemanagedtimeoffrequeststatus) | **POST** /Manage/TimeOffRequests/{timeOffRequestID}/Actions | Apply an action to a managed TimeOffRequest.
[**UpdateMyTimeOffRequestStatus**](TimeOffRequestsApi.md#updatemytimeoffrequeststatus) | **POST** /Me/TimeOffRequests/{timeOffRequestID}/Actions | Apply an action to my Time Off Request.
[**UpdateTimeOffRequestStatus**](TimeOffRequestsApi.md#updatetimeoffrequeststatus) | **POST** /TimeOffRequests/{timeOffRequestID}/Actions | Apply an action to a Time Off Request.


<a name="createmytimeoffrequest"></a>
# **CreateMyTimeOffRequest**
> ResponseBodyGetTimeOffRequestModelMe CreateMyTimeOffRequest (PostTimeOffRequestModelMe model)

Create my Time Off Request.

       Company requirements:    Company must have the Time Off Management optional module.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class CreateMyTimeOffRequestExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TimeOffRequestsApi();
            var model = new PostTimeOffRequestModelMe(); // PostTimeOffRequestModelMe |       Required

            try
            {
                // Create my Time Off Request.
                ResponseBodyGetTimeOffRequestModelMe result = apiInstance.CreateMyTimeOffRequest(model);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TimeOffRequestsApi.CreateMyTimeOffRequest: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **model** | [**PostTimeOffRequestModelMe**](PostTimeOffRequestModelMe.md)|       Required | 

### Return type

[**ResponseBodyGetTimeOffRequestModelMe**](ResponseBodyGetTimeOffRequestModelMe.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded, application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="createtimeoffrequest"></a>
# **CreateTimeOffRequest**
> ResponseBodyGetTimeOffRequestModelRoot CreateTimeOffRequest (PostTimeOffRequestModelRoot model)

Create a Time Off Request.

       User requirements:    User must be an Admin.       Company requirements:    Company must have the Time Off Management optional module.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class CreateTimeOffRequestExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TimeOffRequestsApi();
            var model = new PostTimeOffRequestModelRoot(); // PostTimeOffRequestModelRoot |       Required

            try
            {
                // Create a Time Off Request.
                ResponseBodyGetTimeOffRequestModelRoot result = apiInstance.CreateTimeOffRequest(model);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TimeOffRequestsApi.CreateTimeOffRequest: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **model** | [**PostTimeOffRequestModelRoot**](PostTimeOffRequestModelRoot.md)|       Required | 

### Return type

[**ResponseBodyGetTimeOffRequestModelRoot**](ResponseBodyGetTimeOffRequestModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded, application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getdefaulttimeoffrequest"></a>
# **GetDefaultTimeOffRequest**
> ResponseBodyGetTimeOffRequestModelNew GetDefaultTimeOffRequest ()

Get a new Time Off Request with default values.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class GetDefaultTimeOffRequestExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TimeOffRequestsApi();

            try
            {
                // Get a new Time Off Request with default values.
                ResponseBodyGetTimeOffRequestModelNew result = apiInstance.GetDefaultTimeOffRequest();
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TimeOffRequestsApi.GetDefaultTimeOffRequest: " + e.Message );
            }
        }
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ResponseBodyGetTimeOffRequestModelNew**](ResponseBodyGetTimeOffRequestModelNew.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getmanagedtimeoffrequest"></a>
# **GetManagedTimeOffRequest**
> ResponseBodyGetTimeOffRequestModelManage GetManagedTimeOffRequest (string timeOffRequestID)

Get a managed Time Off Request.

       Company requirements:    Company must have the Time Off Management optional module.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class GetManagedTimeOffRequestExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TimeOffRequestsApi();
            var timeOffRequestID = timeOffRequestID_example;  // string |       Required

            try
            {
                // Get a managed Time Off Request.
                ResponseBodyGetTimeOffRequestModelManage result = apiInstance.GetManagedTimeOffRequest(timeOffRequestID);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TimeOffRequestsApi.GetManagedTimeOffRequest: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **timeOffRequestID** | **string**|       Required | 

### Return type

[**ResponseBodyGetTimeOffRequestModelManage**](ResponseBodyGetTimeOffRequestModelManage.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getmytimeoffrequest"></a>
# **GetMyTimeOffRequest**
> ResponseBodyGetTimeOffRequestModelMe GetMyTimeOffRequest (string timeOffRequestID)

Get my Time Off Request.

       Company requirements:    Company must have the Time Off Management optional module.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class GetMyTimeOffRequestExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TimeOffRequestsApi();
            var timeOffRequestID = timeOffRequestID_example;  // string |       Required

            try
            {
                // Get my Time Off Request.
                ResponseBodyGetTimeOffRequestModelMe result = apiInstance.GetMyTimeOffRequest(timeOffRequestID);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TimeOffRequestsApi.GetMyTimeOffRequest: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **timeOffRequestID** | **string**|       Required | 

### Return type

[**ResponseBodyGetTimeOffRequestModelMe**](ResponseBodyGetTimeOffRequestModelMe.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="gettimeoffrequest"></a>
# **GetTimeOffRequest**
> ResponseBodyGetTimeOffRequestModelRoot GetTimeOffRequest (string timeOffRequestID)

Get a Time Off Request.

       User requirements:    User must be a Manager or an Admin.       Company requirements:    Company must have the Time Off Management optional module.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class GetTimeOffRequestExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TimeOffRequestsApi();
            var timeOffRequestID = timeOffRequestID_example;  // string |       Required

            try
            {
                // Get a Time Off Request.
                ResponseBodyGetTimeOffRequestModelRoot result = apiInstance.GetTimeOffRequest(timeOffRequestID);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TimeOffRequestsApi.GetTimeOffRequest: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **timeOffRequestID** | **string**|       Required | 

### Return type

[**ResponseBodyGetTimeOffRequestModelRoot**](ResponseBodyGetTimeOffRequestModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="listmanagedtimeoffrequestactions"></a>
# **ListManagedTimeOffRequestActions**
> ResponseBodyListGetActionModelRequestActionManage ListManagedTimeOffRequestActions (string timeOffRequestID)

Get actions for a managed Time Off Request.

       Company requirements:    Company must have the Time Off Management optional module.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ListManagedTimeOffRequestActionsExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TimeOffRequestsApi();
            var timeOffRequestID = timeOffRequestID_example;  // string |       Required

            try
            {
                // Get actions for a managed Time Off Request.
                ResponseBodyListGetActionModelRequestActionManage result = apiInstance.ListManagedTimeOffRequestActions(timeOffRequestID);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TimeOffRequestsApi.ListManagedTimeOffRequestActions: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **timeOffRequestID** | **string**|       Required | 

### Return type

[**ResponseBodyListGetActionModelRequestActionManage**](ResponseBodyListGetActionModelRequestActionManage.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="listmanagedtimeoffrequests"></a>
# **ListManagedTimeOffRequests**
> ResponseBodyListGetTimeOffRequestModelManage ListManagedTimeOffRequests (List<string> ID = null, List<string> approvalByUserID = null, List<string> requestedByUserID = null, List<string> timeOffTypeID = null, List<string> status = null, string fromDate = null, string toDate = null, int? limit = null, int? offset = null)

Get managed Time Off Requests.

       Company requirements:    Company must have the Time Off Management optional module.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ListManagedTimeOffRequestsExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TimeOffRequestsApi();
            var ID = new List<string>(); // List<string> |  (optional) 
            var approvalByUserID = new List<string>(); // List<string> |  (optional) 
            var requestedByUserID = new List<string>(); // List<string> |  (optional) 
            var timeOffTypeID = new List<string>(); // List<string> |  (optional) 
            var status = new List<string>(); // List<string> |  (optional) 
            var fromDate = fromDate_example;  // string |  (optional) 
            var toDate = toDate_example;  // string |  (optional) 
            var limit = 56;  // int? |  (optional) 
            var offset = 56;  // int? |  (optional) 

            try
            {
                // Get managed Time Off Requests.
                ResponseBodyListGetTimeOffRequestModelManage result = apiInstance.ListManagedTimeOffRequests(ID, approvalByUserID, requestedByUserID, timeOffTypeID, status, fromDate, toDate, limit, offset);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TimeOffRequestsApi.ListManagedTimeOffRequests: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ID** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **approvalByUserID** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **requestedByUserID** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **timeOffTypeID** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **status** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **fromDate** | **string**|  | [optional] 
 **toDate** | **string**|  | [optional] 
 **limit** | **int?**|  | [optional] 
 **offset** | **int?**|  | [optional] 

### Return type

[**ResponseBodyListGetTimeOffRequestModelManage**](ResponseBodyListGetTimeOffRequestModelManage.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="listmytimeoffrequestactions"></a>
# **ListMyTimeOffRequestActions**
> ResponseBodyListGetActionModelRequestActionMe ListMyTimeOffRequestActions (string timeOffRequestID)

Get available actions for my Time Off Request.

       Company requirements:    Company must have the Time Off Management optional module.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ListMyTimeOffRequestActionsExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TimeOffRequestsApi();
            var timeOffRequestID = timeOffRequestID_example;  // string |       Required

            try
            {
                // Get available actions for my Time Off Request.
                ResponseBodyListGetActionModelRequestActionMe result = apiInstance.ListMyTimeOffRequestActions(timeOffRequestID);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TimeOffRequestsApi.ListMyTimeOffRequestActions: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **timeOffRequestID** | **string**|       Required | 

### Return type

[**ResponseBodyListGetActionModelRequestActionMe**](ResponseBodyListGetActionModelRequestActionMe.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="listmytimeoffrequests"></a>
# **ListMyTimeOffRequests**
> ResponseBodyListGetTimeOffRequestModelMe ListMyTimeOffRequests (List<string> ID = null, List<string> requestedByUserID = null, List<string> approvalByUserID = null, List<string> timeOffTypeID = null, List<string> status = null, string fromDate = null, string toDate = null, int? limit = null, int? offset = null)

Get my Time Off Requests.

       Company requirements:    Company must have the Time Off Management optional module.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ListMyTimeOffRequestsExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TimeOffRequestsApi();
            var ID = new List<string>(); // List<string> |  (optional) 
            var requestedByUserID = new List<string>(); // List<string> |  (optional) 
            var approvalByUserID = new List<string>(); // List<string> |  (optional) 
            var timeOffTypeID = new List<string>(); // List<string> |  (optional) 
            var status = new List<string>(); // List<string> |  (optional) 
            var fromDate = fromDate_example;  // string |  (optional) 
            var toDate = toDate_example;  // string |  (optional) 
            var limit = 56;  // int? |  (optional) 
            var offset = 56;  // int? |  (optional) 

            try
            {
                // Get my Time Off Requests.
                ResponseBodyListGetTimeOffRequestModelMe result = apiInstance.ListMyTimeOffRequests(ID, requestedByUserID, approvalByUserID, timeOffTypeID, status, fromDate, toDate, limit, offset);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TimeOffRequestsApi.ListMyTimeOffRequests: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ID** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **requestedByUserID** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **approvalByUserID** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **timeOffTypeID** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **status** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **fromDate** | **string**|  | [optional] 
 **toDate** | **string**|  | [optional] 
 **limit** | **int?**|  | [optional] 
 **offset** | **int?**|  | [optional] 

### Return type

[**ResponseBodyListGetTimeOffRequestModelMe**](ResponseBodyListGetTimeOffRequestModelMe.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="listtimeoffrequestactions"></a>
# **ListTimeOffRequestActions**
> ResponseBodyListGetActionModelRequestActionRoot ListTimeOffRequestActions (string timeOffRequestID)

Get actions for a Time Off Request.

       Company requirements:    Company must have the Time Off Management optional module.       User requirements:    User must be an Admin.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ListTimeOffRequestActionsExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TimeOffRequestsApi();
            var timeOffRequestID = timeOffRequestID_example;  // string |       Required

            try
            {
                // Get actions for a Time Off Request.
                ResponseBodyListGetActionModelRequestActionRoot result = apiInstance.ListTimeOffRequestActions(timeOffRequestID);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TimeOffRequestsApi.ListTimeOffRequestActions: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **timeOffRequestID** | **string**|       Required | 

### Return type

[**ResponseBodyListGetActionModelRequestActionRoot**](ResponseBodyListGetActionModelRequestActionRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="listtimeoffrequests"></a>
# **ListTimeOffRequests**
> ResponseBodyListGetTimeOffRequestModelRoot ListTimeOffRequests (List<string> ID = null, List<string> requestedByUserID = null, List<string> approvalByUserID = null, List<string> timeOffTypeID = null, List<string> status = null, string fromDate = null, string toDate = null, int? limit = null, int? offset = null)

Get Time Off Requests.

       Company requirements:    Company must have the Time Off Management optional module.       User requirements:    User must be a Manager or an Admin.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ListTimeOffRequestsExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TimeOffRequestsApi();
            var ID = new List<string>(); // List<string> |  (optional) 
            var requestedByUserID = new List<string>(); // List<string> |  (optional) 
            var approvalByUserID = new List<string>(); // List<string> |  (optional) 
            var timeOffTypeID = new List<string>(); // List<string> |  (optional) 
            var status = new List<string>(); // List<string> |  (optional) 
            var fromDate = fromDate_example;  // string |  (optional) 
            var toDate = toDate_example;  // string |  (optional) 
            var limit = 56;  // int? |  (optional) 
            var offset = 56;  // int? |  (optional) 

            try
            {
                // Get Time Off Requests.
                ResponseBodyListGetTimeOffRequestModelRoot result = apiInstance.ListTimeOffRequests(ID, requestedByUserID, approvalByUserID, timeOffTypeID, status, fromDate, toDate, limit, offset);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TimeOffRequestsApi.ListTimeOffRequests: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ID** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **requestedByUserID** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **approvalByUserID** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **timeOffTypeID** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **status** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **fromDate** | **string**|  | [optional] 
 **toDate** | **string**|  | [optional] 
 **limit** | **int?**|  | [optional] 
 **offset** | **int?**|  | [optional] 

### Return type

[**ResponseBodyListGetTimeOffRequestModelRoot**](ResponseBodyListGetTimeOffRequestModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="updatemanagedtimeoffrequeststatus"></a>
# **UpdateManagedTimeOffRequestStatus**
> ResponseBodyGetTimeOffRequestModelManage UpdateManagedTimeOffRequestStatus (string timeOffRequestID, PostTimeOffRequestActionModelManage model)

Apply an action to a managed TimeOffRequest.

       Company requirements:    Company must have the Time Off Management optional module.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class UpdateManagedTimeOffRequestStatusExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TimeOffRequestsApi();
            var timeOffRequestID = timeOffRequestID_example;  // string |       Required
            var model = new PostTimeOffRequestActionModelManage(); // PostTimeOffRequestActionModelManage |       Required

            try
            {
                // Apply an action to a managed TimeOffRequest.
                ResponseBodyGetTimeOffRequestModelManage result = apiInstance.UpdateManagedTimeOffRequestStatus(timeOffRequestID, model);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TimeOffRequestsApi.UpdateManagedTimeOffRequestStatus: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **timeOffRequestID** | **string**|       Required | 
 **model** | [**PostTimeOffRequestActionModelManage**](PostTimeOffRequestActionModelManage.md)|       Required | 

### Return type

[**ResponseBodyGetTimeOffRequestModelManage**](ResponseBodyGetTimeOffRequestModelManage.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded, application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="updatemytimeoffrequeststatus"></a>
# **UpdateMyTimeOffRequestStatus**
> ResponseBodyGetTimeOffRequestModelMe UpdateMyTimeOffRequestStatus (string timeOffRequestID, PostTimeOffRequestActionModelMe model)

Apply an action to my Time Off Request.

       Company requirements:    Company must have the Time Off Management optional module.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class UpdateMyTimeOffRequestStatusExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TimeOffRequestsApi();
            var timeOffRequestID = timeOffRequestID_example;  // string |       Required
            var model = new PostTimeOffRequestActionModelMe(); // PostTimeOffRequestActionModelMe |       Required

            try
            {
                // Apply an action to my Time Off Request.
                ResponseBodyGetTimeOffRequestModelMe result = apiInstance.UpdateMyTimeOffRequestStatus(timeOffRequestID, model);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TimeOffRequestsApi.UpdateMyTimeOffRequestStatus: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **timeOffRequestID** | **string**|       Required | 
 **model** | [**PostTimeOffRequestActionModelMe**](PostTimeOffRequestActionModelMe.md)|       Required | 

### Return type

[**ResponseBodyGetTimeOffRequestModelMe**](ResponseBodyGetTimeOffRequestModelMe.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded, application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="updatetimeoffrequeststatus"></a>
# **UpdateTimeOffRequestStatus**
> ResponseBodyGetTimeOffRequestModelRoot UpdateTimeOffRequestStatus (string timeOffRequestID, PostTimeOffRequestActionModelRoot model)

Apply an action to a Time Off Request.

       Company requirements:    Company must have the Time Off Management optional module.       User requirements:    User must be an Admin.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class UpdateTimeOffRequestStatusExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TimeOffRequestsApi();
            var timeOffRequestID = timeOffRequestID_example;  // string |       Required
            var model = new PostTimeOffRequestActionModelRoot(); // PostTimeOffRequestActionModelRoot |       Required

            try
            {
                // Apply an action to a Time Off Request.
                ResponseBodyGetTimeOffRequestModelRoot result = apiInstance.UpdateTimeOffRequestStatus(timeOffRequestID, model);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TimeOffRequestsApi.UpdateTimeOffRequestStatus: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **timeOffRequestID** | **string**|       Required | 
 **model** | [**PostTimeOffRequestActionModelRoot**](PostTimeOffRequestActionModelRoot.md)|       Required | 

### Return type

[**ResponseBodyGetTimeOffRequestModelRoot**](ResponseBodyGetTimeOffRequestModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded, application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

