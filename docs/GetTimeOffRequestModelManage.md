# IO.Swagger.Model.GetTimeOffRequestModelManage
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Actions** | [**List&lt;GetActionModelRequestAction&gt;**](GetActionModelRequestAction.md) |  | [optional] 
**ApprovalByUser** | [**GetBasicUserModelApprovalByUserGetTimeOffRequestModelManage**](GetBasicUserModelApprovalByUserGetTimeOffRequestModelManage.md) |        Request requirements:    Verbose&#x3D;true | [optional] 
**ApprovalByUserID** | **string** |  | [optional] 
**CreatedDate** | **string** |  | [optional] 
**Dates** | [**List&lt;GetTimeOffRequestDateModelDatesGetTimeOffRequestModelManage&gt;**](GetTimeOffRequestDateModelDatesGetTimeOffRequestModelManage.md) |  | [optional] 
**History** | [**List&lt;GetTimeOffRequestHistoryModelHistoryGetTimeOffRequestModelManage&gt;**](GetTimeOffRequestHistoryModelHistoryGetTimeOffRequestModelManage.md) |        Request requirements:    Verbose&#x3D;true | [optional] 
**ID** | **string** |  | [optional] 
**LegacyID** | **string** |        Request requirements:    CTLegacyScramble&#x3D;true | [optional] 
**Notes** | **string** |  | [optional] 
**RequestedByUser** | [**GetBasicUserModelRequestedByUserGetTimeOffRequestModelManage**](GetBasicUserModelRequestedByUserGetTimeOffRequestModelManage.md) |        Request requirements:    Verbose&#x3D;true | [optional] 
**RequestedByUserID** | **string** |  | [optional] 
**Status** | **string** |  | [optional] 
**TimeOffType** | [**GetBasicTimeOffTypeModelTimeOffTypeGetTimeOffRequestModelManage**](GetBasicTimeOffTypeModelTimeOffTypeGetTimeOffRequestModelManage.md) |        Request requirements:    Verbose&#x3D;true | [optional] 
**TimeOffTypeID** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

