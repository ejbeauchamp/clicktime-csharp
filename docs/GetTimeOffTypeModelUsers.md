# IO.Swagger.Model.GetTimeOffTypeModelUsers
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**AccrualRate** | **double?** |        Company requirements:    Optional module(s): Time Off Management | [optional] 
**AccrualStartDate** | **string** |        Company requirements:    Optional module(s): Time Off Management | [optional] 
**Approver** | [**GetBasicUserModelApproverGetTimeOffTypeModelUsers**](GetBasicUserModelApproverGetTimeOffTypeModelUsers.md) |        Request requirements:    Verbose&#x3D;true       Company requirements:    Optional module(s): Time Off Management | [optional] 
**ApproverID** | **string** |        Company requirements:    Optional module(s): Time Off Management | [optional] 
**CurrentBalance** | **double?** |  | [optional] 
**DisplayOnTimeEntryPages** | **bool?** |        Company requirements:    Optional module(s): Time Off Management    CanViewWorkTypeBalanceDashboard | [optional] 
**EmploymentTypeIDs** | **List&lt;string&gt;** |        Request requirements:    Verbose&#x3D;true       Company requirements:    Leave Type Restrictions: By Employment Type | [optional] 
**ID** | **string** |  | [optional] 
**IsActive** | **bool?** |  | [optional] 
**LegacyID** | **string** |        Request requirements:    CTLegacyScramble&#x3D;true | [optional] 
**MaximumBalance** | **double?** |        Company requirements:    Optional module(s): Time Off Management | [optional] 
**Name** | **string** |  | [optional] 
**RequiresApproval** | **bool?** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

