# IO.Swagger.Api.ExpenseTypesApi

All URIs are relative to *https://api.clicktime.com/V2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**GetExpenseType**](ExpenseTypesApi.md#getexpensetype) | **GET** /ExpenseTypes/{expenseTypeID} | Get an Expense Type.
[**ListExpenseTypes**](ExpenseTypesApi.md#listexpensetypes) | **GET** /ExpenseTypes | Get Expense Types.


<a name="getexpensetype"></a>
# **GetExpenseType**
> ResponseBodyGetExpenseTypeModelRoot GetExpenseType (string expenseTypeID)

Get an Expense Type.

       Company requirements:    Company must have the Expenses optional module.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class GetExpenseTypeExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new ExpenseTypesApi();
            var expenseTypeID = expenseTypeID_example;  // string |       Required

            try
            {
                // Get an Expense Type.
                ResponseBodyGetExpenseTypeModelRoot result = apiInstance.GetExpenseType(expenseTypeID);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ExpenseTypesApi.GetExpenseType: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **expenseTypeID** | **string**|       Required | 

### Return type

[**ResponseBodyGetExpenseTypeModelRoot**](ResponseBodyGetExpenseTypeModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="listexpensetypes"></a>
# **ListExpenseTypes**
> ResponseBodyListGetExpenseTypeModelRoot ListExpenseTypes (List<string> ID = null, List<bool?> isActive = null, int? limit = null, int? offset = null)

Get Expense Types.

       Company requirements:    Company must have the Expenses optional module.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ListExpenseTypesExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new ExpenseTypesApi();
            var ID = new List<string>(); // List<string> |  (optional) 
            var isActive = new List<bool?>(); // List<bool?> |  (optional) 
            var limit = 56;  // int? |  (optional) 
            var offset = 56;  // int? |  (optional) 

            try
            {
                // Get Expense Types.
                ResponseBodyListGetExpenseTypeModelRoot result = apiInstance.ListExpenseTypes(ID, isActive, limit, offset);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ExpenseTypesApi.ListExpenseTypes: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ID** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **isActive** | [**List&lt;bool?&gt;**](bool?.md)|  | [optional] 
 **limit** | **int?**|  | [optional] 
 **offset** | **int?**|  | [optional] 

### Return type

[**ResponseBodyListGetExpenseTypeModelRoot**](ResponseBodyListGetExpenseTypeModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

