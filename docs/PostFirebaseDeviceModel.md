# IO.Swagger.Model.PostFirebaseDeviceModel
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DeviceUniqueID** | **string** |  | 
**FirebaseAppInstanceToken** | **string** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

