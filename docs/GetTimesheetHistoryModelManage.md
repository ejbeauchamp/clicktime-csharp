# IO.Swagger.Model.GetTimesheetHistoryModelManage
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Action** | **string** |  | [optional] 
**ActionByUser** | [**GetBasicUserModelActionByUserGetTimesheetHistoryModelManage**](GetBasicUserModelActionByUserGetTimesheetHistoryModelManage.md) |        Request requirements:    Verbose&#x3D;true | [optional] 
**ActionByUserID** | **string** |  | [optional] 
**ActionByUserName** | **string** |  | [optional] 
**Comment** | **string** |  | [optional] 
**Date** | **string** |  | [optional] 
**Job** | [**GetBasicJobModelJobGetTimesheetHistoryModelManage**](GetBasicJobModelJobGetTimesheetHistoryModelManage.md) |        Request requirements:    Verbose&#x3D;true | [optional] 
**JobID** | **string** |  | [optional] 
**JobStatus** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

