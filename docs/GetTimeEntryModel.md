# IO.Swagger.Model.GetTimeEntryModel
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**BillingRate** | **double?** |  | [optional] 
**BreakTime** | **double?** |  | [optional] 
**_Client** | [**GetBasicClientModel**](GetBasicClientModel.md) |  | [optional] 
**Comment** | **string** |  | [optional] 
**CostRate** | **double?** |  | [optional] 
**CustomFields** | **Dictionary&lt;string, Object&gt;** |  | [optional] 
**Date** | **string** |  | [optional] 
**EndTime** | **string** |  | [optional] 
**Hours** | **double?** |  | [optional] 
**ID** | **string** |  | [optional] 
**Job** | [**GetBasicJobModel**](GetBasicJobModel.md) |  | [optional] 
**JobID** | **string** |  | [optional] 
**LegacyID** | **string** |  | [optional] 
**StartTime** | **string** |  | [optional] 
**Task** | [**GetBasicTaskModel**](GetBasicTaskModel.md) |  | [optional] 
**TaskID** | **string** |  | [optional] 
**User** | [**GetBasicUserModel**](GetBasicUserModel.md) |  | [optional] 
**UserID** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

