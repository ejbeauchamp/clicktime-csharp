# IO.Swagger.Model.GetAllocationModel
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**EstHours** | **double?** |  | [optional] 
**Job** | [**GetBasicJobModel**](GetBasicJobModel.md) |  | [optional] 
**JobID** | **string** |  | [optional] 
**Month** | **string** |  | [optional] 
**User** | [**GetBasicUserModel**](GetBasicUserModel.md) |  | [optional] 
**UserID** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

