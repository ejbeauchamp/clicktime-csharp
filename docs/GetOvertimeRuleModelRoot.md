# IO.Swagger.Model.GetOvertimeRuleModelRoot
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**CreatedDate** | **string** |  | [optional] 
**ID** | **string** |  | [optional] 
**IsActive** | **bool?** |  | [optional] 
**LegacyID** | **string** |        Request requirements:    CTLegacyScramble&#x3D;true | [optional] 
**ModifiedDate** | **string** |  | [optional] 
**OvertimeDefinitionID** | **string** |  | [optional] 
**RuleDefinition** | [**GetBasicRuleDefinitionModelRuleDefinitionGetOvertimeRuleModelRoot**](GetBasicRuleDefinitionModelRuleDefinitionGetOvertimeRuleModelRoot.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

