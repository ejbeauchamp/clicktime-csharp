# IO.Swagger.Model.PostUserModel
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**AccountingPackageID** | **string** |  | [optional] 
**AllowIncompleteTimesheetSubmission** | **bool?** |  | [optional] 
**BillingRate** | **double?** |  | [optional] 
**CostRate** | **double?** |  | [optional] 
**CustomFields** | **Dictionary&lt;string, Object&gt;** |  | [optional] 
**DefaultExpenseTypeID** | **string** |  | [optional] 
**DefaultPaymentTypeID** | **string** |  | [optional] 
**DefaultTaskID** | **string** |  | [optional] 
**DivisionID** | **string** |  | [optional] 
**Email** | **string** |  | 
**EmployeeNumber** | **string** |  | [optional] 
**EmploymentTypeID** | **string** |  | [optional] 
**EnableBreakTime** | **bool?** |  | [optional] 
**EndDate** | **string** |  | [optional] 
**ExpenseApproverID** | **string** | SubjectToExpenseApproval must be true in order to set ExpenseApproverID. | [optional] 
**GDPRConsentStatus** | **string** |  | [optional] 
**IsActive** | **bool?** |  | [optional] 
**MinimumTimeHours** | **double?** |  | [optional] 
**MinimumTimePeriod** | **string** |  | [optional] 
**Name** | **string** |  | 
**Notes** | **string** |  | [optional] 
**PayrollType** | **string** |  | [optional] 
**PreferredTimeEntryView** | **string** |  | [optional] 
**PreferredTimeFormat** | **string** |  | [optional] 
**RequireComments** | **bool?** |  | [optional] 
**RequireStartEndTime** | **bool?** |  | [optional] 
**RequireStopwatch** | **bool?** |  | [optional] 
**Role** | **string** |  | [optional] 
**SkipWeekend** | **bool?** |  | [optional] 
**StartDate** | **string** |  | [optional] 
**SubjectToExpenseApproval** | **bool?** |  | [optional] 
**SubjectToTimesheetApproval** | **bool?** |  | [optional] 
**SubjectToTimesheetCompletion** | **bool?** |  | [optional] 
**TimeOffApproverID** | **string** |  | [optional] 
**TimesheetApproverID** | **string** | SubjectToTimesheetApproval must be true in order to set TimesheetApproverID. | [optional] 
**UseCompanyBillingRate** | **bool?** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

