# IO.Swagger.Model.PostOvertimeRuleModelRoot
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**RuleDefinition** | [**PostBasicRuleDefinitionModelRuleDefinitionPostOvertimeRuleModelRoot**](PostBasicRuleDefinitionModelRuleDefinitionPostOvertimeRuleModelRoot.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

