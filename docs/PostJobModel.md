# IO.Swagger.Model.PostJobModel
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**AccountingPackageID** | **string** |  | [optional] 
**BillingRate** | **double?** |  | [optional] 
**ClientID** | **string** |  | 
**CustomFields** | **Dictionary&lt;string, Object&gt;** |  | [optional] 
**IsActive** | **bool?** |  | [optional] 
**IsBillable** | **bool?** |  | [optional] 
**JobNumber** | **string** |  | 
**Name** | **string** |  | 
**Notes** | **string** |  | [optional] 
**ProjectManagerID** | **string** | Required if TimeRequiresApproval is true. | [optional] 
**SecondaryBillingRateMode** | **string** |  | [optional] 
**TimeRequiresApproval** | **bool?** |  | [optional] 
**UseCompanyBillingRate** | **bool?** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

