# IO.Swagger.Model.GetOvertimeModelNew
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**CompanyID** | **string** |  | [optional] 
**CreatedDate** | **string** |  | [optional] 
**EmploymentTypeID** | **string** |  | [optional] 
**ID** | **string** |  | [optional] 
**IncludeTimeOff** | **bool?** |  | [optional] 
**IsActive** | **bool?** |  | [optional] 
**ModifiedDate** | **string** |  | [optional] 
**Notes** | **string** |  | [optional] 
**OvertimeRules** | [**List&lt;GetOvertimeRuleModelOvertimeRulesGetOvertimeModelNew&gt;**](GetOvertimeRuleModelOvertimeRulesGetOvertimeModelNew.md) |        Request requirements:    Verbose&#x3D;true | [optional] 
**RulePreset** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

