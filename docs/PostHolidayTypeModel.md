# IO.Swagger.Model.PostHolidayTypeModel
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**AccountingPackageID** | **string** |  | [optional] 
**IsActive** | **bool?** |  | [optional] 
**Name** | **string** |  | 
**Notes** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

