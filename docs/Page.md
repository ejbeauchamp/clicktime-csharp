# IO.Swagger.Model.Page
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**count** | **int?** |  | [optional] 
**limit** | **int?** |  | [optional] 
**links** | [**Links**](Links.md) |  | [optional] 
**offset** | **int?** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

