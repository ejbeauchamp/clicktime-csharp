# IO.Swagger.Model.GetTimeOffRequestModelNew
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Actions** | [**List&lt;GetActionModelRequestAction&gt;**](GetActionModelRequestAction.md) |  | [optional] 
**ApprovalByUser** | [**GetBasicUserModelApprovalByUserGetTimeOffRequestModelNew**](GetBasicUserModelApprovalByUserGetTimeOffRequestModelNew.md) |        Request requirements:    Verbose&#x3D;true | [optional] 
**ApprovalByUserID** | **string** |  | [optional] 
**CreatedDate** | **string** |  | [optional] 
**Dates** | [**List&lt;GetTimeOffRequestDateModelDatesGetTimeOffRequestModelNew&gt;**](GetTimeOffRequestDateModelDatesGetTimeOffRequestModelNew.md) |  | [optional] 
**History** | [**List&lt;GetTimeOffRequestHistoryModelHistoryGetTimeOffRequestModelNew&gt;**](GetTimeOffRequestHistoryModelHistoryGetTimeOffRequestModelNew.md) |        Request requirements:    Verbose&#x3D;true | [optional] 
**ID** | **string** |  | [optional] 
**Notes** | **string** |  | [optional] 
**RequestedByUser** | [**GetBasicUserModelRequestedByUserGetTimeOffRequestModelNew**](GetBasicUserModelRequestedByUserGetTimeOffRequestModelNew.md) |        Request requirements:    Verbose&#x3D;true | [optional] 
**RequestedByUserID** | **string** |  | [optional] 
**Status** | **string** |  | [optional] 
**TimeOffType** | [**GetBasicTimeOffTypeModelTimeOffTypeGetTimeOffRequestModelNew**](GetBasicTimeOffTypeModelTimeOffTypeGetTimeOffRequestModelNew.md) |        Request requirements:    Verbose&#x3D;true | [optional] 
**TimeOffTypeID** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

