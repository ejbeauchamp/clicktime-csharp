# IO.Swagger.Model.GetTimeOffRequestModelRoot
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Actions** | [**List&lt;GetActionModelRequestAction&gt;**](GetActionModelRequestAction.md) |        User requirements:    Security Level(s): Admin | [optional] 
**ApprovalByUser** | [**GetBasicUserModelApprovalByUserGetTimeOffRequestModelRoot**](GetBasicUserModelApprovalByUserGetTimeOffRequestModelRoot.md) |        Request requirements:    Verbose&#x3D;true | [optional] 
**ApprovalByUserID** | **string** |  | [optional] 
**CreatedDate** | **string** |        User requirements:    Security Level(s): Admin | [optional] 
**Dates** | [**List&lt;GetTimeOffRequestDateModelDatesGetTimeOffRequestModelRoot&gt;**](GetTimeOffRequestDateModelDatesGetTimeOffRequestModelRoot.md) |  | [optional] 
**History** | [**List&lt;GetTimeOffRequestHistoryModelHistoryGetTimeOffRequestModelRoot&gt;**](GetTimeOffRequestHistoryModelHistoryGetTimeOffRequestModelRoot.md) |        Request requirements:    Verbose&#x3D;true       User requirements:    Security Level(s): Admin | [optional] 
**ID** | **string** |  | [optional] 
**LegacyID** | **string** |        Request requirements:    CTLegacyScramble&#x3D;true | [optional] 
**Notes** | **string** |        User requirements:    Security Level(s): Admin | [optional] 
**RequestedByUser** | [**GetBasicUserModelRequestedByUserGetTimeOffRequestModelRoot**](GetBasicUserModelRequestedByUserGetTimeOffRequestModelRoot.md) |        Request requirements:    Verbose&#x3D;true | [optional] 
**RequestedByUserID** | **string** |  | [optional] 
**Status** | **string** |  | [optional] 
**TimeOffType** | [**GetBasicTimeOffTypeModelTimeOffTypeGetTimeOffRequestModelRoot**](GetBasicTimeOffTypeModelTimeOffTypeGetTimeOffRequestModelRoot.md) |        Request requirements:    Verbose&#x3D;true       User requirements:    Security Level(s): Admin | [optional] 
**TimeOffTypeID** | **string** |        User requirements:    Security Level(s): Admin | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

