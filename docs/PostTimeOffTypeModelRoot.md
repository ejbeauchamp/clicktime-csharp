# IO.Swagger.Model.PostTimeOffTypeModelRoot
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**AccountingPackageID** | **string** |        User requirements:    Security Level(s): Admin | [optional] 
**DefaultAccrualRate** | **double?** |        User requirements:    Security Level(s): Admin       Company requirements:    Optional module(s): Time Off Management | [optional] 
**DefaultApproverID** | **string** |        User requirements:    Security Level(s): Admin       Company requirements:    Optional module(s): Time Off Management | [optional] 
**DefaultMaximumBalance** | **double?** |        User requirements:    Security Level(s): Admin       Company requirements:    Optional module(s): Time Off Management | [optional] 
**DefaultStartingBalance** | **double?** |        User requirements:    Security Level(s): Admin | [optional] 
**DisplayOnTimeEntryPages** | **bool?** |        Company requirements:    Optional module(s): Time Off Management    CanViewWorkTypeBalanceDashboard | [optional] 
**IsActive** | **bool?** |  | [optional] 
**Name** | **string** |  | 
**Notes** | **string** |        User requirements:    Security Level(s): Admin | [optional] 
**RequiresApproval** | **bool?** |        Company requirements:    Optional module(s): Time Off Management | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

