# IO.Swagger.Api.PermissionsApi

All URIs are relative to *https://api.clicktime.com/V2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**ListManagedPermissionDivisions**](PermissionsApi.md#listmanagedpermissiondivisions) | **GET** /Manage/Permissions/Divisions | Get a list of divisions a user manages.
[**ListManagedPermissions**](PermissionsApi.md#listmanagedpermissions) | **GET** /Manage/Permissions | Get all manager permissions for my user account.


<a name="listmanagedpermissiondivisions"></a>
# **ListManagedPermissionDivisions**
> ResponseBodyListGetBasicDivisionModelManage ListManagedPermissionDivisions ()

Get a list of divisions a user manages.

       User requirements:    User must be a Manager or an Admin.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ListManagedPermissionDivisionsExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new PermissionsApi();

            try
            {
                // Get a list of divisions a user manages.
                ResponseBodyListGetBasicDivisionModelManage result = apiInstance.ListManagedPermissionDivisions();
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling PermissionsApi.ListManagedPermissionDivisions: " + e.Message );
            }
        }
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ResponseBodyListGetBasicDivisionModelManage**](ResponseBodyListGetBasicDivisionModelManage.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="listmanagedpermissions"></a>
# **ListManagedPermissions**
> ResponseBodyListGetPermissionModelManage ListManagedPermissions ()

Get all manager permissions for my user account.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ListManagedPermissionsExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new PermissionsApi();

            try
            {
                // Get all manager permissions for my user account.
                ResponseBodyListGetPermissionModelManage result = apiInstance.ListManagedPermissions();
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling PermissionsApi.ListManagedPermissions: " + e.Message );
            }
        }
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ResponseBodyListGetPermissionModelManage**](ResponseBodyListGetPermissionModelManage.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

