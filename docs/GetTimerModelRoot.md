# IO.Swagger.Model.GetTimerModelRoot
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ElapsedSeconds** | **int?** |  | [optional] 
**ID** | **string** |  | [optional] 
**InitialTimeInSeconds** | **int?** |  | [optional] 
**Intervals** | [**List&lt;GetTimerIntervalModelIntervalsGetTimerModelRoot&gt;**](GetTimerIntervalModelIntervalsGetTimerModelRoot.md) |  | [optional] 
**LegacyID** | **string** |        Request requirements:    CTLegacyScramble&#x3D;true | [optional] 
**TimeEntryDate** | **string** |  | [optional] 
**TimeEntryID** | **string** |  | [optional] 
**UserID** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

