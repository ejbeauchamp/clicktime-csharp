# IO.Swagger.Model.GetTimerModel
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ElapsedSeconds** | **int?** |  | [optional] 
**ID** | **string** |  | [optional] 
**InitialTimeInSeconds** | **int?** |  | [optional] 
**Intervals** | [**List&lt;GetTimerIntervalModel&gt;**](GetTimerIntervalModel.md) |  | [optional] 
**LegacyID** | **string** |  | [optional] 
**TimeEntryDate** | **string** |  | [optional] 
**TimeEntryID** | **string** |  | [optional] 
**UserID** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

