# IO.Swagger.Api.TimeOffTypesApi

All URIs are relative to *https://api.clicktime.com/V2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**AddTimeOffTypeEmploymentType**](TimeOffTypesApi.md#addtimeofftypeemploymenttype) | **POST** /TimeOffTypes/{timeOffTypeID}/EmploymentTypes | Add an Employment Type to a Time Off Type.
[**CreateTimeOffType**](TimeOffTypesApi.md#createtimeofftype) | **POST** /TimeOffTypes | Create a Time Off Type.
[**GetDefaultTimeOffType**](TimeOffTypesApi.md#getdefaulttimeofftype) | **GET** /TimeOffTypes/new | Get a new Time Off Type with default values.
[**GetManagedUsersTimeOffTypeFutureBalance**](TimeOffTypesApi.md#getmanageduserstimeofftypefuturebalance) | **GET** /Manage/Users/{userID}/TimeOffTypes/{timeOffTypeID}/FutureBalance/{date} | Get a managed User&#39;s Time Off Type future balance.
[**GetMyTimeOffType**](TimeOffTypesApi.md#getmytimeofftype) | **GET** /Me/TimeOffTypes/{timeOffTypeID} | Get my Time Off Type.
[**GetMyTimeOffTypeFutureBalance**](TimeOffTypesApi.md#getmytimeofftypefuturebalance) | **GET** /Me/TimeOffTypes/{timeOffTypeID}/FutureBalance/{date} | Get my Time Off Type future balance.
[**GetTimeOffType**](TimeOffTypesApi.md#gettimeofftype) | **GET** /TimeOffTypes/{timeOffTypeID} | Get a Time Off Type.
[**GetUserTimeOffType**](TimeOffTypesApi.md#getusertimeofftype) | **GET** /Users/{userID}/TimeOffTypes/{timeOffTypeID} | Get a User&#39;s Time Off Type.
[**ListMyTimeOffTypes**](TimeOffTypesApi.md#listmytimeofftypes) | **GET** /Me/TimeOffTypes | Get my Time Off Types.
[**ListTimeOffTypeEmploymentTypes**](TimeOffTypesApi.md#listtimeofftypeemploymenttypes) | **GET** /TimeOffTypes/{timeOffTypeID}/EmploymentTypes | Get available Employment Types.
[**ListTimeOffTypes**](TimeOffTypesApi.md#listtimeofftypes) | **GET** /TimeOffTypes | Get Time Off Types.
[**ListUserTimeOffTypes**](TimeOffTypesApi.md#listusertimeofftypes) | **GET** /Users/{userID}/TimeOffTypes | Get a User&#39;s Time Off Types.
[**RemoveTimeOffTypeEmploymentType**](TimeOffTypesApi.md#removetimeofftypeemploymenttype) | **DELETE** /TimeOffTypes/{timeOffTypeID}/EmploymentTypes/{employmentTypeID} | Remove Employment Type from Time Off Type.
[**UpdateTimeOffType**](TimeOffTypesApi.md#updatetimeofftype) | **PATCH** /TimeOffTypes/{timeOffTypeID} | Update a Time Off Type.
[**UpdateTimeOffTypeEmploymentTypes**](TimeOffTypesApi.md#updatetimeofftypeemploymenttypes) | **PUT** /TimeOffTypes/{timeOffTypeID}/EmploymentTypes | Set available Employment Types.


<a name="addtimeofftypeemploymenttype"></a>
# **AddTimeOffTypeEmploymentType**
> ResponseBodyListSystemString AddTimeOffTypeEmploymentType (string timeOffTypeID, PostEmploymentTypeAssociationModelRoot model)

Add an Employment Type to a Time Off Type.

       User requirements:    User must be an Admin.       Company requirements:    Leave Type List Control must be restricted by Employment Type.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class AddTimeOffTypeEmploymentTypeExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TimeOffTypesApi();
            var timeOffTypeID = timeOffTypeID_example;  // string |       Required
            var model = new PostEmploymentTypeAssociationModelRoot(); // PostEmploymentTypeAssociationModelRoot |       Required

            try
            {
                // Add an Employment Type to a Time Off Type.
                ResponseBodyListSystemString result = apiInstance.AddTimeOffTypeEmploymentType(timeOffTypeID, model);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TimeOffTypesApi.AddTimeOffTypeEmploymentType: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **timeOffTypeID** | **string**|       Required | 
 **model** | [**PostEmploymentTypeAssociationModelRoot**](PostEmploymentTypeAssociationModelRoot.md)|       Required | 

### Return type

[**ResponseBodyListSystemString**](ResponseBodyListSystemString.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded, application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="createtimeofftype"></a>
# **CreateTimeOffType**
> ResponseBodyGetTimeOffTypeModelRoot CreateTimeOffType (PostTimeOffTypeModelRoot model)

Create a Time Off Type.

       User requirements:    User must be an Admin.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class CreateTimeOffTypeExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TimeOffTypesApi();
            var model = new PostTimeOffTypeModelRoot(); // PostTimeOffTypeModelRoot |       Required

            try
            {
                // Create a Time Off Type.
                ResponseBodyGetTimeOffTypeModelRoot result = apiInstance.CreateTimeOffType(model);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TimeOffTypesApi.CreateTimeOffType: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **model** | [**PostTimeOffTypeModelRoot**](PostTimeOffTypeModelRoot.md)|       Required | 

### Return type

[**ResponseBodyGetTimeOffTypeModelRoot**](ResponseBodyGetTimeOffTypeModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded, application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getdefaulttimeofftype"></a>
# **GetDefaultTimeOffType**
> ResponseBodyGetTimeOffTypeModelNew GetDefaultTimeOffType ()

Get a new Time Off Type with default values.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class GetDefaultTimeOffTypeExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TimeOffTypesApi();

            try
            {
                // Get a new Time Off Type with default values.
                ResponseBodyGetTimeOffTypeModelNew result = apiInstance.GetDefaultTimeOffType();
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TimeOffTypesApi.GetDefaultTimeOffType: " + e.Message );
            }
        }
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ResponseBodyGetTimeOffTypeModelNew**](ResponseBodyGetTimeOffTypeModelNew.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getmanageduserstimeofftypefuturebalance"></a>
# **GetManagedUsersTimeOffTypeFutureBalance**
> ResponseBodyGetFutureBalanceModelManage GetManagedUsersTimeOffTypeFutureBalance (string userID, string timeOffTypeID, DateTime? date)

Get a managed User's Time Off Type future balance.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class GetManagedUsersTimeOffTypeFutureBalanceExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TimeOffTypesApi();
            var userID = userID_example;  // string |       Required
            var timeOffTypeID = timeOffTypeID_example;  // string |       Required
            var date = 2013-10-20T19:20:30+01:00;  // DateTime? |       Required

            try
            {
                // Get a managed User's Time Off Type future balance.
                ResponseBodyGetFutureBalanceModelManage result = apiInstance.GetManagedUsersTimeOffTypeFutureBalance(userID, timeOffTypeID, date);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TimeOffTypesApi.GetManagedUsersTimeOffTypeFutureBalance: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userID** | **string**|       Required | 
 **timeOffTypeID** | **string**|       Required | 
 **date** | **DateTime?**|       Required | 

### Return type

[**ResponseBodyGetFutureBalanceModelManage**](ResponseBodyGetFutureBalanceModelManage.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getmytimeofftype"></a>
# **GetMyTimeOffType**
> ResponseBodyGetTimeOffTypeModelMe GetMyTimeOffType (string timeOffTypeID)

Get my Time Off Type.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class GetMyTimeOffTypeExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TimeOffTypesApi();
            var timeOffTypeID = timeOffTypeID_example;  // string |       Required

            try
            {
                // Get my Time Off Type.
                ResponseBodyGetTimeOffTypeModelMe result = apiInstance.GetMyTimeOffType(timeOffTypeID);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TimeOffTypesApi.GetMyTimeOffType: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **timeOffTypeID** | **string**|       Required | 

### Return type

[**ResponseBodyGetTimeOffTypeModelMe**](ResponseBodyGetTimeOffTypeModelMe.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getmytimeofftypefuturebalance"></a>
# **GetMyTimeOffTypeFutureBalance**
> ResponseBodyGetFutureBalanceModelMe GetMyTimeOffTypeFutureBalance (string timeOffTypeID, DateTime? date)

Get my Time Off Type future balance.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class GetMyTimeOffTypeFutureBalanceExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TimeOffTypesApi();
            var timeOffTypeID = timeOffTypeID_example;  // string |       Required
            var date = 2013-10-20T19:20:30+01:00;  // DateTime? |       Required

            try
            {
                // Get my Time Off Type future balance.
                ResponseBodyGetFutureBalanceModelMe result = apiInstance.GetMyTimeOffTypeFutureBalance(timeOffTypeID, date);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TimeOffTypesApi.GetMyTimeOffTypeFutureBalance: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **timeOffTypeID** | **string**|       Required | 
 **date** | **DateTime?**|       Required | 

### Return type

[**ResponseBodyGetFutureBalanceModelMe**](ResponseBodyGetFutureBalanceModelMe.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="gettimeofftype"></a>
# **GetTimeOffType**
> ResponseBodyGetTimeOffTypeModelRoot GetTimeOffType (string timeOffTypeID)

Get a Time Off Type.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class GetTimeOffTypeExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TimeOffTypesApi();
            var timeOffTypeID = timeOffTypeID_example;  // string |       Required

            try
            {
                // Get a Time Off Type.
                ResponseBodyGetTimeOffTypeModelRoot result = apiInstance.GetTimeOffType(timeOffTypeID);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TimeOffTypesApi.GetTimeOffType: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **timeOffTypeID** | **string**|       Required | 

### Return type

[**ResponseBodyGetTimeOffTypeModelRoot**](ResponseBodyGetTimeOffTypeModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getusertimeofftype"></a>
# **GetUserTimeOffType**
> ResponseBodyGetTimeOffTypeModelUsers GetUserTimeOffType (string userID, string timeOffTypeID)

Get a User's Time Off Type.

       User requirements:    Admin, or Manager with permissions to Add/Edit People.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class GetUserTimeOffTypeExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TimeOffTypesApi();
            var userID = userID_example;  // string |       Required
            var timeOffTypeID = timeOffTypeID_example;  // string |       Required

            try
            {
                // Get a User's Time Off Type.
                ResponseBodyGetTimeOffTypeModelUsers result = apiInstance.GetUserTimeOffType(userID, timeOffTypeID);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TimeOffTypesApi.GetUserTimeOffType: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userID** | **string**|       Required | 
 **timeOffTypeID** | **string**|       Required | 

### Return type

[**ResponseBodyGetTimeOffTypeModelUsers**](ResponseBodyGetTimeOffTypeModelUsers.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="listmytimeofftypes"></a>
# **ListMyTimeOffTypes**
> ResponseBodyListGetTimeOffTypeModelMe ListMyTimeOffTypes (List<string> ID = null, List<bool?> isActive = null, List<bool?> requiresApproval = null, List<string> name = null, int? limit = null, int? offset = null)

Get my Time Off Types.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ListMyTimeOffTypesExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TimeOffTypesApi();
            var ID = new List<string>(); // List<string> |  (optional) 
            var isActive = new List<bool?>(); // List<bool?> |  (optional) 
            var requiresApproval = new List<bool?>(); // List<bool?> |  (optional) 
            var name = new List<string>(); // List<string> |  (optional) 
            var limit = 56;  // int? |  (optional) 
            var offset = 56;  // int? |  (optional) 

            try
            {
                // Get my Time Off Types.
                ResponseBodyListGetTimeOffTypeModelMe result = apiInstance.ListMyTimeOffTypes(ID, isActive, requiresApproval, name, limit, offset);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TimeOffTypesApi.ListMyTimeOffTypes: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ID** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **isActive** | [**List&lt;bool?&gt;**](bool?.md)|  | [optional] 
 **requiresApproval** | [**List&lt;bool?&gt;**](bool?.md)|  | [optional] 
 **name** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **limit** | **int?**|  | [optional] 
 **offset** | **int?**|  | [optional] 

### Return type

[**ResponseBodyListGetTimeOffTypeModelMe**](ResponseBodyListGetTimeOffTypeModelMe.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="listtimeofftypeemploymenttypes"></a>
# **ListTimeOffTypeEmploymentTypes**
> ResponseBodyListSystemString ListTimeOffTypeEmploymentTypes (string timeOffTypeID)

Get available Employment Types.

       Company requirements:    Leave Type List Control must be restricted by Employment Type.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ListTimeOffTypeEmploymentTypesExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TimeOffTypesApi();
            var timeOffTypeID = timeOffTypeID_example;  // string |       Required

            try
            {
                // Get available Employment Types.
                ResponseBodyListSystemString result = apiInstance.ListTimeOffTypeEmploymentTypes(timeOffTypeID);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TimeOffTypesApi.ListTimeOffTypeEmploymentTypes: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **timeOffTypeID** | **string**|       Required | 

### Return type

[**ResponseBodyListSystemString**](ResponseBodyListSystemString.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="listtimeofftypes"></a>
# **ListTimeOffTypes**
> ResponseBodyListGetTimeOffTypeModelRoot ListTimeOffTypes (List<string> ID = null, List<bool?> isActive = null, List<bool?> requiresApproval = null, List<string> name = null, List<string> defaultApproverID = null, int? limit = null, int? offset = null)

Get Time Off Types.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ListTimeOffTypesExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TimeOffTypesApi();
            var ID = new List<string>(); // List<string> |  (optional) 
            var isActive = new List<bool?>(); // List<bool?> |  (optional) 
            var requiresApproval = new List<bool?>(); // List<bool?> |  (optional) 
            var name = new List<string>(); // List<string> |  (optional) 
            var defaultApproverID = new List<string>(); // List<string> |  (optional) 
            var limit = 56;  // int? |  (optional) 
            var offset = 56;  // int? |  (optional) 

            try
            {
                // Get Time Off Types.
                ResponseBodyListGetTimeOffTypeModelRoot result = apiInstance.ListTimeOffTypes(ID, isActive, requiresApproval, name, defaultApproverID, limit, offset);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TimeOffTypesApi.ListTimeOffTypes: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ID** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **isActive** | [**List&lt;bool?&gt;**](bool?.md)|  | [optional] 
 **requiresApproval** | [**List&lt;bool?&gt;**](bool?.md)|  | [optional] 
 **name** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **defaultApproverID** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **limit** | **int?**|  | [optional] 
 **offset** | **int?**|  | [optional] 

### Return type

[**ResponseBodyListGetTimeOffTypeModelRoot**](ResponseBodyListGetTimeOffTypeModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="listusertimeofftypes"></a>
# **ListUserTimeOffTypes**
> ResponseBodyListGetTimeOffTypeModelUsers ListUserTimeOffTypes (string userID, List<string> ID = null, List<bool?> isActive = null, List<bool?> requiresApproval = null, List<string> name = null, int? limit = null, int? offset = null)

Get a User's Time Off Types.

       User requirements:    Admin, or Manager with permissions to Add/Edit People.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ListUserTimeOffTypesExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TimeOffTypesApi();
            var userID = userID_example;  // string |       Required
            var ID = new List<string>(); // List<string> |  (optional) 
            var isActive = new List<bool?>(); // List<bool?> |  (optional) 
            var requiresApproval = new List<bool?>(); // List<bool?> |  (optional) 
            var name = new List<string>(); // List<string> |  (optional) 
            var limit = 56;  // int? |  (optional) 
            var offset = 56;  // int? |  (optional) 

            try
            {
                // Get a User's Time Off Types.
                ResponseBodyListGetTimeOffTypeModelUsers result = apiInstance.ListUserTimeOffTypes(userID, ID, isActive, requiresApproval, name, limit, offset);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TimeOffTypesApi.ListUserTimeOffTypes: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userID** | **string**|       Required | 
 **ID** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **isActive** | [**List&lt;bool?&gt;**](bool?.md)|  | [optional] 
 **requiresApproval** | [**List&lt;bool?&gt;**](bool?.md)|  | [optional] 
 **name** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **limit** | **int?**|  | [optional] 
 **offset** | **int?**|  | [optional] 

### Return type

[**ResponseBodyListGetTimeOffTypeModelUsers**](ResponseBodyListGetTimeOffTypeModelUsers.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="removetimeofftypeemploymenttype"></a>
# **RemoveTimeOffTypeEmploymentType**
> ResponseBodyListSystemString RemoveTimeOffTypeEmploymentType (string timeOffTypeID, string employmentTypeID)

Remove Employment Type from Time Off Type.

       User requirements:    User must be an Admin.       Company requirements:    Leave Type List Control must be restricted by Employment Type.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class RemoveTimeOffTypeEmploymentTypeExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TimeOffTypesApi();
            var timeOffTypeID = timeOffTypeID_example;  // string |       Required
            var employmentTypeID = employmentTypeID_example;  // string |       Required

            try
            {
                // Remove Employment Type from Time Off Type.
                ResponseBodyListSystemString result = apiInstance.RemoveTimeOffTypeEmploymentType(timeOffTypeID, employmentTypeID);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TimeOffTypesApi.RemoveTimeOffTypeEmploymentType: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **timeOffTypeID** | **string**|       Required | 
 **employmentTypeID** | **string**|       Required | 

### Return type

[**ResponseBodyListSystemString**](ResponseBodyListSystemString.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="updatetimeofftype"></a>
# **UpdateTimeOffType**
> ResponseBodyGetTimeOffTypeModelRoot UpdateTimeOffType (string timeOffTypeID, PatchTimeOffTypeModelRoot model)

Update a Time Off Type.

       User requirements:    User must be an Admin.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class UpdateTimeOffTypeExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TimeOffTypesApi();
            var timeOffTypeID = timeOffTypeID_example;  // string |       Required
            var model = new PatchTimeOffTypeModelRoot(); // PatchTimeOffTypeModelRoot |       Required

            try
            {
                // Update a Time Off Type.
                ResponseBodyGetTimeOffTypeModelRoot result = apiInstance.UpdateTimeOffType(timeOffTypeID, model);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TimeOffTypesApi.UpdateTimeOffType: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **timeOffTypeID** | **string**|       Required | 
 **model** | [**PatchTimeOffTypeModelRoot**](PatchTimeOffTypeModelRoot.md)|       Required | 

### Return type

[**ResponseBodyGetTimeOffTypeModelRoot**](ResponseBodyGetTimeOffTypeModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded, application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="updatetimeofftypeemploymenttypes"></a>
# **UpdateTimeOffTypeEmploymentTypes**
> ResponseBodyListSystemString UpdateTimeOffTypeEmploymentTypes (string timeOffTypeID, List<string> employmentTypeIDs)

Set available Employment Types.

       User requirements:    User must be an Admin.       Company requirements:    Leave Type List Control must be restricted by Employment Type.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class UpdateTimeOffTypeEmploymentTypesExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TimeOffTypesApi();
            var timeOffTypeID = timeOffTypeID_example;  // string |       Required
            var employmentTypeIDs = ;  // List<string> |       Required

            try
            {
                // Set available Employment Types.
                ResponseBodyListSystemString result = apiInstance.UpdateTimeOffTypeEmploymentTypes(timeOffTypeID, employmentTypeIDs);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TimeOffTypesApi.UpdateTimeOffTypeEmploymentTypes: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **timeOffTypeID** | **string**|       Required | 
 **employmentTypeIDs** | **List&lt;string&gt;**|       Required | 

### Return type

[**ResponseBodyListSystemString**](ResponseBodyListSystemString.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded, application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

