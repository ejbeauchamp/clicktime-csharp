# IO.Swagger.Model.ResponseBodyGetJobModel
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**GetJobModel**](GetJobModel.md) |  | [optional] 
**errors** | [**List&lt;APIError&gt;**](APIError.md) |  | [optional] 
**jsonapi** | **Dictionary&lt;string, Object&gt;** |  | [optional] 
**meta** | **Dictionary&lt;string, Object&gt;** |  | [optional] 
**page** | [**Page**](Page.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

