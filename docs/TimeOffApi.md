# IO.Swagger.Api.TimeOffApi

All URIs are relative to *https://api.clicktime.com/V2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**CreateMyTimeOff**](TimeOffApi.md#createmytimeoff) | **POST** /Me/TimeOff | Create my Time Off entry.
[**CreateTimeOff**](TimeOffApi.md#createtimeoff) | **POST** /TimeOff | Create a new Time Off entry.
[**DeleteMyTimeOff**](TimeOffApi.md#deletemytimeoff) | **DELETE** /Me/TimeOff/{timeOffID} | Delete my Time Off entry.
[**DeleteTimeOff**](TimeOffApi.md#deletetimeoff) | **DELETE** /TimeOff/{timeOffID} | Delete a Time Off entry.
[**GetDefaultTimeOff**](TimeOffApi.md#getdefaulttimeoff) | **GET** /TimeOff/new | Get a new Time Off entry with default values.
[**GetMyTimeOff**](TimeOffApi.md#getmytimeoff) | **GET** /Me/TimeOff/{timeOffID} | Get my Time Off entry.
[**GetTimeOff**](TimeOffApi.md#gettimeoff) | **GET** /TimeOff/{timeOffID} | Get a Time Off entry by ID.
[**ListMyTimeOff**](TimeOffApi.md#listmytimeoff) | **GET** /Me/TimeOff | Get my Time Off entries.
[**ListTimeOff**](TimeOffApi.md#listtimeoff) | **GET** /TimeOff | Get Time Off entries.
[**UpdateMyTimeOff**](TimeOffApi.md#updatemytimeoff) | **PATCH** /Me/TimeOff/{timeOffID} | Update my Time Off entry.
[**UpdateTimeOff**](TimeOffApi.md#updatetimeoff) | **PATCH** /TimeOff/{timeOffID} | Update a Time Off entry.


<a name="createmytimeoff"></a>
# **CreateMyTimeOff**
> ResponseBodyGetTimeOffModelMe CreateMyTimeOff (PostTimeOffModelMe model)

Create my Time Off entry.

       Company requirements:    DCAA must be disabled.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class CreateMyTimeOffExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TimeOffApi();
            var model = new PostTimeOffModelMe(); // PostTimeOffModelMe |       Required

            try
            {
                // Create my Time Off entry.
                ResponseBodyGetTimeOffModelMe result = apiInstance.CreateMyTimeOff(model);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TimeOffApi.CreateMyTimeOff: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **model** | [**PostTimeOffModelMe**](PostTimeOffModelMe.md)|       Required | 

### Return type

[**ResponseBodyGetTimeOffModelMe**](ResponseBodyGetTimeOffModelMe.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded, application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="createtimeoff"></a>
# **CreateTimeOff**
> ResponseBodyGetTimeOffModelRoot CreateTimeOff (PostTimeOffModelRoot model)

Create a new Time Off entry.

       User requirements:    User must be an Admin.       Company requirements:    DCAA must be disabled.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class CreateTimeOffExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TimeOffApi();
            var model = new PostTimeOffModelRoot(); // PostTimeOffModelRoot |       Required

            try
            {
                // Create a new Time Off entry.
                ResponseBodyGetTimeOffModelRoot result = apiInstance.CreateTimeOff(model);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TimeOffApi.CreateTimeOff: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **model** | [**PostTimeOffModelRoot**](PostTimeOffModelRoot.md)|       Required | 

### Return type

[**ResponseBodyGetTimeOffModelRoot**](ResponseBodyGetTimeOffModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded, application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="deletemytimeoff"></a>
# **DeleteMyTimeOff**
> void DeleteMyTimeOff (string timeOffID)

Delete my Time Off entry.

       Company requirements:    DCAA must be disabled.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class DeleteMyTimeOffExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TimeOffApi();
            var timeOffID = timeOffID_example;  // string | Time Off Type of this time off ID must not require approval.      Required

            try
            {
                // Delete my Time Off entry.
                apiInstance.DeleteMyTimeOff(timeOffID);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TimeOffApi.DeleteMyTimeOff: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **timeOffID** | **string**| Time Off Type of this time off ID must not require approval.      Required | 

### Return type

void (empty response body)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="deletetimeoff"></a>
# **DeleteTimeOff**
> void DeleteTimeOff (string timeOffID)

Delete a Time Off entry.

       User requirements:    User must be an Admin.       Company requirements:    DCAA must be disabled.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class DeleteTimeOffExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TimeOffApi();
            var timeOffID = timeOffID_example;  // string |       Required

            try
            {
                // Delete a Time Off entry.
                apiInstance.DeleteTimeOff(timeOffID);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TimeOffApi.DeleteTimeOff: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **timeOffID** | **string**|       Required | 

### Return type

void (empty response body)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getdefaulttimeoff"></a>
# **GetDefaultTimeOff**
> ResponseBodyGetTimeOffModelNew GetDefaultTimeOff ()

Get a new Time Off entry with default values.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class GetDefaultTimeOffExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TimeOffApi();

            try
            {
                // Get a new Time Off entry with default values.
                ResponseBodyGetTimeOffModelNew result = apiInstance.GetDefaultTimeOff();
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TimeOffApi.GetDefaultTimeOff: " + e.Message );
            }
        }
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ResponseBodyGetTimeOffModelNew**](ResponseBodyGetTimeOffModelNew.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getmytimeoff"></a>
# **GetMyTimeOff**
> ResponseBodyGetTimeOffModelMe GetMyTimeOff (string timeOffID)

Get my Time Off entry.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class GetMyTimeOffExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TimeOffApi();
            var timeOffID = timeOffID_example;  // string |       Required

            try
            {
                // Get my Time Off entry.
                ResponseBodyGetTimeOffModelMe result = apiInstance.GetMyTimeOff(timeOffID);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TimeOffApi.GetMyTimeOff: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **timeOffID** | **string**|       Required | 

### Return type

[**ResponseBodyGetTimeOffModelMe**](ResponseBodyGetTimeOffModelMe.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="gettimeoff"></a>
# **GetTimeOff**
> ResponseBodyGetTimeOffModelRoot GetTimeOff (string timeOffID)

Get a Time Off entry by ID.

       User requirements:    User must be an Admin.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class GetTimeOffExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TimeOffApi();
            var timeOffID = timeOffID_example;  // string |       Required

            try
            {
                // Get a Time Off entry by ID.
                ResponseBodyGetTimeOffModelRoot result = apiInstance.GetTimeOff(timeOffID);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TimeOffApi.GetTimeOff: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **timeOffID** | **string**|       Required | 

### Return type

[**ResponseBodyGetTimeOffModelRoot**](ResponseBodyGetTimeOffModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="listmytimeoff"></a>
# **ListMyTimeOff**
> ResponseBodyListGetTimeOffModelMe ListMyTimeOff (List<string> ID = null, List<string> timeOffTypeID = null, string fromDate = null, string toDate = null, List<string> date = null, int? limit = null, int? offset = null)

Get my Time Off entries.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ListMyTimeOffExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TimeOffApi();
            var ID = new List<string>(); // List<string> |  (optional) 
            var timeOffTypeID = new List<string>(); // List<string> |  (optional) 
            var fromDate = fromDate_example;  // string |  (optional) 
            var toDate = toDate_example;  // string |  (optional) 
            var date = new List<string>(); // List<string> |  (optional) 
            var limit = 56;  // int? |  (optional) 
            var offset = 56;  // int? |  (optional) 

            try
            {
                // Get my Time Off entries.
                ResponseBodyListGetTimeOffModelMe result = apiInstance.ListMyTimeOff(ID, timeOffTypeID, fromDate, toDate, date, limit, offset);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TimeOffApi.ListMyTimeOff: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ID** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **timeOffTypeID** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **fromDate** | **string**|  | [optional] 
 **toDate** | **string**|  | [optional] 
 **date** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **limit** | **int?**|  | [optional] 
 **offset** | **int?**|  | [optional] 

### Return type

[**ResponseBodyListGetTimeOffModelMe**](ResponseBodyListGetTimeOffModelMe.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="listtimeoff"></a>
# **ListTimeOff**
> ResponseBodyListGetTimeOffModelRoot ListTimeOff (List<string> ID = null, List<string> timeOffTypeID = null, List<string> userID = null, string fromDate = null, string toDate = null, List<string> date = null, int? limit = null, int? offset = null)

Get Time Off entries.

       User requirements:    User must be an Admin.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ListTimeOffExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TimeOffApi();
            var ID = new List<string>(); // List<string> |  (optional) 
            var timeOffTypeID = new List<string>(); // List<string> |  (optional) 
            var userID = new List<string>(); // List<string> |  (optional) 
            var fromDate = fromDate_example;  // string |  (optional) 
            var toDate = toDate_example;  // string |  (optional) 
            var date = new List<string>(); // List<string> |  (optional) 
            var limit = 56;  // int? |  (optional) 
            var offset = 56;  // int? |  (optional) 

            try
            {
                // Get Time Off entries.
                ResponseBodyListGetTimeOffModelRoot result = apiInstance.ListTimeOff(ID, timeOffTypeID, userID, fromDate, toDate, date, limit, offset);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TimeOffApi.ListTimeOff: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ID** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **timeOffTypeID** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **userID** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **fromDate** | **string**|  | [optional] 
 **toDate** | **string**|  | [optional] 
 **date** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **limit** | **int?**|  | [optional] 
 **offset** | **int?**|  | [optional] 

### Return type

[**ResponseBodyListGetTimeOffModelRoot**](ResponseBodyListGetTimeOffModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="updatemytimeoff"></a>
# **UpdateMyTimeOff**
> ResponseBodyGetTimeOffModelMe UpdateMyTimeOff (string timeOffID, PatchTimeOffModelMe model)

Update my Time Off entry.

       Company requirements:    DCAA must be disabled.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class UpdateMyTimeOffExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TimeOffApi();
            var timeOffID = timeOffID_example;  // string |       Required
            var model = new PatchTimeOffModelMe(); // PatchTimeOffModelMe |       Required

            try
            {
                // Update my Time Off entry.
                ResponseBodyGetTimeOffModelMe result = apiInstance.UpdateMyTimeOff(timeOffID, model);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TimeOffApi.UpdateMyTimeOff: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **timeOffID** | **string**|       Required | 
 **model** | [**PatchTimeOffModelMe**](PatchTimeOffModelMe.md)|       Required | 

### Return type

[**ResponseBodyGetTimeOffModelMe**](ResponseBodyGetTimeOffModelMe.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded, application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="updatetimeoff"></a>
# **UpdateTimeOff**
> ResponseBodyGetTimeOffModelRoot UpdateTimeOff (string timeOffID, PatchTimeOffModelRoot model)

Update a Time Off entry.

       User requirements:    User must be an Admin.       Company requirements:    DCAA must be disabled.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class UpdateTimeOffExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TimeOffApi();
            var timeOffID = timeOffID_example;  // string |       Required
            var model = new PatchTimeOffModelRoot(); // PatchTimeOffModelRoot |       Required

            try
            {
                // Update a Time Off entry.
                ResponseBodyGetTimeOffModelRoot result = apiInstance.UpdateTimeOff(timeOffID, model);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TimeOffApi.UpdateTimeOff: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **timeOffID** | **string**|       Required | 
 **model** | [**PatchTimeOffModelRoot**](PatchTimeOffModelRoot.md)|       Required | 

### Return type

[**ResponseBodyGetTimeOffModelRoot**](ResponseBodyGetTimeOffModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded, application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

