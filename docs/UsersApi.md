# IO.Swagger.Api.UsersApi

All URIs are relative to *https://api.clicktime.com/V2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**CreateManagedUser**](UsersApi.md#createmanageduser) | **POST** /Manage/Users | Create a User.    This API currently only supports creating standard users.
[**CreateUser**](UsersApi.md#createuser) | **POST** /Users | Create a User.    This API currently only supports creating standard users.
[**DeleteUser**](UsersApi.md#deleteuser) | **DELETE** /Users/{userID} | Delete a User.
[**GetDefaultUser**](UsersApi.md#getdefaultuser) | **GET** /Users/new | Get a new User with default values.
[**GetManagedUser**](UsersApi.md#getmanageduser) | **GET** /Manage/Users/{userID} | Get a User managed by the current user.
[**GetUser**](UsersApi.md#getuser) | **GET** /Users/{userID} | Get a User.
[**GetUserBillingRate**](UsersApi.md#getuserbillingrate) | **GET** /Users/{userID}/BillingRates/{billingRateID} | Get a secondary billing rate for a User.
[**GetUserCustomFieldDefinition**](UsersApi.md#getusercustomfielddefinition) | **GET** /Users/CustomFieldDefinitions/{customFieldDefinitionID} | Get a Custom Field Definition for Users.
[**ListManagedUsers**](UsersApi.md#listmanagedusers) | **GET** /Manage/Users | Get the Users managed by the current user.
[**ListUserBillingRates**](UsersApi.md#listuserbillingrates) | **GET** /Users/{userID}/BillingRates | Get secondary billing rates for a User.
[**ListUserCustomFieldDefinitions**](UsersApi.md#listusercustomfielddefinitions) | **GET** /Users/CustomFieldDefinitions | Get Custom Field Definitions for Users.
[**ListUsers**](UsersApi.md#listusers) | **GET** /Users | Get Users.
[**UpdateManagedUser**](UsersApi.md#updatemanageduser) | **PATCH** /Manage/Users/{userID} | Update a managed User.
[**UpdateUser**](UsersApi.md#updateuser) | **PATCH** /Users/{userID} | Update a User.
[**UpdateUserBillingRate**](UsersApi.md#updateuserbillingrate) | **PATCH** /Users/{userID}/BillingRates/{billingRateID} | Update a secondary billing rate for a User.


<a name="createmanageduser"></a>
# **CreateManagedUser**
> ResponseBodyGetUserModelManage CreateManagedUser (PostUserModelManage model)

Create a User.    This API currently only supports creating standard users.

       User requirements:    Admin, or Manager with permissions to Add/Edit People.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class CreateManagedUserExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new UsersApi();
            var model = new PostUserModelManage(); // PostUserModelManage |       Required

            try
            {
                // Create a User.    This API currently only supports creating standard users.
                ResponseBodyGetUserModelManage result = apiInstance.CreateManagedUser(model);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling UsersApi.CreateManagedUser: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **model** | [**PostUserModelManage**](PostUserModelManage.md)|       Required | 

### Return type

[**ResponseBodyGetUserModelManage**](ResponseBodyGetUserModelManage.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded, application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="createuser"></a>
# **CreateUser**
> ResponseBodyGetUserModelRoot CreateUser (PostUserModelRoot model)

Create a User.    This API currently only supports creating standard users.

       User requirements:    Admin, or Manager with permissions to Add/Edit People (all Divisions).

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class CreateUserExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new UsersApi();
            var model = new PostUserModelRoot(); // PostUserModelRoot |       Required

            try
            {
                // Create a User.    This API currently only supports creating standard users.
                ResponseBodyGetUserModelRoot result = apiInstance.CreateUser(model);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling UsersApi.CreateUser: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **model** | [**PostUserModelRoot**](PostUserModelRoot.md)|       Required | 

### Return type

[**ResponseBodyGetUserModelRoot**](ResponseBodyGetUserModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded, application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="deleteuser"></a>
# **DeleteUser**
> void DeleteUser (string userID)

Delete a User.

       User requirements:    User must be an Admin.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class DeleteUserExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new UsersApi();
            var userID = userID_example;  // string |       Required

            try
            {
                // Delete a User.
                apiInstance.DeleteUser(userID);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling UsersApi.DeleteUser: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userID** | **string**|       Required | 

### Return type

void (empty response body)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getdefaultuser"></a>
# **GetDefaultUser**
> ResponseBodyGetUserModelNew GetDefaultUser ()

Get a new User with default values.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class GetDefaultUserExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new UsersApi();

            try
            {
                // Get a new User with default values.
                ResponseBodyGetUserModelNew result = apiInstance.GetDefaultUser();
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling UsersApi.GetDefaultUser: " + e.Message );
            }
        }
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ResponseBodyGetUserModelNew**](ResponseBodyGetUserModelNew.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getmanageduser"></a>
# **GetManagedUser**
> ResponseBodyGetUserModelManage GetManagedUser (string userID)

Get a User managed by the current user.

       User requirements:    Admin, or Manager with permissions to View People.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class GetManagedUserExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new UsersApi();
            var userID = userID_example;  // string |       Required

            try
            {
                // Get a User managed by the current user.
                ResponseBodyGetUserModelManage result = apiInstance.GetManagedUser(userID);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling UsersApi.GetManagedUser: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userID** | **string**|       Required | 

### Return type

[**ResponseBodyGetUserModelManage**](ResponseBodyGetUserModelManage.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getuser"></a>
# **GetUser**
> ResponseBodyGetUserModelRoot GetUser (string userID)

Get a User.

       User requirements:    User must be a Manager or an Admin.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class GetUserExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new UsersApi();
            var userID = userID_example;  // string |       Required

            try
            {
                // Get a User.
                ResponseBodyGetUserModelRoot result = apiInstance.GetUser(userID);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling UsersApi.GetUser: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userID** | **string**|       Required | 

### Return type

[**ResponseBodyGetUserModelRoot**](ResponseBodyGetUserModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getuserbillingrate"></a>
# **GetUserBillingRate**
> ResponseBodyGetBillingRateModelRoot GetUserBillingRate (string userID, string billingRateID)

Get a secondary billing rate for a User.

       User requirements:    Admin, or Manager with permissions to View Billingrates.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class GetUserBillingRateExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new UsersApi();
            var userID = userID_example;  // string |       Required
            var billingRateID = billingRateID_example;  // string |       Required

            try
            {
                // Get a secondary billing rate for a User.
                ResponseBodyGetBillingRateModelRoot result = apiInstance.GetUserBillingRate(userID, billingRateID);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling UsersApi.GetUserBillingRate: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userID** | **string**|       Required | 
 **billingRateID** | **string**|       Required | 

### Return type

[**ResponseBodyGetBillingRateModelRoot**](ResponseBodyGetBillingRateModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getusercustomfielddefinition"></a>
# **GetUserCustomFieldDefinition**
> ResponseBodyGetCustomFieldDefinitionModelRoot GetUserCustomFieldDefinition (string customFieldDefinitionID)

Get a Custom Field Definition for Users.

       User requirements:    Admin, or Manager with permissions to View People.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class GetUserCustomFieldDefinitionExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new UsersApi();
            var customFieldDefinitionID = customFieldDefinitionID_example;  // string |       Required

            try
            {
                // Get a Custom Field Definition for Users.
                ResponseBodyGetCustomFieldDefinitionModelRoot result = apiInstance.GetUserCustomFieldDefinition(customFieldDefinitionID);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling UsersApi.GetUserCustomFieldDefinition: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customFieldDefinitionID** | **string**|       Required | 

### Return type

[**ResponseBodyGetCustomFieldDefinitionModelRoot**](ResponseBodyGetCustomFieldDefinitionModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="listmanagedusers"></a>
# **ListManagedUsers**
> ResponseBodyListGetUserModelManage ListManagedUsers (List<string> ID = null, List<string> name = null, List<string> email = null, List<string> divisionID = null, List<string> employmentTypeID = null, List<string> timesheetApproverID = null, List<string> expenseApproverID = null, List<string> securityLevel = null, List<string> managerPermission = null, List<bool?> isActive = null, int? limit = null, int? offset = null)

Get the Users managed by the current user.

       User requirements:    Admin, or Manager with permissions to View People.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ListManagedUsersExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new UsersApi();
            var ID = new List<string>(); // List<string> |  (optional) 
            var name = new List<string>(); // List<string> |  (optional) 
            var email = new List<string>(); // List<string> |  (optional) 
            var divisionID = new List<string>(); // List<string> |  (optional) 
            var employmentTypeID = new List<string>(); // List<string> |  (optional) 
            var timesheetApproverID = new List<string>(); // List<string> |  (optional) 
            var expenseApproverID = new List<string>(); // List<string> |  (optional) 
            var securityLevel = new List<string>(); // List<string> |  (optional) 
            var managerPermission = new List<string>(); // List<string> | Please note that values for the ManagerPermission filter must be appropriately URL-encoded, e.g. \"Add%2FEdit+People\". Possible values for this filter are:              - Add/Edit People              - View People              - Add/Edit Jobs              - View Jobs              - Add/Edit Tasks              - View Tasks              - Add/Edit Clients              - View Clients              - Add/Edit Divisions              - View Divisions              - Add/Edit Employment Types              - View Employment Types              - Add/Edit Expense Types              - View Expense Types              - Add/Edit Payment Types              - View Payment Types              - Add/Edit Billingrates              - View Billingrates              - Add/Edit Costs              - View Costs              - Add/Edit Resource Planning              - View Resource Planning              - Review Expenses              - Approve Expenses              - Override Expenses              - Mark Expenses as Paid              - Review timesheets              - Lock timesheets              - Override timesheets              - Notify incomplete              - View for JobsTasksClientsDivisionsPeople              - Run Company Reports              - Approve Time Off (optional) 
            var isActive = new List<bool?>(); // List<bool?> |  (optional) 
            var limit = 56;  // int? |  (optional) 
            var offset = 56;  // int? |  (optional) 

            try
            {
                // Get the Users managed by the current user.
                ResponseBodyListGetUserModelManage result = apiInstance.ListManagedUsers(ID, name, email, divisionID, employmentTypeID, timesheetApproverID, expenseApproverID, securityLevel, managerPermission, isActive, limit, offset);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling UsersApi.ListManagedUsers: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ID** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **name** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **email** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **divisionID** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **employmentTypeID** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **timesheetApproverID** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **expenseApproverID** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **securityLevel** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **managerPermission** | [**List&lt;string&gt;**](string.md)| Please note that values for the ManagerPermission filter must be appropriately URL-encoded, e.g. \&quot;Add%2FEdit+People\&quot;. Possible values for this filter are:              - Add/Edit People              - View People              - Add/Edit Jobs              - View Jobs              - Add/Edit Tasks              - View Tasks              - Add/Edit Clients              - View Clients              - Add/Edit Divisions              - View Divisions              - Add/Edit Employment Types              - View Employment Types              - Add/Edit Expense Types              - View Expense Types              - Add/Edit Payment Types              - View Payment Types              - Add/Edit Billingrates              - View Billingrates              - Add/Edit Costs              - View Costs              - Add/Edit Resource Planning              - View Resource Planning              - Review Expenses              - Approve Expenses              - Override Expenses              - Mark Expenses as Paid              - Review timesheets              - Lock timesheets              - Override timesheets              - Notify incomplete              - View for JobsTasksClientsDivisionsPeople              - Run Company Reports              - Approve Time Off | [optional] 
 **isActive** | [**List&lt;bool?&gt;**](bool?.md)|  | [optional] 
 **limit** | **int?**|  | [optional] 
 **offset** | **int?**|  | [optional] 

### Return type

[**ResponseBodyListGetUserModelManage**](ResponseBodyListGetUserModelManage.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="listuserbillingrates"></a>
# **ListUserBillingRates**
> ResponseBodyListGetBillingRateModelRoot ListUserBillingRates (string userID, List<string> ID = null)

Get secondary billing rates for a User.

       User requirements:    Admin, or Manager with permissions to View Billingrates.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ListUserBillingRatesExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new UsersApi();
            var userID = userID_example;  // string |       Required
            var ID = new List<string>(); // List<string> |  (optional) 

            try
            {
                // Get secondary billing rates for a User.
                ResponseBodyListGetBillingRateModelRoot result = apiInstance.ListUserBillingRates(userID, ID);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling UsersApi.ListUserBillingRates: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userID** | **string**|       Required | 
 **ID** | [**List&lt;string&gt;**](string.md)|  | [optional] 

### Return type

[**ResponseBodyListGetBillingRateModelRoot**](ResponseBodyListGetBillingRateModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="listusercustomfielddefinitions"></a>
# **ListUserCustomFieldDefinitions**
> ResponseBodyListGetCustomFieldDefinitionModelRoot ListUserCustomFieldDefinitions (int? limit = null, int? offset = null)

Get Custom Field Definitions for Users.

       User requirements:    Admin, or Manager with permissions to View People.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ListUserCustomFieldDefinitionsExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new UsersApi();
            var limit = 56;  // int? |  (optional) 
            var offset = 56;  // int? |  (optional) 

            try
            {
                // Get Custom Field Definitions for Users.
                ResponseBodyListGetCustomFieldDefinitionModelRoot result = apiInstance.ListUserCustomFieldDefinitions(limit, offset);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling UsersApi.ListUserCustomFieldDefinitions: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **limit** | **int?**|  | [optional] 
 **offset** | **int?**|  | [optional] 

### Return type

[**ResponseBodyListGetCustomFieldDefinitionModelRoot**](ResponseBodyListGetCustomFieldDefinitionModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="listusers"></a>
# **ListUsers**
> ResponseBodyListGetUserModelRoot ListUsers (List<string> ID = null, List<string> name = null, List<string> email = null, List<string> divisionID = null, List<string> timesheetApproverID = null, List<string> expenseApproverID = null, List<string> employmentTypeID = null, List<string> jobID = null, List<string> securityLevel = null, List<string> managerPermission = null, List<bool?> isActive = null, int? limit = null, int? offset = null)

Get Users.

       User requirements:    User must be a Manager or an Admin.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ListUsersExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new UsersApi();
            var ID = new List<string>(); // List<string> |  (optional) 
            var name = new List<string>(); // List<string> |  (optional) 
            var email = new List<string>(); // List<string> |  (optional) 
            var divisionID = new List<string>(); // List<string> |  (optional) 
            var timesheetApproverID = new List<string>(); // List<string> |  (optional) 
            var expenseApproverID = new List<string>(); // List<string> |  (optional) 
            var employmentTypeID = new List<string>(); // List<string> |  (optional) 
            var jobID = new List<string>(); // List<string> |  (optional) 
            var securityLevel = new List<string>(); // List<string> |  (optional) 
            var managerPermission = new List<string>(); // List<string> | Please note that values for the ManagerPermission filter must be appropriately URL-encoded, e.g. \"Add%2FEdit+People\". Possible values for this filter are:              - Add/Edit People              - View People              - Add/Edit Jobs              - View Jobs              - Add/Edit Tasks              - View Tasks              - Add/Edit Clients              - View Clients              - Add/Edit Divisions              - View Divisions              - Add/Edit Employment Types              - View Employment Types              - Add/Edit Expense Types              - View Expense Types              - Add/Edit Payment Types              - View Payment Types              - Add/Edit Billingrates              - View Billingrates              - Add/Edit Costs              - View Costs              - Add/Edit Resource Planning              - View Resource Planning              - Review Expenses              - Approve Expenses              - Override Expenses              - Mark Expenses as Paid              - Review timesheets              - Lock timesheets              - Override timesheets              - Notify incomplete              - View for JobsTasksClientsDivisionsPeople              - Run Company Reports              - Approve Time Off (optional) 
            var isActive = new List<bool?>(); // List<bool?> |  (optional) 
            var limit = 56;  // int? |  (optional) 
            var offset = 56;  // int? |  (optional) 

            try
            {
                // Get Users.
                ResponseBodyListGetUserModelRoot result = apiInstance.ListUsers(ID, name, email, divisionID, timesheetApproverID, expenseApproverID, employmentTypeID, jobID, securityLevel, managerPermission, isActive, limit, offset);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling UsersApi.ListUsers: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ID** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **name** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **email** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **divisionID** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **timesheetApproverID** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **expenseApproverID** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **employmentTypeID** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **jobID** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **securityLevel** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **managerPermission** | [**List&lt;string&gt;**](string.md)| Please note that values for the ManagerPermission filter must be appropriately URL-encoded, e.g. \&quot;Add%2FEdit+People\&quot;. Possible values for this filter are:              - Add/Edit People              - View People              - Add/Edit Jobs              - View Jobs              - Add/Edit Tasks              - View Tasks              - Add/Edit Clients              - View Clients              - Add/Edit Divisions              - View Divisions              - Add/Edit Employment Types              - View Employment Types              - Add/Edit Expense Types              - View Expense Types              - Add/Edit Payment Types              - View Payment Types              - Add/Edit Billingrates              - View Billingrates              - Add/Edit Costs              - View Costs              - Add/Edit Resource Planning              - View Resource Planning              - Review Expenses              - Approve Expenses              - Override Expenses              - Mark Expenses as Paid              - Review timesheets              - Lock timesheets              - Override timesheets              - Notify incomplete              - View for JobsTasksClientsDivisionsPeople              - Run Company Reports              - Approve Time Off | [optional] 
 **isActive** | [**List&lt;bool?&gt;**](bool?.md)|  | [optional] 
 **limit** | **int?**|  | [optional] 
 **offset** | **int?**|  | [optional] 

### Return type

[**ResponseBodyListGetUserModelRoot**](ResponseBodyListGetUserModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="updatemanageduser"></a>
# **UpdateManagedUser**
> ResponseBodyGetUserModelManage UpdateManagedUser (string userID, PatchUserModelManage model)

Update a managed User.

       User requirements:    Admin, or Manager with permissions to Add/Edit People.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class UpdateManagedUserExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new UsersApi();
            var userID = userID_example;  // string |       Required
            var model = new PatchUserModelManage(); // PatchUserModelManage |       Required

            try
            {
                // Update a managed User.
                ResponseBodyGetUserModelManage result = apiInstance.UpdateManagedUser(userID, model);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling UsersApi.UpdateManagedUser: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userID** | **string**|       Required | 
 **model** | [**PatchUserModelManage**](PatchUserModelManage.md)|       Required | 

### Return type

[**ResponseBodyGetUserModelManage**](ResponseBodyGetUserModelManage.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded, application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="updateuser"></a>
# **UpdateUser**
> ResponseBodyGetUserModelRoot UpdateUser (string userID, PatchUserModelRoot model)

Update a User.

       User requirements:    Admin, or Manager with permissions to Add/Edit People (all Divisions).

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class UpdateUserExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new UsersApi();
            var userID = userID_example;  // string |       Required
            var model = new PatchUserModelRoot(); // PatchUserModelRoot |       Required

            try
            {
                // Update a User.
                ResponseBodyGetUserModelRoot result = apiInstance.UpdateUser(userID, model);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling UsersApi.UpdateUser: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userID** | **string**|       Required | 
 **model** | [**PatchUserModelRoot**](PatchUserModelRoot.md)|       Required | 

### Return type

[**ResponseBodyGetUserModelRoot**](ResponseBodyGetUserModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded, application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="updateuserbillingrate"></a>
# **UpdateUserBillingRate**
> ResponseBodyGetBillingRateModelRoot UpdateUserBillingRate (string userID, string billingRateID, PatchBillingRateModelRoot model)

Update a secondary billing rate for a User.

       User requirements:    Admin, or Manager with permissions to View Billingrates.    Admin, or Manager with permissions to Add/Edit People.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class UpdateUserBillingRateExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new UsersApi();
            var userID = userID_example;  // string |       Required
            var billingRateID = billingRateID_example;  // string |       Required
            var model = new PatchBillingRateModelRoot(); // PatchBillingRateModelRoot |       Required

            try
            {
                // Update a secondary billing rate for a User.
                ResponseBodyGetBillingRateModelRoot result = apiInstance.UpdateUserBillingRate(userID, billingRateID, model);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling UsersApi.UpdateUserBillingRate: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userID** | **string**|       Required | 
 **billingRateID** | **string**|       Required | 
 **model** | [**PatchBillingRateModelRoot**](PatchBillingRateModelRoot.md)|       Required | 

### Return type

[**ResponseBodyGetBillingRateModelRoot**](ResponseBodyGetBillingRateModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded, application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

