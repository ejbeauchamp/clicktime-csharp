# IO.Swagger.Model.GetLogTimesheetModel
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**EndDate** | **string** |  | [optional] 
**ID** | **string** |  | [optional] 
**LegacyID** | **string** |  | [optional] 
**StartDate** | **string** |  | [optional] 
**UserID** | **string** |  | [optional] 
**UserName** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

