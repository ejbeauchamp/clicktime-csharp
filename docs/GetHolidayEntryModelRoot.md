# IO.Swagger.Model.GetHolidayEntryModelRoot
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Date** | **string** |  | [optional] 
**HolidayType** | [**GetBasicTimeOffTypeModelHolidayTypeGetHolidayEntryModelRoot**](GetBasicTimeOffTypeModelHolidayTypeGetHolidayEntryModelRoot.md) |        Request requirements:    Verbose&#x3D;true | [optional] 
**HolidayTypeID** | **string** |  | [optional] 
**Hours** | **double?** |  | [optional] 
**ID** | **string** |  | [optional] 
**LegacyID** | **string** |        Request requirements:    CTLegacyScramble&#x3D;true | [optional] 
**Notes** | **string** |  | [optional] 
**User** | [**GetBasicUserModelUserGetHolidayEntryModelRoot**](GetBasicUserModelUserGetHolidayEntryModelRoot.md) |        Request requirements:    Verbose&#x3D;true | [optional] 
**UserID** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

