# IO.Swagger.Model.GetBasicRuleDefinitionModelRuleDefinitionGetOvertimeRuleModelNew
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Multiplier** | **double?** |  | [optional] 
**PayCode** | **string** |  | [optional] 
**RuleType** | **string** |  | [optional] 
**Threshold** | **double?** |  | [optional] 
**TimeSpan** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

