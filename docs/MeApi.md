# IO.Swagger.Api.MeApi

All URIs are relative to *https://api.clicktime.com/V2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**CreateMyFirebaseDevice**](MeApi.md#createmyfirebasedevice) | **POST** /Me/FirebaseDevice | Create an Firebase Device Type.
[**GetMyInfo**](MeApi.md#getmyinfo) | **GET** /Me | Get the current User.
[**GetMyServices**](MeApi.md#getmyservices) | **GET** /Me/Services | Get my services.
[**ResetMyAuthToken**](MeApi.md#resetmyauthtoken) | **POST** /Me/ResetAuthToken | Create new auth token.
[**UpdateMyInfo**](MeApi.md#updatemyinfo) | **PATCH** /Me | Update the current user


<a name="createmyfirebasedevice"></a>
# **CreateMyFirebaseDevice**
> Object CreateMyFirebaseDevice (PostFirebaseDeviceModelRoot model)

Create an Firebase Device Type.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class CreateMyFirebaseDeviceExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new MeApi();
            var model = new PostFirebaseDeviceModelRoot(); // PostFirebaseDeviceModelRoot |       Required

            try
            {
                // Create an Firebase Device Type.
                Object result = apiInstance.CreateMyFirebaseDevice(model);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling MeApi.CreateMyFirebaseDevice: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **model** | [**PostFirebaseDeviceModelRoot**](PostFirebaseDeviceModelRoot.md)|       Required | 

### Return type

**Object**

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded, application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getmyinfo"></a>
# **GetMyInfo**
> ResponseBodyGetUserModelMe GetMyInfo ()

Get the current User.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class GetMyInfoExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");
            // Configure HTTP basic authorization: basic
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";

            var apiInstance = new MeApi();

            try
            {
                // Get the current User.
                ResponseBodyGetUserModelMe result = apiInstance.GetMyInfo();
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling MeApi.GetMyInfo: " + e.Message );
            }
        }
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ResponseBodyGetUserModelMe**](ResponseBodyGetUserModelMe.md)

### Authorization

[apiKey](../README.md#apiKey), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getmyservices"></a>
# **GetMyServices**
> ResponseBodyListSystemString GetMyServices ()

Get my services.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class GetMyServicesExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new MeApi();

            try
            {
                // Get my services.
                ResponseBodyListSystemString result = apiInstance.GetMyServices();
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling MeApi.GetMyServices: " + e.Message );
            }
        }
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ResponseBodyListSystemString**](ResponseBodyListSystemString.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="resetmyauthtoken"></a>
# **ResetMyAuthToken**
> ResponseBodyGetUserAuthTokenModel ResetMyAuthToken ()

Create new auth token.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ResetMyAuthTokenExample
    {
        public void main()
        {
            // Configure HTTP basic authorization: basic
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";

            var apiInstance = new MeApi();

            try
            {
                // Create new auth token.
                ResponseBodyGetUserAuthTokenModel result = apiInstance.ResetMyAuthToken();
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling MeApi.ResetMyAuthToken: " + e.Message );
            }
        }
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ResponseBodyGetUserAuthTokenModel**](ResponseBodyGetUserAuthTokenModel.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="updatemyinfo"></a>
# **UpdateMyInfo**
> ResponseBodyGetUserModelMe UpdateMyInfo (PatchUserModelMe model)

Update the current user

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class UpdateMyInfoExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new MeApi();
            var model = new PatchUserModelMe(); // PatchUserModelMe |       Required

            try
            {
                // Update the current user
                ResponseBodyGetUserModelMe result = apiInstance.UpdateMyInfo(model);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling MeApi.UpdateMyInfo: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **model** | [**PatchUserModelMe**](PatchUserModelMe.md)|       Required | 

### Return type

[**ResponseBodyGetUserModelMe**](ResponseBodyGetUserModelMe.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded, application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

