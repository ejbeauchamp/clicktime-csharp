# IO.Swagger.Api.TasksApi

All URIs are relative to *https://api.clicktime.com/V2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**AddTaskJob**](TasksApi.md#addtaskjob) | **POST** /Tasks/{taskID}/Jobs | Make a Job available to a Task.
[**CreateTask**](TasksApi.md#createtask) | **POST** /Tasks | Create a Task.
[**DeleteTask**](TasksApi.md#deletetask) | **DELETE** /Tasks/{taskID} | Delete a Task.
[**GetDefaultTask**](TasksApi.md#getdefaulttask) | **GET** /Tasks/new | Get a new Task with default values.
[**GetMyTask**](TasksApi.md#getmytask) | **GET** /Me/Tasks/{taskID} | Get my Task.
[**GetTask**](TasksApi.md#gettask) | **GET** /Tasks/{taskID} | Get a Task.
[**GetTaskCustomFieldDefinition**](TasksApi.md#gettaskcustomfielddefinition) | **GET** /Tasks/CustomFieldDefinitions/{customFieldDefinitionID} | Get a Custom Field Definition for Tasks.
[**ListMyTasks**](TasksApi.md#listmytasks) | **GET** /Me/Tasks | Get my Tasks.
[**ListTaskCustomFieldDefinitions**](TasksApi.md#listtaskcustomfielddefinitions) | **GET** /Tasks/CustomFieldDefinitions | Get Custom Field Definitions for Tasks.
[**ListTaskJobs**](TasksApi.md#listtaskjobs) | **GET** /Tasks/{taskID}/Jobs | Get available Jobs for a Task.
[**ListTasks**](TasksApi.md#listtasks) | **GET** /Tasks | Get Tasks.
[**RemoveTaskJob**](TasksApi.md#removetaskjob) | **DELETE** /Tasks/{taskID}/Jobs/{jobID} | Make a Job unavailable to a Task.
[**UpdateTask**](TasksApi.md#updatetask) | **PATCH** /Tasks/{taskID} | Update a Task.
[**UpdateTaskJobs**](TasksApi.md#updatetaskjobs) | **PUT** /Tasks/{taskID}/Jobs | Specify all Jobs available to a Task.


<a name="addtaskjob"></a>
# **AddTaskJob**
> ResponseBodyListSystemString AddTaskJob (string taskID, PostJobAvailabilityModelRoot model)

Make a Job available to a Task.

       Company requirements:    Task List Controls must be configured by Job.       User requirements:    Admin, or Manager with permissions to Add/Edit Tasks.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class AddTaskJobExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TasksApi();
            var taskID = taskID_example;  // string |       Required
            var model = new PostJobAvailabilityModelRoot(); // PostJobAvailabilityModelRoot |       Required

            try
            {
                // Make a Job available to a Task.
                ResponseBodyListSystemString result = apiInstance.AddTaskJob(taskID, model);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TasksApi.AddTaskJob: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **taskID** | **string**|       Required | 
 **model** | [**PostJobAvailabilityModelRoot**](PostJobAvailabilityModelRoot.md)|       Required | 

### Return type

[**ResponseBodyListSystemString**](ResponseBodyListSystemString.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded, application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="createtask"></a>
# **CreateTask**
> ResponseBodyGetTaskModelRoot CreateTask (PostTaskModelRoot model)

Create a Task.

       User requirements:    Admin, or Manager with permissions to Add/Edit Tasks.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class CreateTaskExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TasksApi();
            var model = new PostTaskModelRoot(); // PostTaskModelRoot |       Required

            try
            {
                // Create a Task.
                ResponseBodyGetTaskModelRoot result = apiInstance.CreateTask(model);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TasksApi.CreateTask: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **model** | [**PostTaskModelRoot**](PostTaskModelRoot.md)|       Required | 

### Return type

[**ResponseBodyGetTaskModelRoot**](ResponseBodyGetTaskModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded, application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="deletetask"></a>
# **DeleteTask**
> void DeleteTask (string taskID)

Delete a Task.

       User requirements:    User must be an Admin.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class DeleteTaskExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TasksApi();
            var taskID = taskID_example;  // string |       Required

            try
            {
                // Delete a Task.
                apiInstance.DeleteTask(taskID);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TasksApi.DeleteTask: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **taskID** | **string**|       Required | 

### Return type

void (empty response body)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getdefaulttask"></a>
# **GetDefaultTask**
> ResponseBodyGetTaskModelNew GetDefaultTask ()

Get a new Task with default values.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class GetDefaultTaskExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TasksApi();

            try
            {
                // Get a new Task with default values.
                ResponseBodyGetTaskModelNew result = apiInstance.GetDefaultTask();
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TasksApi.GetDefaultTask: " + e.Message );
            }
        }
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ResponseBodyGetTaskModelNew**](ResponseBodyGetTaskModelNew.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getmytask"></a>
# **GetMyTask**
> ResponseBodyGetTaskModelMe GetMyTask (string taskID)

Get my Task.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class GetMyTaskExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TasksApi();
            var taskID = taskID_example;  // string |       Required

            try
            {
                // Get my Task.
                ResponseBodyGetTaskModelMe result = apiInstance.GetMyTask(taskID);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TasksApi.GetMyTask: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **taskID** | **string**|       Required | 

### Return type

[**ResponseBodyGetTaskModelMe**](ResponseBodyGetTaskModelMe.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="gettask"></a>
# **GetTask**
> ResponseBodyGetTaskModelRoot GetTask (string taskID)

Get a Task.

       User requirements:    User must be a Manager or an Admin.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class GetTaskExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TasksApi();
            var taskID = taskID_example;  // string |       Required

            try
            {
                // Get a Task.
                ResponseBodyGetTaskModelRoot result = apiInstance.GetTask(taskID);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TasksApi.GetTask: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **taskID** | **string**|       Required | 

### Return type

[**ResponseBodyGetTaskModelRoot**](ResponseBodyGetTaskModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="gettaskcustomfielddefinition"></a>
# **GetTaskCustomFieldDefinition**
> ResponseBodyGetCustomFieldDefinitionModelRoot GetTaskCustomFieldDefinition (string customFieldDefinitionID)

Get a Custom Field Definition for Tasks.

       User requirements:    Admin, or Manager with permissions to View Tasks.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class GetTaskCustomFieldDefinitionExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TasksApi();
            var customFieldDefinitionID = customFieldDefinitionID_example;  // string |       Required

            try
            {
                // Get a Custom Field Definition for Tasks.
                ResponseBodyGetCustomFieldDefinitionModelRoot result = apiInstance.GetTaskCustomFieldDefinition(customFieldDefinitionID);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TasksApi.GetTaskCustomFieldDefinition: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customFieldDefinitionID** | **string**|       Required | 

### Return type

[**ResponseBodyGetCustomFieldDefinitionModelRoot**](ResponseBodyGetCustomFieldDefinitionModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="listmytasks"></a>
# **ListMyTasks**
> ResponseBodyListGetTaskModelMe ListMyTasks (List<string> ID = null, List<bool?> isActive = null, List<string> taskName = null, List<string> taskCode = null, List<bool?> myTasks = null, int? limit = null, int? offset = null)

Get my Tasks.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ListMyTasksExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TasksApi();
            var ID = new List<string>(); // List<string> |  (optional) 
            var isActive = new List<bool?>(); // List<bool?> |  (optional) 
            var taskName = new List<string>(); // List<string> |  (optional) 
            var taskCode = new List<string>(); // List<string> |  (optional) 
            var myTasks = new List<bool?>(); // List<bool?> |  (optional) 
            var limit = 56;  // int? |  (optional) 
            var offset = 56;  // int? |  (optional) 

            try
            {
                // Get my Tasks.
                ResponseBodyListGetTaskModelMe result = apiInstance.ListMyTasks(ID, isActive, taskName, taskCode, myTasks, limit, offset);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TasksApi.ListMyTasks: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ID** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **isActive** | [**List&lt;bool?&gt;**](bool?.md)|  | [optional] 
 **taskName** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **taskCode** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **myTasks** | [**List&lt;bool?&gt;**](bool?.md)|  | [optional] 
 **limit** | **int?**|  | [optional] 
 **offset** | **int?**|  | [optional] 

### Return type

[**ResponseBodyListGetTaskModelMe**](ResponseBodyListGetTaskModelMe.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="listtaskcustomfielddefinitions"></a>
# **ListTaskCustomFieldDefinitions**
> ResponseBodyListGetCustomFieldDefinitionModelRoot ListTaskCustomFieldDefinitions (int? limit = null, int? offset = null)

Get Custom Field Definitions for Tasks.

       User requirements:    Admin, or Manager with permissions to View Tasks.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ListTaskCustomFieldDefinitionsExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TasksApi();
            var limit = 56;  // int? |  (optional) 
            var offset = 56;  // int? |  (optional) 

            try
            {
                // Get Custom Field Definitions for Tasks.
                ResponseBodyListGetCustomFieldDefinitionModelRoot result = apiInstance.ListTaskCustomFieldDefinitions(limit, offset);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TasksApi.ListTaskCustomFieldDefinitions: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **limit** | **int?**|  | [optional] 
 **offset** | **int?**|  | [optional] 

### Return type

[**ResponseBodyListGetCustomFieldDefinitionModelRoot**](ResponseBodyListGetCustomFieldDefinitionModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="listtaskjobs"></a>
# **ListTaskJobs**
> ResponseBodyListSystemString ListTaskJobs (string taskID, int? limit = null, int? offset = null)

Get available Jobs for a Task.

       Company requirements:    Task List Controls must be configured by Job.       User requirements:    Admin, or Manager with permissions to View Tasks.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ListTaskJobsExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TasksApi();
            var taskID = taskID_example;  // string |       Required
            var limit = 56;  // int? |  (optional) 
            var offset = 56;  // int? |  (optional) 

            try
            {
                // Get available Jobs for a Task.
                ResponseBodyListSystemString result = apiInstance.ListTaskJobs(taskID, limit, offset);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TasksApi.ListTaskJobs: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **taskID** | **string**|       Required | 
 **limit** | **int?**|  | [optional] 
 **offset** | **int?**|  | [optional] 

### Return type

[**ResponseBodyListSystemString**](ResponseBodyListSystemString.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="listtasks"></a>
# **ListTasks**
> ResponseBodyListGetTaskModelRoot ListTasks (List<string> ID = null, List<bool?> isActive = null, List<string> taskName = null, List<string> taskCode = null, int? limit = null, int? offset = null)

Get Tasks.

       User requirements:    User must be a Manager or an Admin.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ListTasksExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TasksApi();
            var ID = new List<string>(); // List<string> |  (optional) 
            var isActive = new List<bool?>(); // List<bool?> |  (optional) 
            var taskName = new List<string>(); // List<string> |  (optional) 
            var taskCode = new List<string>(); // List<string> |  (optional) 
            var limit = 56;  // int? |  (optional) 
            var offset = 56;  // int? |  (optional) 

            try
            {
                // Get Tasks.
                ResponseBodyListGetTaskModelRoot result = apiInstance.ListTasks(ID, isActive, taskName, taskCode, limit, offset);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TasksApi.ListTasks: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ID** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **isActive** | [**List&lt;bool?&gt;**](bool?.md)|  | [optional] 
 **taskName** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **taskCode** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **limit** | **int?**|  | [optional] 
 **offset** | **int?**|  | [optional] 

### Return type

[**ResponseBodyListGetTaskModelRoot**](ResponseBodyListGetTaskModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="removetaskjob"></a>
# **RemoveTaskJob**
> ResponseBodyListSystemString RemoveTaskJob (string taskID, string jobID)

Make a Job unavailable to a Task.

       Company requirements:    Task List Controls must be configured by Job.       User requirements:    Admin, or Manager with permissions to Add/Edit Tasks.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class RemoveTaskJobExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TasksApi();
            var taskID = taskID_example;  // string |       Required
            var jobID = jobID_example;  // string |       Required

            try
            {
                // Make a Job unavailable to a Task.
                ResponseBodyListSystemString result = apiInstance.RemoveTaskJob(taskID, jobID);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TasksApi.RemoveTaskJob: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **taskID** | **string**|       Required | 
 **jobID** | **string**|       Required | 

### Return type

[**ResponseBodyListSystemString**](ResponseBodyListSystemString.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="updatetask"></a>
# **UpdateTask**
> ResponseBodyGetTaskModelRoot UpdateTask (string taskID, PatchTaskModelRoot model)

Update a Task.

       User requirements:    Admin, or Manager with permissions to Add/Edit Tasks.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class UpdateTaskExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TasksApi();
            var taskID = taskID_example;  // string |       Required
            var model = new PatchTaskModelRoot(); // PatchTaskModelRoot |       Required

            try
            {
                // Update a Task.
                ResponseBodyGetTaskModelRoot result = apiInstance.UpdateTask(taskID, model);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TasksApi.UpdateTask: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **taskID** | **string**|       Required | 
 **model** | [**PatchTaskModelRoot**](PatchTaskModelRoot.md)|       Required | 

### Return type

[**ResponseBodyGetTaskModelRoot**](ResponseBodyGetTaskModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded, application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="updatetaskjobs"></a>
# **UpdateTaskJobs**
> ResponseBodyListSystemString UpdateTaskJobs (string taskID, List<string> jobIDs)

Specify all Jobs available to a Task.

       Company requirements:    Task List Controls must be configured by Job.       User requirements:    Admin, or Manager with permissions to Add/Edit Tasks.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class UpdateTaskJobsExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TasksApi();
            var taskID = taskID_example;  // string |       Required
            var jobIDs = ;  // List<string> |       Required

            try
            {
                // Specify all Jobs available to a Task.
                ResponseBodyListSystemString result = apiInstance.UpdateTaskJobs(taskID, jobIDs);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TasksApi.UpdateTaskJobs: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **taskID** | **string**|       Required | 
 **jobIDs** | **List&lt;string&gt;**|       Required | 

### Return type

[**ResponseBodyListSystemString**](ResponseBodyListSystemString.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded, application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

