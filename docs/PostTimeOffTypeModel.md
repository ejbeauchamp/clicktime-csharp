# IO.Swagger.Model.PostTimeOffTypeModel
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**AccountingPackageID** | **string** |  | [optional] 
**DefaultAccrualRate** | **double?** |  | [optional] 
**DefaultApproverID** | **string** |  | [optional] 
**DefaultMaximumBalance** | **double?** |  | [optional] 
**DefaultStartingBalance** | **double?** |  | [optional] 
**DisplayOnTimeEntryPages** | **bool?** |  | [optional] 
**IsActive** | **bool?** |  | [optional] 
**Name** | **string** |  | 
**Notes** | **string** |  | [optional] 
**RequiresApproval** | **bool?** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

