# IO.Swagger.Model.PostTimerIntervalModelIntervalsPostTimerModelRoot
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**End** | **string** | hh:mm:ss | 
**Start** | **string** | hh:mm:ss | 
**TimeZoneOffset** | **string** | +hh:mm or -hh:mm from UTC | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

