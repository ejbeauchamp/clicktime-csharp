# IO.Swagger.Model.GetTimeOffModel
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Date** | **string** |  | [optional] 
**Hours** | **double?** |  | [optional] 
**ID** | **string** |  | [optional] 
**LegacyID** | **string** |  | [optional] 
**Notes** | **string** |  | [optional] 
**TimeOffRequestID** | **string** |  | [optional] 
**TimeOffType** | [**GetBasicTimeOffTypeModel**](GetBasicTimeOffTypeModel.md) |  | [optional] 
**TimeOffTypeID** | **string** |  | [optional] 
**User** | [**GetBasicUserModel**](GetBasicUserModel.md) |  | [optional] 
**UserID** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

