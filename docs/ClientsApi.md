# IO.Swagger.Api.ClientsApi

All URIs are relative to *https://api.clicktime.com/V2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**CreateClient**](ClientsApi.md#createclient) | **POST** /Clients | Create a Client.
[**DeleteClient**](ClientsApi.md#deleteclient) | **DELETE** /Clients/{clientID} | Delete a client.
[**GetClient**](ClientsApi.md#getclient) | **GET** /Clients/{clientID} | Get a Client.
[**GetClientBillingRate**](ClientsApi.md#getclientbillingrate) | **GET** /Clients/{clientID}/BillingRates/{billingRateID} | Get a secondary billing rate for a Client.
[**GetClientCustomFieldDefinition**](ClientsApi.md#getclientcustomfielddefinition) | **GET** /Clients/CustomFieldDefinitions/{customFieldDefinitionID} | Get a Custom Field Definition for Clients.
[**GetDefaultClient**](ClientsApi.md#getdefaultclient) | **GET** /Clients/new | Get a new Client with default values.
[**GetMyClient**](ClientsApi.md#getmyclient) | **GET** /Me/Clients/{clientID} | Get my Client.
[**ListClientBillingRates**](ClientsApi.md#listclientbillingrates) | **GET** /Clients/{clientID}/BillingRates | Get secondary billing rates for a Client.
[**ListClientCustomFieldDefinitions**](ClientsApi.md#listclientcustomfielddefinitions) | **GET** /Clients/CustomFieldDefinitions | Get Custom Field Definitions for Clients.
[**ListClients**](ClientsApi.md#listclients) | **GET** /Clients | Get Clients.
[**ListMyClients**](ClientsApi.md#listmyclients) | **GET** /Me/Clients | Get my Clients.
[**UpdateClient**](ClientsApi.md#updateclient) | **PATCH** /Clients/{clientID} | Update a Client.
[**UpdateClientBillingRate**](ClientsApi.md#updateclientbillingrate) | **PATCH** /Clients/{clientID}/BillingRates/{billingRateID} | Update a secondary billing rate for a Client.


<a name="createclient"></a>
# **CreateClient**
> ResponseBodyGetClientModelRoot CreateClient (PostClientModelRoot model)

Create a Client.

       User requirements:    Admin, or Manager with permissions to Add/Edit Clients.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class CreateClientExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new ClientsApi();
            var model = new PostClientModelRoot(); // PostClientModelRoot |       Required

            try
            {
                // Create a Client.
                ResponseBodyGetClientModelRoot result = apiInstance.CreateClient(model);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ClientsApi.CreateClient: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **model** | [**PostClientModelRoot**](PostClientModelRoot.md)|       Required | 

### Return type

[**ResponseBodyGetClientModelRoot**](ResponseBodyGetClientModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded, application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="deleteclient"></a>
# **DeleteClient**
> void DeleteClient (string clientID)

Delete a client.

       User requirements:    User must be an Admin.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class DeleteClientExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new ClientsApi();
            var clientID = clientID_example;  // string |       Required

            try
            {
                // Delete a client.
                apiInstance.DeleteClient(clientID);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ClientsApi.DeleteClient: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clientID** | **string**|       Required | 

### Return type

void (empty response body)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getclient"></a>
# **GetClient**
> ResponseBodyGetClientModelRoot GetClient (string clientID)

Get a Client.

       User requirements:    User must be a Manager or an Admin.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class GetClientExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new ClientsApi();
            var clientID = clientID_example;  // string |       Required

            try
            {
                // Get a Client.
                ResponseBodyGetClientModelRoot result = apiInstance.GetClient(clientID);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ClientsApi.GetClient: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clientID** | **string**|       Required | 

### Return type

[**ResponseBodyGetClientModelRoot**](ResponseBodyGetClientModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getclientbillingrate"></a>
# **GetClientBillingRate**
> ResponseBodyGetBillingRateModelRoot GetClientBillingRate (string clientID, string billingRateID)

Get a secondary billing rate for a Client.

       User requirements:    Admin, or Manager with permissions to View Billingrates.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class GetClientBillingRateExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new ClientsApi();
            var clientID = clientID_example;  // string |       Required
            var billingRateID = billingRateID_example;  // string |       Required

            try
            {
                // Get a secondary billing rate for a Client.
                ResponseBodyGetBillingRateModelRoot result = apiInstance.GetClientBillingRate(clientID, billingRateID);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ClientsApi.GetClientBillingRate: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clientID** | **string**|       Required | 
 **billingRateID** | **string**|       Required | 

### Return type

[**ResponseBodyGetBillingRateModelRoot**](ResponseBodyGetBillingRateModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getclientcustomfielddefinition"></a>
# **GetClientCustomFieldDefinition**
> ResponseBodyGetCustomFieldDefinitionModelRoot GetClientCustomFieldDefinition (string customFieldDefinitionID)

Get a Custom Field Definition for Clients.

       User requirements:    Admin, or Manager with permissions to View Clients.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class GetClientCustomFieldDefinitionExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new ClientsApi();
            var customFieldDefinitionID = customFieldDefinitionID_example;  // string |       Required

            try
            {
                // Get a Custom Field Definition for Clients.
                ResponseBodyGetCustomFieldDefinitionModelRoot result = apiInstance.GetClientCustomFieldDefinition(customFieldDefinitionID);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ClientsApi.GetClientCustomFieldDefinition: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customFieldDefinitionID** | **string**|       Required | 

### Return type

[**ResponseBodyGetCustomFieldDefinitionModelRoot**](ResponseBodyGetCustomFieldDefinitionModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getdefaultclient"></a>
# **GetDefaultClient**
> ResponseBodyGetClientModelNew GetDefaultClient ()

Get a new Client with default values.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class GetDefaultClientExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new ClientsApi();

            try
            {
                // Get a new Client with default values.
                ResponseBodyGetClientModelNew result = apiInstance.GetDefaultClient();
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ClientsApi.GetDefaultClient: " + e.Message );
            }
        }
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ResponseBodyGetClientModelNew**](ResponseBodyGetClientModelNew.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getmyclient"></a>
# **GetMyClient**
> ResponseBodyGetClientModelMe GetMyClient (string clientID)

Get my Client.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class GetMyClientExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new ClientsApi();
            var clientID = clientID_example;  // string |       Required

            try
            {
                // Get my Client.
                ResponseBodyGetClientModelMe result = apiInstance.GetMyClient(clientID);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ClientsApi.GetMyClient: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clientID** | **string**|       Required | 

### Return type

[**ResponseBodyGetClientModelMe**](ResponseBodyGetClientModelMe.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="listclientbillingrates"></a>
# **ListClientBillingRates**
> ResponseBodyListGetBillingRateModelRoot ListClientBillingRates (string clientID, List<string> ID = null)

Get secondary billing rates for a Client.

       User requirements:    Admin, or Manager with permissions to View Billingrates.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ListClientBillingRatesExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new ClientsApi();
            var clientID = clientID_example;  // string |       Required
            var ID = new List<string>(); // List<string> |  (optional) 

            try
            {
                // Get secondary billing rates for a Client.
                ResponseBodyListGetBillingRateModelRoot result = apiInstance.ListClientBillingRates(clientID, ID);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ClientsApi.ListClientBillingRates: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clientID** | **string**|       Required | 
 **ID** | [**List&lt;string&gt;**](string.md)|  | [optional] 

### Return type

[**ResponseBodyListGetBillingRateModelRoot**](ResponseBodyListGetBillingRateModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="listclientcustomfielddefinitions"></a>
# **ListClientCustomFieldDefinitions**
> ResponseBodyListGetCustomFieldDefinitionModelRoot ListClientCustomFieldDefinitions (int? limit = null, int? offset = null)

Get Custom Field Definitions for Clients.

       User requirements:    Admin, or Manager with permissions to View Clients.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ListClientCustomFieldDefinitionsExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new ClientsApi();
            var limit = 56;  // int? |  (optional) 
            var offset = 56;  // int? |  (optional) 

            try
            {
                // Get Custom Field Definitions for Clients.
                ResponseBodyListGetCustomFieldDefinitionModelRoot result = apiInstance.ListClientCustomFieldDefinitions(limit, offset);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ClientsApi.ListClientCustomFieldDefinitions: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **limit** | **int?**|  | [optional] 
 **offset** | **int?**|  | [optional] 

### Return type

[**ResponseBodyListGetCustomFieldDefinitionModelRoot**](ResponseBodyListGetCustomFieldDefinitionModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="listclients"></a>
# **ListClients**
> ResponseBodyListGetClientModelRoot ListClients (List<string> ID = null, List<bool?> isActive = null, List<string> name = null, List<string> shortName = null, List<string> clientNumber = null, int? limit = null, int? offset = null)

Get Clients.

       User requirements:    User must be a Manager or an Admin.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ListClientsExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new ClientsApi();
            var ID = new List<string>(); // List<string> |  (optional) 
            var isActive = new List<bool?>(); // List<bool?> |  (optional) 
            var name = new List<string>(); // List<string> |  (optional) 
            var shortName = new List<string>(); // List<string> |  (optional) 
            var clientNumber = new List<string>(); // List<string> |  (optional) 
            var limit = 56;  // int? |  (optional) 
            var offset = 56;  // int? |  (optional) 

            try
            {
                // Get Clients.
                ResponseBodyListGetClientModelRoot result = apiInstance.ListClients(ID, isActive, name, shortName, clientNumber, limit, offset);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ClientsApi.ListClients: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ID** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **isActive** | [**List&lt;bool?&gt;**](bool?.md)|  | [optional] 
 **name** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **shortName** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **clientNumber** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **limit** | **int?**|  | [optional] 
 **offset** | **int?**|  | [optional] 

### Return type

[**ResponseBodyListGetClientModelRoot**](ResponseBodyListGetClientModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="listmyclients"></a>
# **ListMyClients**
> ResponseBodyListGetClientModelMe ListMyClients (List<string> ID = null, List<bool?> isActive = null, List<string> name = null, List<string> shortName = null, List<string> clientNumber = null, int? limit = null, int? offset = null)

Get my Clients.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ListMyClientsExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new ClientsApi();
            var ID = new List<string>(); // List<string> |  (optional) 
            var isActive = new List<bool?>(); // List<bool?> |  (optional) 
            var name = new List<string>(); // List<string> |  (optional) 
            var shortName = new List<string>(); // List<string> |  (optional) 
            var clientNumber = new List<string>(); // List<string> |  (optional) 
            var limit = 56;  // int? |  (optional) 
            var offset = 56;  // int? |  (optional) 

            try
            {
                // Get my Clients.
                ResponseBodyListGetClientModelMe result = apiInstance.ListMyClients(ID, isActive, name, shortName, clientNumber, limit, offset);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ClientsApi.ListMyClients: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ID** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **isActive** | [**List&lt;bool?&gt;**](bool?.md)|  | [optional] 
 **name** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **shortName** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **clientNumber** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **limit** | **int?**|  | [optional] 
 **offset** | **int?**|  | [optional] 

### Return type

[**ResponseBodyListGetClientModelMe**](ResponseBodyListGetClientModelMe.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="updateclient"></a>
# **UpdateClient**
> ResponseBodyGetClientModelRoot UpdateClient (string clientID, PatchClientModelRoot model)

Update a Client.

       User requirements:    Admin, or Manager with permissions to Add/Edit Clients.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class UpdateClientExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new ClientsApi();
            var clientID = clientID_example;  // string |       Required
            var model = new PatchClientModelRoot(); // PatchClientModelRoot |       Required

            try
            {
                // Update a Client.
                ResponseBodyGetClientModelRoot result = apiInstance.UpdateClient(clientID, model);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ClientsApi.UpdateClient: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clientID** | **string**|       Required | 
 **model** | [**PatchClientModelRoot**](PatchClientModelRoot.md)|       Required | 

### Return type

[**ResponseBodyGetClientModelRoot**](ResponseBodyGetClientModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded, application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="updateclientbillingrate"></a>
# **UpdateClientBillingRate**
> ResponseBodyGetBillingRateModelRoot UpdateClientBillingRate (string clientID, string billingRateID, PatchBillingRateModelRoot model)

Update a secondary billing rate for a Client.

       User requirements:    Admin, or Manager with permissions to View Billingrates.    Admin, or Manager with permissions to Add/Edit Clients.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class UpdateClientBillingRateExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new ClientsApi();
            var clientID = clientID_example;  // string |       Required
            var billingRateID = billingRateID_example;  // string |       Required
            var model = new PatchBillingRateModelRoot(); // PatchBillingRateModelRoot |       Required

            try
            {
                // Update a secondary billing rate for a Client.
                ResponseBodyGetBillingRateModelRoot result = apiInstance.UpdateClientBillingRate(clientID, billingRateID, model);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ClientsApi.UpdateClientBillingRate: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clientID** | **string**|       Required | 
 **billingRateID** | **string**|       Required | 
 **model** | [**PatchBillingRateModelRoot**](PatchBillingRateModelRoot.md)|       Required | 

### Return type

[**ResponseBodyGetBillingRateModelRoot**](ResponseBodyGetBillingRateModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded, application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

