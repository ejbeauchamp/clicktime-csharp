# IO.Swagger.Model.Links
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**first** | **string** |  | [optional] 
**last** | **string** |  | [optional] 
**next** | **string** |  | [optional] 
**previous** | **string** |  | [optional] 
**self** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

