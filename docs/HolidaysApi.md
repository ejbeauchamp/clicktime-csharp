# IO.Swagger.Api.HolidaysApi

All URIs are relative to *https://api.clicktime.com/V2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**CreateHoliday**](HolidaysApi.md#createholiday) | **POST** /Holidays | Create a holiday
[**DeleteHoliday**](HolidaysApi.md#deleteholiday) | **DELETE** /Holidays/{holidayID} | Delete a holiday
[**GetDefaultHoliday**](HolidaysApi.md#getdefaultholiday) | **GET** /Holidays/new | Get a new Holiday with default values.
[**GetHoliday**](HolidaysApi.md#getholiday) | **GET** /Holidays/{holidayID} | Get a Holiday.
[**ListHolidays**](HolidaysApi.md#listholidays) | **GET** /Holidays | Get Holidays.
[**UpdateHoliday**](HolidaysApi.md#updateholiday) | **PATCH** /Holidays/{holidayID} | Update a holiday


<a name="createholiday"></a>
# **CreateHoliday**
> ResponseBodyGetHolidayModelRoot CreateHoliday (PostHolidayModelRoot model)

Create a holiday

       Company requirements:    DCAA must be disabled.    Company must have the Time Off Management optional module.       User requirements:    User must be an Admin.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class CreateHolidayExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new HolidaysApi();
            var model = new PostHolidayModelRoot(); // PostHolidayModelRoot |       Required

            try
            {
                // Create a holiday
                ResponseBodyGetHolidayModelRoot result = apiInstance.CreateHoliday(model);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling HolidaysApi.CreateHoliday: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **model** | [**PostHolidayModelRoot**](PostHolidayModelRoot.md)|       Required | 

### Return type

[**ResponseBodyGetHolidayModelRoot**](ResponseBodyGetHolidayModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded, application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="deleteholiday"></a>
# **DeleteHoliday**
> void DeleteHoliday (string holidayID)

Delete a holiday

       Company requirements:    DCAA must be disabled.       User requirements:    User must be an Admin.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class DeleteHolidayExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new HolidaysApi();
            var holidayID = holidayID_example;  // string |       Required

            try
            {
                // Delete a holiday
                apiInstance.DeleteHoliday(holidayID);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling HolidaysApi.DeleteHoliday: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **holidayID** | **string**|       Required | 

### Return type

void (empty response body)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getdefaultholiday"></a>
# **GetDefaultHoliday**
> ResponseBodyGetHolidayModelNew GetDefaultHoliday ()

Get a new Holiday with default values.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class GetDefaultHolidayExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new HolidaysApi();

            try
            {
                // Get a new Holiday with default values.
                ResponseBodyGetHolidayModelNew result = apiInstance.GetDefaultHoliday();
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling HolidaysApi.GetDefaultHoliday: " + e.Message );
            }
        }
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ResponseBodyGetHolidayModelNew**](ResponseBodyGetHolidayModelNew.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getholiday"></a>
# **GetHoliday**
> ResponseBodyGetHolidayModelRoot GetHoliday (string holidayID)

Get a Holiday.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class GetHolidayExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new HolidaysApi();
            var holidayID = holidayID_example;  // string |       Required

            try
            {
                // Get a Holiday.
                ResponseBodyGetHolidayModelRoot result = apiInstance.GetHoliday(holidayID);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling HolidaysApi.GetHoliday: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **holidayID** | **string**|       Required | 

### Return type

[**ResponseBodyGetHolidayModelRoot**](ResponseBodyGetHolidayModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="listholidays"></a>
# **ListHolidays**
> ResponseBodyListGetHolidayModelRoot ListHolidays (List<string> ID = null, List<string> holidayTypeID = null, List<string> date = null, List<bool?> isActive = null, string startDate = null, string endDate = null, int? limit = null, int? offset = null)

Get Holidays.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ListHolidaysExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new HolidaysApi();
            var ID = new List<string>(); // List<string> |  (optional) 
            var holidayTypeID = new List<string>(); // List<string> |  (optional) 
            var date = new List<string>(); // List<string> |  (optional) 
            var isActive = new List<bool?>(); // List<bool?> |  (optional) 
            var startDate = startDate_example;  // string |  (optional) 
            var endDate = endDate_example;  // string |  (optional) 
            var limit = 56;  // int? |  (optional) 
            var offset = 56;  // int? |  (optional) 

            try
            {
                // Get Holidays.
                ResponseBodyListGetHolidayModelRoot result = apiInstance.ListHolidays(ID, holidayTypeID, date, isActive, startDate, endDate, limit, offset);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling HolidaysApi.ListHolidays: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ID** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **holidayTypeID** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **date** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **isActive** | [**List&lt;bool?&gt;**](bool?.md)|  | [optional] 
 **startDate** | **string**|  | [optional] 
 **endDate** | **string**|  | [optional] 
 **limit** | **int?**|  | [optional] 
 **offset** | **int?**|  | [optional] 

### Return type

[**ResponseBodyListGetHolidayModelRoot**](ResponseBodyListGetHolidayModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="updateholiday"></a>
# **UpdateHoliday**
> ResponseBodyGetHolidayModelRoot UpdateHoliday (string holidayID, PatchHolidayModelRoot model)

Update a holiday

       Company requirements:    DCAA must be disabled.    Company must have the Time Off Management optional module.       User requirements:    User must be an Admin.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class UpdateHolidayExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new HolidaysApi();
            var holidayID = holidayID_example;  // string |       Required
            var model = new PatchHolidayModelRoot(); // PatchHolidayModelRoot |       Required

            try
            {
                // Update a holiday
                ResponseBodyGetHolidayModelRoot result = apiInstance.UpdateHoliday(holidayID, model);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling HolidaysApi.UpdateHoliday: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **holidayID** | **string**|       Required | 
 **model** | [**PatchHolidayModelRoot**](PatchHolidayModelRoot.md)|       Required | 

### Return type

[**ResponseBodyGetHolidayModelRoot**](ResponseBodyGetHolidayModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded, application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

