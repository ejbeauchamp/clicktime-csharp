# IO.Swagger.Api.ExpenseItemsApi

All URIs are relative to *https://api.clicktime.com/V2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**GetDefaultExpenseItem**](ExpenseItemsApi.md#getdefaultexpenseitem) | **GET** /ExpenseItems/new | Get a new Expense Item with default values.


<a name="getdefaultexpenseitem"></a>
# **GetDefaultExpenseItem**
> ResponseBodyGetExpenseItemModelNew GetDefaultExpenseItem ()

Get a new Expense Item with default values.

       Company requirements:    Company must have the Expenses optional module.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class GetDefaultExpenseItemExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new ExpenseItemsApi();

            try
            {
                // Get a new Expense Item with default values.
                ResponseBodyGetExpenseItemModelNew result = apiInstance.GetDefaultExpenseItem();
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ExpenseItemsApi.GetDefaultExpenseItem: " + e.Message );
            }
        }
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ResponseBodyGetExpenseItemModelNew**](ResponseBodyGetExpenseItemModelNew.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

