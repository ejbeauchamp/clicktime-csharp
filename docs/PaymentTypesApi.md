# IO.Swagger.Api.PaymentTypesApi

All URIs are relative to *https://api.clicktime.com/V2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**GetDefaultPaymentType**](PaymentTypesApi.md#getdefaultpaymenttype) | **GET** /PaymentTypes/new | Get a new Payment Type with default values.
[**GetPaymentType**](PaymentTypesApi.md#getpaymenttype) | **GET** /PaymentTypes/{paymentTypeID} | Get a Payment Type.
[**ListPaymentTypes**](PaymentTypesApi.md#listpaymenttypes) | **GET** /PaymentTypes | Get Payment Types.


<a name="getdefaultpaymenttype"></a>
# **GetDefaultPaymentType**
> ResponseBodyGetPaymentTypeModelNew GetDefaultPaymentType ()

Get a new Payment Type with default values.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class GetDefaultPaymentTypeExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new PaymentTypesApi();

            try
            {
                // Get a new Payment Type with default values.
                ResponseBodyGetPaymentTypeModelNew result = apiInstance.GetDefaultPaymentType();
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling PaymentTypesApi.GetDefaultPaymentType: " + e.Message );
            }
        }
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ResponseBodyGetPaymentTypeModelNew**](ResponseBodyGetPaymentTypeModelNew.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getpaymenttype"></a>
# **GetPaymentType**
> ResponseBodyGetPaymentTypeModelRoot GetPaymentType (string paymentTypeID)

Get a Payment Type.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class GetPaymentTypeExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new PaymentTypesApi();
            var paymentTypeID = paymentTypeID_example;  // string |       Required

            try
            {
                // Get a Payment Type.
                ResponseBodyGetPaymentTypeModelRoot result = apiInstance.GetPaymentType(paymentTypeID);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling PaymentTypesApi.GetPaymentType: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **paymentTypeID** | **string**|       Required | 

### Return type

[**ResponseBodyGetPaymentTypeModelRoot**](ResponseBodyGetPaymentTypeModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="listpaymenttypes"></a>
# **ListPaymentTypes**
> ResponseBodyListGetPaymentTypeModelRoot ListPaymentTypes (List<string> ID = null, List<bool?> isActive = null, List<bool?> reimbursable = null, int? limit = null, int? offset = null)

Get Payment Types.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ListPaymentTypesExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new PaymentTypesApi();
            var ID = new List<string>(); // List<string> |  (optional) 
            var isActive = new List<bool?>(); // List<bool?> |  (optional) 
            var reimbursable = new List<bool?>(); // List<bool?> |  (optional) 
            var limit = 56;  // int? |  (optional) 
            var offset = 56;  // int? |  (optional) 

            try
            {
                // Get Payment Types.
                ResponseBodyListGetPaymentTypeModelRoot result = apiInstance.ListPaymentTypes(ID, isActive, reimbursable, limit, offset);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling PaymentTypesApi.ListPaymentTypes: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ID** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **isActive** | [**List&lt;bool?&gt;**](bool?.md)|  | [optional] 
 **reimbursable** | [**List&lt;bool?&gt;**](bool?.md)|  | [optional] 
 **limit** | **int?**|  | [optional] 
 **offset** | **int?**|  | [optional] 

### Return type

[**ResponseBodyListGetPaymentTypeModelRoot**](ResponseBodyListGetPaymentTypeModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

