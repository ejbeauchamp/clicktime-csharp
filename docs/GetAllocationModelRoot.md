# IO.Swagger.Model.GetAllocationModelRoot
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**EstHours** | **double?** |  | [optional] 
**Job** | [**GetBasicJobModelJobGetAllocationModelRoot**](GetBasicJobModelJobGetAllocationModelRoot.md) |        Request requirements:    Verbose&#x3D;true | [optional] 
**JobID** | **string** |  | [optional] 
**Month** | **string** |  | [optional] 
**User** | [**GetBasicUserModelUserGetAllocationModelRoot**](GetBasicUserModelUserGetAllocationModelRoot.md) |        Request requirements:    Verbose&#x3D;true | [optional] 
**UserID** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

