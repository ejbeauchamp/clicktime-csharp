# IO.Swagger.Model.GetCurrencyModel
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Currency** | **string** |  | [optional] 
**CurrencyISO** | **string** |  | [optional] 
**MarketRate** | **double?** |  | [optional] 
**Rate** | **double?** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

