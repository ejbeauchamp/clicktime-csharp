# IO.Swagger.Api.AllocationsApi

All URIs are relative to *https://api.clicktime.com/V2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**CreateAllocation**](AllocationsApi.md#createallocation) | **POST** /Allocations | Create an allocation.
[**DeleteAllocation**](AllocationsApi.md#deleteallocation) | **DELETE** /Allocations/Jobs/{jobID}/Users/{userID}/Month/{month} | Delete an allocation.
[**ListAllocations**](AllocationsApi.md#listallocations) | **GET** /Allocations | Get allocations.
[**UpdateAllocation**](AllocationsApi.md#updateallocation) | **PATCH** /Allocations/Jobs/{jobID}/Users/{userID}/Month/{month} | Update an allocation.


<a name="createallocation"></a>
# **CreateAllocation**
> ResponseBodyGetAllocationModelRoot CreateAllocation (PostAllocationModelRoot model)

Create an allocation.

       Company requirements:    Company must have the ResourcePlanning optional module.    Estimation model: Resource Planning       User requirements:    Admin, or Manager with permissions to Add/Edit Resource Planning.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class CreateAllocationExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new AllocationsApi();
            var model = new PostAllocationModelRoot(); // PostAllocationModelRoot |       Required

            try
            {
                // Create an allocation.
                ResponseBodyGetAllocationModelRoot result = apiInstance.CreateAllocation(model);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling AllocationsApi.CreateAllocation: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **model** | [**PostAllocationModelRoot**](PostAllocationModelRoot.md)|       Required | 

### Return type

[**ResponseBodyGetAllocationModelRoot**](ResponseBodyGetAllocationModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded, application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="deleteallocation"></a>
# **DeleteAllocation**
> void DeleteAllocation (string jobID, string userID, string month)

Delete an allocation.

       Company requirements:    Company must have the ResourcePlanning optional module.    Estimation model: Resource Planning       User requirements:    Admin, or Manager with permissions to Add/Edit Resource Planning.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class DeleteAllocationExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new AllocationsApi();
            var jobID = jobID_example;  // string |       Required
            var userID = userID_example;  // string |       Required
            var month = month_example;  // string |       Required

            try
            {
                // Delete an allocation.
                apiInstance.DeleteAllocation(jobID, userID, month);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling AllocationsApi.DeleteAllocation: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **jobID** | **string**|       Required | 
 **userID** | **string**|       Required | 
 **month** | **string**|       Required | 

### Return type

void (empty response body)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="listallocations"></a>
# **ListAllocations**
> ResponseBodyGetAllocationModelRoot ListAllocations (List<string> jobID = null, List<string> userID = null, List<string> divisionID = null, string startMonth = null, string endMonth = null, string jobIsActive = null, string userIsActive = null, int? limit = null, int? offset = null)

Get allocations.

       Company requirements:    Company must have the ResourcePlanning optional module.    Estimation model: Resource Planning       User requirements:    Admin, or Manager with permissions to View Resource Planning.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ListAllocationsExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new AllocationsApi();
            var jobID = new List<string>(); // List<string> |  (optional) 
            var userID = new List<string>(); // List<string> |  (optional) 
            var divisionID = new List<string>(); // List<string> |  (optional) 
            var startMonth = startMonth_example;  // string |  (optional) 
            var endMonth = endMonth_example;  // string |  (optional) 
            var jobIsActive = jobIsActive_example;  // string |  (optional) 
            var userIsActive = userIsActive_example;  // string |  (optional) 
            var limit = 56;  // int? |  (optional) 
            var offset = 56;  // int? |  (optional) 

            try
            {
                // Get allocations.
                ResponseBodyGetAllocationModelRoot result = apiInstance.ListAllocations(jobID, userID, divisionID, startMonth, endMonth, jobIsActive, userIsActive, limit, offset);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling AllocationsApi.ListAllocations: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **jobID** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **userID** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **divisionID** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **startMonth** | **string**|  | [optional] 
 **endMonth** | **string**|  | [optional] 
 **jobIsActive** | **string**|  | [optional] 
 **userIsActive** | **string**|  | [optional] 
 **limit** | **int?**|  | [optional] 
 **offset** | **int?**|  | [optional] 

### Return type

[**ResponseBodyGetAllocationModelRoot**](ResponseBodyGetAllocationModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="updateallocation"></a>
# **UpdateAllocation**
> ResponseBodyGetAllocationModel UpdateAllocation (string jobID, string userID, string month, PatchAllocationModelRoot model)

Update an allocation.

       Company requirements:    Company must have the ResourcePlanning optional module.    Estimation model: Resource Planning       User requirements:    Admin, or Manager with permissions to Add/Edit Resource Planning.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class UpdateAllocationExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new AllocationsApi();
            var jobID = jobID_example;  // string |       Required
            var userID = userID_example;  // string |       Required
            var month = month_example;  // string |       Required
            var model = new PatchAllocationModelRoot(); // PatchAllocationModelRoot |       Required

            try
            {
                // Update an allocation.
                ResponseBodyGetAllocationModel result = apiInstance.UpdateAllocation(jobID, userID, month, model);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling AllocationsApi.UpdateAllocation: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **jobID** | **string**|       Required | 
 **userID** | **string**|       Required | 
 **month** | **string**|       Required | 
 **model** | [**PatchAllocationModelRoot**](PatchAllocationModelRoot.md)|       Required | 

### Return type

[**ResponseBodyGetAllocationModel**](ResponseBodyGetAllocationModel.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded, application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

