# IO.Swagger.Model.GetTimeOffRequestModel
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Actions** | [**List&lt;GetActionModelRequestAction&gt;**](GetActionModelRequestAction.md) |  | [optional] 
**ApprovalByUser** | [**GetBasicUserModel**](GetBasicUserModel.md) |  | [optional] 
**ApprovalByUserID** | **string** |  | [optional] 
**CreatedDate** | **string** |  | [optional] 
**Dates** | [**List&lt;GetTimeOffRequestDateModel&gt;**](GetTimeOffRequestDateModel.md) |  | [optional] 
**History** | [**List&lt;GetTimeOffRequestHistoryModel&gt;**](GetTimeOffRequestHistoryModel.md) |  | [optional] 
**ID** | **string** |  | [optional] 
**LegacyID** | **string** |  | [optional] 
**Notes** | **string** |  | [optional] 
**RequestedByUser** | [**GetBasicUserModel**](GetBasicUserModel.md) |  | [optional] 
**RequestedByUserID** | **string** |  | [optional] 
**Status** | **string** |  | [optional] 
**TimeOffType** | [**GetBasicTimeOffTypeModel**](GetBasicTimeOffTypeModel.md) |  | [optional] 
**TimeOffTypeID** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

