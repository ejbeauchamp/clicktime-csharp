# IO.Swagger.Api.HolidayTypesApi

All URIs are relative to *https://api.clicktime.com/V2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**AddHolidayTypeEmploymentType**](HolidayTypesApi.md#addholidaytypeemploymenttype) | **POST** /HolidayTypes/{holidayTypeID}/EmploymentTypes | Associate an Employment Type with a Holiday Type.
[**CreateHolidayType**](HolidayTypesApi.md#createholidaytype) | **POST** /HolidayTypes | Create a Holiday Type.
[**GetDefaultHolidayType**](HolidayTypesApi.md#getdefaultholidaytype) | **GET** /HolidayTypes/new | Get a new Holiday Type with default values.
[**GetHolidayType**](HolidayTypesApi.md#getholidaytype) | **GET** /HolidayTypes/{holidayTypeID} | Get a Holiday Type.
[**ListHolidayTypeEmploymentTypes**](HolidayTypesApi.md#listholidaytypeemploymenttypes) | **GET** /HolidayTypes/{holidayTypeID}/EmploymentTypes | Get a Holiday Type&#39;s Employment Types.
[**ListHolidayTypes**](HolidayTypesApi.md#listholidaytypes) | **GET** /HolidayTypes | Get Holiday Types.
[**RemoveHolidayTypeEmploymentType**](HolidayTypesApi.md#removeholidaytypeemploymenttype) | **DELETE** /HolidayTypes/{holidayTypeID}/EmploymentTypes/{employmentTypeID} | Delete a Holiday Type / Employment Type association.
[**UpdateHolidayType**](HolidayTypesApi.md#updateholidaytype) | **PATCH** /HolidayTypes/{holidayTypeID} | Update a Holiday Type
[**UpdateHolidayTypeEmploymentTypes**](HolidayTypesApi.md#updateholidaytypeemploymenttypes) | **PUT** /HolidayTypes/{holidayTypeID}/EmploymentTypes | Replace the list of Holiday Type / Employment Type associations.


<a name="addholidaytypeemploymenttype"></a>
# **AddHolidayTypeEmploymentType**
> ResponseBodyListSystemString AddHolidayTypeEmploymentType (string holidayTypeID, PostEmploymentTypeAssociationModelRoot model)

Associate an Employment Type with a Holiday Type.

       Company requirements:    DCAA must be disabled.    Leave Type List Control must be restricted by Employment Type.    Company must have the Time Off Management optional module.       User requirements:    User must be an Admin.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class AddHolidayTypeEmploymentTypeExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new HolidayTypesApi();
            var holidayTypeID = holidayTypeID_example;  // string |       Required
            var model = new PostEmploymentTypeAssociationModelRoot(); // PostEmploymentTypeAssociationModelRoot |       Required

            try
            {
                // Associate an Employment Type with a Holiday Type.
                ResponseBodyListSystemString result = apiInstance.AddHolidayTypeEmploymentType(holidayTypeID, model);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling HolidayTypesApi.AddHolidayTypeEmploymentType: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **holidayTypeID** | **string**|       Required | 
 **model** | [**PostEmploymentTypeAssociationModelRoot**](PostEmploymentTypeAssociationModelRoot.md)|       Required | 

### Return type

[**ResponseBodyListSystemString**](ResponseBodyListSystemString.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded, application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="createholidaytype"></a>
# **CreateHolidayType**
> ResponseBodyGetHolidayTypeModelRoot CreateHolidayType (PostHolidayTypeModelRoot model)

Create a Holiday Type.

       Company requirements:    DCAA must be disabled.    Company must have the Time Off Management optional module.       User requirements:    User must be an Admin.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class CreateHolidayTypeExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new HolidayTypesApi();
            var model = new PostHolidayTypeModelRoot(); // PostHolidayTypeModelRoot |       Required

            try
            {
                // Create a Holiday Type.
                ResponseBodyGetHolidayTypeModelRoot result = apiInstance.CreateHolidayType(model);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling HolidayTypesApi.CreateHolidayType: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **model** | [**PostHolidayTypeModelRoot**](PostHolidayTypeModelRoot.md)|       Required | 

### Return type

[**ResponseBodyGetHolidayTypeModelRoot**](ResponseBodyGetHolidayTypeModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded, application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getdefaultholidaytype"></a>
# **GetDefaultHolidayType**
> ResponseBodyGetHolidayTypeModelNew GetDefaultHolidayType ()

Get a new Holiday Type with default values.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class GetDefaultHolidayTypeExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new HolidayTypesApi();

            try
            {
                // Get a new Holiday Type with default values.
                ResponseBodyGetHolidayTypeModelNew result = apiInstance.GetDefaultHolidayType();
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling HolidayTypesApi.GetDefaultHolidayType: " + e.Message );
            }
        }
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ResponseBodyGetHolidayTypeModelNew**](ResponseBodyGetHolidayTypeModelNew.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getholidaytype"></a>
# **GetHolidayType**
> ResponseBodyGetHolidayTypeModelRoot GetHolidayType (string holidayTypeID)

Get a Holiday Type.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class GetHolidayTypeExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new HolidayTypesApi();
            var holidayTypeID = holidayTypeID_example;  // string |       Required

            try
            {
                // Get a Holiday Type.
                ResponseBodyGetHolidayTypeModelRoot result = apiInstance.GetHolidayType(holidayTypeID);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling HolidayTypesApi.GetHolidayType: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **holidayTypeID** | **string**|       Required | 

### Return type

[**ResponseBodyGetHolidayTypeModelRoot**](ResponseBodyGetHolidayTypeModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="listholidaytypeemploymenttypes"></a>
# **ListHolidayTypeEmploymentTypes**
> ResponseBodyListSystemString ListHolidayTypeEmploymentTypes (string holidayTypeID)

Get a Holiday Type's Employment Types.

       Company requirements:    Leave Type List Control must be restricted by Employment Type.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ListHolidayTypeEmploymentTypesExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new HolidayTypesApi();
            var holidayTypeID = holidayTypeID_example;  // string |       Required

            try
            {
                // Get a Holiday Type's Employment Types.
                ResponseBodyListSystemString result = apiInstance.ListHolidayTypeEmploymentTypes(holidayTypeID);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling HolidayTypesApi.ListHolidayTypeEmploymentTypes: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **holidayTypeID** | **string**|       Required | 

### Return type

[**ResponseBodyListSystemString**](ResponseBodyListSystemString.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="listholidaytypes"></a>
# **ListHolidayTypes**
> ResponseBodyListGetHolidayTypeModelRoot ListHolidayTypes (List<string> ID = null, List<bool?> isActive = null, List<string> name = null, int? limit = null, int? offset = null)

Get Holiday Types.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ListHolidayTypesExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new HolidayTypesApi();
            var ID = new List<string>(); // List<string> |  (optional) 
            var isActive = new List<bool?>(); // List<bool?> |  (optional) 
            var name = new List<string>(); // List<string> |  (optional) 
            var limit = 56;  // int? |  (optional) 
            var offset = 56;  // int? |  (optional) 

            try
            {
                // Get Holiday Types.
                ResponseBodyListGetHolidayTypeModelRoot result = apiInstance.ListHolidayTypes(ID, isActive, name, limit, offset);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling HolidayTypesApi.ListHolidayTypes: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ID** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **isActive** | [**List&lt;bool?&gt;**](bool?.md)|  | [optional] 
 **name** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **limit** | **int?**|  | [optional] 
 **offset** | **int?**|  | [optional] 

### Return type

[**ResponseBodyListGetHolidayTypeModelRoot**](ResponseBodyListGetHolidayTypeModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="removeholidaytypeemploymenttype"></a>
# **RemoveHolidayTypeEmploymentType**
> ResponseBodyListSystemString RemoveHolidayTypeEmploymentType (string holidayTypeID, string employmentTypeID)

Delete a Holiday Type / Employment Type association.

       Company requirements:    DCAA must be disabled.    Leave Type List Control must be restricted by Employment Type.    Company must have the Time Off Management optional module.       User requirements:    User must be an Admin.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class RemoveHolidayTypeEmploymentTypeExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new HolidayTypesApi();
            var holidayTypeID = holidayTypeID_example;  // string |       Required
            var employmentTypeID = employmentTypeID_example;  // string |       Required

            try
            {
                // Delete a Holiday Type / Employment Type association.
                ResponseBodyListSystemString result = apiInstance.RemoveHolidayTypeEmploymentType(holidayTypeID, employmentTypeID);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling HolidayTypesApi.RemoveHolidayTypeEmploymentType: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **holidayTypeID** | **string**|       Required | 
 **employmentTypeID** | **string**|       Required | 

### Return type

[**ResponseBodyListSystemString**](ResponseBodyListSystemString.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="updateholidaytype"></a>
# **UpdateHolidayType**
> ResponseBodyGetHolidayTypeModelRoot UpdateHolidayType (string holidayTypeID, PatchHolidayTypeModelRoot model)

Update a Holiday Type

       Company requirements:    DCAA must be disabled.       User requirements:    User must be an Admin.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class UpdateHolidayTypeExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new HolidayTypesApi();
            var holidayTypeID = holidayTypeID_example;  // string |       Required
            var model = new PatchHolidayTypeModelRoot(); // PatchHolidayTypeModelRoot |       Required

            try
            {
                // Update a Holiday Type
                ResponseBodyGetHolidayTypeModelRoot result = apiInstance.UpdateHolidayType(holidayTypeID, model);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling HolidayTypesApi.UpdateHolidayType: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **holidayTypeID** | **string**|       Required | 
 **model** | [**PatchHolidayTypeModelRoot**](PatchHolidayTypeModelRoot.md)|       Required | 

### Return type

[**ResponseBodyGetHolidayTypeModelRoot**](ResponseBodyGetHolidayTypeModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded, application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="updateholidaytypeemploymenttypes"></a>
# **UpdateHolidayTypeEmploymentTypes**
> ResponseBodyListSystemString UpdateHolidayTypeEmploymentTypes (string holidayTypeID, List<string> employmentTypeIDs)

Replace the list of Holiday Type / Employment Type associations.

       Company requirements:    DCAA must be disabled.    Leave Type List Control must be restricted by Employment Type.       User requirements:    User must be an Admin.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class UpdateHolidayTypeEmploymentTypesExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new HolidayTypesApi();
            var holidayTypeID = holidayTypeID_example;  // string |       Required
            var employmentTypeIDs = ;  // List<string> |       Required

            try
            {
                // Replace the list of Holiday Type / Employment Type associations.
                ResponseBodyListSystemString result = apiInstance.UpdateHolidayTypeEmploymentTypes(holidayTypeID, employmentTypeIDs);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling HolidayTypesApi.UpdateHolidayTypeEmploymentTypes: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **holidayTypeID** | **string**|       Required | 
 **employmentTypeIDs** | **List&lt;string&gt;**|       Required | 

### Return type

[**ResponseBodyListSystemString**](ResponseBodyListSystemString.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded, application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

