# IO.Swagger.Api.CurrenciesApi

All URIs are relative to *https://api.clicktime.com/V2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**GetMyCurrency**](CurrenciesApi.md#getmycurrency) | **GET** /Me/Currencies/{currencyISO} | Get my Currency.
[**ListMyCurrencies**](CurrenciesApi.md#listmycurrencies) | **GET** /Me/Currencies | Get my Currencies.


<a name="getmycurrency"></a>
# **GetMyCurrency**
> ResponseBodyGetCurrencyModelMe GetMyCurrency (string currencyISO)

Get my Currency.

       Company requirements:    Company must have the Expenses optional module.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class GetMyCurrencyExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new CurrenciesApi();
            var currencyISO = currencyISO_example;  // string |       Required

            try
            {
                // Get my Currency.
                ResponseBodyGetCurrencyModelMe result = apiInstance.GetMyCurrency(currencyISO);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling CurrenciesApi.GetMyCurrency: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **currencyISO** | **string**|       Required | 

### Return type

[**ResponseBodyGetCurrencyModelMe**](ResponseBodyGetCurrencyModelMe.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="listmycurrencies"></a>
# **ListMyCurrencies**
> ResponseBodyListGetCurrencyModelMe ListMyCurrencies (List<string> currencyISO = null, int? limit = null, int? offset = null)

Get my Currencies.

       Company requirements:    Company must have the Expenses optional module.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ListMyCurrenciesExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new CurrenciesApi();
            var currencyISO = new List<string>(); // List<string> |  (optional) 
            var limit = 56;  // int? |  (optional) 
            var offset = 56;  // int? |  (optional) 

            try
            {
                // Get my Currencies.
                ResponseBodyListGetCurrencyModelMe result = apiInstance.ListMyCurrencies(currencyISO, limit, offset);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling CurrenciesApi.ListMyCurrencies: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **currencyISO** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **limit** | **int?**|  | [optional] 
 **offset** | **int?**|  | [optional] 

### Return type

[**ResponseBodyListGetCurrencyModelMe**](ResponseBodyListGetCurrencyModelMe.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

