# IO.Swagger.Model.GetExpenseItemModel
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Amount** | **double?** |  | [optional] 
**AmountForeign** | **double?** |  | [optional] 
**AmountForeignCurrency** | **string** |  | [optional] 
**AmountForeignExchangeRate** | **double?** |  | [optional] 
**Billable** | **bool?** |  | [optional] 
**Comment** | **string** |  | [optional] 
**Currency** | **string** |  | [optional] 
**Description** | **string** |  | [optional] 
**ExpenseDate** | **string** |  | [optional] 
**ExpenseSheet** | [**GetBasicExpenseSheetModel**](GetBasicExpenseSheetModel.md) |  | [optional] 
**ExpenseSheetID** | **string** |  | [optional] 
**ExpenseType** | [**GetBasicExpenseTypeModel**](GetBasicExpenseTypeModel.md) |  | [optional] 
**ExpenseTypeID** | **string** |  | [optional] 
**HasForeignCurrency** | **bool?** |  | [optional] 
**ID** | **string** |  | [optional] 
**Job** | [**GetBasicJobModel**](GetBasicJobModel.md) |  | [optional] 
**JobID** | **string** |  | [optional] 
**LegacyID** | **string** |  | [optional] 
**PaymentType** | [**GetBasicPaymentTypeModel**](GetBasicPaymentTypeModel.md) |  | [optional] 
**PaymentTypeID** | **string** |  | [optional] 
**Quantity** | **double?** |  | [optional] 
**Rate** | **double?** |  | [optional] 
**ReceiptURL** | **string** |  | [optional] 
**Reimbursable** | **bool?** |  | [optional] 
**User** | [**GetBasicUserModel**](GetBasicUserModel.md) |  | [optional] 
**UserID** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

