# IO.Swagger.Model.GetTimeOffTypeModelRoot
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**AccountingPackageID** | **string** |        User requirements:    Security Level(s): Admin | [optional] 
**DefaultAccrualRate** | **double?** |        User requirements:    Security Level(s): Admin       Company requirements:    Optional module(s): Time Off Management | [optional] 
**DefaultApprover** | [**GetBasicUserModelDefaultApproverGetTimeOffTypeModelRoot**](GetBasicUserModelDefaultApproverGetTimeOffTypeModelRoot.md) |        Request requirements:    Verbose&#x3D;true       User requirements:    Security Level(s): Admin       Company requirements:    Optional module(s): Time Off Management | [optional] 
**DefaultApproverID** | **string** |        User requirements:    Security Level(s): Admin       Company requirements:    Optional module(s): Time Off Management | [optional] 
**DefaultMaximumBalance** | **double?** |        User requirements:    Security Level(s): Admin       Company requirements:    Optional module(s): Time Off Management | [optional] 
**DefaultStartingBalance** | **double?** |        User requirements:    Security Level(s): Admin | [optional] 
**DisplayOnTimeEntryPages** | **bool?** |        Company requirements:    Optional module(s): Time Off Management    CanViewWorkTypeBalanceDashboard | [optional] 
**EmploymentTypeIDs** | **List&lt;string&gt;** |        Request requirements:    Verbose&#x3D;true       Company requirements:    Leave Type Restrictions: By Employment Type | [optional] 
**ID** | **string** |  | [optional] 
**IsActive** | **bool?** |  | [optional] 
**LegacyID** | **string** |        Request requirements:    CTLegacyScramble&#x3D;true | [optional] 
**Name** | **string** |  | [optional] 
**Notes** | **string** |        User requirements:    Security Level(s): Admin | [optional] 
**RequiresApproval** | **bool?** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

