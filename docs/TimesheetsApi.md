# IO.Swagger.Api.TimesheetsApi

All URIs are relative to *https://api.clicktime.com/V2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**CreateManagedTimeEntry**](TimesheetsApi.md#createmanagedtimeentry) | **POST** /Manage/Timesheets/TimeEntries | Create a managed Time Entry.
[**CreateManagedTimeOff**](TimesheetsApi.md#createmanagedtimeoff) | **POST** /Manage/Timesheets/TimeOff | Create a Time Off entry for a managed Timesheet.
[**DeleteManagedTimeEntry**](TimesheetsApi.md#deletemanagedtimeentry) | **DELETE** /Manage/Timesheets/TimeEntries/{timeEntryID} | Delete a managed Time Entry.
[**DeleteManagedTimeOff**](TimesheetsApi.md#deletemanagedtimeoff) | **DELETE** /Manage/Timesheets/TimeOff/{timeOffID} | Delete a Time Off entry for a managed Timesheet.
[**GetManagedTimeEntry**](TimesheetsApi.md#getmanagedtimeentry) | **GET** /Manage/Timesheets/TimeEntries/{timeEntryID} | Get managed Time Entry.
[**GetManagedTimeOff**](TimesheetsApi.md#getmanagedtimeoff) | **GET** /Manage/Timesheets/TimeOff/{timeOffID} | Get a Time Off entry for managed Timesheets.
[**GetManagedTimesheet**](TimesheetsApi.md#getmanagedtimesheet) | **GET** /Manage/Timesheets/{timesheetID} | Get a managed Timesheet.
[**GetManagedTimesheetByDateByUser**](TimesheetsApi.md#getmanagedtimesheetbydatebyuser) | **GET** /Manage/Timesheets/{date}/Users/{userID} | Get a User&#39;s Timesheet by date. If the timesheet does not exist it will be created.
[**GetManagedTimesheetTimeOff**](TimesheetsApi.md#getmanagedtimesheettimeoff) | **GET** /Manage/Timesheets/{timesheetID}/TimeOff/{timeOffID} | Get a Time Off entry for a managed Timesheet.
[**GetMyTimesheet**](TimesheetsApi.md#getmytimesheet) | **GET** /Me/Timesheets/{timesheetID} | Get my Timesheet by ID.
[**GetMyTimesheetByDate**](TimesheetsApi.md#getmytimesheetbydate) | **GET** /Me/Timesheets/{date} | Get my Timesheet by date.
[**GetTimesheet**](TimesheetsApi.md#gettimesheet) | **GET** /Timesheets/{timesheetID} | Get a Timesheet.
[**GetTimesheetByDateByUser**](TimesheetsApi.md#gettimesheetbydatebyuser) | **GET** /Timesheets/{date}/Users/{userID} | Get a User&#39;s Timesheet by date.  If the timesheet does not exist it will be created.
[**ListManagedTimeEntries**](TimesheetsApi.md#listmanagedtimeentries) | **GET** /Manage/Timesheets/TimeEntries | Get managed Time Entries.
[**ListManagedTimeOff**](TimesheetsApi.md#listmanagedtimeoff) | **GET** /Manage/Timesheets/TimeOff | Get Time Off entries for managed Timesheets.
[**ListManagedTimesheetActions**](TimesheetsApi.md#listmanagedtimesheetactions) | **GET** /Manage/Timesheets/{timesheetID}/Actions | Get available Actions for a managed Timesheet with warnings about blocked actions in the meta tag of the response.
[**ListManagedTimesheetHistory**](TimesheetsApi.md#listmanagedtimesheethistory) | **GET** /Manage/Timesheets/{timesheetID}/History | Get a managed Timesheet&#39;s status history.
[**ListManagedTimesheetTimeOff**](TimesheetsApi.md#listmanagedtimesheettimeoff) | **GET** /Manage/Timesheets/{timesheetID}/TimeOff | Get Time Off entries for a managed Timesheet.
[**ListManagedTimesheets**](TimesheetsApi.md#listmanagedtimesheets) | **GET** /Manage/Timesheets | Get managed Timesheets.
[**ListMyTimesheetActions**](TimesheetsApi.md#listmytimesheetactions) | **GET** /Me/Timesheets/{timesheetID}/Actions | Get available Actions for my Timesheet by ID with warnings about blocked actions in the meta tag of the response.
[**ListMyTimesheetActionsByDate**](TimesheetsApi.md#listmytimesheetactionsbydate) | **GET** /Me/Timesheets/{date}/Actions | Get available Actions for my Timesheet by date with warnings about blocked actions in the meta tag of the response.
[**ListMyTimesheetHistory**](TimesheetsApi.md#listmytimesheethistory) | **GET** /Me/Timesheets/{timesheetID}/History | Get my Timesheet&#39;s status history.
[**ListMyTimesheets**](TimesheetsApi.md#listmytimesheets) | **GET** /Me/Timesheets | Get my Timesheets.
[**ListTimesheetActions**](TimesheetsApi.md#listtimesheetactions) | **GET** /Timesheets/{timesheetID}/Actions | Get available Actions for a Timesheet with warnings about blocked actions in the meta tag of the response.
[**ListTimesheetHistory**](TimesheetsApi.md#listtimesheethistory) | **GET** /Timesheets/{timesheetID}/History | Get a Timesheet&#39;s status history.
[**ListTimesheetTimeEntries**](TimesheetsApi.md#listtimesheettimeentries) | **GET** /Timesheets/{timesheetID}/TimeEntries | Get a Timesheet&#39;s Time Entries.
[**ListTimesheetTimeOff**](TimesheetsApi.md#listtimesheettimeoff) | **GET** /Timesheets/{timesheetID}/TimeOff | Get a Timesheet&#39;s Time Off entries.
[**ListTimesheets**](TimesheetsApi.md#listtimesheets) | **GET** /Timesheets | Get Timesheets.
[**UpdateManagedTimeEntry**](TimesheetsApi.md#updatemanagedtimeentry) | **PATCH** /Manage/Timesheets/TimeEntries/{timeEntryID} | Update a managed Time Entry.
[**UpdateManagedTimeOff**](TimesheetsApi.md#updatemanagedtimeoff) | **PATCH** /Manage/Timesheets/TimeOff/{timeOffID} | Update a Time Off entry for a managed Timesheet.
[**UpdateManagedTimesheetStatus**](TimesheetsApi.md#updatemanagedtimesheetstatus) | **POST** /Manage/Timesheets/{timesheetID}/Actions | Apply an Action to a managed Timesheet.
[**UpdateMyTimesheetStatus**](TimesheetsApi.md#updatemytimesheetstatus) | **POST** /Me/Timesheets/{timesheetID}/Actions | Apply an Action to my Timesheet by ID.
[**UpdateMyTimesheetStatusByDate**](TimesheetsApi.md#updatemytimesheetstatusbydate) | **POST** /Me/Timesheets/{date}/Actions | Apply an Action to my Timesheet by date.
[**UpdateTimesheetStatus**](TimesheetsApi.md#updatetimesheetstatus) | **POST** /Timesheets/{timesheetID}/Actions | Apply an Action to a Timesheet.


<a name="createmanagedtimeentry"></a>
# **CreateManagedTimeEntry**
> ResponseBodyGetTimeEntryModelManage CreateManagedTimeEntry (PostTimeEntryModelManage model)

Create a managed Time Entry.

       Company requirements:    DCAA must be disabled.    Company must not have SubJobs enabled.    Company must have the TimesheetApprovals optional module.       User requirements:    Admin, or Manager with permissions to Override timesheets.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class CreateManagedTimeEntryExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TimesheetsApi();
            var model = new PostTimeEntryModelManage(); // PostTimeEntryModelManage |       Required

            try
            {
                // Create a managed Time Entry.
                ResponseBodyGetTimeEntryModelManage result = apiInstance.CreateManagedTimeEntry(model);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TimesheetsApi.CreateManagedTimeEntry: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **model** | [**PostTimeEntryModelManage**](PostTimeEntryModelManage.md)|       Required | 

### Return type

[**ResponseBodyGetTimeEntryModelManage**](ResponseBodyGetTimeEntryModelManage.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded, application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="createmanagedtimeoff"></a>
# **CreateManagedTimeOff**
> ResponseBodyGetTimeOffModelRoot CreateManagedTimeOff (PostTimeOffModelRoot model)

Create a Time Off entry for a managed Timesheet.

       User requirements:    Admin, or Manager with permissions to Override timesheets.       Company requirements:    Company must have the TimesheetApprovals optional module.    DCAA must be disabled.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class CreateManagedTimeOffExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TimesheetsApi();
            var model = new PostTimeOffModelRoot(); // PostTimeOffModelRoot |       Required

            try
            {
                // Create a Time Off entry for a managed Timesheet.
                ResponseBodyGetTimeOffModelRoot result = apiInstance.CreateManagedTimeOff(model);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TimesheetsApi.CreateManagedTimeOff: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **model** | [**PostTimeOffModelRoot**](PostTimeOffModelRoot.md)|       Required | 

### Return type

[**ResponseBodyGetTimeOffModelRoot**](ResponseBodyGetTimeOffModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded, application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="deletemanagedtimeentry"></a>
# **DeleteManagedTimeEntry**
> void DeleteManagedTimeEntry (string timeEntryID)

Delete a managed Time Entry.

       Company requirements:    DCAA must be disabled.    Company must not have SubJobs enabled.    Company must have the TimesheetApprovals optional module.       User requirements:    Admin, or Manager with permissions to Override timesheets.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class DeleteManagedTimeEntryExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TimesheetsApi();
            var timeEntryID = timeEntryID_example;  // string |       Required

            try
            {
                // Delete a managed Time Entry.
                apiInstance.DeleteManagedTimeEntry(timeEntryID);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TimesheetsApi.DeleteManagedTimeEntry: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **timeEntryID** | **string**|       Required | 

### Return type

void (empty response body)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="deletemanagedtimeoff"></a>
# **DeleteManagedTimeOff**
> void DeleteManagedTimeOff (string timeOffID)

Delete a Time Off entry for a managed Timesheet.

       User requirements:    Admin, or Manager with permissions to Override timesheets.       Company requirements:    Company must have the TimesheetApprovals optional module.    DCAA must be disabled.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class DeleteManagedTimeOffExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TimesheetsApi();
            var timeOffID = timeOffID_example;  // string |       Required

            try
            {
                // Delete a Time Off entry for a managed Timesheet.
                apiInstance.DeleteManagedTimeOff(timeOffID);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TimesheetsApi.DeleteManagedTimeOff: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **timeOffID** | **string**|       Required | 

### Return type

void (empty response body)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getmanagedtimeentry"></a>
# **GetManagedTimeEntry**
> ResponseBodyGetTimeEntryModelManage GetManagedTimeEntry (string timeEntryID)

Get managed Time Entry.

       User requirements:    Admin, or Manager with permissions to Lock timesheets.       Company requirements:    Company must have the TimesheetApprovals optional module.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class GetManagedTimeEntryExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TimesheetsApi();
            var timeEntryID = timeEntryID_example;  // string |       Required

            try
            {
                // Get managed Time Entry.
                ResponseBodyGetTimeEntryModelManage result = apiInstance.GetManagedTimeEntry(timeEntryID);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TimesheetsApi.GetManagedTimeEntry: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **timeEntryID** | **string**|       Required | 

### Return type

[**ResponseBodyGetTimeEntryModelManage**](ResponseBodyGetTimeEntryModelManage.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getmanagedtimeoff"></a>
# **GetManagedTimeOff**
> ResponseBodyGetTimeOffModelRoot GetManagedTimeOff (string timeOffID)

Get a Time Off entry for managed Timesheets.

       User requirements:    Admin, or Manager with permissions to Lock timesheets.       Company requirements:    Company must have the TimesheetApprovals optional module.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class GetManagedTimeOffExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TimesheetsApi();
            var timeOffID = timeOffID_example;  // string |       Required

            try
            {
                // Get a Time Off entry for managed Timesheets.
                ResponseBodyGetTimeOffModelRoot result = apiInstance.GetManagedTimeOff(timeOffID);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TimesheetsApi.GetManagedTimeOff: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **timeOffID** | **string**|       Required | 

### Return type

[**ResponseBodyGetTimeOffModelRoot**](ResponseBodyGetTimeOffModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getmanagedtimesheet"></a>
# **GetManagedTimesheet**
> ResponseBodyGetTimesheetModelManage GetManagedTimesheet (string timesheetID)

Get a managed Timesheet.

       Company requirements:    Company must have the TimesheetApprovals optional module.       User requirements:    Admin, or Manager with permissions to Lock timesheets.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class GetManagedTimesheetExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TimesheetsApi();
            var timesheetID = timesheetID_example;  // string |       Required

            try
            {
                // Get a managed Timesheet.
                ResponseBodyGetTimesheetModelManage result = apiInstance.GetManagedTimesheet(timesheetID);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TimesheetsApi.GetManagedTimesheet: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **timesheetID** | **string**|       Required | 

### Return type

[**ResponseBodyGetTimesheetModelManage**](ResponseBodyGetTimesheetModelManage.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getmanagedtimesheetbydatebyuser"></a>
# **GetManagedTimesheetByDateByUser**
> ResponseBodyGetTimesheetModelRoot GetManagedTimesheetByDateByUser (DateTime? date, string userID)

Get a User's Timesheet by date. If the timesheet does not exist it will be created.

       Company requirements:    Company must have the TimesheetApprovals optional module.       User requirements:    Admin, or Manager with permissions to Lock timesheets.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class GetManagedTimesheetByDateByUserExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TimesheetsApi();
            var date = 2013-10-20T19:20:30+01:00;  // DateTime? |       Required
            var userID = userID_example;  // string |       Required

            try
            {
                // Get a User's Timesheet by date. If the timesheet does not exist it will be created.
                ResponseBodyGetTimesheetModelRoot result = apiInstance.GetManagedTimesheetByDateByUser(date, userID);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TimesheetsApi.GetManagedTimesheetByDateByUser: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **date** | **DateTime?**|       Required | 
 **userID** | **string**|       Required | 

### Return type

[**ResponseBodyGetTimesheetModelRoot**](ResponseBodyGetTimesheetModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getmanagedtimesheettimeoff"></a>
# **GetManagedTimesheetTimeOff**
> ResponseBodyGetTimeOffModelRoot GetManagedTimesheetTimeOff (string timesheetID, string timeOffID)

Get a Time Off entry for a managed Timesheet.

       User requirements:    Admin, or Manager with permissions to Lock timesheets.       Company requirements:    Company must have the TimesheetApprovals optional module.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class GetManagedTimesheetTimeOffExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TimesheetsApi();
            var timesheetID = timesheetID_example;  // string |       Required
            var timeOffID = timeOffID_example;  // string |       Required

            try
            {
                // Get a Time Off entry for a managed Timesheet.
                ResponseBodyGetTimeOffModelRoot result = apiInstance.GetManagedTimesheetTimeOff(timesheetID, timeOffID);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TimesheetsApi.GetManagedTimesheetTimeOff: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **timesheetID** | **string**|       Required | 
 **timeOffID** | **string**|       Required | 

### Return type

[**ResponseBodyGetTimeOffModelRoot**](ResponseBodyGetTimeOffModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getmytimesheet"></a>
# **GetMyTimesheet**
> ResponseBodyGetTimesheetModelMe GetMyTimesheet (string timesheetID)

Get my Timesheet by ID.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class GetMyTimesheetExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TimesheetsApi();
            var timesheetID = timesheetID_example;  // string |       Required

            try
            {
                // Get my Timesheet by ID.
                ResponseBodyGetTimesheetModelMe result = apiInstance.GetMyTimesheet(timesheetID);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TimesheetsApi.GetMyTimesheet: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **timesheetID** | **string**|       Required | 

### Return type

[**ResponseBodyGetTimesheetModelMe**](ResponseBodyGetTimesheetModelMe.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getmytimesheetbydate"></a>
# **GetMyTimesheetByDate**
> ResponseBodyGetTimesheetModelMe GetMyTimesheetByDate (DateTime? date)

Get my Timesheet by date.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class GetMyTimesheetByDateExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TimesheetsApi();
            var date = 2013-10-20T19:20:30+01:00;  // DateTime? |       Required

            try
            {
                // Get my Timesheet by date.
                ResponseBodyGetTimesheetModelMe result = apiInstance.GetMyTimesheetByDate(date);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TimesheetsApi.GetMyTimesheetByDate: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **date** | **DateTime?**|       Required | 

### Return type

[**ResponseBodyGetTimesheetModelMe**](ResponseBodyGetTimesheetModelMe.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="gettimesheet"></a>
# **GetTimesheet**
> ResponseBodyGetTimesheetModelRoot GetTimesheet (string timesheetID)

Get a Timesheet.

       User requirements:    Admin, or Manager with permissions to Review timesheets.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class GetTimesheetExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TimesheetsApi();
            var timesheetID = timesheetID_example;  // string |       Required

            try
            {
                // Get a Timesheet.
                ResponseBodyGetTimesheetModelRoot result = apiInstance.GetTimesheet(timesheetID);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TimesheetsApi.GetTimesheet: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **timesheetID** | **string**|       Required | 

### Return type

[**ResponseBodyGetTimesheetModelRoot**](ResponseBodyGetTimesheetModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="gettimesheetbydatebyuser"></a>
# **GetTimesheetByDateByUser**
> ResponseBodyGetTimesheetModelRoot GetTimesheetByDateByUser (DateTime? date, string userID)

Get a User's Timesheet by date.  If the timesheet does not exist it will be created.

       User requirements:    Admin, or Manager with permissions to Review timesheets.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class GetTimesheetByDateByUserExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TimesheetsApi();
            var date = 2013-10-20T19:20:30+01:00;  // DateTime? |       Required
            var userID = userID_example;  // string |       Required

            try
            {
                // Get a User's Timesheet by date.  If the timesheet does not exist it will be created.
                ResponseBodyGetTimesheetModelRoot result = apiInstance.GetTimesheetByDateByUser(date, userID);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TimesheetsApi.GetTimesheetByDateByUser: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **date** | **DateTime?**|       Required | 
 **userID** | **string**|       Required | 

### Return type

[**ResponseBodyGetTimesheetModelRoot**](ResponseBodyGetTimesheetModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="listmanagedtimeentries"></a>
# **ListManagedTimeEntries**
> ResponseBodyListGetTimeEntryModelManage ListManagedTimeEntries (List<string> ID = null, List<string> jobID = null, List<string> taskID = null, List<string> userID = null, string startDate = null, string endDate = null, List<string> entryDate = null, int? limit = null, int? offset = null)

Get managed Time Entries.

       User requirements:    Admin, or Manager with permissions to Lock timesheets.       Company requirements:    Company must have the TimesheetApprovals optional module.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ListManagedTimeEntriesExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TimesheetsApi();
            var ID = new List<string>(); // List<string> |  (optional) 
            var jobID = new List<string>(); // List<string> |  (optional) 
            var taskID = new List<string>(); // List<string> |  (optional) 
            var userID = new List<string>(); // List<string> |  (optional) 
            var startDate = startDate_example;  // string |  (optional) 
            var endDate = endDate_example;  // string |  (optional) 
            var entryDate = new List<string>(); // List<string> |  (optional) 
            var limit = 56;  // int? | Up to 500 (optional) 
            var offset = 56;  // int? |  (optional) 

            try
            {
                // Get managed Time Entries.
                ResponseBodyListGetTimeEntryModelManage result = apiInstance.ListManagedTimeEntries(ID, jobID, taskID, userID, startDate, endDate, entryDate, limit, offset);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TimesheetsApi.ListManagedTimeEntries: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ID** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **jobID** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **taskID** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **userID** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **startDate** | **string**|  | [optional] 
 **endDate** | **string**|  | [optional] 
 **entryDate** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **limit** | **int?**| Up to 500 | [optional] 
 **offset** | **int?**|  | [optional] 

### Return type

[**ResponseBodyListGetTimeEntryModelManage**](ResponseBodyListGetTimeEntryModelManage.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="listmanagedtimeoff"></a>
# **ListManagedTimeOff**
> ResponseBodyListGetTimeOffModelRoot ListManagedTimeOff (List<string> ID = null, List<string> timeOffTypeID = null, List<string> userID = null, string fromDate = null, string toDate = null, int? limit = null, int? offset = null)

Get Time Off entries for managed Timesheets.

       User requirements:    Admin, or Manager with permissions to Lock timesheets.       Company requirements:    Company must have the TimesheetApprovals optional module.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ListManagedTimeOffExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TimesheetsApi();
            var ID = new List<string>(); // List<string> |  (optional) 
            var timeOffTypeID = new List<string>(); // List<string> |  (optional) 
            var userID = new List<string>(); // List<string> |  (optional) 
            var fromDate = fromDate_example;  // string |  (optional) 
            var toDate = toDate_example;  // string |  (optional) 
            var limit = 56;  // int? |  (optional) 
            var offset = 56;  // int? |  (optional) 

            try
            {
                // Get Time Off entries for managed Timesheets.
                ResponseBodyListGetTimeOffModelRoot result = apiInstance.ListManagedTimeOff(ID, timeOffTypeID, userID, fromDate, toDate, limit, offset);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TimesheetsApi.ListManagedTimeOff: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ID** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **timeOffTypeID** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **userID** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **fromDate** | **string**|  | [optional] 
 **toDate** | **string**|  | [optional] 
 **limit** | **int?**|  | [optional] 
 **offset** | **int?**|  | [optional] 

### Return type

[**ResponseBodyListGetTimeOffModelRoot**](ResponseBodyListGetTimeOffModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="listmanagedtimesheetactions"></a>
# **ListManagedTimesheetActions**
> ResponseBodyListGetActionModelTimesheetActionManage ListManagedTimesheetActions (string timesheetID)

Get available Actions for a managed Timesheet with warnings about blocked actions in the meta tag of the response.

       Company requirements:    Company must have the TimesheetApprovals optional module.       User requirements:    Admin, or Manager with permissions to Lock timesheets.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ListManagedTimesheetActionsExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TimesheetsApi();
            var timesheetID = timesheetID_example;  // string |       Required

            try
            {
                // Get available Actions for a managed Timesheet with warnings about blocked actions in the meta tag of the response.
                ResponseBodyListGetActionModelTimesheetActionManage result = apiInstance.ListManagedTimesheetActions(timesheetID);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TimesheetsApi.ListManagedTimesheetActions: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **timesheetID** | **string**|       Required | 

### Return type

[**ResponseBodyListGetActionModelTimesheetActionManage**](ResponseBodyListGetActionModelTimesheetActionManage.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="listmanagedtimesheethistory"></a>
# **ListManagedTimesheetHistory**
> ResponseBodyListGetTimesheetHistoryModelManage ListManagedTimesheetHistory (string timesheetID)

Get a managed Timesheet's status history.

       Company requirements:    Company must have the TimesheetApprovals optional module.       User requirements:    Admin, or Manager with permissions to Lock timesheets.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ListManagedTimesheetHistoryExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TimesheetsApi();
            var timesheetID = timesheetID_example;  // string |       Required

            try
            {
                // Get a managed Timesheet's status history.
                ResponseBodyListGetTimesheetHistoryModelManage result = apiInstance.ListManagedTimesheetHistory(timesheetID);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TimesheetsApi.ListManagedTimesheetHistory: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **timesheetID** | **string**|       Required | 

### Return type

[**ResponseBodyListGetTimesheetHistoryModelManage**](ResponseBodyListGetTimesheetHistoryModelManage.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="listmanagedtimesheettimeoff"></a>
# **ListManagedTimesheetTimeOff**
> ResponseBodyListGetTimeOffModelRoot ListManagedTimesheetTimeOff (string timesheetID, List<string> ID = null, List<string> timeOffTypeID = null, int? limit = null, int? offset = null)

Get Time Off entries for a managed Timesheet.

       User requirements:    Admin, or Manager with permissions to Lock timesheets.       Company requirements:    Company must have the TimesheetApprovals optional module.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ListManagedTimesheetTimeOffExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TimesheetsApi();
            var timesheetID = timesheetID_example;  // string |       Required
            var ID = new List<string>(); // List<string> |  (optional) 
            var timeOffTypeID = new List<string>(); // List<string> |  (optional) 
            var limit = 56;  // int? |  (optional) 
            var offset = 56;  // int? |  (optional) 

            try
            {
                // Get Time Off entries for a managed Timesheet.
                ResponseBodyListGetTimeOffModelRoot result = apiInstance.ListManagedTimesheetTimeOff(timesheetID, ID, timeOffTypeID, limit, offset);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TimesheetsApi.ListManagedTimesheetTimeOff: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **timesheetID** | **string**|       Required | 
 **ID** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **timeOffTypeID** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **limit** | **int?**|  | [optional] 
 **offset** | **int?**|  | [optional] 

### Return type

[**ResponseBodyListGetTimeOffModelRoot**](ResponseBodyListGetTimeOffModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="listmanagedtimesheets"></a>
# **ListManagedTimesheets**
> ResponseBodyListGetTimesheetModelManage ListManagedTimesheets (List<string> status = null, List<string> userID = null, List<string> divisionID = null, List<string> employmentTypeID = null, string fromDate = null, string toDate = null, int? limit = null, int? offset = null)

Get managed Timesheets.

       Company requirements:    Company must have the TimesheetApprovals optional module.       User requirements:    Admin, or Manager with permissions to Lock timesheets.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ListManagedTimesheetsExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TimesheetsApi();
            var status = new List<string>(); // List<string> |  (optional) 
            var userID = new List<string>(); // List<string> |  (optional) 
            var divisionID = new List<string>(); // List<string> |  (optional) 
            var employmentTypeID = new List<string>(); // List<string> |  (optional) 
            var fromDate = fromDate_example;  // string |  (optional) 
            var toDate = toDate_example;  // string |  (optional) 
            var limit = 56;  // int? |  (optional) 
            var offset = 56;  // int? |  (optional) 

            try
            {
                // Get managed Timesheets.
                ResponseBodyListGetTimesheetModelManage result = apiInstance.ListManagedTimesheets(status, userID, divisionID, employmentTypeID, fromDate, toDate, limit, offset);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TimesheetsApi.ListManagedTimesheets: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **status** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **userID** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **divisionID** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **employmentTypeID** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **fromDate** | **string**|  | [optional] 
 **toDate** | **string**|  | [optional] 
 **limit** | **int?**|  | [optional] 
 **offset** | **int?**|  | [optional] 

### Return type

[**ResponseBodyListGetTimesheetModelManage**](ResponseBodyListGetTimesheetModelManage.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="listmytimesheetactions"></a>
# **ListMyTimesheetActions**
> ResponseBodyListGetActionModelTimesheetActionMe ListMyTimesheetActions (string timesheetID)

Get available Actions for my Timesheet by ID with warnings about blocked actions in the meta tag of the response.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ListMyTimesheetActionsExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TimesheetsApi();
            var timesheetID = timesheetID_example;  // string |       Required

            try
            {
                // Get available Actions for my Timesheet by ID with warnings about blocked actions in the meta tag of the response.
                ResponseBodyListGetActionModelTimesheetActionMe result = apiInstance.ListMyTimesheetActions(timesheetID);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TimesheetsApi.ListMyTimesheetActions: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **timesheetID** | **string**|       Required | 

### Return type

[**ResponseBodyListGetActionModelTimesheetActionMe**](ResponseBodyListGetActionModelTimesheetActionMe.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="listmytimesheetactionsbydate"></a>
# **ListMyTimesheetActionsByDate**
> ResponseBodyListGetActionModelTimesheetActionMe ListMyTimesheetActionsByDate (DateTime? date)

Get available Actions for my Timesheet by date with warnings about blocked actions in the meta tag of the response.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ListMyTimesheetActionsByDateExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TimesheetsApi();
            var date = 2013-10-20T19:20:30+01:00;  // DateTime? |       Required

            try
            {
                // Get available Actions for my Timesheet by date with warnings about blocked actions in the meta tag of the response.
                ResponseBodyListGetActionModelTimesheetActionMe result = apiInstance.ListMyTimesheetActionsByDate(date);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TimesheetsApi.ListMyTimesheetActionsByDate: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **date** | **DateTime?**|       Required | 

### Return type

[**ResponseBodyListGetActionModelTimesheetActionMe**](ResponseBodyListGetActionModelTimesheetActionMe.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="listmytimesheethistory"></a>
# **ListMyTimesheetHistory**
> ResponseBodyListGetTimesheetHistoryModelMe ListMyTimesheetHistory (string timesheetID)

Get my Timesheet's status history.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ListMyTimesheetHistoryExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TimesheetsApi();
            var timesheetID = timesheetID_example;  // string |       Required

            try
            {
                // Get my Timesheet's status history.
                ResponseBodyListGetTimesheetHistoryModelMe result = apiInstance.ListMyTimesheetHistory(timesheetID);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TimesheetsApi.ListMyTimesheetHistory: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **timesheetID** | **string**|       Required | 

### Return type

[**ResponseBodyListGetTimesheetHistoryModelMe**](ResponseBodyListGetTimesheetHistoryModelMe.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="listmytimesheets"></a>
# **ListMyTimesheets**
> ResponseBodyListGetTimesheetModelMe ListMyTimesheets (List<string> status = null, string fromDate = null, string toDate = null, int? limit = null, int? offset = null)

Get my Timesheets.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ListMyTimesheetsExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TimesheetsApi();
            var status = new List<string>(); // List<string> |  (optional) 
            var fromDate = fromDate_example;  // string |  (optional) 
            var toDate = toDate_example;  // string |  (optional) 
            var limit = 56;  // int? |  (optional) 
            var offset = 56;  // int? |  (optional) 

            try
            {
                // Get my Timesheets.
                ResponseBodyListGetTimesheetModelMe result = apiInstance.ListMyTimesheets(status, fromDate, toDate, limit, offset);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TimesheetsApi.ListMyTimesheets: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **status** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **fromDate** | **string**|  | [optional] 
 **toDate** | **string**|  | [optional] 
 **limit** | **int?**|  | [optional] 
 **offset** | **int?**|  | [optional] 

### Return type

[**ResponseBodyListGetTimesheetModelMe**](ResponseBodyListGetTimesheetModelMe.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="listtimesheetactions"></a>
# **ListTimesheetActions**
> ResponseBodyListGetActionModelTimesheetActionRoot ListTimesheetActions (string timesheetID)

Get available Actions for a Timesheet with warnings about blocked actions in the meta tag of the response.

       User requirements:    Admin, or Manager with permissions to Review timesheets.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ListTimesheetActionsExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TimesheetsApi();
            var timesheetID = timesheetID_example;  // string |       Required

            try
            {
                // Get available Actions for a Timesheet with warnings about blocked actions in the meta tag of the response.
                ResponseBodyListGetActionModelTimesheetActionRoot result = apiInstance.ListTimesheetActions(timesheetID);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TimesheetsApi.ListTimesheetActions: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **timesheetID** | **string**|       Required | 

### Return type

[**ResponseBodyListGetActionModelTimesheetActionRoot**](ResponseBodyListGetActionModelTimesheetActionRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="listtimesheethistory"></a>
# **ListTimesheetHistory**
> ResponseBodyListGetTimesheetHistoryModelRoot ListTimesheetHistory (string timesheetID)

Get a Timesheet's status history.

       User requirements:    Admin, or Manager with permissions to Review timesheets.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ListTimesheetHistoryExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TimesheetsApi();
            var timesheetID = timesheetID_example;  // string |       Required

            try
            {
                // Get a Timesheet's status history.
                ResponseBodyListGetTimesheetHistoryModelRoot result = apiInstance.ListTimesheetHistory(timesheetID);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TimesheetsApi.ListTimesheetHistory: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **timesheetID** | **string**|       Required | 

### Return type

[**ResponseBodyListGetTimesheetHistoryModelRoot**](ResponseBodyListGetTimesheetHistoryModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="listtimesheettimeentries"></a>
# **ListTimesheetTimeEntries**
> ResponseBodyListGetTimeEntryModelRoot ListTimesheetTimeEntries (string timesheetID, List<string> jobID = null)

Get a Timesheet's Time Entries.

       User requirements:    Admin, or Manager with permissions to Review timesheets.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ListTimesheetTimeEntriesExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TimesheetsApi();
            var timesheetID = timesheetID_example;  // string |       Required
            var jobID = new List<string>(); // List<string> |  (optional) 

            try
            {
                // Get a Timesheet's Time Entries.
                ResponseBodyListGetTimeEntryModelRoot result = apiInstance.ListTimesheetTimeEntries(timesheetID, jobID);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TimesheetsApi.ListTimesheetTimeEntries: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **timesheetID** | **string**|       Required | 
 **jobID** | [**List&lt;string&gt;**](string.md)|  | [optional] 

### Return type

[**ResponseBodyListGetTimeEntryModelRoot**](ResponseBodyListGetTimeEntryModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="listtimesheettimeoff"></a>
# **ListTimesheetTimeOff**
> ResponseBodyListGetTimeOffModelRoot ListTimesheetTimeOff (string timesheetID, List<string> ID = null, List<string> timeOffTypeID = null, int? limit = null, int? offset = null)

Get a Timesheet's Time Off entries.

       User requirements:    Admin, or Manager with permissions to Review timesheets.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ListTimesheetTimeOffExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TimesheetsApi();
            var timesheetID = timesheetID_example;  // string |       Required
            var ID = new List<string>(); // List<string> |  (optional) 
            var timeOffTypeID = new List<string>(); // List<string> |  (optional) 
            var limit = 56;  // int? |  (optional) 
            var offset = 56;  // int? |  (optional) 

            try
            {
                // Get a Timesheet's Time Off entries.
                ResponseBodyListGetTimeOffModelRoot result = apiInstance.ListTimesheetTimeOff(timesheetID, ID, timeOffTypeID, limit, offset);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TimesheetsApi.ListTimesheetTimeOff: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **timesheetID** | **string**|       Required | 
 **ID** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **timeOffTypeID** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **limit** | **int?**|  | [optional] 
 **offset** | **int?**|  | [optional] 

### Return type

[**ResponseBodyListGetTimeOffModelRoot**](ResponseBodyListGetTimeOffModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="listtimesheets"></a>
# **ListTimesheets**
> ResponseBodyListGetTimesheetModelRoot ListTimesheets (List<string> status = null, List<string> userID = null, List<string> divisionID = null, List<string> employmentTypeID = null, string fromDate = null, string toDate = null, int? limit = null, int? offset = null)

Get Timesheets.

       User requirements:    Admin, or Manager with permissions to Review timesheets.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ListTimesheetsExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TimesheetsApi();
            var status = new List<string>(); // List<string> |  (optional) 
            var userID = new List<string>(); // List<string> |  (optional) 
            var divisionID = new List<string>(); // List<string> |  (optional) 
            var employmentTypeID = new List<string>(); // List<string> |  (optional) 
            var fromDate = fromDate_example;  // string |  (optional) 
            var toDate = toDate_example;  // string |  (optional) 
            var limit = 56;  // int? |  (optional) 
            var offset = 56;  // int? |  (optional) 

            try
            {
                // Get Timesheets.
                ResponseBodyListGetTimesheetModelRoot result = apiInstance.ListTimesheets(status, userID, divisionID, employmentTypeID, fromDate, toDate, limit, offset);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TimesheetsApi.ListTimesheets: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **status** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **userID** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **divisionID** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **employmentTypeID** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **fromDate** | **string**|  | [optional] 
 **toDate** | **string**|  | [optional] 
 **limit** | **int?**|  | [optional] 
 **offset** | **int?**|  | [optional] 

### Return type

[**ResponseBodyListGetTimesheetModelRoot**](ResponseBodyListGetTimesheetModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="updatemanagedtimeentry"></a>
# **UpdateManagedTimeEntry**
> ResponseBodyGetTimeEntryModelManage UpdateManagedTimeEntry (string timeEntryID, PatchTimeEntryModelManage model)

Update a managed Time Entry.

       Company requirements:    DCAA must be disabled.    Company must not have SubJobs enabled.    Company must have the TimesheetApprovals optional module.       User requirements:    Admin, or Manager with permissions to Override timesheets.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class UpdateManagedTimeEntryExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TimesheetsApi();
            var timeEntryID = timeEntryID_example;  // string |       Required
            var model = new PatchTimeEntryModelManage(); // PatchTimeEntryModelManage |       Required

            try
            {
                // Update a managed Time Entry.
                ResponseBodyGetTimeEntryModelManage result = apiInstance.UpdateManagedTimeEntry(timeEntryID, model);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TimesheetsApi.UpdateManagedTimeEntry: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **timeEntryID** | **string**|       Required | 
 **model** | [**PatchTimeEntryModelManage**](PatchTimeEntryModelManage.md)|       Required | 

### Return type

[**ResponseBodyGetTimeEntryModelManage**](ResponseBodyGetTimeEntryModelManage.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded, application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="updatemanagedtimeoff"></a>
# **UpdateManagedTimeOff**
> ResponseBodyGetTimeOffModelRoot UpdateManagedTimeOff (string timeOffID, PatchTimeOffModelRoot model)

Update a Time Off entry for a managed Timesheet.

       User requirements:    Admin, or Manager with permissions to Override timesheets.       Company requirements:    Company must have the TimesheetApprovals optional module.    DCAA must be disabled.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class UpdateManagedTimeOffExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TimesheetsApi();
            var timeOffID = timeOffID_example;  // string |       Required
            var model = new PatchTimeOffModelRoot(); // PatchTimeOffModelRoot |       Required

            try
            {
                // Update a Time Off entry for a managed Timesheet.
                ResponseBodyGetTimeOffModelRoot result = apiInstance.UpdateManagedTimeOff(timeOffID, model);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TimesheetsApi.UpdateManagedTimeOff: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **timeOffID** | **string**|       Required | 
 **model** | [**PatchTimeOffModelRoot**](PatchTimeOffModelRoot.md)|       Required | 

### Return type

[**ResponseBodyGetTimeOffModelRoot**](ResponseBodyGetTimeOffModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded, application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="updatemanagedtimesheetstatus"></a>
# **UpdateManagedTimesheetStatus**
> ResponseBodyGetTimesheetModelManage UpdateManagedTimesheetStatus (string timesheetID, PostTimesheetActionModelManage model)

Apply an Action to a managed Timesheet.

       User requirements:    Admin, or Manager with permissions to Lock timesheets.       Company requirements:    Company must have the TimesheetApprovals optional module.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class UpdateManagedTimesheetStatusExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TimesheetsApi();
            var timesheetID = timesheetID_example;  // string |       Required
            var model = new PostTimesheetActionModelManage(); // PostTimesheetActionModelManage |       Required

            try
            {
                // Apply an Action to a managed Timesheet.
                ResponseBodyGetTimesheetModelManage result = apiInstance.UpdateManagedTimesheetStatus(timesheetID, model);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TimesheetsApi.UpdateManagedTimesheetStatus: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **timesheetID** | **string**|       Required | 
 **model** | [**PostTimesheetActionModelManage**](PostTimesheetActionModelManage.md)|       Required | 

### Return type

[**ResponseBodyGetTimesheetModelManage**](ResponseBodyGetTimesheetModelManage.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded, application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="updatemytimesheetstatus"></a>
# **UpdateMyTimesheetStatus**
> ResponseBodyGetTimesheetModelMe UpdateMyTimesheetStatus (string timesheetID, PostTimesheetActionModelMe model)

Apply an Action to my Timesheet by ID.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class UpdateMyTimesheetStatusExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TimesheetsApi();
            var timesheetID = timesheetID_example;  // string |       Required
            var model = new PostTimesheetActionModelMe(); // PostTimesheetActionModelMe |       Required

            try
            {
                // Apply an Action to my Timesheet by ID.
                ResponseBodyGetTimesheetModelMe result = apiInstance.UpdateMyTimesheetStatus(timesheetID, model);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TimesheetsApi.UpdateMyTimesheetStatus: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **timesheetID** | **string**|       Required | 
 **model** | [**PostTimesheetActionModelMe**](PostTimesheetActionModelMe.md)|       Required | 

### Return type

[**ResponseBodyGetTimesheetModelMe**](ResponseBodyGetTimesheetModelMe.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded, application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="updatemytimesheetstatusbydate"></a>
# **UpdateMyTimesheetStatusByDate**
> ResponseBodyGetTimesheetModelMe UpdateMyTimesheetStatusByDate (DateTime? date, PostTimesheetActionModelMe model)

Apply an Action to my Timesheet by date.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class UpdateMyTimesheetStatusByDateExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TimesheetsApi();
            var date = 2013-10-20T19:20:30+01:00;  // DateTime? |       Required
            var model = new PostTimesheetActionModelMe(); // PostTimesheetActionModelMe |       Required

            try
            {
                // Apply an Action to my Timesheet by date.
                ResponseBodyGetTimesheetModelMe result = apiInstance.UpdateMyTimesheetStatusByDate(date, model);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TimesheetsApi.UpdateMyTimesheetStatusByDate: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **date** | **DateTime?**|       Required | 
 **model** | [**PostTimesheetActionModelMe**](PostTimesheetActionModelMe.md)|       Required | 

### Return type

[**ResponseBodyGetTimesheetModelMe**](ResponseBodyGetTimesheetModelMe.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded, application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="updatetimesheetstatus"></a>
# **UpdateTimesheetStatus**
> ResponseBodyGetTimesheetModelRoot UpdateTimesheetStatus (string timesheetID, PostTimesheetActionModelRoot model)

Apply an Action to a Timesheet.

       User requirements:    Admin, or Manager with permissions to Lock timesheets.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class UpdateTimesheetStatusExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TimesheetsApi();
            var timesheetID = timesheetID_example;  // string |       Required
            var model = new PostTimesheetActionModelRoot(); // PostTimesheetActionModelRoot |       Required

            try
            {
                // Apply an Action to a Timesheet.
                ResponseBodyGetTimesheetModelRoot result = apiInstance.UpdateTimesheetStatus(timesheetID, model);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TimesheetsApi.UpdateTimesheetStatus: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **timesheetID** | **string**|       Required | 
 **model** | [**PostTimesheetActionModelRoot**](PostTimesheetActionModelRoot.md)|       Required | 

### Return type

[**ResponseBodyGetTimesheetModelRoot**](ResponseBodyGetTimesheetModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded, application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

