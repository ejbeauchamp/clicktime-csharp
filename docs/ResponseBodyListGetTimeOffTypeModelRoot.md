# IO.Swagger.Model.ResponseBodyListGetTimeOffTypeModelRoot
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**List&lt;GetTimeOffTypeModelRoot&gt;**](GetTimeOffTypeModelRoot.md) |  | [optional] 
**errors** | [**List&lt;APIError&gt;**](APIError.md) |  | [optional] 
**jsonapi** | **Dictionary&lt;string, Object&gt;** |  | [optional] 
**meta** | **Dictionary&lt;string, Object&gt;** |  | [optional] 
**page** | [**Page**](Page.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

