# IO.Swagger.Api.JobApprovalsApi

All URIs are relative to *https://api.clicktime.com/V2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**JobApprovalsGetManageJobApprovalsTimeEntries**](JobApprovalsApi.md#jobapprovalsgetmanagejobapprovalstimeentries) | **GET** /Manage/JobApprovals/{jobApprovalID}/TimeEntries | Get a Job Approval&#39;s Time Entries.


<a name="jobapprovalsgetmanagejobapprovalstimeentries"></a>
# **JobApprovalsGetManageJobApprovalsTimeEntries**
> ResponseBodyListGetReportTimeModelManage JobApprovalsGetManageJobApprovalsTimeEntries (string jobApprovalID)

Get a Job Approval's Time Entries.

       User requirements:    Admin, or Manager with permissions to Lock timesheets.       Company requirements:    Company must have the TimesheetApprovals optional module.    Job Approvals: enabled

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class JobApprovalsGetManageJobApprovalsTimeEntriesExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new JobApprovalsApi();
            var jobApprovalID = jobApprovalID_example;  // string |       Required

            try
            {
                // Get a Job Approval's Time Entries.
                ResponseBodyListGetReportTimeModelManage result = apiInstance.JobApprovalsGetManageJobApprovalsTimeEntries(jobApprovalID);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling JobApprovalsApi.JobApprovalsGetManageJobApprovalsTimeEntries: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **jobApprovalID** | **string**|       Required | 

### Return type

[**ResponseBodyListGetReportTimeModelManage**](ResponseBodyListGetReportTimeModelManage.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

