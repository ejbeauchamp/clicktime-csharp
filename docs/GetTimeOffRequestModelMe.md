# IO.Swagger.Model.GetTimeOffRequestModelMe
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Actions** | [**List&lt;GetActionModelRequestAction&gt;**](GetActionModelRequestAction.md) |  | [optional] 
**ApprovalByUser** | [**GetBasicUserModelApprovalByUserGetTimeOffRequestModelMe**](GetBasicUserModelApprovalByUserGetTimeOffRequestModelMe.md) |        Request requirements:    Verbose&#x3D;true | [optional] 
**ApprovalByUserID** | **string** |  | [optional] 
**CreatedDate** | **string** |  | [optional] 
**Dates** | [**List&lt;GetTimeOffRequestDateModelDatesGetTimeOffRequestModelMe&gt;**](GetTimeOffRequestDateModelDatesGetTimeOffRequestModelMe.md) |  | [optional] 
**History** | [**List&lt;GetTimeOffRequestHistoryModelHistoryGetTimeOffRequestModelMe&gt;**](GetTimeOffRequestHistoryModelHistoryGetTimeOffRequestModelMe.md) |        Request requirements:    Verbose&#x3D;true | [optional] 
**ID** | **string** |  | [optional] 
**LegacyID** | **string** |        Request requirements:    CTLegacyScramble&#x3D;true | [optional] 
**Notes** | **string** |  | [optional] 
**RequestedByUser** | [**GetBasicUserModelRequestedByUserGetTimeOffRequestModelMe**](GetBasicUserModelRequestedByUserGetTimeOffRequestModelMe.md) |        Request requirements:    Verbose&#x3D;true | [optional] 
**RequestedByUserID** | **string** |  | [optional] 
**Status** | **string** |  | [optional] 
**TimeOffType** | [**GetBasicTimeOffTypeModelTimeOffTypeGetTimeOffRequestModelMe**](GetBasicTimeOffTypeModelTimeOffTypeGetTimeOffRequestModelMe.md) |        Request requirements:    Verbose&#x3D;true | [optional] 
**TimeOffTypeID** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

