# IO.Swagger.Model.GetTimesheetHistoryModel
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Action** | **string** |  | [optional] 
**ActionByUser** | [**GetBasicUserModel**](GetBasicUserModel.md) |  | [optional] 
**ActionByUserID** | **string** |  | [optional] 
**ActionByUserName** | **string** |  | [optional] 
**Comment** | **string** |  | [optional] 
**Date** | **string** |  | [optional] 
**Job** | [**GetBasicJobModel**](GetBasicJobModel.md) |  | [optional] 
**JobID** | **string** |  | [optional] 
**JobStatus** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

