# IO.Swagger.Model.GetTimesheetModel
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Actions** | [**List&lt;GetActionModelTimesheetAction&gt;**](GetActionModelTimesheetAction.md) | The actions listed in this endpoint’s response are generalized. To obtain  a more explicit set of available actions for a particular Timesheet ID, it  is recommended to use the appropriate /Timesheets/ID/Actions endpoint. | [optional] 
**ApprovedBy** | **string** |  | [optional] 
**ApprovedByUserID** | **string** |  | [optional] 
**ApprovedDate** | **string** |  | [optional] 
**Comment** | **string** |  | [optional] 
**DayTotals** | [**List&lt;GetDayTotalModel&gt;**](GetDayTotalModel.md) |  | [optional] 
**EndDate** | **string** |  | [optional] 
**ID** | **string** |  | [optional] 
**LegacyID** | **string** |  | [optional] 
**RejectedBy** | **string** |  | [optional] 
**RejectedByUserID** | **string** |  | [optional] 
**RejectedDate** | **string** |  | [optional] 
**StartDate** | **string** |  | [optional] 
**Status** | **string** |  | [optional] 
**SubmittedBy** | **string** |  | [optional] 
**SubmittedByUserID** | **string** |  | [optional] 
**SubmittedDate** | **string** |  | [optional] 
**User** | [**GetBasicUserModel**](GetBasicUserModel.md) |  | [optional] 
**UserID** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

