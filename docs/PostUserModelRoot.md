# IO.Swagger.Model.PostUserModelRoot
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**AccountingPackageID** | **string** |  | [optional] 
**AllowIncompleteTimesheetSubmission** | **bool?** |        Company requirements:    Timesheet Approval Permission | [optional] 
**BillingRate** | **double?** |        User requirements:    Security Level(s): Admin, Manager (Add/Edit Billingrates)       Company requirements:    Billing rate model(s): User, User x Job, User x Client | [optional] 
**CostRate** | **double?** |        User requirements:    Security Level(s): Admin, Manager (Add/Edit Costs) | [optional] 
**CustomFields** | **Dictionary&lt;string, Object&gt;** |        Company requirements:    Custom Fields must be configured for Users. | [optional] 
**DefaultExpenseTypeID** | **string** |        Company requirements:    Expense Permission | [optional] 
**DefaultPaymentTypeID** | **string** |        Company requirements:    Expense Permission | [optional] 
**DefaultTaskID** | **string** |  | [optional] 
**DivisionID** | **string** |  | [optional] 
**Email** | **string** |  | 
**EmployeeNumber** | **string** |  | [optional] 
**EmploymentTypeID** | **string** |  | [optional] 
**EnableBreakTime** | **bool?** |  | [optional] 
**EndDate** | **string** |  | [optional] 
**ExpenseApproverID** | **string** | SubjectToExpenseApproval must be true in order to set ExpenseApproverID.       Company requirements:    Expense Permission | [optional] 
**GDPRConsentStatus** | **string** |  | [optional] 
**IsActive** | **bool?** |  | [optional] 
**MinimumTimeHours** | **double?** |  | [optional] 
**MinimumTimePeriod** | **string** |  | [optional] 
**Name** | **string** |  | 
**Notes** | **string** |  | [optional] 
**PayrollType** | **string** |  | [optional] 
**PreferredTimeEntryView** | **string** |  | [optional] 
**PreferredTimeFormat** | **string** |  | [optional] 
**RequireComments** | **bool?** |  | [optional] 
**RequireStartEndTime** | **bool?** |  | [optional] 
**RequireStopwatch** | **bool?** |  | [optional] 
**Role** | **string** |  | [optional] 
**SkipWeekend** | **bool?** |  | [optional] 
**StartDate** | **string** |  | [optional] 
**SubjectToExpenseApproval** | **bool?** |        Company requirements:    Expense Permission | [optional] 
**SubjectToTimesheetApproval** | **bool?** |        Company requirements:    Timesheet Approval Permission | [optional] 
**SubjectToTimesheetCompletion** | **bool?** |  | [optional] 
**TimeOffApproverID** | **string** |        Company requirements:    Time Off Management Permission | [optional] 
**TimesheetApproverID** | **string** | SubjectToTimesheetApproval must be true in order to set TimesheetApproverID.       Company requirements:    Timesheet Approval Permission | [optional] 
**UseCompanyBillingRate** | **bool?** |        User requirements:    Security Level(s): Admin, Manager (Add/Edit Billingrates)       Company requirements:    Billing rate model(s): User, User x Job, User x Client | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

