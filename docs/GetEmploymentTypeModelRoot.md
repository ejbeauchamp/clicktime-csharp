# IO.Swagger.Model.GetEmploymentTypeModelRoot
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**AccountingPackageID** | **string** |        User requirements:    Security Level(s): Admin, Manager (View Employment Types) | [optional] 
**HolidayTypeIDs** | **List&lt;string&gt;** |        Request requirements:    Verbose&#x3D;true       User requirements:    Security Level(s): Admin, Manager (View Employment Types)       Company requirements:    Leave Type Restrictions: By Employment Type | [optional] 
**ID** | **string** |  | [optional] 
**IsActive** | **bool?** |  | [optional] 
**LegacyID** | **string** |        Request requirements:    CTLegacyScramble&#x3D;true | [optional] 
**Name** | **string** |  | [optional] 
**Notes** | **string** |        User requirements:    Security Level(s): Admin, Manager (View Employment Types) | [optional] 
**TimeOffTypeIDs** | **List&lt;string&gt;** |        Request requirements:    Verbose&#x3D;true       User requirements:    Security Level(s): Admin, Manager (View Employment Types)       Company requirements:    Leave Type Restrictions: By Employment Type | [optional] 
**Users** | [**List&lt;GetBasicUserModelUsersGetEmploymentTypeModelRoot&gt;**](GetBasicUserModelUsersGetEmploymentTypeModelRoot.md) |        Request requirements:    Verbose&#x3D;true       User requirements:    Security Level(s): Admin, Manager (View Employment Types) | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

