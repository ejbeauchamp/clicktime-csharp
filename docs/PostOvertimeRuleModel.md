# IO.Swagger.Model.PostOvertimeRuleModel
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**RuleDefinition** | [**PostBasicRuleDefinitionModel**](PostBasicRuleDefinitionModel.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

