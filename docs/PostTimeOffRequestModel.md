# IO.Swagger.Model.PostTimeOffRequestModel
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Dates** | [**List&lt;PostTimeOffRequestDateModel&gt;**](PostTimeOffRequestDateModel.md) |  | 
**Notes** | **string** |  | [optional] 
**RequestedByUserID** | **string** |  | [optional] 
**TimeOffTypeID** | **string** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

