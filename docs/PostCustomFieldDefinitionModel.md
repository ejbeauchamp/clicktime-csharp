# IO.Swagger.Model.PostCustomFieldDefinitionModel
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DataType** | **string** |  | 
**DisplayName** | **string** |  | 
**EntityType** | **string** |  | 
**IsRequired** | **bool?** |  | 
**Name** | **string** |  | 
**PulldownValues** | **List&lt;string&gt;** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

