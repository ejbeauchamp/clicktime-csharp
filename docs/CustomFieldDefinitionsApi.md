# IO.Swagger.Api.CustomFieldDefinitionsApi

All URIs are relative to *https://api.clicktime.com/V2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**CreateCustomFieldDefinition**](CustomFieldDefinitionsApi.md#createcustomfielddefinition) | **POST** /CustomFieldDefinitions | Create a Custom Field Definition
[**DeleteCustomFieldDefinition**](CustomFieldDefinitionsApi.md#deletecustomfielddefinition) | **DELETE** /CustomFieldDefinitions/{customFieldDefinitionID} | Delete a Custom Field Definition.
[**GetCustomFieldDefinition**](CustomFieldDefinitionsApi.md#getcustomfielddefinition) | **GET** /CustomFieldDefinitions/{customFieldDefinitionID} | Get a Custom Field Definition.
[**GetDefaultCustomFieldDefinition**](CustomFieldDefinitionsApi.md#getdefaultcustomfielddefinition) | **GET** /CustomFieldDefinitions/new | Get a new Custom Field Definition with default values.
[**ListCustomFieldDefinitions**](CustomFieldDefinitionsApi.md#listcustomfielddefinitions) | **GET** /CustomFieldDefinitions | Get Custom Field Definitions.
[**UpdateCustomFieldDefinition**](CustomFieldDefinitionsApi.md#updatecustomfielddefinition) | **PATCH** /CustomFieldDefinitions/{customFieldDefinitionID} | Update a Custom Field Definition.


<a name="createcustomfielddefinition"></a>
# **CreateCustomFieldDefinition**
> ResponseBodyGetCustomFieldDefinitionModelRoot CreateCustomFieldDefinition (PostCustomFieldDefinitionModelRoot model)

Create a Custom Field Definition

       User requirements:    User must be an Admin.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class CreateCustomFieldDefinitionExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new CustomFieldDefinitionsApi();
            var model = new PostCustomFieldDefinitionModelRoot(); // PostCustomFieldDefinitionModelRoot |       Required

            try
            {
                // Create a Custom Field Definition
                ResponseBodyGetCustomFieldDefinitionModelRoot result = apiInstance.CreateCustomFieldDefinition(model);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling CustomFieldDefinitionsApi.CreateCustomFieldDefinition: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **model** | [**PostCustomFieldDefinitionModelRoot**](PostCustomFieldDefinitionModelRoot.md)|       Required | 

### Return type

[**ResponseBodyGetCustomFieldDefinitionModelRoot**](ResponseBodyGetCustomFieldDefinitionModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded, application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="deletecustomfielddefinition"></a>
# **DeleteCustomFieldDefinition**
> void DeleteCustomFieldDefinition (string customFieldDefinitionID)

Delete a Custom Field Definition.

       User requirements:    User must be an Admin.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class DeleteCustomFieldDefinitionExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new CustomFieldDefinitionsApi();
            var customFieldDefinitionID = customFieldDefinitionID_example;  // string |       Required

            try
            {
                // Delete a Custom Field Definition.
                apiInstance.DeleteCustomFieldDefinition(customFieldDefinitionID);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling CustomFieldDefinitionsApi.DeleteCustomFieldDefinition: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customFieldDefinitionID** | **string**|       Required | 

### Return type

void (empty response body)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getcustomfielddefinition"></a>
# **GetCustomFieldDefinition**
> ResponseBodyGetCustomFieldDefinitionModelRoot GetCustomFieldDefinition (string customFieldDefinitionID)

Get a Custom Field Definition.

       User requirements:    User must be an Admin.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class GetCustomFieldDefinitionExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new CustomFieldDefinitionsApi();
            var customFieldDefinitionID = customFieldDefinitionID_example;  // string |       Required

            try
            {
                // Get a Custom Field Definition.
                ResponseBodyGetCustomFieldDefinitionModelRoot result = apiInstance.GetCustomFieldDefinition(customFieldDefinitionID);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling CustomFieldDefinitionsApi.GetCustomFieldDefinition: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customFieldDefinitionID** | **string**|       Required | 

### Return type

[**ResponseBodyGetCustomFieldDefinitionModelRoot**](ResponseBodyGetCustomFieldDefinitionModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getdefaultcustomfielddefinition"></a>
# **GetDefaultCustomFieldDefinition**
> ResponseBodyGetCustomFieldDefinitionModelNew GetDefaultCustomFieldDefinition ()

Get a new Custom Field Definition with default values.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class GetDefaultCustomFieldDefinitionExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new CustomFieldDefinitionsApi();

            try
            {
                // Get a new Custom Field Definition with default values.
                ResponseBodyGetCustomFieldDefinitionModelNew result = apiInstance.GetDefaultCustomFieldDefinition();
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling CustomFieldDefinitionsApi.GetDefaultCustomFieldDefinition: " + e.Message );
            }
        }
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ResponseBodyGetCustomFieldDefinitionModelNew**](ResponseBodyGetCustomFieldDefinitionModelNew.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="listcustomfielddefinitions"></a>
# **ListCustomFieldDefinitions**
> ResponseBodyListGetCustomFieldDefinitionModelRoot ListCustomFieldDefinitions (List<string> ID = null, List<string> dataType = null, List<string> entityType = null, List<bool?> isRequired = null, int? limit = null, int? offset = null)

Get Custom Field Definitions.

       User requirements:    User must be an Admin.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ListCustomFieldDefinitionsExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new CustomFieldDefinitionsApi();
            var ID = new List<string>(); // List<string> |  (optional) 
            var dataType = new List<string>(); // List<string> |  (optional) 
            var entityType = new List<string>(); // List<string> |  (optional) 
            var isRequired = new List<bool?>(); // List<bool?> |  (optional) 
            var limit = 56;  // int? |  (optional) 
            var offset = 56;  // int? |  (optional) 

            try
            {
                // Get Custom Field Definitions.
                ResponseBodyListGetCustomFieldDefinitionModelRoot result = apiInstance.ListCustomFieldDefinitions(ID, dataType, entityType, isRequired, limit, offset);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling CustomFieldDefinitionsApi.ListCustomFieldDefinitions: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ID** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **dataType** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **entityType** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **isRequired** | [**List&lt;bool?&gt;**](bool?.md)|  | [optional] 
 **limit** | **int?**|  | [optional] 
 **offset** | **int?**|  | [optional] 

### Return type

[**ResponseBodyListGetCustomFieldDefinitionModelRoot**](ResponseBodyListGetCustomFieldDefinitionModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="updatecustomfielddefinition"></a>
# **UpdateCustomFieldDefinition**
> ResponseBodyGetCustomFieldDefinitionModelRoot UpdateCustomFieldDefinition (string customFieldDefinitionID, PatchCustomFieldDefinitionModelRoot model)

Update a Custom Field Definition.

       User requirements:    User must be an Admin.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class UpdateCustomFieldDefinitionExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new CustomFieldDefinitionsApi();
            var customFieldDefinitionID = customFieldDefinitionID_example;  // string |       Required
            var model = new PatchCustomFieldDefinitionModelRoot(); // PatchCustomFieldDefinitionModelRoot |       Required

            try
            {
                // Update a Custom Field Definition.
                ResponseBodyGetCustomFieldDefinitionModelRoot result = apiInstance.UpdateCustomFieldDefinition(customFieldDefinitionID, model);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling CustomFieldDefinitionsApi.UpdateCustomFieldDefinition: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customFieldDefinitionID** | **string**|       Required | 
 **model** | [**PatchCustomFieldDefinitionModelRoot**](PatchCustomFieldDefinitionModelRoot.md)|       Required | 

### Return type

[**ResponseBodyGetCustomFieldDefinitionModelRoot**](ResponseBodyGetCustomFieldDefinitionModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded, application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

