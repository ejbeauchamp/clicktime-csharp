# IO.Swagger.Model.GetTimeEntryModelNew
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**BillingRate** | **double?** |        User requirements:    Security Level(s): Admin, Manager (View Billingrates) | [optional] 
**BreakTime** | **double?** |  | [optional] 
**Comment** | **string** |  | [optional] 
**CostRate** | **double?** |        User requirements:    Security Level(s): Admin, Manager (View Costs) | [optional] 
**CustomFields** | **Dictionary&lt;string, Object&gt;** |        Company requirements:    Custom Fields must be configured for Time Entries. | [optional] 
**Date** | **string** |  | [optional] 
**EndTime** | **string** |  | [optional] 
**Hours** | **double?** |  | [optional] 
**ID** | **string** |  | [optional] 
**JobID** | **string** |  | [optional] 
**StartTime** | **string** |  | [optional] 
**TaskID** | **string** |  | [optional] 
**UserID** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

