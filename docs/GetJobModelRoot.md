# IO.Swagger.Model.GetJobModelRoot
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**AccountingPackageID** | **string** |        User requirements:    Security Level(s): Admin, Manager (View Jobs) | [optional] 
**BillingRate** | **double?** |        User requirements:    Security Level(s): Admin, Manager (View Billingrates)    Security Level(s): Admin, Manager (View Jobs)       Company requirements:    Billing rate model(s): Job | [optional] 
**_Client** | [**GetBasicClientModelClientGetJobModelRoot**](GetBasicClientModelClientGetJobModelRoot.md) |        Request requirements:    Verbose&#x3D;true | [optional] 
**ClientID** | **string** |  | [optional] 
**CustomFields** | **Dictionary&lt;string, Object&gt;** |        Company requirements:    Custom Fields must be configured for Jobs.       User requirements:    Security Level(s): Admin, Manager (View Jobs) | [optional] 
**ID** | **string** |  | [optional] 
**IsActive** | **bool?** |  | [optional] 
**IsBillable** | **bool?** |        User requirements:    Security Level(s): Admin, Manager (View Jobs)       Company requirements:    Billability by: Job | [optional] 
**JobNumber** | **string** |  | [optional] 
**LegacyID** | **string** |        Request requirements:    CTLegacyScramble&#x3D;true | [optional] 
**LifetimeBudget** | **double?** |        Request requirements:    Verbose&#x3D;true       User requirements:    Security Level(s): Admin, Manager (View Jobs)       Company requirements:    Estimation model: Resource Planning | [optional] 
**ListDisplayName** | **string** |  | [optional] 
**ListDisplayText** | **string** |  | [optional] 
**Name** | **string** |  | [optional] 
**Notes** | **string** |        User requirements:    Security Level(s): Admin, Manager (View Jobs) | [optional] 
**ProjectManager** | [**GetMinimalUserModelProjectManagerGetJobModelRoot**](GetMinimalUserModelProjectManagerGetJobModelRoot.md) |        Request requirements:    Verbose&#x3D;true       User requirements:    Security Level(s): Admin, Manager (View Jobs) | [optional] 
**ProjectManagerID** | **string** |        User requirements:    Security Level(s): Admin, Manager (View Jobs) | [optional] 
**SecondaryBillingRateMode** | **string** |        User requirements:    Security Level(s): Admin, Manager (View Billingrates)    Security Level(s): Admin, Manager (View Jobs)       Company requirements:    Billing rate model(s): Task x Job, User x Job | [optional] 
**TimeRequiresApproval** | **bool?** |        User requirements:    Security Level(s): Admin, Manager (View Jobs)       Company requirements:    Job Approvals: enabled | [optional] 
**UseCompanyBillingRate** | **bool?** |        User requirements:    Security Level(s): Admin, Manager (View Billingrates)    Security Level(s): Admin, Manager (View Jobs)       Company requirements:    Billing rate model(s): Job | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

