# IO.Swagger.Model.GetTimeOffTypeModel
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**AccountingPackageID** | **string** |  | [optional] 
**AccrualRate** | **double?** |  | [optional] 
**AccrualStartDate** | **string** |  | [optional] 
**Approver** | [**GetBasicUserModel**](GetBasicUserModel.md) |  | [optional] 
**ApproverID** | **string** |  | [optional] 
**CurrentBalance** | **double?** |  | [optional] 
**DefaultAccrualRate** | **double?** |  | [optional] 
**DefaultApprover** | [**GetBasicUserModel**](GetBasicUserModel.md) |  | [optional] 
**DefaultApproverID** | **string** |  | [optional] 
**DefaultMaximumBalance** | **double?** |  | [optional] 
**DefaultStartingBalance** | **double?** |  | [optional] 
**DisplayOnTimeEntryPages** | **bool?** |  | [optional] 
**EmploymentTypeIDs** | **List&lt;string&gt;** |  | [optional] 
**ID** | **string** |  | [optional] 
**IsActive** | **bool?** |  | [optional] 
**LegacyID** | **string** |  | [optional] 
**MaximumBalance** | **double?** |  | [optional] 
**Name** | **string** |  | [optional] 
**Notes** | **string** |  | [optional] 
**RequiresApproval** | **bool?** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

