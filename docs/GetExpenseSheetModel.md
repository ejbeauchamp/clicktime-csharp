# IO.Swagger.Model.GetExpenseSheetModel
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Actions** | [**List&lt;GetActionModelExpenseSheetAction&gt;**](GetActionModelExpenseSheetAction.md) |  | [optional] 
**ApprovedBy** | **string** |  | [optional] 
**ApprovedByUserID** | **string** |  | [optional] 
**ApprovedDate** | **string** |  | [optional] 
**Check** | **string** |  | [optional] 
**Description** | **string** |  | [optional] 
**EnableForeignCurrency** | **bool?** |  | [optional] 
**ExpenseSheetDate** | **string** |  | [optional] 
**ID** | **string** |  | [optional] 
**LegacyID** | **string** |  | [optional] 
**Paid** | **bool?** |  | [optional] 
**ReimbursableAmount** | **double?** |  | [optional] 
**RejectedBy** | **string** |  | [optional] 
**RejectedByUserID** | **string** |  | [optional] 
**RejectedDate** | **string** |  | [optional] 
**Status** | **string** |  | [optional] 
**SubmittedBy** | **string** |  | [optional] 
**SubmittedByUserID** | **string** |  | [optional] 
**SubmittedDate** | **string** |  | [optional] 
**Title** | **string** |  | [optional] 
**TotalAmount** | **double?** |  | [optional] 
**TrackingID** | **string** |  | [optional] 
**User** | [**GetBasicUserModel**](GetBasicUserModel.md) |  | [optional] 
**UserID** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

