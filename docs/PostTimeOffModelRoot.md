# IO.Swagger.Model.PostTimeOffModelRoot
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Date** | **string** |  | 
**Hours** | **double?** |  | 
**Notes** | **string** |  | [optional] 
**TimeOffTypeID** | **string** | Time Off Type must not require approval. | 
**UserID** | **string** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

