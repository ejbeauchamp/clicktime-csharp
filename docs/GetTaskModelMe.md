# IO.Swagger.Model.GetTaskModelMe
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ID** | **string** |  | [optional] 
**IsActive** | **bool?** |  | [optional] 
**LegacyID** | **string** |        Request requirements:    CTLegacyScramble&#x3D;true | [optional] 
**ListDisplayText** | **string** |  | [optional] 
**MyTasks** | **bool?** |        Request requirements:    Verbose&#x3D;true       Company requirements:    Task restriction by mytasks | [optional] 
**Name** | **string** |  | [optional] 
**TaskCode** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

