# IO.Swagger.Model.PostJobAvailabilityModel
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**JobID** | **string** | The ID of a job to be made available. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

