# IO.Swagger.Model.PatchCustomFieldDefinitionModelRoot
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** |  | [optional] 
**IsRequired** | **bool?** |  | [optional] 
**Name** | **string** |  | [optional] 
**PulldownValues** | **List&lt;string&gt;** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

