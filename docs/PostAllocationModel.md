# IO.Swagger.Model.PostAllocationModel
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**EstHours** | **double?** |  | 
**JobID** | **string** |  | 
**Month** | **string** |  | 
**UserID** | **string** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

