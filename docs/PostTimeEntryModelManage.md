# IO.Swagger.Model.PostTimeEntryModelManage
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**BillingRate** | **double?** |        User requirements:    Security Level(s): Admin | [optional] 
**BreakTime** | **double?** |  | [optional] 
**Comment** | **string** | Company may require a user to enter a comment with Time Entries. | [optional] 
**CostRate** | **double?** |        User requirements:    Security Level(s): Admin | [optional] 
**CustomFields** | **Dictionary&lt;string, Object&gt;** |        Company requirements:    Custom Fields must be configured for Time Entries. | [optional] 
**Date** | **string** |  | [optional] 
**EndTime** | **string** | Company may require a user to enter an end time with Time Entries.  Format: HH:MM (24-hour) | [optional] 
**Hours** | **double?** |  | 
**JobID** | **string** |  | 
**StartTime** | **string** | Company may require a user to enter a start time with Time Entries.  Format: HH:MM (24-hour) | [optional] 
**TaskID** | **string** |  | 
**UserID** | **string** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

