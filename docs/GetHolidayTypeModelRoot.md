# IO.Swagger.Model.GetHolidayTypeModelRoot
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**AccountingPackageID** | **string** |        User requirements:    Security Level(s): Admin | [optional] 
**Dates** | [**List&lt;GetHolidayModelDatesGetHolidayTypeModelRoot&gt;**](GetHolidayModelDatesGetHolidayTypeModelRoot.md) |        Request requirements:    Verbose&#x3D;true | [optional] 
**EmploymentTypeIDs** | **List&lt;string&gt;** |        Company requirements:    Leave Type Restrictions: By Employment Type | [optional] 
**ID** | **string** |  | [optional] 
**IsActive** | **bool?** |  | [optional] 
**LegacyID** | **string** |        Request requirements:    CTLegacyScramble&#x3D;true | [optional] 
**Name** | **string** |  | [optional] 
**Notes** | **string** |        User requirements:    Security Level(s): Admin | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

