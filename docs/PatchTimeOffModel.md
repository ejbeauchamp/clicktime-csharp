# IO.Swagger.Model.PatchTimeOffModel
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Hours** | **double?** |  | [optional] 
**Notes** | **string** |  | [optional] 
**TimeOffTypeID** | **string** | Time Off Type must not require approval. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

