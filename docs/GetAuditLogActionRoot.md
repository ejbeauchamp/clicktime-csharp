# IO.Swagger.Model.GetAuditLogActionRoot
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Action** | **string** |  | [optional] 
**ActionComment** | **string** |  | [optional] 
**FieldName** | **string** |  | [optional] 
**Item** | **string** |  | [optional] 
**ItemID** | **string** |  | [optional] 
**LegacyItemID** | **string** |        Request requirements:    CTLegacyScramble&#x3D;true | [optional] 
**LegacyNewID** | **string** |        Request requirements:    CTLegacyScramble&#x3D;true | [optional] 
**LegacyOriginalID** | **string** |        Request requirements:    CTLegacyScramble&#x3D;true | [optional] 
**LogDate** | **string** |  | [optional] 
**LogID** | **int?** |  | [optional] 
**NewID** | **string** |  | [optional] 
**NewValue** | **string** |  | [optional] 
**OriginalID** | **string** |  | [optional] 
**OriginalValue** | **string** |  | [optional] 
**Timesheet** | [**GetLogTimesheetModelTimesheetGetAuditLogActionRoot**](GetLogTimesheetModelTimesheetGetAuditLogActionRoot.md) |        Request requirements:    Verbose&#x3D;true | [optional] 
**TransactionID** | **int?** |  | [optional] 
**UserID** | **string** |  | [optional] 
**UserName** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

