# IO.Swagger.Api.HolidayEntriesApi

All URIs are relative to *https://api.clicktime.com/V2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**DeleteHolidayEntry**](HolidayEntriesApi.md#deleteholidayentry) | **DELETE** /HolidayEntries/{holidayEntryID} | Delete a Holiday Entry.
[**GetHolidayEntry**](HolidayEntriesApi.md#getholidayentry) | **GET** /HolidayEntries/{holidayEntryID} | Get a Holiday Entry.
[**GetManagedTimesheetHolidayEntry**](HolidayEntriesApi.md#getmanagedtimesheetholidayentry) | **GET** /Manage/Timesheets/{timesheetID}/HolidayEntries/{holidayEntryID} | Get a Holiday Entry for a Timesheet for which you are the approver.
[**GetMyHolidayEntry**](HolidayEntriesApi.md#getmyholidayentry) | **GET** /Me/HolidayEntries/{holidayEntryID} | Get my Holiday Entry.
[**GetTimesheetHolidayEntry**](HolidayEntriesApi.md#gettimesheetholidayentry) | **GET** /Timesheets/{timesheetID}/HolidayEntries/{holidayEntryID} | Get a Holiday Entry for a Timesheet.
[**ListHolidayEntries**](HolidayEntriesApi.md#listholidayentries) | **GET** /HolidayEntries | Get Holiday Entries.
[**ListManagedHoldayEntries**](HolidayEntriesApi.md#listmanagedholdayentries) | **GET** /Manage/Timesheets/HolidayEntries | Get Holiday Entries for Users whose Timesheets you approve.
[**ListManagedTimesheetHolidayEntries**](HolidayEntriesApi.md#listmanagedtimesheetholidayentries) | **GET** /Manage/Timesheets/{timesheetID}/HolidayEntries | Get Holiday Entries for a Timesheet for which you are the approver.
[**ListMyHolidayEntries**](HolidayEntriesApi.md#listmyholidayentries) | **GET** /Me/HolidayEntries | Get my Holiday Entries.
[**ListTimesheetHolidayEntries**](HolidayEntriesApi.md#listtimesheetholidayentries) | **GET** /Timesheets/{timesheetID}/HolidayEntries | Get Holiday Entries for a Timesheet.
[**UpdateHolidayEntry**](HolidayEntriesApi.md#updateholidayentry) | **PATCH** /HolidayEntries/{holidayEntryID} | Update a Holiday Entry.


<a name="deleteholidayentry"></a>
# **DeleteHolidayEntry**
> void DeleteHolidayEntry (string holidayEntryID)

Delete a Holiday Entry.

       Company requirements:    DCAA must be disabled.       User requirements:    User must be an Admin.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class DeleteHolidayEntryExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new HolidayEntriesApi();
            var holidayEntryID = holidayEntryID_example;  // string |       Required

            try
            {
                // Delete a Holiday Entry.
                apiInstance.DeleteHolidayEntry(holidayEntryID);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling HolidayEntriesApi.DeleteHolidayEntry: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **holidayEntryID** | **string**|       Required | 

### Return type

void (empty response body)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getholidayentry"></a>
# **GetHolidayEntry**
> ResponseBodyGetHolidayEntryModelRoot GetHolidayEntry (string holidayEntryID)

Get a Holiday Entry.

       Company requirements:    DCAA must be disabled.       User requirements:    User must be an Admin.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class GetHolidayEntryExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new HolidayEntriesApi();
            var holidayEntryID = holidayEntryID_example;  // string |       Required

            try
            {
                // Get a Holiday Entry.
                ResponseBodyGetHolidayEntryModelRoot result = apiInstance.GetHolidayEntry(holidayEntryID);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling HolidayEntriesApi.GetHolidayEntry: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **holidayEntryID** | **string**|       Required | 

### Return type

[**ResponseBodyGetHolidayEntryModelRoot**](ResponseBodyGetHolidayEntryModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getmanagedtimesheetholidayentry"></a>
# **GetManagedTimesheetHolidayEntry**
> ResponseBodyGetHolidayEntryModelManage GetManagedTimesheetHolidayEntry (string timesheetID, string holidayEntryID)

Get a Holiday Entry for a Timesheet for which you are the approver.

       Company requirements:    DCAA must be disabled.    Company must have the TimesheetApprovals optional module.       User requirements:    Admin, or Manager with permissions to Lock timesheets.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class GetManagedTimesheetHolidayEntryExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new HolidayEntriesApi();
            var timesheetID = timesheetID_example;  // string |       Required
            var holidayEntryID = holidayEntryID_example;  // string |       Required

            try
            {
                // Get a Holiday Entry for a Timesheet for which you are the approver.
                ResponseBodyGetHolidayEntryModelManage result = apiInstance.GetManagedTimesheetHolidayEntry(timesheetID, holidayEntryID);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling HolidayEntriesApi.GetManagedTimesheetHolidayEntry: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **timesheetID** | **string**|       Required | 
 **holidayEntryID** | **string**|       Required | 

### Return type

[**ResponseBodyGetHolidayEntryModelManage**](ResponseBodyGetHolidayEntryModelManage.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getmyholidayentry"></a>
# **GetMyHolidayEntry**
> ResponseBodyGetHolidayEntryModelMe GetMyHolidayEntry (string holidayEntryID)

Get my Holiday Entry.

       Company requirements:    DCAA must be disabled.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class GetMyHolidayEntryExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new HolidayEntriesApi();
            var holidayEntryID = holidayEntryID_example;  // string |       Required

            try
            {
                // Get my Holiday Entry.
                ResponseBodyGetHolidayEntryModelMe result = apiInstance.GetMyHolidayEntry(holidayEntryID);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling HolidayEntriesApi.GetMyHolidayEntry: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **holidayEntryID** | **string**|       Required | 

### Return type

[**ResponseBodyGetHolidayEntryModelMe**](ResponseBodyGetHolidayEntryModelMe.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="gettimesheetholidayentry"></a>
# **GetTimesheetHolidayEntry**
> ResponseBodyGetHolidayEntryModelRoot GetTimesheetHolidayEntry (string timesheetID, string holidayEntryID)

Get a Holiday Entry for a Timesheet.

       Company requirements:    DCAA must be disabled.       User requirements:    Admin, or Manager with permissions to Review timesheets.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class GetTimesheetHolidayEntryExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new HolidayEntriesApi();
            var timesheetID = timesheetID_example;  // string |       Required
            var holidayEntryID = holidayEntryID_example;  // string |       Required

            try
            {
                // Get a Holiday Entry for a Timesheet.
                ResponseBodyGetHolidayEntryModelRoot result = apiInstance.GetTimesheetHolidayEntry(timesheetID, holidayEntryID);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling HolidayEntriesApi.GetTimesheetHolidayEntry: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **timesheetID** | **string**|       Required | 
 **holidayEntryID** | **string**|       Required | 

### Return type

[**ResponseBodyGetHolidayEntryModelRoot**](ResponseBodyGetHolidayEntryModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="listholidayentries"></a>
# **ListHolidayEntries**
> ResponseBodyListGetHolidayEntryModelRoot ListHolidayEntries (List<string> ID = null, List<string> holidayTypeID = null, List<string> holidayID = null, List<string> userID = null, string fromDate = null, string toDate = null, string isActive = null, int? limit = null, int? offset = null)

Get Holiday Entries.

       Company requirements:    DCAA must be disabled.       User requirements:    User must be an Admin.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ListHolidayEntriesExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new HolidayEntriesApi();
            var ID = new List<string>(); // List<string> |  (optional) 
            var holidayTypeID = new List<string>(); // List<string> |  (optional) 
            var holidayID = new List<string>(); // List<string> |  (optional) 
            var userID = new List<string>(); // List<string> |  (optional) 
            var fromDate = fromDate_example;  // string |  (optional) 
            var toDate = toDate_example;  // string |  (optional) 
            var isActive = isActive_example;  // string |  (optional) 
            var limit = 56;  // int? |  (optional) 
            var offset = 56;  // int? |  (optional) 

            try
            {
                // Get Holiday Entries.
                ResponseBodyListGetHolidayEntryModelRoot result = apiInstance.ListHolidayEntries(ID, holidayTypeID, holidayID, userID, fromDate, toDate, isActive, limit, offset);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling HolidayEntriesApi.ListHolidayEntries: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ID** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **holidayTypeID** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **holidayID** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **userID** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **fromDate** | **string**|  | [optional] 
 **toDate** | **string**|  | [optional] 
 **isActive** | **string**|  | [optional] 
 **limit** | **int?**|  | [optional] 
 **offset** | **int?**|  | [optional] 

### Return type

[**ResponseBodyListGetHolidayEntryModelRoot**](ResponseBodyListGetHolidayEntryModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="listmanagedholdayentries"></a>
# **ListManagedHoldayEntries**
> ResponseBodyListGetHolidayEntryModelRoot ListManagedHoldayEntries (List<string> ID = null, List<string> holidayTypeID = null, List<string> holidayID = null, List<string> userID = null, string fromDate = null, string toDate = null, int? limit = null, int? offset = null)

Get Holiday Entries for Users whose Timesheets you approve.

       Company requirements:    DCAA must be disabled.    Company must have the TimesheetApprovals optional module.       User requirements:    Admin, or Manager with permissions to Lock timesheets.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ListManagedHoldayEntriesExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new HolidayEntriesApi();
            var ID = new List<string>(); // List<string> |  (optional) 
            var holidayTypeID = new List<string>(); // List<string> |  (optional) 
            var holidayID = new List<string>(); // List<string> |  (optional) 
            var userID = new List<string>(); // List<string> |  (optional) 
            var fromDate = fromDate_example;  // string |  (optional) 
            var toDate = toDate_example;  // string |  (optional) 
            var limit = 56;  // int? |  (optional) 
            var offset = 56;  // int? |  (optional) 

            try
            {
                // Get Holiday Entries for Users whose Timesheets you approve.
                ResponseBodyListGetHolidayEntryModelRoot result = apiInstance.ListManagedHoldayEntries(ID, holidayTypeID, holidayID, userID, fromDate, toDate, limit, offset);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling HolidayEntriesApi.ListManagedHoldayEntries: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ID** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **holidayTypeID** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **holidayID** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **userID** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **fromDate** | **string**|  | [optional] 
 **toDate** | **string**|  | [optional] 
 **limit** | **int?**|  | [optional] 
 **offset** | **int?**|  | [optional] 

### Return type

[**ResponseBodyListGetHolidayEntryModelRoot**](ResponseBodyListGetHolidayEntryModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="listmanagedtimesheetholidayentries"></a>
# **ListManagedTimesheetHolidayEntries**
> ResponseBodyListGetHolidayEntryModelManage ListManagedTimesheetHolidayEntries (string timesheetID, List<string> ID = null, List<string> holidayTypeID = null, List<string> holidayID = null, int? limit = null, int? offset = null)

Get Holiday Entries for a Timesheet for which you are the approver.

       Company requirements:    DCAA must be disabled.    Company must have the TimesheetApprovals optional module.       User requirements:    Admin, or Manager with permissions to Lock timesheets.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ListManagedTimesheetHolidayEntriesExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new HolidayEntriesApi();
            var timesheetID = timesheetID_example;  // string |       Required
            var ID = new List<string>(); // List<string> |  (optional) 
            var holidayTypeID = new List<string>(); // List<string> |  (optional) 
            var holidayID = new List<string>(); // List<string> |  (optional) 
            var limit = 56;  // int? |  (optional) 
            var offset = 56;  // int? |  (optional) 

            try
            {
                // Get Holiday Entries for a Timesheet for which you are the approver.
                ResponseBodyListGetHolidayEntryModelManage result = apiInstance.ListManagedTimesheetHolidayEntries(timesheetID, ID, holidayTypeID, holidayID, limit, offset);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling HolidayEntriesApi.ListManagedTimesheetHolidayEntries: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **timesheetID** | **string**|       Required | 
 **ID** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **holidayTypeID** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **holidayID** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **limit** | **int?**|  | [optional] 
 **offset** | **int?**|  | [optional] 

### Return type

[**ResponseBodyListGetHolidayEntryModelManage**](ResponseBodyListGetHolidayEntryModelManage.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="listmyholidayentries"></a>
# **ListMyHolidayEntries**
> ResponseBodyListGetHolidayEntryModelMe ListMyHolidayEntries (List<string> ID = null, List<string> holidayTypeID = null, string fromDate = null, string toDate = null, List<string> holidayID = null, string isActive = null, int? limit = null, int? offset = null)

Get my Holiday Entries.

       Company requirements:    DCAA must be disabled.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ListMyHolidayEntriesExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new HolidayEntriesApi();
            var ID = new List<string>(); // List<string> |  (optional) 
            var holidayTypeID = new List<string>(); // List<string> |  (optional) 
            var fromDate = fromDate_example;  // string |  (optional) 
            var toDate = toDate_example;  // string |  (optional) 
            var holidayID = new List<string>(); // List<string> |  (optional) 
            var isActive = isActive_example;  // string |  (optional) 
            var limit = 56;  // int? |  (optional) 
            var offset = 56;  // int? |  (optional) 

            try
            {
                // Get my Holiday Entries.
                ResponseBodyListGetHolidayEntryModelMe result = apiInstance.ListMyHolidayEntries(ID, holidayTypeID, fromDate, toDate, holidayID, isActive, limit, offset);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling HolidayEntriesApi.ListMyHolidayEntries: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ID** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **holidayTypeID** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **fromDate** | **string**|  | [optional] 
 **toDate** | **string**|  | [optional] 
 **holidayID** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **isActive** | **string**|  | [optional] 
 **limit** | **int?**|  | [optional] 
 **offset** | **int?**|  | [optional] 

### Return type

[**ResponseBodyListGetHolidayEntryModelMe**](ResponseBodyListGetHolidayEntryModelMe.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="listtimesheetholidayentries"></a>
# **ListTimesheetHolidayEntries**
> ResponseBodyListGetHolidayEntryModelRoot ListTimesheetHolidayEntries (string timesheetID, List<string> ID = null, List<string> holidayTypeID = null, List<string> holidayID = null, int? limit = null, int? offset = null)

Get Holiday Entries for a Timesheet.

       Company requirements:    DCAA must be disabled.       User requirements:    Admin, or Manager with permissions to Review timesheets.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ListTimesheetHolidayEntriesExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new HolidayEntriesApi();
            var timesheetID = timesheetID_example;  // string |       Required
            var ID = new List<string>(); // List<string> |  (optional) 
            var holidayTypeID = new List<string>(); // List<string> |  (optional) 
            var holidayID = new List<string>(); // List<string> |  (optional) 
            var limit = 56;  // int? |  (optional) 
            var offset = 56;  // int? |  (optional) 

            try
            {
                // Get Holiday Entries for a Timesheet.
                ResponseBodyListGetHolidayEntryModelRoot result = apiInstance.ListTimesheetHolidayEntries(timesheetID, ID, holidayTypeID, holidayID, limit, offset);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling HolidayEntriesApi.ListTimesheetHolidayEntries: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **timesheetID** | **string**|       Required | 
 **ID** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **holidayTypeID** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **holidayID** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **limit** | **int?**|  | [optional] 
 **offset** | **int?**|  | [optional] 

### Return type

[**ResponseBodyListGetHolidayEntryModelRoot**](ResponseBodyListGetHolidayEntryModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="updateholidayentry"></a>
# **UpdateHolidayEntry**
> ResponseBodyGetHolidayEntryModelRoot UpdateHolidayEntry (string holidayEntryID, PatchHolidayEntryModelRoot model)

Update a Holiday Entry.

       Company requirements:    DCAA must be disabled.       User requirements:    User must be an Admin.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class UpdateHolidayEntryExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new HolidayEntriesApi();
            var holidayEntryID = holidayEntryID_example;  // string |       Required
            var model = new PatchHolidayEntryModelRoot(); // PatchHolidayEntryModelRoot |       Required

            try
            {
                // Update a Holiday Entry.
                ResponseBodyGetHolidayEntryModelRoot result = apiInstance.UpdateHolidayEntry(holidayEntryID, model);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling HolidayEntriesApi.UpdateHolidayEntry: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **holidayEntryID** | **string**|       Required | 
 **model** | [**PatchHolidayEntryModelRoot**](PatchHolidayEntryModelRoot.md)|       Required | 

### Return type

[**ResponseBodyGetHolidayEntryModelRoot**](ResponseBodyGetHolidayEntryModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded, application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

