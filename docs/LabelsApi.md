# IO.Swagger.Api.LabelsApi

All URIs are relative to *https://api.clicktime.com/V2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**CreateLabel**](LabelsApi.md#createlabel) | **POST** /Labels | Create a Label.
[**DeleteLabel**](LabelsApi.md#deletelabel) | **DELETE** /Labels/{labelID} | Delete a Label.
[**GetDefaultLabel**](LabelsApi.md#getdefaultlabel) | **GET** /Labels/new | Get a new Label with default values.
[**GetLabel**](LabelsApi.md#getlabel) | **GET** /Labels/{labelID} | Get a Label.
[**ListLabels**](LabelsApi.md#listlabels) | **GET** /Labels | Get Labels.
[**UpdateLabel**](LabelsApi.md#updatelabel) | **PATCH** /Labels/{labelID} | Update a Label.


<a name="createlabel"></a>
# **CreateLabel**
> ResponseBodyGetLabelModelRoot CreateLabel (PostLabelModelRoot model)

Create a Label.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class CreateLabelExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new LabelsApi();
            var model = new PostLabelModelRoot(); // PostLabelModelRoot |       Required

            try
            {
                // Create a Label.
                ResponseBodyGetLabelModelRoot result = apiInstance.CreateLabel(model);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling LabelsApi.CreateLabel: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **model** | [**PostLabelModelRoot**](PostLabelModelRoot.md)|       Required | 

### Return type

[**ResponseBodyGetLabelModelRoot**](ResponseBodyGetLabelModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded, application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="deletelabel"></a>
# **DeleteLabel**
> void DeleteLabel (string labelID)

Delete a Label.

       User requirements:    User must be an Admin.       Company requirements:    Company must be a test Company.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class DeleteLabelExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new LabelsApi();
            var labelID = labelID_example;  // string |       Required

            try
            {
                // Delete a Label.
                apiInstance.DeleteLabel(labelID);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling LabelsApi.DeleteLabel: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **labelID** | **string**|       Required | 

### Return type

void (empty response body)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getdefaultlabel"></a>
# **GetDefaultLabel**
> ResponseBodyGetLabelModelNew GetDefaultLabel ()

Get a new Label with default values.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class GetDefaultLabelExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new LabelsApi();

            try
            {
                // Get a new Label with default values.
                ResponseBodyGetLabelModelNew result = apiInstance.GetDefaultLabel();
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling LabelsApi.GetDefaultLabel: " + e.Message );
            }
        }
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ResponseBodyGetLabelModelNew**](ResponseBodyGetLabelModelNew.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getlabel"></a>
# **GetLabel**
> ResponseBodyGetLabelModelRoot GetLabel (string labelID)

Get a Label.

       User requirements:    Admin, or Manager with permissions to Run Company Reports.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class GetLabelExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new LabelsApi();
            var labelID = labelID_example;  // string |       Required

            try
            {
                // Get a Label.
                ResponseBodyGetLabelModelRoot result = apiInstance.GetLabel(labelID);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling LabelsApi.GetLabel: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **labelID** | **string**|       Required | 

### Return type

[**ResponseBodyGetLabelModelRoot**](ResponseBodyGetLabelModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="listlabels"></a>
# **ListLabels**
> ResponseBodyListGetLabelModelRoot ListLabels (List<string> ID = null, List<string> name = null, List<bool?> isActive = null, int? limit = null, int? offset = null)

Get Labels.

       User requirements:    Admin, or Manager with permissions to Run Company Reports.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ListLabelsExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new LabelsApi();
            var ID = new List<string>(); // List<string> |  (optional) 
            var name = new List<string>(); // List<string> |  (optional) 
            var isActive = new List<bool?>(); // List<bool?> |  (optional) 
            var limit = 56;  // int? |  (optional) 
            var offset = 56;  // int? |  (optional) 

            try
            {
                // Get Labels.
                ResponseBodyListGetLabelModelRoot result = apiInstance.ListLabels(ID, name, isActive, limit, offset);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling LabelsApi.ListLabels: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ID** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **name** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **isActive** | [**List&lt;bool?&gt;**](bool?.md)|  | [optional] 
 **limit** | **int?**|  | [optional] 
 **offset** | **int?**|  | [optional] 

### Return type

[**ResponseBodyListGetLabelModelRoot**](ResponseBodyListGetLabelModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="updatelabel"></a>
# **UpdateLabel**
> ResponseBodyGetLabelModelRoot UpdateLabel (string labelID, PatchLabelModelRoot model)

Update a Label.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class UpdateLabelExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new LabelsApi();
            var labelID = labelID_example;  // string |       Required
            var model = new PatchLabelModelRoot(); // PatchLabelModelRoot |       Required

            try
            {
                // Update a Label.
                ResponseBodyGetLabelModelRoot result = apiInstance.UpdateLabel(labelID, model);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling LabelsApi.UpdateLabel: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **labelID** | **string**|       Required | 
 **model** | [**PatchLabelModelRoot**](PatchLabelModelRoot.md)|       Required | 

### Return type

[**ResponseBodyGetLabelModelRoot**](ResponseBodyGetLabelModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded, application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

