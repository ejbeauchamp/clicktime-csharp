# IO.Swagger.Model.GetReportTimeModelRoot
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**BillingRate** | **double?** |        User requirements:    Security Level(s): Admin, Manager (View Billingrates) | [optional] 
**Comment** | **string** |        Request requirements:    Verbose&#x3D;true | [optional] 
**CostRate** | **double?** |        User requirements:    Security Level(s): Admin, Manager (View Costs) | [optional] 
**CustomFields** | **Dictionary&lt;string, Object&gt;** |        Request requirements:    Verbose&#x3D;true       Company requirements:    Custom Fields must be configured for Time Entries. | [optional] 
**Date** | **string** |  | [optional] 
**Hours** | **double?** |  | [optional] 
**ID** | **string** |  | [optional] 
**IsBillable** | **bool?** |        User requirements:    Security Level(s): Admin, Manager | [optional] 
**JobID** | **string** |  | [optional] 
**LegacyID** | **string** |        Request requirements:    CTLegacyScramble&#x3D;true | [optional] 
**TaskID** | **string** |  | [optional] 
**UserID** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

