# IO.Swagger.Api.ExpenseSheetsApi

All URIs are relative to *https://api.clicktime.com/V2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**CreateMyExpenseSheet**](ExpenseSheetsApi.md#createmyexpensesheet) | **POST** /Me/ExpenseSheets | Create my Expense Sheet.
[**GetDefaultExpenseSheet**](ExpenseSheetsApi.md#getdefaultexpensesheet) | **GET** /ExpenseSheets/new | Get a new Expense Sheet with default values.
[**GetMyExpenseSheet**](ExpenseSheetsApi.md#getmyexpensesheet) | **GET** /Me/ExpenseSheets/{expenseSheetID} | Get my Expense Sheet.
[**ListMyExpenseSheets**](ExpenseSheetsApi.md#listmyexpensesheets) | **GET** /Me/ExpenseSheets | Get my Expense Sheets.
[**UpdateMyExpenseSheet**](ExpenseSheetsApi.md#updatemyexpensesheet) | **PATCH** /Me/ExpenseSheets/{expenseSheetID} | Update my Expense Sheet.


<a name="createmyexpensesheet"></a>
# **CreateMyExpenseSheet**
> ResponseBodyGetExpenseSheetModelMe CreateMyExpenseSheet (PostExpenseSheetModelMe model)

Create my Expense Sheet.

       Company requirements:    Company must have the Expenses optional module.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class CreateMyExpenseSheetExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new ExpenseSheetsApi();
            var model = new PostExpenseSheetModelMe(); // PostExpenseSheetModelMe |       Required

            try
            {
                // Create my Expense Sheet.
                ResponseBodyGetExpenseSheetModelMe result = apiInstance.CreateMyExpenseSheet(model);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ExpenseSheetsApi.CreateMyExpenseSheet: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **model** | [**PostExpenseSheetModelMe**](PostExpenseSheetModelMe.md)|       Required | 

### Return type

[**ResponseBodyGetExpenseSheetModelMe**](ResponseBodyGetExpenseSheetModelMe.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded, application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getdefaultexpensesheet"></a>
# **GetDefaultExpenseSheet**
> ResponseBodyGetExpenseSheetModelNew GetDefaultExpenseSheet ()

Get a new Expense Sheet with default values.

       Company requirements:    Company must have the Expenses optional module.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class GetDefaultExpenseSheetExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new ExpenseSheetsApi();

            try
            {
                // Get a new Expense Sheet with default values.
                ResponseBodyGetExpenseSheetModelNew result = apiInstance.GetDefaultExpenseSheet();
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ExpenseSheetsApi.GetDefaultExpenseSheet: " + e.Message );
            }
        }
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ResponseBodyGetExpenseSheetModelNew**](ResponseBodyGetExpenseSheetModelNew.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getmyexpensesheet"></a>
# **GetMyExpenseSheet**
> ResponseBodyGetExpenseSheetModelMe GetMyExpenseSheet (string expenseSheetID)

Get my Expense Sheet.

       Company requirements:    Company must have the Expenses optional module.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class GetMyExpenseSheetExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new ExpenseSheetsApi();
            var expenseSheetID = expenseSheetID_example;  // string |       Required

            try
            {
                // Get my Expense Sheet.
                ResponseBodyGetExpenseSheetModelMe result = apiInstance.GetMyExpenseSheet(expenseSheetID);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ExpenseSheetsApi.GetMyExpenseSheet: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **expenseSheetID** | **string**|       Required | 

### Return type

[**ResponseBodyGetExpenseSheetModelMe**](ResponseBodyGetExpenseSheetModelMe.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="listmyexpensesheets"></a>
# **ListMyExpenseSheets**
> ResponseBodyListGetExpenseSheetModelMe ListMyExpenseSheets (List<string> ID = null, string fromDate = null, string toDate = null, List<string> status = null, List<bool?> paid = null, int? limit = null, int? offset = null)

Get my Expense Sheets.

       Company requirements:    Company must have the Expenses optional module.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ListMyExpenseSheetsExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new ExpenseSheetsApi();
            var ID = new List<string>(); // List<string> |  (optional) 
            var fromDate = fromDate_example;  // string |  (optional) 
            var toDate = toDate_example;  // string |  (optional) 
            var status = new List<string>(); // List<string> |  (optional) 
            var paid = new List<bool?>(); // List<bool?> |  (optional) 
            var limit = 56;  // int? |  (optional) 
            var offset = 56;  // int? |  (optional) 

            try
            {
                // Get my Expense Sheets.
                ResponseBodyListGetExpenseSheetModelMe result = apiInstance.ListMyExpenseSheets(ID, fromDate, toDate, status, paid, limit, offset);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ExpenseSheetsApi.ListMyExpenseSheets: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ID** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **fromDate** | **string**|  | [optional] 
 **toDate** | **string**|  | [optional] 
 **status** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **paid** | [**List&lt;bool?&gt;**](bool?.md)|  | [optional] 
 **limit** | **int?**|  | [optional] 
 **offset** | **int?**|  | [optional] 

### Return type

[**ResponseBodyListGetExpenseSheetModelMe**](ResponseBodyListGetExpenseSheetModelMe.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="updatemyexpensesheet"></a>
# **UpdateMyExpenseSheet**
> ResponseBodyGetExpenseSheetModelMe UpdateMyExpenseSheet (string expenseSheetID, PatchExpenseSheetModelMe model)

Update my Expense Sheet.

       Company requirements:    Company must have the Expenses optional module.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class UpdateMyExpenseSheetExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new ExpenseSheetsApi();
            var expenseSheetID = expenseSheetID_example;  // string |       Required
            var model = new PatchExpenseSheetModelMe(); // PatchExpenseSheetModelMe |       Required

            try
            {
                // Update my Expense Sheet.
                ResponseBodyGetExpenseSheetModelMe result = apiInstance.UpdateMyExpenseSheet(expenseSheetID, model);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ExpenseSheetsApi.UpdateMyExpenseSheet: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **expenseSheetID** | **string**|       Required | 
 **model** | [**PatchExpenseSheetModelMe**](PatchExpenseSheetModelMe.md)|       Required | 

### Return type

[**ResponseBodyGetExpenseSheetModelMe**](ResponseBodyGetExpenseSheetModelMe.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded, application/json, text/json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

