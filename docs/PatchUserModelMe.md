# IO.Swagger.Model.PatchUserModelMe
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DefaultExpenseTypeID** | **string** |        Company requirements:    Expense Permission | [optional] 
**DefaultPaymentTypeID** | **string** |        Company requirements:    Expense Permission | [optional] 
**DefaultTaskID** | **string** |  | [optional] 
**ExpenseSheetApprovedAlert** | **bool?** |  | [optional] 
**ExpenseSheetPaidAlert** | **bool?** |  | [optional] 
**GDPRConsentStatus** | **string** |  | [optional] 
**JobSelectionMethod** | **string** |  | [optional] 
**Name** | **string** |  | [optional] 
**NumEntries** | **int?** |  | [optional] 
**PreferredTimeEntryView** | **string** |  | [optional] 
**PreferredTimeFormat** | **string** |  | [optional] 
**SkipWeekend** | **bool?** |  | [optional] 
**TimesheetApprovedAlert** | **bool?** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

