# IO.Swagger.Model.GetClientModel
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**AccountingPackageID** | **string** |  | [optional] 
**BillingRate** | **double?** |  | [optional] 
**ClientNumber** | **string** |  | [optional] 
**CreatedDate** | **DateTime?** |  | [optional] 
**CustomFields** | **Dictionary&lt;string, Object&gt;** |  | [optional] 
**ID** | **string** |  | [optional] 
**IsActive** | **bool?** |  | [optional] 
**LegacyID** | **string** |  | [optional] 
**ListDisplayText** | **string** |  | [optional] 
**Name** | **string** |  | [optional] 
**Notes** | **string** |  | [optional] 
**SecondaryBillingRateMode** | **string** |  | [optional] 
**ShortName** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

