# IO.Swagger.Api.ReportsApi

All URIs are relative to *https://api.clicktime.com/V2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**ListReportAuditLog**](ReportsApi.md#listreportauditlog) | **GET** /Reports/AuditLog | Get audit log entries.
[**ListReportExpenses**](ReportsApi.md#listreportexpenses) | **GET** /Reports/Expense | Get Expenses.
[**ListReportTime**](ReportsApi.md#listreporttime) | **GET** /Reports/Time | Time Entry reporting.
[**ListReportTimeSummary**](ReportsApi.md#listreporttimesummary) | **GET** /Reports/TimeSummary | Get Time summaries.


<a name="listreportauditlog"></a>
# **ListReportAuditLog**
> ResponseBodyListGetAuditLogActionRoot ListReportAuditLog (string startDate = null, string endDate = null, int? limit = null, int? offset = null)

Get audit log entries.

       User requirements:    User must be an Admin.       Company requirements:    Company must have the AdvancedLogging optional module.    Audit logging must be enabled.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ListReportAuditLogExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new ReportsApi();
            var startDate = startDate_example;  // string |  (optional) 
            var endDate = endDate_example;  // string |  (optional) 
            var limit = 56;  // int? |  (optional) 
            var offset = 56;  // int? |  (optional) 

            try
            {
                // Get audit log entries.
                ResponseBodyListGetAuditLogActionRoot result = apiInstance.ListReportAuditLog(startDate, endDate, limit, offset);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ReportsApi.ListReportAuditLog: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **startDate** | **string**|  | [optional] 
 **endDate** | **string**|  | [optional] 
 **limit** | **int?**|  | [optional] 
 **offset** | **int?**|  | [optional] 

### Return type

[**ResponseBodyListGetAuditLogActionRoot**](ResponseBodyListGetAuditLogActionRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="listreportexpenses"></a>
# **ListReportExpenses**
> ResponseBodyListGetReportExpenseModelRoot ListReportExpenses (List<string> ID = null, List<string> labelID = null, List<string> jobID = null, List<string> userID = null, List<string> expenseSheetStatus = null, string startDate = null, string endDate = null, int? limit = null, int? offset = null)

Get Expenses.

       User requirements:    Admin, or Manager with permissions to Run Company Reports.       Company requirements:    Company must have the Expenses optional module.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ListReportExpensesExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new ReportsApi();
            var ID = new List<string>(); // List<string> |  (optional) 
            var labelID = new List<string>(); // List<string> |  (optional) 
            var jobID = new List<string>(); // List<string> |  (optional) 
            var userID = new List<string>(); // List<string> |  (optional) 
            var expenseSheetStatus = new List<string>(); // List<string> |  (optional) 
            var startDate = startDate_example;  // string |  (optional) 
            var endDate = endDate_example;  // string |  (optional) 
            var limit = 56;  // int? |  (optional) 
            var offset = 56;  // int? |  (optional) 

            try
            {
                // Get Expenses.
                ResponseBodyListGetReportExpenseModelRoot result = apiInstance.ListReportExpenses(ID, labelID, jobID, userID, expenseSheetStatus, startDate, endDate, limit, offset);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ReportsApi.ListReportExpenses: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ID** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **labelID** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **jobID** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **userID** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **expenseSheetStatus** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **startDate** | **string**|  | [optional] 
 **endDate** | **string**|  | [optional] 
 **limit** | **int?**|  | [optional] 
 **offset** | **int?**|  | [optional] 

### Return type

[**ResponseBodyListGetReportExpenseModelRoot**](ResponseBodyListGetReportExpenseModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="listreporttime"></a>
# **ListReportTime**
> ResponseBodyListGetReportTimeModelRoot ListReportTime (List<string> jobID = null, List<string> clientID = null, List<string> userID = null, List<string> taskID = null, List<string> divisionID = null, List<string> labelID = null, List<string> timesheetID = null, List<string> timesheetStatus = null, string startDate = null, string endDate = null, List<bool?> isBillable = null, int? limit = null, int? offset = null)

Time Entry reporting.

       User requirements:    Admin, or Manager with permissions to Run Company Reports.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ListReportTimeExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new ReportsApi();
            var jobID = new List<string>(); // List<string> |  (optional) 
            var clientID = new List<string>(); // List<string> |  (optional) 
            var userID = new List<string>(); // List<string> |  (optional) 
            var taskID = new List<string>(); // List<string> |  (optional) 
            var divisionID = new List<string>(); // List<string> |  (optional) 
            var labelID = new List<string>(); // List<string> |  (optional) 
            var timesheetID = new List<string>(); // List<string> |  (optional) 
            var timesheetStatus = new List<string>(); // List<string> |  (optional) 
            var startDate = startDate_example;  // string |  (optional) 
            var endDate = endDate_example;  // string |  (optional) 
            var isBillable = new List<bool?>(); // List<bool?> |  (optional) 
            var limit = 56;  // int? |  (optional) 
            var offset = 56;  // int? |  (optional) 

            try
            {
                // Time Entry reporting.
                ResponseBodyListGetReportTimeModelRoot result = apiInstance.ListReportTime(jobID, clientID, userID, taskID, divisionID, labelID, timesheetID, timesheetStatus, startDate, endDate, isBillable, limit, offset);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ReportsApi.ListReportTime: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **jobID** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **clientID** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **userID** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **taskID** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **divisionID** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **labelID** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **timesheetID** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **timesheetStatus** | [**List&lt;string&gt;**](string.md)|  | [optional] 
 **startDate** | **string**|  | [optional] 
 **endDate** | **string**|  | [optional] 
 **isBillable** | [**List&lt;bool?&gt;**](bool?.md)|  | [optional] 
 **limit** | **int?**|  | [optional] 
 **offset** | **int?**|  | [optional] 

### Return type

[**ResponseBodyListGetReportTimeModelRoot**](ResponseBodyListGetReportTimeModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="listreporttimesummary"></a>
# **ListReportTimeSummary**
> ResponseBodyListGetSummaryModelRoot ListReportTimeSummary (string jobID = null, string groupBy = null)

Get Time summaries.

       User requirements:    Admin, or Manager with permissions to Run Company Reports.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ListReportTimeSummaryExample
    {
        public void main()
        {
            // Configure API key authorization: apiKey
            Configuration.Default.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new ReportsApi();
            var jobID = jobID_example;  // string |  (optional) 
            var groupBy = groupBy_example;  // string |  (optional) 

            try
            {
                // Get Time summaries.
                ResponseBodyListGetSummaryModelRoot result = apiInstance.ListReportTimeSummary(jobID, groupBy);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ReportsApi.ListReportTimeSummary: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **jobID** | **string**|  | [optional] 
 **groupBy** | **string**|  | [optional] 

### Return type

[**ResponseBodyListGetSummaryModelRoot**](ResponseBodyListGetSummaryModelRoot.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

